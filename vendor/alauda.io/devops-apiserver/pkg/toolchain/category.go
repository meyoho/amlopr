package toolchain

// Category category of toolchain item
type Category struct {
	// Name is the name of the item
	Name string `json:"name"`
	// DisplayName is a set of humanized and translated names
	DisplayName DisplayName `json:"displayName"`
	// Enabled specifies the enabled of the item.
	Enabled bool `json:"enabled"`
	// APIPath is a specification of the toolchain type
	APIPath string `json:"apiPath"`

	// Items for the category
	Items []*Item `json:"items"`
}

// GetName returns the name
func (c *Category) GetName() string {
	return c.Name
}

// Add add items to category
func (c *Category) Add(items ...*Item) *Category {
	if c.Items == nil {
		c.Items = items
		return c
	}
	c.Items = append(c.Items, items...)
	return c
}

// GetItems return items of category
func (c *Category) GetItems() []*Item {
	return c.Items
}

// Merge merges two categories
func (c *Category) Merge(merge *Category) *Category {
	// copy attributes
	c.APIPath = merge.APIPath
	c.DisplayName = merge.DisplayName
	// analize items
	for _, mergedItem := range merge.Items {
		itemFound := false
		for _, ourItem := range c.Items {
			if mergedItem.GetName() != ourItem.GetName() {
				continue
			}
			itemFound = true
			ourItem.Merge(mergedItem)
		}
		if !itemFound {
			c.Add(mergedItem)
		}
	}
	return c
}
