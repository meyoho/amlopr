package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementBinding is the binding referenced to ProjectManagement
type ProjectManagementBinding struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the ProjectManagementBinding.
	// +optional
	Spec ProjectManagementBindingSpec
	// Most recently observed status of the ProjectManagementBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus
}

func (b *ProjectManagementBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Secret.Namespace != "" {
		namespace = b.Spec.Secret.Namespace
	}
	return namespace
}

func (b *ProjectManagementBinding) GetSecretName() string {
	return b.Spec.Secret.Name
}

// ProjectManagementBindingSpec is the spec in ProjectManagementBinding
type ProjectManagementBindingSpec struct {
	ProjectManagement LocalObjectReference
	//Secret defines the secret type.
	//+optional
	Secret SecretKeySetRef
	//+optional
	ProjectManagementProjectInfos []ProjectManagementProjectInfo
}

type ProjectManagementProjectInfo struct {
	ID   string
	Name string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementBindingList is a list of ProjectManagementBinding objects.
type ProjectManagementBindingList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of ProjectManagementBinding
	Items []ProjectManagementBinding
}

var _ ToolBindingLister = &ProjectManagementBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *ProjectManagementBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeProjectManagementBinding
	}
	return
}

func (binding *ProjectManagementBinding) GetStatus() *ServiceStatus {
	return &binding.Status
}
