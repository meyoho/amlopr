/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ ToolInterface = &DocumentManagement{}

func (docManagement *DocumentManagement) GetKind() string {
	return TypeDocumentManagement
}

func (docManagement *DocumentManagement) GetHostPort() HostPort {
	return docManagement.Spec.HTTP
}

func (docManagement *DocumentManagement) GetKindType() string {
	return docManagement.GetType().String()
}

func (docManagement *DocumentManagement) GetObjectMeta() metav1.Object {
	return &docManagement.ObjectMeta
}

func (docManagement *DocumentManagement) GetStatus() *ServiceStatus {
	if docManagement == nil {
		return nil
	}
	return &(docManagement.Status)
}

func (docManagement *DocumentManagement) GetToolSpec() ToolSpec {
	return docManagement.Spec.ToolSpec
}

// DocumentManagementType type for the DocumentManagement
type DocumentManagementType string

const (
	// DocumentManageTypeConfluence Confluence
	// +alauda:toolchain-gen:class=panel,category=DocumentManagementBinding,en=Document Management,zh=文档管理,index=1
	// +alauda:toolchain-gen:class=item,category=documentManagement,name=confluence,en=Confluence,zh=Confluence,apipath=documentmanagements,enabled=true,kind=DocumentManagement,type=Confluence,roleSyncEnabled=false
	// +alauda:toolchain-gen:class=secret,category=documentManagement,itemName=confluence,type=kubernetes.io/basic-auth,en=Input Username and Password as used on login,zh=用户名和密码均为登录时的用户名和密码
	DocumentManageTypeConfluence DocumentManagementType = "Confluence"
)

func (c DocumentManagementType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +alauda:toolchain-gen:class=category,name=documentManagement,en=Document Management,zh=文档管理,enabled=true,index=1

// DocumentManagement struct holds DocumentManagement data
type DocumentManagement struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the DocumentManagement.
	// +optional
	Spec DocumentManagementSpec `json:"spec"`
	// Most recently observed status of the DocumentManagement.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// Get Documentmanagement service type
func (c *DocumentManagement) GetType() DocumentManagementType {
	return c.Spec.Type
}

func (c *DocumentManagement) GetEndpoint() string {
	endpoint := c.Spec.HTTP.Host
	endpoint = strings.TrimRight(endpoint, "/")
	return endpoint
}

func (c *DocumentManagement) GetSecretNamespace() string {
	namespace := c.GetNamespace()
	if c.Spec.Secret.Namespace != "" {
		namespace = c.Spec.Secret.Namespace
	}
	return namespace
}

func (c *DocumentManagement) GetSecretName() string {
	return c.Spec.Secret.Name
}

// DocumentManagementSpec is the spec in DocumentManagement
type DocumentManagementSpec struct {
	ToolSpec `json:",inline"`
	// Type defines the service type.
	Type DocumentManagementType `json:"type"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// DocumentManagementList is a list of DocumentManagement objects
type DocumentManagementList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of DocumentManagement
	Items []DocumentManagement `json:"items"`
}
