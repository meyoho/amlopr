package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// region TestTool

// TestToolType type for the TestTool
type TestToolType string

const (
	// TestToolTypeRedwoodHQ RedwoodHQ
	// +alauda:toolchain-gen:class=item,category=testTool,name=redwoodhq,en=RedwoodHQ,zh=RedwoodHQ,apipath=testtools,enabled=true,kind=testtool,type=RedwoodHQ,roleSyncEnabled=false
	TestToolTypeRedwoodHQ TestToolType = "RedwoodHQ"
)

func (c TestToolType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +alauda:toolchain-gen:class=category,name=testTool,en=Test Tool,zh=测试工具,enabled=true,index=6

// TestTool struct holds TestTool data
type TestTool struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the TestTool.
	// +optional
	Spec TestToolSpec `json:"spec"`
	// Most recently observed status of the TestTool.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// TestToolSpec is the spec in TestTool
type TestToolSpec struct {
	ToolSpec `json:",inline"`
	// Type defines the service type.
	Type TestToolType `json:"type"`
	// Public defines the public of the service.
	// +optional
	Public bool `json:"public"`
	// Data defines the data.
	// +optional
	Data map[string]string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestToolList is a list of TestTool objects.
type TestToolList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of TestTool
	Items []TestTool `json:"items"`
}

var _ ToolInterface = &TestTool{}

func (testTool *TestTool) GetKind() string {
	return TypeTestTool
}

func (testTool *TestTool) GetKindType() string {
	return testTool.Spec.Type.String()
}

func (testTool *TestTool) GetHostPort() HostPort {
	return testTool.Spec.HTTP
}

func (testTool *TestTool) GetObjectMeta() metav1.Object {
	return &testTool.ObjectMeta
}

func (testTool *TestTool) GetStatus() *ServiceStatus {
	return &testTool.Status
}

func (testTool *TestTool) GetToolSpec() ToolSpec {
	return testTool.Spec.ToolSpec
}
