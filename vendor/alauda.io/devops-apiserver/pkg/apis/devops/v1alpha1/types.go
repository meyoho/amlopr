package v1alpha1

import (
	"bytes"
	"errors"
	"fmt"
	"strings"
	"time"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// ListEverything is a list options used to list all objects without any filtering.
var ListEverything = metav1.ListOptions{
	LabelSelector:   labels.Everything().String(),
	FieldSelector:   fields.Everything().String(),
	ResourceVersion: "0",
}

// LocalObjectReference simple local reference for local objects
// contains enough information to let you locate the
// referenced object inside the same namespace
// k8s.io/api/core/v1/types.go
type LocalObjectReference struct {
	// Name of the referent.
	// More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
	// TODO: Add other useful fields. apiVersion, kind, uid?
	// +optional
	Name string `json:"name,omitempty"`
}

// HostPort defines a host/port  construct
type HostPort struct {
	// Host defines the host.
	// +optional
	Host string `json:"host"`

	// AccessURL defines an access URL for the tool
	// useful specially if the API url (host) is different than the
	// Access URL
	// +optional
	AccessURL string `json:"accessUrl"`
}

// GetAccessURL returns access url if set, otherwise returns Host
func (host HostPort) GetAccessURL() (url string) {
	url = host.Host
	if host.AccessURL != "" {
		url = host.AccessURL
	}
	return url
}

// ServiceStatusPhase defines the repo status
type ServiceStatusPhase string

func (phase ServiceStatusPhase) String() string {
	return string(phase)
}

const (
	// ServiceStatusPhaseCreating means the resource is creating
	ServiceStatusPhaseCreating ServiceStatusPhase = StatusCreating
	// ServiceStatusPhaseReady means the connection is ok
	ServiceStatusPhaseReady ServiceStatusPhase = StatusReady
	// ServiceStatusPhaseError means the connection is bad
	ServiceStatusPhaseError ServiceStatusPhase = StatusError
	// ServiceStatusPhaseWaitingToDelete means the resource will be deleted when no resourced reference
	ServiceStatusPhaseWaitingToDelete ServiceStatusPhase = StatusWaitingToDelete
	// ServiceStatusPhaseListTagError means registry list tag detail error
	ServiceStatusPhaseListTagError ServiceStatusPhase = StatusListTagError
	// ServiceStatusNeedsAuthorization needs authorization intervation from user.
	// Generally used on multi-step authorization schemes like oAuth2 etc
	ServiceStatusNeedsAuthorization ServiceStatusPhase = StatusNeedsAuthorization
)

// ServiceStatus defines the status of the service.
type ServiceStatus struct {
	// Current condition of the service.
	// One of: "Creating" or "Ready" or "Error" or "WaitingToDelete".
	// +optional
	Phase ServiceStatusPhase `json:"phase"`
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human-readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
	// LastUpdate is the latest time when updated the service.
	// +optional
	LastUpdate *metav1.Time `json:"lastUpdated"`
	// HTTPStatus is http status of the service.
	// +optional
	HTTPStatus *HostPortStatus `json:"http,omitempty"`
	// Conditions is a list of BindingCondition objects.
	// +optional
	Conditions []BindingCondition `json:"conditions"`
}

// String print a string for ServiceStatus
func (status ServiceStatus) String() string {
	var buff bytes.Buffer
	if status.Phase != "" {
		buff.WriteString(fmt.Sprintf(`Phase "%s",`, status.Phase))
	}
	if status.Message != "" {
		buff.WriteString(fmt.Sprintf(`Msg "%s",`, status.Message))
	}
	if status.Reason != "" {
		buff.WriteString(fmt.Sprintf(`Reason "%s",`, status.Reason))
	}
	buff.WriteString(fmt.Sprintf(`Conditions len(%d),`, len(status.Conditions)))
	return buff.String()
}

// CleanConditionsLastAttemptByOwner set nil to lastAttempt to conditions of a given owner
func (status ServiceStatus) CleanConditionsLastAttemptByOwner(owner string) ServiceStatus {
	if len(status.Conditions) > 0 {
		for i := 0; i < len(status.Conditions); i++ {
			if status.Conditions[i].Owner == owner {
				status.Conditions[i].LastAttempt = nil
			}
		}
	}
	return status
}

// HostPortStatus defines a status for a HostPort setting
type HostPortStatus struct {
	// StatusCode is the status code of http response
	// +optional
	StatusCode int `json:"statusCode"`
	// Response is the response of the http request.
	// +optional
	Response string `json:"response,omitempty"`
	// Version is the version of the http request.
	// +optional
	Version string `json:"version,omitempty"`
	// Delay means the http request will attempt later
	// +optional
	Delay *time.Duration `json:"delay,omitempty"`
	// Last time we probed the http request.
	// +optional
	LastAttempt *metav1.Time `json:"lastAttempt"`
	// Error Message of http request
	// +optional
	ErrorMessage string `json:"errorMessage"`
}

// Condition generic condition for devops objects
type Condition struct {
	// Type is the type of the condition.
	// +optional
	Type string `json:"type"`
	// Last time we probed the condition.
	// +optional
	LastAttempt *metav1.Time `json:"lastAttempt"`
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human-readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
	// Status is the status of the condition.
	// +optional
	Status string `json:"status"`
}

// BindingCondition defines the resource associated with the binding.
// The binding controller will check the status of the resource periodic and change it's status.
// The resource can be found by "name"+"type"+"binding's namespace"
type BindingCondition struct {
	// Name defines the name.
	// +optional
	Name string `json:"name"`
	// namespace defines the name.
	// +optional
	Namespace string `json:"namespace,omitempty"`
	// Type defines the type.
	// +optional
	Type string `json:"type"`
	// Last time we probed the condition.
	// +optional
	LastAttempt *metav1.Time `json:"lastAttempt"`
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human-readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
	// Status defines the status.
	// +optional
	Status string `json:"status,omitempty"`
	// Owner defins who own current condition
	// +optional
	Owner string `json:"owner,omitempty"`
}

type BindingConditions []BindingCondition

func (conds BindingConditions) Aggregate() (conditions *BindingCondition) {

	if len(conds) == 0 {
		return nil
	}

	for _, cond := range conds {
		// Any error condition will cause status of tbr changing to error
		if cond.Status == StatusError {
			return &cond
		}
	}
	return nil
}

// ReplaceBy remove the conditions that owned by owner and append the `appendConds` conditions
func (conds BindingConditions) ReplaceBy(owner string, appendConds []BindingCondition) []BindingCondition {

	// clean the conditions that controll by current controller
	result := make([]BindingCondition, 0, len(conds))

	for _, cond := range conds {
		if cond.Owner != owner {
			result = append(result, cond)
		}
	}

	//append new conds
	result = append(result, appendConds...)

	return result
}

func (conds BindingConditions) Errors() error {
	if len(conds) == 0 {
		return nil
	}

	sb := strings.Builder{}
	for i, cond := range conds {
		if cond.Status == StatusError {
			sb.WriteString(fmt.Sprintf("Error-%d : %s \n", i, cond.Reason))
		}
	}

	msg := sb.String()
	if len(msg) == 0 {
		return nil
	}
	return errors.New(msg)
}

// DisplayName defines a set of readable names
type DisplayName struct {
	// EN is a human readable Chinese name.
	EN string `json:"en"`
	// ZH is a human readable English name.
	ZH string `json:"zh"`
}

// SecretKeySetRef reference of a set of username/api token keys in a Secret
type SecretKeySetRef struct {
	corev1.SecretReference `json:",inline"`
}

// Phaser returns its own phase
type Phaser interface {
	GetPhase() string
}
