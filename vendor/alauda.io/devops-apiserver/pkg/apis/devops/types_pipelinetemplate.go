package devops

import (
	"fmt"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTaskTemplate specified a task for a pipeline
type PipelineTaskTemplate struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the PipelineTaskTemplate
	Spec PipelineTaskTemplateSpec
}

func (tasktemplate PipelineTaskTemplate) GetTaskTemplateExports(object runtime.Object) []GlobalParameter {

	tasktemplateobject, _ := object.(*PipelineTaskTemplate)
	tasktemplatexports := tasktemplateobject.Spec.Exports
	return tasktemplatexports
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTaskTemplateList is list of PipelineTaskTemplate
type PipelineTaskTemplateList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	Items []PipelineTaskTemplate
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTaskTemplate is kind of cluster PipelineTaskTemplate
type ClusterPipelineTaskTemplate struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the PipelineTaskTemplate
	Spec PipelineTaskTemplateSpec
}

func (tasktemplate ClusterPipelineTaskTemplate) GetTaskTemplateExports(object runtime.Object) []GlobalParameter {

	tasktemplateobject, _ := object.(*ClusterPipelineTaskTemplate)
	tasktemplatexports := tasktemplateobject.Spec.Exports
	return tasktemplatexports
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTaskTemplateList is a list of ClusterPipelineTaskTemplate
type ClusterPipelineTaskTemplateList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	Items []ClusterPipelineTaskTemplate
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineExportedVariables represent the export for the pipelinetemplate
type PipelineExportedVariables struct {
	metav1.TypeMeta
	Values []GlobalParameter
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ExportShowOptions struct {
	metav1.TypeMeta
	// SourceType specifies the type of pipeline scm.
	TaskName    string
	FormatValue string
}

// PipelineDependency for PipelineTaskTemplate dependency on jenkins plugins
type PipelineDependency struct {
	// Plugins hold Jenkins plugins dependency for the specific task
	// +optional
	Plugins []JenkinsPlugin
}

// JenkinsPlugin is Jenkins plugin info
type JenkinsPlugin struct {
	// Name is the name of plugin
	Name string
	// Version is the version of plugin
	Version string
}

// PipelineTemplateTaskEngine describe the kind of engine used for the PipelineTaskTemplate
type PipelineTemplateTaskEngine string

const (
	// PipelineTaskTemplateEngineGoTemplate go template for rendering
	PipelineTaskTemplateEngineGoTemplate = "gotpl"
)

// IsValid check PipelineTemplateTaskEngine whether valid
func (engine PipelineTemplateTaskEngine) IsValid() bool {
	return engine == PipelineTaskTemplateEngineGoTemplate
}

// IsEmpty check PipelineTemplateTaskEngine whether empty
func (engine PipelineTemplateTaskEngine) IsEmpty() bool {
	return engine == ""
}

// PipelineTaskTemplateSpec represents PipelineTaskTemplate's specs
type PipelineTaskTemplateSpec struct {
	// Engine the way of how to render taskTemplate
	// +optinal
	Engine PipelineTemplateTaskEngine
	// Agent indicates where the task should be running
	// +optional
	Agent *JenkinsAgent
	// Body task template body
	Body string
	// Exports all envrionments will be exports
	// +optional
	Exports []GlobalParameter
	// Parameters that will be use in running
	// +optional
	Parameters []PipelineParameter
	// Arguments the task template's arguments
	// +optional
	Arguments []PipelineTaskArgument
	// Dependencies indicates plugins denpendencies of task
	// +optional
	Dependencies *PipelineDependency
}

// GlobalParameter for export
type GlobalParameter struct {
	// Name the name of parameter
	Name string
	// Description description of parameter
	// +optional
	Description *I18nName
}

// PipelineTaskArgument sepcified a arugment for PipelineTaskTemplate
type PipelineTaskArgument struct {
	// Name the name of task
	Name string
	// Schema schema of task
	Schema PipelineTaskArgumentSchema
	// Display display of task
	Display PipelineTaskArgumentDisplay
	// Required indicate whether required
	// +optional
	Required bool
	// Default default value of arugment
	// +optional
	Default string
	// Validation validation of arugment
	// +optional
	Validation *PipelineTaskArgumentValidation
	// Relation relation between arguments
	// +optional
	Relation []PipelineTaskArgumentAction
}

// PipelineTaskArgumentValidation for task arument validation
type PipelineTaskArgumentValidation struct {
	// Pattern pattern of validation
	// +optional
	Pattern string
	// MaxLength maxLength of this field
	// +optional
	MaxLength int
}

// PipelineTaskArgumentAction action for task argument
type PipelineTaskArgumentAction struct {
	// Action action for task argument
	Action string
	// When time condition for task execution
	When PipelineTaskArgumentWhen
}

// PipelineTaskArgumentWhen action time config
type PipelineTaskArgumentWhen struct {
	// Name name of when
	Name string
	// Value value of when
	Value bool
	// +optional
	All []RelationWhenItem
	// +optional
	Any []RelationWhenItem
}

// RelationWhenItem all condition in pipelinetaskarguementwhtn
type RelationWhenItem struct {
	Name  string
	Value bool
}

// PipelineTemplateEngine describe the kind of engine used for the PipelineTemplate
type PipelineTemplateEngine string

const (
	// PipelineTemplateEngineGraph render template as the graph form
	PipelineTemplateEngineGraph PipelineTemplateEngine = "graph"
)

// IsValid check whether engine is valid
func (engine PipelineTemplateEngine) IsValid() bool {
	return engine == PipelineTemplateEngineGraph
}

// IsEmpty check whether engine is empty
func (engine PipelineTemplateEngine) IsEmpty() bool {
	return engine == ""
}

// +genclient
// +genclient:method=Preview,verb=create,subresource=preview,input=JenkinsfilePreviewOptions,result=JenkinsfilePreview
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTemplate specified a jenkinsFile template
type PipelineTemplate struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Spec specification of PipelineTemplate
	Spec PipelineTemplateSpec
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTemplateList is a list of PipelineTemplate
type PipelineTemplateList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items items of PipelineTemplates
	Items []PipelineTemplate
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTemplate specified a cluster kind of PipelineTemplate
type ClusterPipelineTemplate struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Spec specification of PipelineTemplate
	Spec PipelineTemplateSpec
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTemplateList is a list of ClusterPipelineTemplate
type ClusterPipelineTemplateList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items items of ClusterPipelineTemplate
	Items []ClusterPipelineTemplate
}

// PipelineTemplateSpec represents PipelineTemplate's specs
type PipelineTemplateSpec struct {
	// Engine the way how to render PipelineTemplate
	// +optinal
	Engine PipelineTemplateEngine
	// WithSCM indicate if we use scm in Pipeline
	// +optional
	WithSCM bool
	// Agent indicate where the pipeline will running
	// +optional
	Agent *JenkinsAgent
	// Stages contains all stages of a pipeline script
	Stages []PipelineStage
	// Parameters will need before pipeline run
	// +optional
	Parameters []PipelineParameter
	// Arguments is arguments for templates
	// +optional
	Arguments []PipelineTemplateArgumentGroup
	// Environments is environment for jenkinsfile
	// +optional
	Environments []PipelineEnvironment
	// +optional
	Options PipelineOptions
	// +optional
	Triggers PipelineTriggers
	// +optional
	Post map[string][]PipelineTemplateTask
	// +optional
	ConstValues ConstValues
}

type PipelineTriggers struct {
	Raw string
}

type ConstValues struct {
	// +optional
	Tasks map[string]TaskConstValue
}

type TaskConstValue struct {
	Args map[string]string
	// +optional
	Options PipelineOptions
	// +optional
	Approve PipelineTaskApprove
}

// PipelineOptions  specifed options for templatespec
type PipelineOptions struct {
	Timeout int
}

// JenkinsAgent specifed agent for PipelineTemplate
type JenkinsAgent struct {
	// Label is the label for agents
	Label string
	// Raw is the text plain for Jenkins agent
	Raw string
}

// PipelineStage specifed stage for pipeline
type PipelineStage struct {
	// Name is name for a stage
	Name string
	// Display is display for a stage if it is null it will use Name as Display
	// +optional
	Display I18nName
	// Tasks contains all tasks which will running
	Tasks []PipelineTemplateTask
	// +optional
	Conditions map[string][]string
}

// PipelineTemplateTask spcifed task template for PipelineTemplate
type PipelineTemplateTask struct {
	// Name is the name of a task
	Name string

	// Display is display for a task If display is null it will use Name as Display
	// +optional
	Display I18nName
	// Agent indicate that where the current task will running
	// +optional
	Agent *JenkinsAgent
	// Type is type of a task
	Type string
	// Kind is kind of a task
	Kind string
	// Options is some options for a task
	// +optional
	Options *PipelineTaskOption
	// Approve is a option for maual confirm
	// +optional
	Approve *PipelineTaskApprove
	// Environments contains custom define variables
	// +optional
	Environments []PipelineEnvironment
	// Relation relation between arguments
	// +optional
	Relation []PipelineTaskArgumentAction
	// +optional
	Conditions map[string][]string
}

// PipelineEnvironment specifed environment for Pipeline
type PipelineEnvironment struct {
	// Name is a key for environment map
	Name string
	// Value is a value for environment map
	Value string
}

// PipelineTaskApprove specfied approve option for pipeline
type PipelineTaskApprove struct {
	// Message is the message show to users
	Message string
	// Timeout is timeout for waiting
	// +optional
	Timeout int64
}

// PipelineTaskOption spcified task option for task template
type PipelineTaskOption struct {
	// Timeout is timeout for a operation
	// +optional
	Timeout int64
}

// PipelineTemplateArgumentGroup specifed argument group for PipelineTemplate
type PipelineTemplateArgumentGroup struct {
	// DisplayName is used to display
	DisplayName I18nName
	// Items contains all argument for templates
	Items []PipelineTemplateArgumentValue
}

// I18nName spcified name for Piepline's stage or aruguments
type I18nName struct {
	// Zh is the Chinese name
	Zh string
	// EN is the English name
	En string
}

// PipelineTemplateArgument specified arugment for PipelineTemplate
type PipelineTemplateArgument struct {
	// Name is the name of a PipelineTemplate
	Name string
	// Schema is the schema of a template
	Schema PipelineTaskArgumentSchema
	// Binding mean bind argument to task
	// +optional
	Binding []string
	// Display is used to display
	Display PipelineTaskArgumentDisplay
	// Required specific argument is required
	// +optional
	Required bool
	// Default specific argument has default value
	// +optional
	Default string
	// Validation specific validation for argument
	// +optional
	Validation *PipelineTaskArgumentValidation
	// Relation relation between arguments
	// +optional
	Relation []PipelineTaskArgumentAction
}

// PipelineTemplateArgumentValue hold argument and value
type PipelineTemplateArgumentValue struct {
	PipelineTemplateArgument
	// Value is the value of a argument
	// +optional
	Value string
}

// PipelineTaskArgumentSchema specifed arugment schema
type PipelineTaskArgumentSchema struct {
	// Type is the type of a argument
	Type string
}

// PipelineTaskArgumentDisplay specifed the way of dipslay
type PipelineTaskArgumentDisplay struct {
	// Type is the type of a argument
	Type string
	// Name contains multi-languages name
	Name I18nName
	// Related related to other arguments
	// +optional
	Related string
	// Description is used to describe the arugments
	// +optional
	Description I18nName
	// Advanced field has default value
	// +optional
	Advanced bool
	// Args is used to add extra data to this argument
	// +optional
	Args map[string]string
}

// PipelineConfigTemplate is instance of template
type PipelineConfigTemplate struct {
	metav1.TypeMeta
	metav1.ObjectMeta

	// Labels add some marks
	// +optional
	Labels map[string]string
	// Spec specification of PipelineConfigTemplate
	Spec PipelineConfigTemplateSpec
}

// PipelineConfigTemplateSpec specified for  PipelineConfigTemplate
type PipelineConfigTemplateSpec struct {
	// Engine is a engine for render PipelineTemplate
	// +optional
	Engine PipelineTemplateEngine
	// WithSCM indicate if pipeline needs a scm
	// +optional
	WithSCM bool
	// Agent agent will indicates where the pipeline will running
	// +optional
	Agent *JenkinsAgent
	// Stages contains all stages for a pipeline
	Stages []PipelineStageInstance
	// Parameters is for execute process usage
	// +optional
	Parameters []PipelineParameter
	// Arguments contains all arguments need by template
	// +optional
	Arguments []PipelineTemplateArgumentGroup
	// Dependencies indicates plugins denpendencies of task
	// +optional
	Dependencies *PipelineDependency
	// Environments contains env config for jenkinsfile
	// +optional
	Environments []PipelineEnvironment
}

// PipelineStageInstance is a instance of PipelineStage
type PipelineStageInstance struct {
	// Name is the name a stage
	Name string
	// Tasks contains all task include in a stage
	Tasks []PipelineTemplateTaskInstance
}

// PipelineTemplateTaskInstance is a instance of PipelineTemplateTask
type PipelineTemplateTaskInstance struct {
	metav1.TypeMeta
	metav1.ObjectMeta

	Spec PipelineTemplateTaskInstanceSpec
}

// PipelineTemplateTaskInstanceSpec specified PipelineTemplateTaskInstance
type PipelineTemplateTaskInstanceSpec struct {
	// Agent specific task running agent
	// +optional
	Agent *JenkinsAgent
	// Engine will render task template
	// +optional
	Engine PipelineTemplateTaskEngine
	// Type is type of task
	Type string
	// Body is the body of a pipeline
	Body string
	// Options is option for task
	// +optional
	Options *PipelineTaskOption
	// Approve mean task need to approve
	// +optional
	Approve *PipelineTaskApprove
	// Environments is variables for pipeline
	// +optional
	Environments []PipelineEnvironment
	// Exports mean task will export some variables
	// +optional
	Exports []GlobalParameter
	// Arguments contains all arguments include in templates
	// +optional
	Arguments []PipelineTemplateArgument
	// Relation relation between task and arguments
	// +optional
	Relation []PipelineTaskArgumentAction
}

type versionable interface {
	GetVersionedName() (string, error)
}

// PipelineTemplateInterface interface of PipelineTemplate
type PipelineTemplateInterface interface {
	runtime.Object
	GetPiplineTempateSpec() *PipelineTemplateSpec
	// GetTypeMeta() *metav1.TypeMeta // TypeMeta is not always has kind
	GetKind() string
	metav1.ObjectMetaAccessor
	metav1.Object
	versionable
	fmt.Stringer
}

// PipelineTaskTemplateInterface interface of PipelineTaskTemplate
type PipelineTaskTemplateInterface interface {
	runtime.Object
	GetPiplineTaskTempateSpec() *PipelineTaskTemplateSpec
	// GetTypeMeta() *metav1.TypeMeta // TypeMeta is not always has kind
	GetKind() string
	metav1.ObjectMetaAccessor
	metav1.Object
	versionable
	fmt.Stringer
}

var _ PipelineTemplateInterface = &ClusterPipelineTemplate{}
var _ PipelineTemplateInterface = &PipelineTemplate{}
var _ PipelineTaskTemplateInterface = &ClusterPipelineTaskTemplate{}
var _ PipelineTaskTemplateInterface = &PipelineTaskTemplate{}

func (template *ClusterPipelineTemplate) String() string {
	return fmt.Sprintf("%s/%s", template.GetKind(), template.Name)
}

// GetTypeMeta get typemeta
func (template *ClusterPipelineTemplate) GetKind() string {
	return TypeClusterPipelineTemplate
}

// GetPiplineTempateSpec get PipelineTemplateSpec
func (template *ClusterPipelineTemplate) GetPiplineTempateSpec() *PipelineTemplateSpec {
	return &template.Spec
}

// GetVersionedName get name appended version suffix
func (template *ClusterPipelineTemplate) GetVersionedName() (string, error) {
	version := template.Annotations[AnnotationsTemplateVersion]
	if version == "" {
		return "", fmt.Errorf("ClusterPipelineTemplate '%s/%s' has no version", template.Namespace, template.Name)
	}

	templateName := template.Annotations[AnnotationsTemplateName]
	if templateName == "" {
		templateName = strings.TrimSuffix(template.Name, "."+version)
	}

	return fmt.Sprintf("%s.%s", templateName, version), nil
}

func (template *PipelineTemplate) String() string {
	return fmt.Sprintf("%s/%s/%s", template.GetKind(), template.Namespace, template.Name)
}

// GetKind get kind
func (template *PipelineTemplate) GetKind() string {
	return TypePipelineTemplate
}

// GetPiplineTempateSpec get PipelineTemplateSpec
func (template *PipelineTemplate) GetPiplineTempateSpec() *PipelineTemplateSpec {
	return &template.Spec
}

// GetVersionedName get name appended version suffix
func (template *PipelineTemplate) GetVersionedName() (string, error) {
	version := template.Annotations[AnnotationsTemplateVersion]
	if version == "" {
		return "", fmt.Errorf("PipelineTemplate '%s/%s' has no version", template.Namespace, template.Name)
	}
	templateName := template.Annotations[AnnotationsTemplateName]
	if templateName == "" {
		templateName = strings.TrimSuffix(template.Name, "."+version)
	}
	return fmt.Sprintf("%s.%s", template.Name, version), nil
}

func (template *ClusterPipelineTaskTemplate) String() string {
	return fmt.Sprintf("%s/%s", template.GetKind(), template.Name)
}

// GetKind get kind
func (template *ClusterPipelineTaskTemplate) GetKind() string {
	return TypeClusterPipelineTaskTemplate
}

// GetPiplineTaskTempateSpec get PipelineTemplateSpec
func (template *ClusterPipelineTaskTemplate) GetPiplineTaskTempateSpec() *PipelineTaskTemplateSpec {
	return &template.Spec
}

// GetVersionedName get name appended version suffix
func (template *ClusterPipelineTaskTemplate) GetVersionedName() (string, error) {
	version := template.Annotations[AnnotationsTemplateVersion]
	if version == "" {
		return "", fmt.Errorf("ClusterPipelineTaskTemplate '%s/%s' has no version", template.Namespace, template.Name)
	}
	templateName := template.Annotations[AnnotationsTemplateName]
	if templateName == "" {
		templateName = strings.TrimSuffix(template.Name, "."+version)
	}
	return fmt.Sprintf("%s.%s", template.Name, version), nil
}

func (template *PipelineTaskTemplate) String() string {
	return fmt.Sprintf("%s/%s/%s", template.GetKind(), template.Namespace, template.Name)
}

// GetKind get kind
func (template *PipelineTaskTemplate) GetKind() string {
	return TypePipelineTaskTemplate
}

// GetPiplineTaskTempateSpec get PipelineTemplateSpec
func (template *PipelineTaskTemplate) GetPiplineTaskTempateSpec() *PipelineTaskTemplateSpec {
	return &template.Spec
}

// GetVersionedName get name appended version suffix
func (template *PipelineTaskTemplate) GetVersionedName() (string, error) {
	version := template.Annotations[AnnotationsTemplateVersion]
	if version == "" {
		return "", fmt.Errorf("PipelineTaskTemplate '%s/%s' has no version", template.Namespace, template.Name)
	}
	templateName := template.Annotations[AnnotationsTemplateName]
	if templateName == "" {
		templateName = strings.TrimSuffix(template.Name, "."+version)
	}
	return fmt.Sprintf("%s.%s", template.Name, version), nil
}

func NewPipelineTemplate(kind string) PipelineTemplateInterface {
	switch kind {
	case TypePipelineTemplate:
		return &PipelineTemplate{}
	default:
		return &ClusterPipelineTemplate{}
	}
}

func NewPipelineTaskTemplate(kind string) PipelineTaskTemplateInterface {
	switch kind {
	case TypeClusterPipelineTaskTemplate:
		return &ClusterPipelineTaskTemplate{}
	default:
		return &PipelineTaskTemplate{}
	}
}
