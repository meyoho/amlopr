package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestToolBinding is the binding referenced to TestTool
type TestToolBinding struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the TestToolBinding.
	// +optional
	Spec TestToolBindingSpec
	// Most recently observed status of the TestToolBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus
}

// TestToolBindingSpec is the spec in TestToolBinding
type TestToolBindingSpec struct {
	TestTool LocalObjectReference
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestToolBindingList is a list of TestToolBinding objects.
type TestToolBindingList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of TestToolBinding
	Items []TestToolBinding
}

// endregion
