package devops

import (
	"regexp"
	"strconv"

	"k8s.io/apimachinery/pkg/util/validation"
)

func init() {
	// add CodeRepoService
	addAnnotationToolFunc(TypeCodeRepoService, annotateCodeRepoService)
	addAnnotationToolFunc(TypeJenkins, annotateJenkins)
	addAnnotationToolFunc(TypeImageRegistry, annotateImageRegistry)
	addAnnotationToolFunc(TypeProjectManagement, annotateProjectManagement)
	addAnnotationToolFunc(TypeCodeQualityTool, annotateCodeQualityTool)
}

// AnnotateTool general internal function to annotate specific tools according to
// its own schema
func AnnotateTool(tool ToolInterface) map[string]string {
	if tool == nil {
		return nil
	}
	annotations := defaultToolFunc(tool)
	// if has a specific tool it should use it
	if function, ok := annotationsFunctions[tool.GetKind()]; ok {
		for k, v := range function(tool) {
			annotations[k] = v
		}
	}
	return annotations
}

// AnnotateToolBindingReplica annotate ToolBindingReplica according the tool
func AnnotateToolBindingReplica(tool ToolInterface) map[string]string {
	if tool == nil {
		return nil
	}

	annotations := map[string]string{}

	annotations[AnnotationsToolHttpHost] = tool.GetToolSpec().HTTP.Host
	annotations[AnnotationsToolAccessURL] = tool.GetToolSpec().HTTP.AccessURL
	annotations[AnnotationsToolItemKind] = tool.GetKind()
	annotations[AnnotationsToolItemType] = tool.GetKindType()
	annotations[AnnotationsToolName] = tool.GetObjectMeta().GetName()

	if tool.GetObjectMeta().GetAnnotations() != nil {
		if project, ok := tool.GetObjectMeta().GetAnnotations()[AnnotationsKeyProject]; ok {
			annotations[AnnotationsToolItemProject] = project
		}
	}

	return annotations
}

// CurateLabels removes all non valid label values
func CurateLabels(labels map[string]string) map[string]string {

	if labels == nil {
		return labels
	}
	toDelete := []string{}
	for k, v := range labels {
		err := validation.IsValidLabelValue(v)
		if len(err) != 0 {
			toDelete = append(toDelete, k)
		}
	}
	for _, k := range toDelete {
		delete(labels, k)
	}
	return labels
}

var labelValueRegex = regexp.MustCompile("(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])?")

// addAnnotationToolFunc internal method to add a annotation function for a kind
func addAnnotationToolFunc(kind string, function annotateToolFunc) {
	annotationsFunctions[kind] = function
}

// annotateToolFunc specific function to annotate a tool
type annotateToolFunc func(ToolInterface) map[string]string

var annotationsFunctions = map[string]annotateToolFunc{}

// defaultToolFunc default no-op function for annotating tools
func defaultToolFunc(tool ToolInterface) (annotations map[string]string) {
	annotations = tool.GetObjectMeta().GetAnnotations()
	if annotations == nil {
		annotations = map[string]string{}
	}
	// is subscription?
	if value, ok := annotations[AnnotationsToolSubscription]; !ok && value == "" {
		annotations[AnnotationsToolSubscription] = FalseString
	}
	// get kind
	if value, ok := annotations[AnnotationsToolItemKind]; !ok && value == "" {
		annotations[AnnotationsToolItemKind] = tool.GetKind()
	}
	// get kind type
	if value, ok := annotations[AnnotationsToolItemType]; !ok && value == "" {
		annotations[AnnotationsToolItemType] = tool.GetKindType()
	}
	return
}

// annotateCodeRepoService internal function to annotate CodeRepoService in admission process
func annotateCodeRepoService(tool ToolInterface) (annotations map[string]string) {
	annotations = map[string]string{}
	if codeRepoService, ok := tool.(*CodeRepoService); ok {
		annotations[AnnotationsToolItemPublic] = strconv.FormatBool(codeRepoService.Spec.Public)
		annotations[AnnotationsToolItemKind] = codeRepoService.Spec.Type.String()
		annotations[AnnotationsCreateAppUrl] = codeRepoService.GetHtml()
		annotations[AnnotationsToolType] = ToolChainCodeRepositoryName
	}
	return
}

// annotateImageRegistry internal function to annotate ImageRegistry in admission process
func annotateImageRegistry(tool ToolInterface) (annotations map[string]string) {
	annotations = map[string]string{}
	if imageRegistry, ok := tool.(*ImageRegistry); ok {
		annotations[AnnotationsToolItemPublic] = strconv.FormatBool(imageRegistry.Spec.Type == RegistryTypeDockerHub)
		annotations[AnnotationsToolItemKind] = imageRegistry.Spec.Type.String()
		annotations[AnnotationsCreateAppUrl] = imageRegistry.GetEndpoint()
		annotations[AnnotationsToolType] = ToolChainArtifactRepositoryName
	}
	return
}

// annotateJenkins internal function to annotate Jenkins in admission process
func annotateJenkins(tool ToolInterface) (annotations map[string]string) {
	annotations = map[string]string{}
	if jenkins, ok := tool.(*Jenkins); ok {
		annotations[AnnotationsToolItemPublic] = FalseString
		annotations[AnnotationsToolItemKind] = TypeJenkins
		annotations[AnnotationsCreateAppUrl] = jenkins.GetHostPort().Host
		annotations[AnnotationsToolType] = ToolChainContinuousIntegrationName
	}
	return
}

// annotateProjectManagement internal function to annotate ProjectManagement objects in admission process
func annotateProjectManagement(tool ToolInterface) (annotations map[string]string) {
	annotations = map[string]string{}
	if projectManagement, ok := tool.(*ProjectManagement); ok {
		annotations[AnnotationsToolItemPublic] = FalseString
		annotations[AnnotationsToolItemKind] = projectManagement.GetKindType()
		annotations[AnnotationsCreateAppUrl] = projectManagement.GetHostPort().Host
		annotations[AnnotationsToolType] = ToolChainProjectManagementName
	}
	return
}

// annotateCodeQualityTool internal function to annotate CodeQualityTool objects in admission process
func annotateCodeQualityTool(tool ToolInterface) (annotations map[string]string) {
	annotations = map[string]string{}
	if codeQualityTool, ok := tool.(*CodeQualityTool); ok {
		annotations[AnnotationsToolItemPublic] = FalseString
		annotations[AnnotationsToolItemKind] = codeQualityTool.GetKindType()
		annotations[AnnotationsCreateAppUrl] = codeQualityTool.GetHostPort().Host
		annotations[AnnotationsToolType] = ToolChainCodeQualityToolName
	}
	return
}
