package role

// Conversor defines a conversion mechanism for a type
type Conversor interface {
	ConvertToStandard(project []*ProjectUserRoleAssignment, platform *Platform) []*ProjectUserRoleAssignment
	ConvertToPlatform(projects []*ProjectUserAssigmentOperation, platform *Platform) []*ProjectUserAssigmentOperation
}

// ConversorBySyncType returns a conversor according to the SyncType
func ConversorBySyncType(sync SyncType) Conversor {
	switch sync {
	case SyncTypeRBAC, SyncTypeRBACMutualExclusive:
		return RBACConversor{}
	case SyncTypeCustom:
		return CustomConversor{}
	default:
		return NoOpConversor{}
	}
}

// NoOpConversor does nothing
type NoOpConversor struct{}

var _ Conversor = NoOpConversor{}

// ConvertToStandard no op conversion
func (NoOpConversor) ConvertToStandard(project []*ProjectUserRoleAssignment, platform *Platform) []*ProjectUserRoleAssignment {
	return project
}

// ConvertToPlatform no op conversion
func (NoOpConversor) ConvertToPlatform(projects []*ProjectUserAssigmentOperation, platform *Platform) []*ProjectUserAssigmentOperation {
	return projects
}
