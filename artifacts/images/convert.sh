#! /bin/bash
usage(){
echo "Usage: $0 image-type version

where:
    image-type in [ tensorflow|serving ]
    version is valid result of listag.sh
" 1>&2; exit 1;
}

if [ $# -lt 2 ]; then
usage
fi

setRegistry(){
 defaultRegFile=".registry"
 if [[ ! -f ${defaultRegFile} ]] ;then
  echo "registry=index.alauda.cn/alaudaorg" > ${defaultRegFile}
 fi
 . ".registry"
 reg=${registry}
}

run(){
case "$1" in
"tensorflow")
echo "process tensorflow/tensorflow image ... "
echo "docker pull tensorflow/tensorflow:$2"
docker pull tensorflow/tensorflow:$2
echo "docker tag tensorflow/tensorflow:$2 $reg/tensorflow:$2"
docker tag tensorflow/tensorflow:$2 $reg/tensorflow:$2
echo "docker login $reg/tensorflow:$2"
docker login $reg/tensorflow:$2
echo "docker push $reg/tensorflow:$2"
docker push $reg/tensorflow:$2
;;
"serving")
echo "process tensorflow/serving image ..."
echo "docker pull tensorflow/serving:$2"
docker pull tensorflow/serving:$2
echo "docker tag tensorflow/serving:$2 $reg/tensorflow-serving:$2"
docker tag tensorflow/serving:$2 $reg/tensorflow-serving:$2
echo "docker login $reg/tensorflow-serving:$2"
docker login $reg/tensorflow-serving:$2
echo "docker push $reg/tensorflow-serving:$2"
docker push $reg/tensorflow-serving:$2
;;
*)
echo "unsupport image type"
;;
esac
}
###
setRegistry
run $1 $2
