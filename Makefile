
# Image URL to use all building/pushing image targets


TAG=dev-$(shell cat .version)-$(shell git config --get user.email | sed -e "s/@/-/")
IMG ?= index.alauda.cn/alaudak8s/amlopr:${TAG}
BASE_TOKEN=$(shell echo $(TAG))-int-dev
INT_TOKEN=$(shell echo $(BASE_TOKEN) | sed 's|\.|-|g')

all: test manager

# Run tests
# -p to controll package parallel test
test: generate fmt vet manifests
	go test ./pkg/... ./cmd/... -coverprofile cover.out -p=2

# Build manager binary
manager: generate fmt vet
	go build -o bin/manager alauda.io/amlopr/cmd/manager

# Run against the configured Kubernetes cluster in ~/.kube/config
run: generate fmt vet
	go run ./cmd/manager/main.go

# Install CRDs into a cluster
install: manifests
	kubectl apply -f config/crds

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deployold: manifests
	kubectl apply -f config/crds
	kustomize build config/default | kubectl apply -f -

deploy: manifests
	kubectl apply -f config/crds
	kubectl apply -f config/manager


# Generate manifests e.g. CRD, RBAC etc.
manifests:
	go run vendor/sigs.k8s.io/controller-tools/cmd/controller-gen/main.go all
#	make modify

undeployold:
	kubectl delete -f config/crds
	kustomize build config/default | kubectl delete -f -

undeploy:
	kubectl delete -f config/crds
	kubectl delete -f config/manager

installcrd:
	kubectl apply -f config/samples

# Run go fmt against code
fmt:
	go fmt ./pkg/... ./cmd/...

# Run go vet against code
vet:
	go vet ./pkg/... ./cmd/...

# Generate code
generate:
	go generate ./pkg/... ./cmd/...
	vendor/k8s.io/code-generator/generate-groups.sh client alauda.io/amlopr/pkg/client  alauda.io/amlopr/pkg/apis ml:v1alpha1


# Build the docker image
docker-build: test
	docker build . -t ${IMG}
	@echo "updating kustomize image patch file for manager resource"
	sed -i'' -e 's@image: .*@image: '"${IMG}"'@' ./config/default/manager_image_patch.yaml

# Push the docker image
docker-push:
	docker push ${IMG}


# Build the test images
build-test-image:
	docker build -t index.alauda.cn/alaudak8s/amlopr-tests:$(INT_TOKEN) -f Dockerfile.tests .
	docker push index.alauda.cn/alaudak8s/amlopr-tests:$(INT_TOKEN)

# cleanup int sonobuoy test environment
int-cleanup:
	kubectl delete -f tmp.yaml || true
	curl -X DELETE -H 'Authorization: Token '$(INT_TOKEN) http://sonobuoy-alaudak8s.myalauda.cn/status/
	rm tmp.yaml || true

# run case in sonobuoy test environment
integration: build-test-image int-cleanup
	cp test/integration-tests.yaml tmp.yaml
	sed -i '' "s|%TOKEN%|$(INT_TOKEN)|g" tmp.yaml
	sed -i '' "s|%IMAGE%|index.alauda.cn/alaudak8s/amlopr-tests:$(INT_TOKEN)|g" tmp.yaml
	kubectl create -f tmp.yaml

# query test result of sonobuoy test
int-status:
	curl -H 'Authorization: Token '$(INT_TOKEN) http://sonobuoy-alaudak8s.myalauda.cn/status/ | python -m json.tool

