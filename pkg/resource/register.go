package resource

import (
	crdres "alauda.io/amlopr/pkg/resource/apiextensions/v1beta1"
	sigappv1beta1res "alauda.io/amlopr/pkg/resource/app/v1beta1"
	devopsv1alpha1res "alauda.io/amlopr/pkg/resource/devops/v1alpha1"
	mlv1alpha1res "alauda.io/amlopr/pkg/resource/ml/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

var AllKnownTypesWithGroupVersion = make(map[schema.GroupVersion][]runtime.Object)

func init() {
	AllKnownTypesWithGroupVersion[crdres.SchemeGroupVersion] = crdres.KnownTypes
	AllKnownTypesWithGroupVersion[sigappv1beta1res.SchemeGroupVersion] = sigappv1beta1res.KnownTypes
	AllKnownTypesWithGroupVersion[devopsv1alpha1res.SchemeGroupVersion] = devopsv1alpha1res.KnownTypes
	AllKnownTypesWithGroupVersion[mlv1alpha1res.SchemeGroupVersion] = mlv1alpha1res.KnownTypes
}

func Register(mgr manager.Manager) error {
	for gv, t := range AllKnownTypesWithGroupVersion {
		mgr.GetScheme().AddKnownTypes(gv, t...)
		metav1.AddToGroupVersion(mgr.GetScheme(), gv)
	}
	return nil
}
