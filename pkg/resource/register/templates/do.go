package templates

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"encoding/json"
	"fmt"
	"github.com/ghodss/yaml"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"path"
	"path/filepath"
	"sigs.k8s.io/controller-runtime/pkg/client/apiutil"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"strings"
)

var log = logf.Log.WithName("templates")

type BytesResource struct {
	namespacedName     types.NamespacedName
	runtimeObject      common.Object
	existRuntimeObject common.Object
}

type AmlTemplateRegister struct {
	CLINamesapce     string
	templateBasePath string
	//client           client.Client
	mgr manager.Manager
}

func NewAmlTemplateRegister(mgr manager.Manager, basePath, namespace string) *AmlTemplateRegister {
	return &AmlTemplateRegister{
		mgr:              mgr,
		templateBasePath: basePath,
		CLINamesapce:     namespace,
	}
}

func (t *AmlTemplateRegister) getNamespace(nsFromYaml, nsFromCli string) string {
	if nsFromCli != "" {
		return nsFromCli
	}
	if nsFromYaml != "" {
		return nsFromYaml
	} else {
		return "default"
	}
}

// RegisterTemplate RegisterTemplate when start up
// basePath  模板文件的根目录
// namespace 如果为空，会按照yaml中模板定义的namespace 创建，如果模板中namespace 也为空 则创建到default
func (t *AmlTemplateRegister) RegisterTemplate() error {
	var err error

	err = t.registerAmlTemplates()
	if err != nil {
		return err
	}
	err = t.registerDevOpsTemplate()
	if err != nil {
		return err
	}
	return nil
}

func (t *AmlTemplateRegister) registerDevOpsTemplate() error {
	basePath := t.templateBasePath
	var err error
	var allResourceTemplates []BytesResource

	ClusterPipelineTemplatePath := "devopstemplate/clustertemplate/pipelinetemplate"
	ClusterTaskTemplatePath := "devopstemplate/clustertemplate/tasktemplate"
	clusterPipelineResource, err := getClusterPipelineBytesResource(basePath, ClusterPipelineTemplatePath)
	if err != nil {
		return err
	}
	clusterTaskResource, err := getClusterTaskBytesResource(basePath, ClusterTaskTemplatePath)
	if err != nil {
		return err
	}
	allResourceTemplates = append(allResourceTemplates, clusterPipelineResource...)
	allResourceTemplates = append(allResourceTemplates, clusterTaskResource...)

	for _, aByteResource := range allResourceTemplates {
		err = t.createOrUpdateResource(
			aByteResource.namespacedName,
			aByteResource.existRuntimeObject,
			aByteResource.runtimeObject)
		if err != nil {
			return err
		}
	}
	return nil
}

func getClusterPipelineBytesResource(basePath, templateFolder string) ([]BytesResource, error) {
	var result []BytesResource
	clusterFiles, err := findFilesByFolderName(basePath, templateFolder)
	if err != nil {
		return nil, err
	}

	for _, b := range clusterFiles {
		clusterPipelineTemplate := &devopsv1alpha1.ClusterPipelineTemplate{}
		if err := json.Unmarshal(b, &clusterPipelineTemplate); err != nil {
			return nil, err
		}
		result = append(result, BytesResource{namespacedName: types.NamespacedName{Name: clusterPipelineTemplate.Name},
			runtimeObject:      clusterPipelineTemplate,
			existRuntimeObject: &devopsv1alpha1.ClusterPipelineTemplate{}})
	}
	return result, nil
}

func getClusterTaskBytesResource(basePath, templateFolder string) ([]BytesResource, error) {
	var result []BytesResource
	clusterFiles, err := findFilesByFolderName(basePath, templateFolder)
	if err != nil {
		return nil, err
	}

	for _, b := range clusterFiles {
		clusterPipelineTaskTemplate := &devopsv1alpha1.ClusterPipelineTaskTemplate{}
		if err := json.Unmarshal(b, &clusterPipelineTaskTemplate); err != nil {
			return nil, err
		}
		result = append(result, BytesResource{namespacedName: types.NamespacedName{Name: clusterPipelineTaskTemplate.Name},
			runtimeObject:      clusterPipelineTaskTemplate,
			existRuntimeObject: &devopsv1alpha1.ClusterPipelineTaskTemplate{}})
	}
	return result, nil
}

func findFilesByFolderName(basePath, folder string) (byteFiles [][]byte, err error) {
	destPath := path.Clean(strings.Join([]string{basePath, folder}, string(filepath.Separator)))
	fileInfolist, err := ioutil.ReadDir(destPath)
	if err != nil {
		return nil, err
	}
	byteFiles = make([][]byte, 0)
	for _, info := range fileInfolist {
		if info.IsDir() ||
			!(strings.HasSuffix(info.Name(), ".yaml") || strings.HasSuffix(info.Name(), ".yml")) {
			continue
		}
		var fileBytes, jsonBlob []byte
		fPath := path.Clean(strings.Join([]string{destPath, info.Name()}, string(filepath.Separator)))
		if fileBytes, err = ioutil.ReadFile(fPath); err != nil {
			return
		}
		if jsonBlob, err = yaml.YAMLToJSON(fileBytes); err != nil {
			return
		}
		byteFiles = append(byteFiles, jsonBlob)
	}
	return
}

func (t *AmlTemplateRegister) createOrUpdateResource(namespacedName types.NamespacedName, old, new common.Object) error {
	client := t.mgr.GetClient()
	err := client.Get(context.TODO(), namespacedName, old)

	if err != nil {
		if errors.IsNotFound(err) {
			if err = client.Create(context.TODO(), new); err != nil {
				return err
			}
			gvk, err := apiutil.GVKForObject(new, t.mgr.GetScheme())
			if err == nil {
				log.Info(fmt.Sprintf("created: ns=%v name=%v kind=%v ready", namespacedName.Namespace, namespacedName.Name, gvk.Kind))
			} else {
				log.Info(fmt.Sprintf("created: ns=%v name=%v ready", namespacedName.Namespace, namespacedName.Name))
			}
		}
		return err
	}
	new.SetResourceVersion(old.GetResourceVersion())
	if err = client.Update(context.TODO(), new); err != nil {
		return err
	}
	gvk, err := apiutil.GVKForObject(new, t.mgr.GetScheme())
	if err == nil {
		log.Info(fmt.Sprintf("update: ns=%v name=%v kind=%v updated", namespacedName.Namespace, namespacedName.Name, gvk.Kind))
	} else {
		log.Info(fmt.Sprintf("update: ns=%v name=%v updated", namespacedName.Namespace, namespacedName.Name))
	}

	return nil
}

func (t *AmlTemplateRegister) getNamespacedPipelineBytesResource(basePath, templateFolder string) ([]BytesResource, error) {
	var result []BytesResource
	byteFiles, err := findFilesByFolderName(basePath, templateFolder)
	if err != nil {
		return nil, err
	}

	for _, b := range byteFiles {
		amlPipelineTemplate := &mlv1alpha1.AMLPipelineTemplate{}
		if err := json.Unmarshal(b, &amlPipelineTemplate); err != nil {
			return nil, err
		}
		// choose the most wanted namespace
		amlPipelineTemplate.Namespace = t.getNamespace(amlPipelineTemplate.Namespace, t.CLINamesapce)
		result = append(result, BytesResource{
			namespacedName: types.NamespacedName{
				Namespace: t.getNamespace(amlPipelineTemplate.Namespace, t.CLINamesapce),
				Name:      amlPipelineTemplate.Name,
			},
			runtimeObject:      amlPipelineTemplate,
			existRuntimeObject: &mlv1alpha1.AMLPipelineTemplate{}})
	}
	return result, nil
}

func (t *AmlTemplateRegister) registerAmlTemplates() error {
	basePath := t.templateBasePath
	var err error
	var allResourceTemplates []BytesResource
	AmlTemplatePath := "amlpipelinetemplate"
	amlPipelineResource, err := t.getNamespacedPipelineBytesResource(basePath, AmlTemplatePath)
	if err != nil {
		return err
	}
	allResourceTemplates = append(allResourceTemplates, amlPipelineResource...)
	for _, aByteResource := range allResourceTemplates {
		err = t.createOrUpdateResource(aByteResource.namespacedName,
			aByteResource.existRuntimeObject,
			aByteResource.runtimeObject)
		if err != nil {
			return err
		}
	}
	return nil
}
