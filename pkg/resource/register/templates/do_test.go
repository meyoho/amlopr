package templates

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func Test_ReadFiles(t *testing.T) {

	base := "/Users/max/gocode/src/alauda.io/amlopr/config/template/devopstemplate"
	pptmp := "clustertemplate/pipelinetemplate"
	if _, err := findFilesByFolderName(base, pptmp); err != nil {
		fmt.Println(err)
	}
}

func Test_hassuffix(t *testing.T) {
	s1 := "abc.yaml"
	s2 := "abc.yml"
	assert.Equal(t, strings.HasSuffix(s1, ".yaml"), true, "")
	assert.Equal(t, strings.HasSuffix(s2, ".yml"), true, "")

}
