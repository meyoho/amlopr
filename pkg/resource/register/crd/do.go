package crd

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/ghodss/yaml"
	"io/ioutil"
	"k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"strings"
)

var log = logf.Log.WithName("crd")

func RegisterCrds(path string, client client.Client) error {
	fileInfo, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}
	if !strings.HasSuffix(path, "/") {
		path = path + "/"
	}
	for _, info := range fileInfo {
		var fileBytes, jsonBlob []byte
		var err error
		if fileBytes, err = ioutil.ReadFile(path + info.Name()); err != nil {
			return err
		}
		if jsonBlob, err = yaml.YAMLToJSON(fileBytes); err != nil {
			return err
		}
		newCrd := &v1beta1.CustomResourceDefinition{}
		if err = json.Unmarshal(jsonBlob, &newCrd); err != nil {
			return err
		}
		oldCrd := &v1beta1.CustomResourceDefinition{}
		err = client.Get(context.TODO(), types.NamespacedName{Name: newCrd.Name}, oldCrd)
		if err != nil {
			if errors.IsNotFound(err) {
				if err = client.Create(context.TODO(), newCrd); err != nil {
					log.Error(err, fmt.Sprintf("create crd error: %v", newCrd.Name))
					return err
				}
				log.Info(fmt.Sprintf("created: %v ready", newCrd.Name))
				continue
			}
			return err
		}
		copyedCrd := oldCrd.DeepCopy()
		copyedCrd.Spec.Group = newCrd.Spec.Group
		copyedCrd.Spec.Scope = newCrd.Spec.Scope
		*copyedCrd.Spec.Validation = *newCrd.Spec.Validation
		copyedCrd.Spec.Version = newCrd.Spec.Version

		if err = client.Update(context.TODO(), copyedCrd); err != nil {
			log.Error(err, fmt.Sprintf("updated crd error: %v", copyedCrd.Name))
			return err
		}
		log.Info(fmt.Sprintf("updated: %v ready", copyedCrd.Name))
	}
	return nil
}
