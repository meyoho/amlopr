package devops

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/runtime/scheme"
)

var (
	// SchemeGroupVersion is group version used to register these objects
	SchemeGroupVersion = schema.GroupVersion{Group: "devops.alauda.io", Version: "v1alpha1"}

	// SchemeBuilder is used to add go types to the GroupVersionKind scheme
	SchemeBuilder = &scheme.Builder{GroupVersion: SchemeGroupVersion}

	// AddToScheme is required by pkg/client/...
	AddToScheme = SchemeBuilder.AddToScheme

	KnownTypes = []runtime.Object{
		&devopsv1alpha1.ClusterPipelineTemplate{},
		&devopsv1alpha1.ClusterPipelineTemplateList{},
		&devopsv1alpha1.ClusterPipelineTaskTemplate{},
		&devopsv1alpha1.ClusterPipelineTaskTemplateList{},
		&devopsv1alpha1.PipelineTemplate{},
		&devopsv1alpha1.PipelineTemplateList{},
		&devopsv1alpha1.PipelineTaskTemplate{},
		&devopsv1alpha1.PipelineTaskTemplateList{},
	}
)
