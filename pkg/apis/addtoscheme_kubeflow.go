package apis

import (
	kubeflowv1beta1 "alauda.io/amlopr/pkg/apis/kubeflow/v1beta1"
	kubeflowv1beta2 "alauda.io/amlopr/pkg/apis/kubeflow/v1beta2"
)

func init() {
	// Register the types with the Scheme so the components can map objects to GroupVersionKinds and back
	AddToSchemes = append(AddToSchemes, kubeflowv1beta1.SchemeBuilder.AddToScheme)
	AddToSchemes = append(AddToSchemes, kubeflowv1beta2.SchemeBuilder.AddToScheme)
}
