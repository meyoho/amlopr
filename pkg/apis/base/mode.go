package base

type Mode string

const (
	Global   Mode = "global"
	Business Mode = "business"
)
