package base

import "alauda.io/amlopr/pkg/controller/common"

const (
	ClusterLabel = "cluster.ml"
)

func GetMLJenkinsBindingLabel(clusterName string) *common.AmlLabel {
	return common.NewAmlLabel().
		Add(MLLabelAppKey, MLLabelAppValue).
		Add(ClusterLabel, clusterName)
}
