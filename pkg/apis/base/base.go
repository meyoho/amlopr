package base

import (
	"alauda.io/amlopr/pkg/config"
	"alauda.io/amlopr/pkg/controller/common"
)

const (
	MLLabelAppKey   = "app.ml"
	MLLabelAppValue = "true"
)

func MLNamespaceLabel() *common.AmlLabel {
	return common.NewAmlLabel().
		Add(MLLabelAppKey, MLLabelAppValue)
}

func ProjectMLNamespaceLabel(project string) *common.AmlLabel {
	return common.NewAmlLabel().
		Add(MLLabelAppKey, MLLabelAppValue).
		Add(projectKey(), project)
}

func projectKey() string {
	return config.GetDomain() + "/project"
}

func NewMLAppLabel() *common.AmlLabel {
	return common.NewAmlLabel().Add(MLLabelAppKey, MLLabelAppValue)
}
