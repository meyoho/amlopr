package v1alpha1

import "alauda.io/amlopr/pkg/config"

const (
	LabelAMLPipelineConfig     = "AMLPipelineConfig"
	EnvKeyNvidiaVisibleDevices = "NVIDIA_VISIBLE_DEVICES"

	NvidiaGPUResourceType      = "nvidia.com/gpu"
	AMDGPUResourceType         = "amd.com/gpu"
	TencentVCoreResourceType   = "tencent.com/vcuda-core"
	TenCentVMemoryResourceType = "tencent.com/vcuda-memory"
)

func AnnotationsKeyPipelineLastNumber() string {
	return config.GetDomain() + "/aml.pipeline.last.number"
}

func AnnotationsKeyPipelineNumber() string {
	return config.GetDomain() + "/aml.pipeline.number"
}

func AnnotationsKeyNvidiaGpuVisibilityEnhancement() string {
	return "ml." + config.GetDomain() + "/nvidia.visibility.enhancement"
}
