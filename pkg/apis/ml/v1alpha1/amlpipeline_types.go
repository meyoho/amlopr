/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// AMLPipelineSpec defines the desired state of AMLPipeline
type AMLPipelineSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	User string `json:"user"`

	CancelUser string `json:"cancelUser"`

	AMLPipelineConfig v1.LocalObjectReference `json:"amlPipelineConfig"`

	Parameters []devops.PipelineTemplateArgumentGroup `json:"parameters,omitempty"`
}

// AMLPipelineStatus defines the observed state of AMLPipeline
type AMLPipelineStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	CRDList []CRDReference `json:"crdList,omitempty"`

	AmlBaseStatus
}

type CRDReference struct {
	Annotation  Annotation `json:"annotation,omitempty"`
	UUID        string     `json:"uuid,omitempty"`
	Kind        string     `json:"kind,omitempty"`
	Name        string     `json:"name,omitempty" protobuf:"bytes,1,opt,name=name"`
	Namespace   string     `json:"namespace,omitempty"`
	ClusterName string     `json:"clustername,omitempty"`
}

type Annotation struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// AMLPipeline is the Schema for the amlpipelines API
// +k8s:openapi-gen=true
type AMLPipeline struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   AMLPipelineSpec   `json:"spec,omitempty"`
	Status AMLPipelineStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// AMLPipelineList contains a list of AMLPipeline
type AMLPipelineList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []AMLPipeline `json:"items"`
}

var AMLPipelineStateEnum = struct {
	Unknown   BaseState
	Creating  BaseState
	Running   BaseState
	Cancelled BaseState
	Success   BaseState
	Failed    BaseState
	Error     BaseState
	Deleting  BaseState
}{"", "Creating", "Running", "Cancelled", "Success", "Failed", "Error", "Deleting"}

var AMLPipelinePhaseEnum = struct {
	Creating  BaseStage
	Running   BaseStage
	Cancelled BaseStage
	Success   BaseStage
	Failed    BaseStage
	Deleting  BaseStage
}{"Creating", "Running", "Cancelled", "Success", "Failed", "Deleting"}

func init() {
	SchemeBuilder.Register(&AMLPipeline{}, &AMLPipelineList{})
}
