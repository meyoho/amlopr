package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type BaseState string
type BaseStage string

const (
	/*
		StageCreating combined by state: Initializing , Creating , Synced, PartialSynced, SyncFailed
		StagePending  combined by state: Pending
		StageRunning  combined by state: Success
		StageFailed   combined by state: InitializedError
	*/

	StageCreating BaseStage = "Creating"
	StagePending  BaseStage = "Pending"
	StageRunning  BaseStage = "Running"
	StageFailed   BaseStage = "Failed"
	StageStopped  BaseStage = "Stopped"

	//stands for detail state
	StateUnknown          BaseState = ""
	StateInitializing     BaseState = "Initializing"
	StateInitializedError BaseState = "InitializedError"

	StateCreating BaseState = "Creating"

	StateSynced        BaseState = "Synced"
	StatePartialSynced BaseState = "PartialSynced"
	StateSyncFailed    BaseState = "SyncFailed"

	StateSuccess BaseState = "Success"
	StatePending BaseState = "Pending"

	StateFailed BaseState = "Failed"

	StateStopping   BaseState = "Stopping"
	StateStopped    BaseState = "Stopped"
	StateStopFailed BaseState = "StopFailed"
)

type AmlBaseStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Phase        BaseStage    `json:"phase"`
	State        BaseState    `json:"state"`
	Reason       string       `json:"reason,omitempty"`
	Message      string       `json:"message,omitempty"`
	CreateAt     *metav1.Time `json:"createAt,omitempty"`
	StartTime    *metav1.Time `json:"startTime,omitempty"`
	FinishTime   *metav1.Time `json:"finishTime,omitempty"`
	LastUpdateAt *metav1.Time `json:"lastUpdateAt,omitempty"`
}
