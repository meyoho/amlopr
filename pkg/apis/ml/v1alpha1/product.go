package v1alpha1

import "alauda.io/amlopr/pkg/config"

// aml product
const (
	AnnotationProductAMLValue     = "Alauda Machine Learning"
	AnnotationProductVersionValue = "v0.6"
)

func AnnotationProductAMLKey() string {
	return config.GetDomain() + "/product"
}

func AnnotationProductVersionKey() string {
	return config.GetDomain() + "/product.version"
}

func LabelAMLComponentKey() string {
	return "ml." + config.GetDomain() + "/component"
}
