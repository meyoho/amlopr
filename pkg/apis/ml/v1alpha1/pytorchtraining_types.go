/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	pytorchv1beta2 "github.com/kubeflow/pytorch-operator/pkg/apis/pytorch/v1beta2"
	"github.com/kubeflow/tf-operator/pkg/apis/common/v1beta2"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// PyTorchTrainingSpec defines the desired state of PyTorchTraining
type PyTorchTrainingSpec struct {
	PyTorchJob PyTorchJob `json:"pyTorchJob,omitempty"`
}

type PyTorchQuota = map[pytorchv1beta2.PyTorchReplicaType]ResourceQuota

type PyTorchJob struct {
	PyTorchJobSpec *pytorchv1beta2.PyTorchJobSpec `json:"PyTorchJobSpec,omitempty"`
	Origin         OriginPyTorchJobSpec           `json:"origin,omitempty"`
}

type OriginPyTorchJobSpec struct {
	PyTorchQuota PyTorchQuota `json:"pyTorchQuota,omitempty"`
	Image        Image        `json:"image"`
	Args         []string     `json:"args,omitempty"`
	Env          []v1.EnvVar  `json:"env,omitempty"`
	BackOffLimit *int32       `json:"backoffLimit,omitempty"`
}

// PyTorchTrainingStatus defines the observed state of PyTorchTraining
type PyTorchTrainingStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	AmlBaseStatus
	PyTorchJobStatus *v1beta2.JobStatus `json:"pyTorchJobStatus,omitempty"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PyTorchTraining is the Schema for the pytorchtrainings API
// +k8s:openapi-gen=true
type PyTorchTraining struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PyTorchTrainingSpec   `json:"spec,omitempty"`
	Status PyTorchTrainingStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PyTorchTrainingList contains a list of PyTorchTraining
type PyTorchTrainingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []PyTorchTraining `json:"items"`
}

func init() {
	SchemeBuilder.Register(&PyTorchTraining{}, &PyTorchTrainingList{})
}
