/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"testing"

	"github.com/onsi/gomega"
	"golang.org/x/net/context"
	"k8s.io/apimachinery/pkg/types"
)

func TestStorageAMLPipelineTemplate(t *testing.T) {
	key := types.NamespacedName{
		Name:      "foo",
		Namespace: "default",
	}
	created, err := getPipelineTemplate(key)
	if err != nil {
		panic(err)
	}
	g := gomega.NewGomegaWithT(t)

	// Test Create
	fetched := &AMLPipelineTemplate{}
	//c.Create(context.TODO(), created)
	g.Expect(c.Create(context.TODO(), created)).NotTo(gomega.HaveOccurred())

	g.Expect(c.Get(context.TODO(), key, fetched)).NotTo(gomega.HaveOccurred())
	g.Expect(fetched).To(gomega.Equal(created))

	// Test Updating the Labels
	updated := fetched.DeepCopy()
	updated.Labels = map[string]string{"hello": "world"}
	g.Expect(c.Update(context.TODO(), updated)).NotTo(gomega.HaveOccurred())

	fetched = &AMLPipelineTemplate{}
	g.Expect(c.Get(context.TODO(), key, fetched)).NotTo(gomega.HaveOccurred())
	g.Expect(fetched).To(gomega.Equal(updated))

	// Test Delete
	g.Expect(c.Delete(context.TODO(), fetched)).NotTo(gomega.HaveOccurred())
	g.Expect(c.Get(context.TODO(), key, fetched)).To(gomega.HaveOccurred())
}

func getPipelineTemplate(key types.NamespacedName) (*AMLPipelineTemplate, error) {
	created := AMLPipelineTemplate{}
	err := ReadCrdFromSampleFolder("ml_v1alpha1_amlpipelinetemplate.yaml", &created)
	if err != nil {
		return nil, err
	}
	created.Name = key.Name
	created.Namespace = key.Namespace
	return &created, nil
}
