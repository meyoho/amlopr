/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"github.com/kubeflow/tf-operator/pkg/apis/common/v1beta1"
	tensorflowv1beta1 "github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta1"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// TensorflowTrainingSpec defines the desired state of TensorflowTraining
type TensorflowTrainingSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	TFJob TFJob `json:"tfJob,omitempty"`
}

type TensorflowQuota = map[tensorflowv1beta1.TFReplicaType]ResourceQuota

type OriginTFJobSpec struct {
	TensorflowQuota TensorflowQuota `json:"tensorflowQuota,omitempty"`
	Image           Image           `json:"image,omitempty"`
	GpuImage        Image           `json:"gpuImage,omitempty"`
	Args            []string        `json:"args,omitempty"`
	Env             []v1.EnvVar     `json:"env,omitempty"`
	UseGpuImage     bool            `json:"useGpuImage,omitempty"`
}

//TODO devops中是否有此结构定义
type Image struct {
	RepositoryPath  string `json:"repositoryPath"`
	Tag             string `json:"tag,omitempty"`
	SecretName      string `json:"secretName"`
	SecretNamespace string `json:"secretNamespace"`
	//CredentialId    string `json:"credentialId,omitempty"`
}

type ResourceQuota struct {
	Replica int32                `json:"replica"`
	Quota   v1.ResourceQuotaSpec `json:"quota"`
}

type TFJob struct {
	TFJobSpec *tensorflowv1beta1.TFJobSpec `json:"tfJobSpec,omitempty"`
	Origin    OriginTFJobSpec              `json:"origin,omitempty"`
}

// TensorflowTrainingStatus defines the observed state of TensorflowTraining
type TensorflowTrainingStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	AmlBaseStatus
	TFJobStatus *v1beta1.JobStatus `json:"tfJobStatus,omitempty"`
}

var TrainingStateEnum = struct {
	Initializing  BaseState
	ImageBuilding BaseState
	Running       BaseState
	Success       BaseState
	Failed        BaseState
	Error         BaseState
}{"", "ImageBuilding", "Running", "Success", "Failed", "Error"}

var TrainingPhaseEnum = struct {
	Initializing  BaseStage
	ImageBuilding BaseStage
	Running       BaseStage
	Finished      BaseStage
}{"", "ImageBuilding", "Running", "Finished"}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TensorflowTraining is the Schema for the tensorflowtrainings API
// +k8s:openapi-gen=true
type TensorflowTraining struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   TensorflowTrainingSpec   `json:"spec,omitempty"`
	Status TensorflowTrainingStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TensorflowTrainingList contains a list of TensorflowTraining
type TensorflowTrainingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []TensorflowTraining `json:"items"`
}

func init() {
	SchemeBuilder.Register(&TensorflowTraining{}, &TensorflowTrainingList{})
}
