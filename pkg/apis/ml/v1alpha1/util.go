package v1alpha1

import (
	"io/ioutil"
	"path/filepath"
	"sigs.k8s.io/yaml"
)

func ReadCrdFromSampleFolder(fileName string, t interface{}) error {
	return ReadCrdFromFile("../../../../config/samples/"+fileName, t)
}

func ReadCrdFromFile(filePath string, t interface{}) error {
	dir, err := filepath.Abs(filepath.Dir(filePath))
	println(dir)
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}
	return yaml.Unmarshal(data, t)
}
