/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"fmt"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"strings"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

type ModelDeploymentState string

const (
	StateModelDeploymentUnknown       ModelDeploymentState = ""
	StateModelDeploymentBuilding      ModelDeploymentState = "ImageBuilding"
	StateModelDeploymentBuildError    ModelDeploymentState = "BuildFailed"
	StateModelDeploymentDeploying     ModelDeploymentState = "Deploying"
	StateModelDeploymentRedeploying   ModelDeploymentState = "ReDeploying"
	StateModelDeploymentRunning       ModelDeploymentState = "Running"
	StateModelDeploymentDeployFailed  ModelDeploymentState = "Failed"
	StateModelDeploymentDeleting      ModelDeploymentState = "Deleting"
	StateModelDeploymentDeploySucceed ModelDeploymentState = "Deployed"
	StateModelDeploymentUpdating      ModelDeploymentState = "Updating"

	ModelServiceFinalizer = "model-service"
)

type ModelSourceType string

type ModelServiceType string

const (
	ModelInRepo       ModelSourceType = "repo"
	ModelInDataVolume ModelSourceType = "datavolume"

	StandardService ModelServiceType = "standard"
	IstioService    ModelServiceType = "istio"
)

type ModelSource struct {
	SourceType ModelSourceType    `json:"type"` //pvc or repo
	Repository CodeRepositoryInfo `json:"repository,omitempty"`
	DataVolume DataVolumeInfo     `json:"dataVolume,omitempty"`
}

type CodeRepositoryInfo struct {
	Url               string         `json:"url"`
	Branch            string         `json:"branch,omitempty"`
	RelativeDirectory string         `json:"relativeDirectory,omitempty"`
	Credential        CredentialInfo `json:"credential"`
}

type CredentialInfo struct {
	Id string `json:"id"`
}
type DataVolumeInfo struct {
	User string `json:"user"` // mount_point name of specific user
	Path string `json:"path"`
}

type ImageBuildInfo struct {
	IsNeed         bool   `json:"isNeed"`
	BaseImage      string `json:"baseImage,omitempty"`
	BaseImageTag   string `json:"baseImageTag,omitempty"`
	DockerfilePath string `json:"dockerfilePath,omitempty"`
	BuildArguments string `json:"buildArguments,omitempty"`
	BuildContext   string `json:"buildContext,omitempty"`
	Retries        int32  `json:"retries,omitempty"`
	Timeout        string `json:"timeout,omitempty"`
	Cluster        string `json:"cluster,omitempty"`
	NameSpace      string `json:"namespace,omitempty"`
}

type ImageRepositoryInfo struct {
	Url                   string         `json:"url"`
	Tag                   string         `json:"tag"`
	Credential            CredentialInfo `json:"credential,omitempty"`
	OriginSecretName      string         `json:"originSecretName,omitempty"`
	OriginSecretNamespace string         `json:"originSecretNamespace,omitempty"`
}

func (image ImageRepositoryInfo) GetImage() string {
	httpPrefix := "http://"
	httpsPrefix := "https://"
	var img = image.Url
	if strings.HasPrefix(image.Url, httpPrefix) ||
		strings.HasPrefix(image.Url, httpsPrefix) {
		img = strings.Replace(strings.Replace(image.Url, httpPrefix, "", -1), httpsPrefix, "", -1)
	}
	return fmt.Sprintf("%s:%s", img, image.Tag)
}

type ModelInfo struct {
	ModelName string `json:"modelName"`
	Version   string `json:"version,omitempty"`
	Inputs    string `json:"inputs,omitempty"`
	Framework string `json:"framework,omitempty"`
}

type ModelDeploymentResource struct {
	Request corev1.ResourceList `json:"request,omitempty"`
	Limit   corev1.ResourceList `json:"limit"`
}
type ModelServiceInfo struct {
	Name                  string `json:"name"`
	DisplayName           string `json:"displayName"`
	AmlPipelineConfigName string `json:"amlPipelineConfigName,omitempty"`
	User                  string `json:"user"`
}

type DeployInfo struct {
	User            string                  `json:"user"`
	Port            Port                    `json:"port,omitempty"`
	Timeout         string                  `json:"timeout,omitempty"`
	DestNameSpace   string                  `json:"destNameSpace"`
	ServiceType     ModelServiceType        `json:"serviceType"`
	Resource        ModelDeploymentResource `json:"resource"`
	Replica         int32                   `json:"replica,omitempty"`
	Args            []string                `json:"args,omitempty"`
	Command         []string                `json:"command,omitempty"`
	CustomLabels    map[string]string       `json:"labels,omitempty"`
	CustomEnvParams map[string]string       `json:"env_params,omitempty"`
	CustomService   CustomService           `json:"customService,omitempty"`
}
type Port struct {
	Grpc    int32 `json:"grpc"`
	Restful int32 `json:"restful"`
	Swagger int32 `json:"swagger"`
}

type CustomService struct {
	Type            string          `json:"type"`
	ClusterServices []ClusterIpInfo `json:"clusterServices,omitempty"`
}
type ClusterIpInfo struct {
	ServiceName string `json:"serviceName"`
	Protocol    string `json:"protocol"`
	SourcePort  int32  `json:"sourcePort"`
	TargetPort  int32  `json:"targetPort"`
}

// ModelDeploymentSpec defines the desired state of ModelDeployment
type ModelDeploymentSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Source          ModelSource         `json:"source,omitempty"`
	ImageBuild      ImageBuildInfo      `json:"imageBuild,omitempty"`
	ImageRepository ImageRepositoryInfo `json:"imageRepository"`
	ModelInfo       ModelInfo           `json:"model"`
	ModelService    ModelServiceInfo    `json:"modelService"`
	DeployInfo      DeployInfo          `json:"deployInfo"`
}

// ModelDeploymentStatus defines the observed state of ModelDeployment
type ModelDeploymentStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	State                 ModelDeploymentState  `json:"state"`
	Reason                string                `json:"reason,omitempty"`
	Message               string                `json:"message,omitempty"`
	ManagedResourceResult ManagedResourceResult `json:"managedResource,omitempty"`
	//AssemblyState         string                `json:"assemblyState,omitempty"`
	PipelineInfo PipelineInfo `json:"pipelineInfo,omitempty"`
	Apis         []ServiceApi `json:"apis,omitempty"`
}
type ServiceApi struct {
	Name string `json:"name"`
	Kind string `json:"kind"`
	Url  string `json:"url"`
	Verb string `json:"verb"`
	Desc string `json:"desc"`
}
type ManagedResourceResult struct {
	DeploymentResult DeploymentResult `json:"deployment,omitempty"`
	ServiceResult    ServiceResult    `json:"service,omitempty"`
}
type DeploymentResult struct {
	RuntimeStateResult
	ReadyReplicas int32            `json:"readyReplicas,omitempty"`
	PodStatus     corev1.PodStatus `json:"podStatus"`
}
type ServiceResult struct {
	RuntimeStateResult
	GRpcURL    string `json:"grpcURL,omitempty"`
	RestfulURL string `json:"restfulURL,omitempty"`
	SwaggerURL string `json:"swaggerURL,omitempty"`
}
type RuntimeStateResult struct {
	Ready     bool               `json:"ready,omitempty"`
	Instances []ResourceInstance `json:"instances,omitempty"`
}

type ResourceInstance struct {
	ApiVersion string `json:"apiVersion"`
	Kind       string `json:"kind"`
	Name       string `json:"name"`
}
type PipelineInfo struct {
	Behavior      string           `json:"behavior"`
	LatestCreator string           `json:"latestCreator"`
	History       []RelationStatus `json:"history,omitempty"`
}
type RelationStatus struct {
	Name   string `json:"name"`
	Exists int    `json:"exists"` // 1 有效 ,0 失效
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ModelDeployment is the Schema for the modeldeploys API
// +k8s:openapi-gen=true
// +kubebuilder:resource:shortName=modeldeploy
// +kubebuilder:printcolumn:name="ModelService",type=string,JSONPath=.spec.modelService.name
// +kubebuilder:printcolumn:name="Framework",type=string,JSONPath=.spec.model.framework
// +kubebuilder:printcolumn:name="State",type=string,JSONPath=.status.state
type ModelDeployment struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ModelDeploymentSpec   `json:"spec,omitempty"`
	Status ModelDeploymentStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ModelDeploymentList contains a list of ModelDeployment
type ModelDeploymentList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ModelDeployment `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ModelDeployment{}, &ModelDeploymentList{})
}
