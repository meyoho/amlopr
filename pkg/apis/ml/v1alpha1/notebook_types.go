/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.
type FrameworkType string

const (
	Tensorflow FrameworkType = "tensorflow"
	Pytorch    FrameworkType = "pytorch"
)

type Framework struct {
	Type                  FrameworkType `json:"type"`
	Version               string        `json:"version"`
	Image                 string        `json:"image,omitempty"`
	ImagePullSecret       string        `json:"imagePullSecret,omitempty"`
	OriginSecretNamespace string        `json:"originSecretNamespace,omitempty"`
	OriginSecretName      string        `json:"originSecretName,omitempty"`
}

type NotebookPVC struct {
	ClaimName string `json:"claimName"`
	MountPath string `json:"mountPath"`
	ReadOnly  bool   `json:"readOnly"`
	SubPath   string `json:"subPath"`
}

// NoteBookSpec defines the desired state of NoteBook
type NoteBookSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	User      string                   `json:"user"`
	Framework Framework                `json:"framework"`
	Quota     corev1.ResourceQuotaSpec `json:"quota"`
	// Storage
	Pvc []NotebookPVC `json:"pvc,omitempty"`
}

// NoteBookStatus defines the observed state of NoteBook
type NoteBookStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	AmlBaseStatus

	//SVC,Pod status
	ServiceAspect ServiceAspect `json:"service,omitempty"`
	PodAspect     PodAspect     `json:"pod,omitempty"`
}

type ServiceAspect struct {
	Ready  bool                 `json:"ready,omitempty"`
	Spec   corev1.ServiceSpec   `json:"spec,omitempty"`
	Status corev1.ServiceStatus `json:"status,omitempty"`
}

type PodAspect struct {
	Ready  bool             `json:"ready,omitempty"`
	Spec   corev1.PodSpec   `json:"spec,omitempty"`
	Status corev1.PodStatus `json:"status,omitempty"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// NoteBook is the Schema for the notebooks API
// +k8s:openapi-gen=true
// +kubebuilder:printcolumn:name="Framework",type=string,JSONPath=.spec.framework.type
// +kubebuilder:printcolumn:name="Phase",type=string,JSONPath=.status.phase
// +kubebuilder:printcolumn:name="State",type=string,JSONPath=.status.state
type NoteBook struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   NoteBookSpec   `json:"spec,omitempty"`
	Status NoteBookStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// NoteBookList contains a list of NoteBook
type NoteBookList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []NoteBook `json:"items"`
}

func init() {
	SchemeBuilder.Register(&NoteBook{}, &NoteBookList{})
}
