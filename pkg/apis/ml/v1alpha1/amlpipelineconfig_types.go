/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// AMLPipelineConfigSpec defines the desired state of AMLPipelineConfig
type AMLPipelineConfigSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	//JenkinsBinding      devops.LocalObjectReference `json:"jenkinsBinding,omitempty"`
	RunPolicy           devops.PipelineRunPolicy `json:"runPolicy"`
	AMLPipelineTemplate LocalObjectReference     `json:"amlPipelineTemplate"`
	Framework           string                   `json:"framework,omitempty"`
	DisplayName         string                   `json:"displayName,omitempty"`
	User                string                   `json:"user"`

	Arguments  []devops.PipelineTemplateArgumentGroup `json:"arguments,omitempty"`
	Parameters []devops.PipelineTemplateArgumentGroup `json:"parameters,omitempty"`
}

// AMLPipelineConfigStatus defines the observed state of AMLPipelineConfig
type AMLPipelineConfigStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	AmlBaseStatus
}

type AMLPipelineConfigState string

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// AMLPipelineConfig is the Schema for the amlpipelineconfigs API
// +k8s:openapi-gen=true
type AMLPipelineConfig struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   AMLPipelineConfigSpec   `json:"spec,omitempty"`
	Status AMLPipelineConfigStatus `json:"status,omitempty"`
}

var AMLPipelineConfigStateEnum = struct {
	Creating BaseState
	Updating BaseState
	Ready    BaseState
	Error    BaseState
	Deleting BaseState
}{"", "Updating", "Ready", "Error", "Deleting"}

var AMLPipelineConfigPhaseEnum = struct {
	Creating BaseStage
	Updating BaseStage
	Ready    BaseStage
	Deleting BaseStage
}{"", "Updating", "Ready", "Deleting"}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// AMLPipelineConfigList contains a list of AMLPipelineConfig
type AMLPipelineConfigList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []AMLPipelineConfig `json:"items"`
}

func init() {
	SchemeBuilder.Register(&AMLPipelineConfig{}, &AMLPipelineConfigList{})
}
