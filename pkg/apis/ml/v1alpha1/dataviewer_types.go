/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// DataViewerSpec defines the desired state of DataViewer
type DataViewerSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Type Type `json:"type"`
	DataManageSpec
	DataPath        string             `json:"data_path"`
	PodTemplateSpec v1.PodTemplateSpec `json:"podTemplateSpec"`
}

type Type string

const (
	DataManager Type = "DataManager"
	Tensorboard Type = "Tensorboard"
)

// DataViewerStatus defines the observed state of DataViewer
type DataViewerStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	URL string `json:"url,omitempty"`
	AmlBaseStatus
}

type DataManageSpec struct {
	BaseUrl string `json:"baseUrl"`
	Pvc     []PVC  `json:"pvc"`
}

var DataViewerStateEnum = struct {
	Pending BaseState
	Created BaseState
	Running BaseState
	Stopped BaseState
	Error   BaseState
}{"Pending", "Created", "Running", "Stopped", "Error"}

var DataViewerPhaseEnum = struct {
	Running BaseStage
	Stopped BaseStage
}{"Running", "Stopped"}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// DataViewer is the Schema for the dataviewers API
// +k8s:openapi-gen=true
type DataViewer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DataViewerSpec   `json:"spec,omitempty"`
	Status DataViewerStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// DataViewerList contains a list of DataViewer
type DataViewerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DataViewer `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DataViewer{}, &DataViewerList{})
}
