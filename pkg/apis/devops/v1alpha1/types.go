package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
)

func init() {
	SchemeBuilder.Register(&devops.PipelineConfig{}, &devops.PipelineConfigList{})
	SchemeBuilder.Register(&devops.ClusterPipelineTemplate{}, &devops.ClusterPipelineTemplateList{})
	SchemeBuilder.Register(&devops.ClusterPipelineTaskTemplate{}, &devops.ClusterPipelineTaskTemplateList{})
	SchemeBuilder.Register(&devops.PipelineTaskTemplate{}, &devops.PipelineTaskTemplateList{})
	SchemeBuilder.Register(&devops.PipelineTemplate{}, &devops.PipelineTemplateList{})
	SchemeBuilder.Register(&devops.Pipeline{}, &devops.PipelineList{})
	SchemeBuilder.Register(&devops.JenkinsBinding{}, &devops.JenkinsBindingList{})
}
