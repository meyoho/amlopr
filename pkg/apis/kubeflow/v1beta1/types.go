package v1beta1

import (
	"github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta1"
)

func init() {
	SchemeBuilder.Register(&v1beta1.TFJob{}, &v1beta1.TFJobList{})
}
