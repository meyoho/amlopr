package v1beta1

import (
	"github.com/kubeflow/pytorch-operator/pkg/apis/pytorch/v1beta2"
)

func init() {
	SchemeBuilder.Register(&v1beta2.PyTorchJob{}, &v1beta2.PyTorchJobList{})
}
