/*
Copyright The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	v1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeModelDeployments implements ModelDeploymentInterface
type FakeModelDeployments struct {
	Fake *FakeMlV1alpha1
	ns   string
}

var modeldeploymentsResource = schema.GroupVersionResource{Group: "ml.alauda.io", Version: "v1alpha1", Resource: "modeldeployments"}

var modeldeploymentsKind = schema.GroupVersionKind{Group: "ml.alauda.io", Version: "v1alpha1", Kind: "ModelDeployment"}

// Get takes name of the modelDeployment, and returns the corresponding modelDeployment object, and an error if there is any.
func (c *FakeModelDeployments) Get(name string, options v1.GetOptions) (result *v1alpha1.ModelDeployment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(modeldeploymentsResource, c.ns, name), &v1alpha1.ModelDeployment{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.ModelDeployment), err
}

// List takes label and field selectors, and returns the list of ModelDeployments that match those selectors.
func (c *FakeModelDeployments) List(opts v1.ListOptions) (result *v1alpha1.ModelDeploymentList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(modeldeploymentsResource, modeldeploymentsKind, c.ns, opts), &v1alpha1.ModelDeploymentList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &v1alpha1.ModelDeploymentList{ListMeta: obj.(*v1alpha1.ModelDeploymentList).ListMeta}
	for _, item := range obj.(*v1alpha1.ModelDeploymentList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested modelDeployments.
func (c *FakeModelDeployments) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(modeldeploymentsResource, c.ns, opts))

}

// Create takes the representation of a modelDeployment and creates it.  Returns the server's representation of the modelDeployment, and an error, if there is any.
func (c *FakeModelDeployments) Create(modelDeployment *v1alpha1.ModelDeployment) (result *v1alpha1.ModelDeployment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(modeldeploymentsResource, c.ns, modelDeployment), &v1alpha1.ModelDeployment{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.ModelDeployment), err
}

// Update takes the representation of a modelDeployment and updates it. Returns the server's representation of the modelDeployment, and an error, if there is any.
func (c *FakeModelDeployments) Update(modelDeployment *v1alpha1.ModelDeployment) (result *v1alpha1.ModelDeployment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(modeldeploymentsResource, c.ns, modelDeployment), &v1alpha1.ModelDeployment{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.ModelDeployment), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeModelDeployments) UpdateStatus(modelDeployment *v1alpha1.ModelDeployment) (*v1alpha1.ModelDeployment, error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateSubresourceAction(modeldeploymentsResource, "status", c.ns, modelDeployment), &v1alpha1.ModelDeployment{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.ModelDeployment), err
}

// Delete takes name of the modelDeployment and deletes it. Returns an error if one occurs.
func (c *FakeModelDeployments) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(modeldeploymentsResource, c.ns, name), &v1alpha1.ModelDeployment{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeModelDeployments) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(modeldeploymentsResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &v1alpha1.ModelDeploymentList{})
	return err
}

// Patch applies the patch and returns the patched modelDeployment.
func (c *FakeModelDeployments) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.ModelDeployment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(modeldeploymentsResource, c.ns, name, pt, data, subresources...), &v1alpha1.ModelDeployment{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.ModelDeployment), err
}
