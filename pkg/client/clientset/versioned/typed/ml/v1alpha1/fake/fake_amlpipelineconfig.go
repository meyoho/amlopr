/*
Copyright The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	v1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeAMLPipelineConfigs implements AMLPipelineConfigInterface
type FakeAMLPipelineConfigs struct {
	Fake *FakeMlV1alpha1
	ns   string
}

var amlpipelineconfigsResource = schema.GroupVersionResource{Group: "ml.alauda.io", Version: "v1alpha1", Resource: "amlpipelineconfigs"}

var amlpipelineconfigsKind = schema.GroupVersionKind{Group: "ml.alauda.io", Version: "v1alpha1", Kind: "AMLPipelineConfig"}

// Get takes name of the aMLPipelineConfig, and returns the corresponding aMLPipelineConfig object, and an error if there is any.
func (c *FakeAMLPipelineConfigs) Get(name string, options v1.GetOptions) (result *v1alpha1.AMLPipelineConfig, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(amlpipelineconfigsResource, c.ns, name), &v1alpha1.AMLPipelineConfig{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.AMLPipelineConfig), err
}

// List takes label and field selectors, and returns the list of AMLPipelineConfigs that match those selectors.
func (c *FakeAMLPipelineConfigs) List(opts v1.ListOptions) (result *v1alpha1.AMLPipelineConfigList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(amlpipelineconfigsResource, amlpipelineconfigsKind, c.ns, opts), &v1alpha1.AMLPipelineConfigList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &v1alpha1.AMLPipelineConfigList{ListMeta: obj.(*v1alpha1.AMLPipelineConfigList).ListMeta}
	for _, item := range obj.(*v1alpha1.AMLPipelineConfigList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested aMLPipelineConfigs.
func (c *FakeAMLPipelineConfigs) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(amlpipelineconfigsResource, c.ns, opts))

}

// Create takes the representation of a aMLPipelineConfig and creates it.  Returns the server's representation of the aMLPipelineConfig, and an error, if there is any.
func (c *FakeAMLPipelineConfigs) Create(aMLPipelineConfig *v1alpha1.AMLPipelineConfig) (result *v1alpha1.AMLPipelineConfig, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(amlpipelineconfigsResource, c.ns, aMLPipelineConfig), &v1alpha1.AMLPipelineConfig{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.AMLPipelineConfig), err
}

// Update takes the representation of a aMLPipelineConfig and updates it. Returns the server's representation of the aMLPipelineConfig, and an error, if there is any.
func (c *FakeAMLPipelineConfigs) Update(aMLPipelineConfig *v1alpha1.AMLPipelineConfig) (result *v1alpha1.AMLPipelineConfig, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(amlpipelineconfigsResource, c.ns, aMLPipelineConfig), &v1alpha1.AMLPipelineConfig{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.AMLPipelineConfig), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeAMLPipelineConfigs) UpdateStatus(aMLPipelineConfig *v1alpha1.AMLPipelineConfig) (*v1alpha1.AMLPipelineConfig, error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateSubresourceAction(amlpipelineconfigsResource, "status", c.ns, aMLPipelineConfig), &v1alpha1.AMLPipelineConfig{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.AMLPipelineConfig), err
}

// Delete takes name of the aMLPipelineConfig and deletes it. Returns an error if one occurs.
func (c *FakeAMLPipelineConfigs) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(amlpipelineconfigsResource, c.ns, name), &v1alpha1.AMLPipelineConfig{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeAMLPipelineConfigs) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(amlpipelineconfigsResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &v1alpha1.AMLPipelineConfigList{})
	return err
}

// Patch applies the patch and returns the patched aMLPipelineConfig.
func (c *FakeAMLPipelineConfigs) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.AMLPipelineConfig, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(amlpipelineconfigsResource, c.ns, name, pt, data, subresources...), &v1alpha1.AMLPipelineConfig{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.AMLPipelineConfig), err
}
