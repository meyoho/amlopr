package webhook

import (
	"alauda.io/amlopr/pkg/webhook/amlpipeline"
	"alauda.io/amlopr/pkg/webhook/amlpipelineconfig"
	"alauda.io/amlopr/pkg/webhook/modeldeployment"
	"alauda.io/amlopr/pkg/webhook/pod"
	"alauda.io/amlopr/pkg/webhook/projectbinding"
)

func init() {
	//有风险
	//RegisterWebhook(amlpipeline.NewDeleteAnnotator)
	RegisterGlobalWebhook(amlpipeline.NewAnnotator)
	RegisterGlobalWebhook(amlpipelineconfig.NewAnnotator)
	RegisterWebhook(projectbinding.NewAnnotator)
	RegisterWebhook(pod.NewAnnotator)
	RegisterWebhook(modeldeployment.NewAnnotator)

	AddToManagerFuncs = append(AddToManagerFuncs, Add)
	AddToGlobalManagerFuncs = append(AddToGlobalManagerFuncs, AddGlobal)
}
