package webhook

import (
	"alauda.io/amlopr/pkg/apis/base"
	"alauda.io/amlopr/pkg/config"
	config2 "alauda.io/amlopr/pkg/config"
	"alauda.io/amlopr/pkg/webhook/utils"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

func newServer(mgr manager.Manager, mode base.Mode) (*webhook.Server, error) {
	webhookName := "amlopr-webhook"
	if mode == base.Business {
		webhookName = "aml-bus-opr-webhook"
	}
	webHookOpts := webhook.ServerOptions{
		Port:    8443,
		CertDir: "/tmp/cert",
		BootstrapOptions: &webhook.BootstrapOptions{
			MutatingWebhookConfigName:   webhookName + "-mutate",
			ValidatingWebhookConfigName: webhookName + "-validate",
			Service:                     getService(mode, webhookName),
			Secret:                      &types.NamespacedName{Namespace: config.AlaudaNameSpace(), Name: webhookName + "-server-secret"},
		},
	}
	if config.IsDevMode() {
		webHookOpts.Secret = nil
		webHookOpts.Service = &webhook.Service{Namespace: config.AlaudaNameSpace(), Name: webhookName}
	}
	return webhook.NewServer(webhookName, mgr, webHookOpts)
}

func getService(mode base.Mode, webhookName string) *webhook.Service {
	return &webhook.Service{
		Namespace: config.AlaudaNameSpace(),
		Name:      webhookName,
		Selectors: getLabels(mode),
	}
}

func getLabels(mode base.Mode) map[string]string {
	var value string
	switch mode {
	//set in charts
	case base.Global:
		value = "global"
	case base.Business:
		value = "business"
	}
	return map[string]string{"mode": value, config2.GetDomain() + "/aml-controller": "true"}
}

var webhookFuncs []func(manager.Manager) (webhook.Webhook, error)

func RegisterWebhook(f func(manager.Manager) (webhook.Webhook, error)) {
	webhookFuncs = append(webhookFuncs, f)
}

func Add(mgr manager.Manager, mode base.Mode) error {
	server, err := newServer(mgr, mode)
	if err != nil {
		return err
	}

	return utils.RegisterWebhooksToServer(mgr, server, webhookFuncs)
}

var webhookGlobalFuncs []func(manager.Manager) (webhook.Webhook, error)

func RegisterGlobalWebhook(f func(manager.Manager) (webhook.Webhook, error)) {
	webhookGlobalFuncs = append(webhookGlobalFuncs, f)
}

func AddGlobal(mgr manager.Manager, mode base.Mode) error {
	server, err := newServer(mgr, mode)
	if err != nil {
		return err
	}

	return utils.RegisterWebhooksToServer(mgr, server, webhookGlobalFuncs)
}
