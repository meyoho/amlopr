package amlpipeline

import (
	"alauda.io/amlopr/pkg/apis/base"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
	"context"
	"fmt"
	"github.com/golang/glog"
	"github.com/juju/errors"
	admissionregistration "k8s.io/api/admissionregistration/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"net/http"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/builder"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
	"strconv"
)

type amlPipelineAnnotator struct {
	client  client.Client
	decoder types.Decoder
	scheme  *runtime.Scheme
}

func NewAnnotator(mgr manager.Manager) (webhook.Webhook, error) {
	wh, err := builder.NewWebhookBuilder().Mutating().
		Operations(admissionregistration.Create).
		FailurePolicy(admissionregistration.Fail).
		ForType(&v1alpha1.AMLPipeline{}).
		Handlers(&amlPipelineAnnotator{scheme: mgr.GetScheme()}).
		WithManager(mgr).
		Build()
	if err != nil {
		glog.V(5).Info(errors.Trace(err))
	}
	return wh, err
}

var _ admission.Handler = &amlPipelineAnnotator{}

func (a *amlPipelineAnnotator) Handle(ctx context.Context, req types.Request) types.Response {
	amlPipeline := &v1alpha1.AMLPipeline{}
	err := a.decoder.Decode(req, amlPipeline)
	if err != nil {
		return admission.ErrorResponse(http.StatusBadRequest, err)
	}

	copyPipeline := amlPipeline.DeepCopy()
	err = a.mutateAMLPipelineFn(ctx, copyPipeline)
	if err != nil {
		return admission.ErrorResponse(http.StatusInternalServerError, err)
	}

	return admission.PatchResponse(amlPipeline, copyPipeline)

}

func (a *amlPipelineAnnotator) mutateAMLPipelineFn(ctx context.Context, pipeline *v1alpha1.AMLPipeline) error {
	namespacedName := utils.NewNamespacedName(pipeline.Spec.AMLPipelineConfig.Name, pipeline.Namespace)
	amlPipelineConfig := &v1alpha1.AMLPipelineConfig{}
	err := a.client.Get(ctx, namespacedName, amlPipelineConfig)
	if err != nil {
		return err
	}

	nextNumber := a.GetNextNumber(amlPipelineConfig)
	numberStr := strconv.Itoa(nextNumber)

	err = a.updateConfigLastNumber(numberStr, amlPipelineConfig, err, ctx)
	if err != nil {
		return err
	}

	pipeline.Finalizers = append(pipeline.Finalizers, base.PipelineFinalizer)

	pipeline.Annotations = common.NewAmlAnnotation().AddMap(pipeline.Annotations).
		AddForce(v1alpha1.AnnotationsKeyPipelineNumber(), numberStr).
		Get()

	pipeline.Labels = common.NewAmlLabel(pipeline.Labels).
		AddForce(v1alpha1.LabelAMLPipelineConfig, pipeline.Spec.AMLPipelineConfig.Name).
		Get()
	pipeline.Name = fmt.Sprintf("%s-%s", pipeline.Spec.AMLPipelineConfig.Name, numberStr)
	//这边修改状态，kubectl报错。Internal error occurred: Internal error occurred: jsonpatch replace operation does not apply: doc is missing path: /status/phase
	//pipeline.Status.State = v1alpha1.AMLPipelineStateEnum.Creating
	//pipeline.Status.Phase = v1alpha1.AMLPipelinePhaseEnum.Creating

	controllerutil.SetControllerReference(amlPipelineConfig, pipeline, a.scheme)
	return nil
}

func (a *amlPipelineAnnotator) updateConfigLastNumber(nextNumber string, amlPipelineConfig *v1alpha1.AMLPipelineConfig, err error, ctx context.Context) error {
	amlPipelineConfig.Annotations = common.NewAmlAnnotation().AddMap(amlPipelineConfig.Annotations).
		AddForce(v1alpha1.AnnotationsKeyPipelineLastNumber(), nextNumber).Get()
	err = a.client.Update(ctx, amlPipelineConfig)
	return err
}

func (a *amlPipelineAnnotator) GetNextNumber(amlPipelineConfig *v1alpha1.AMLPipelineConfig) int {
	value := 0
	if len(amlPipelineConfig.Annotations) > 0 && amlPipelineConfig.Annotations[v1alpha1.AnnotationsKeyPipelineLastNumber()] != "" {
		glog.V(5).Infof("AMLPipelineConfig: %s/%s - last number: %s", amlPipelineConfig.GetNamespace(), amlPipelineConfig.GetName(), amlPipelineConfig.Annotations[v1alpha1.AnnotationsKeyPipelineLastNumber()])
		value, _ = strconv.Atoi(amlPipelineConfig.Annotations[v1alpha1.AnnotationsKeyPipelineLastNumber()])
	}
	value++
	return value
}

// podAnnotator implements inject.Client.
var _ inject.Client = &amlPipelineAnnotator{}

// InjectClient injects the client into the podAnnotator
func (a *amlPipelineAnnotator) InjectClient(c client.Client) error {
	a.client = c
	return nil
}

// podAnnotator implements inject.Decoder.
var _ inject.Decoder = &amlPipelineAnnotator{}

// InjectDecoder injects the decoder into the podAnnotator
func (a *amlPipelineAnnotator) InjectDecoder(d types.Decoder) error {
	a.decoder = d
	return nil
}
