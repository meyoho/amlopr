package amlpipeline

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/amlpipeline"
	"context"
	"fmt"
	"github.com/golang/glog"
	"github.com/juju/errors"
	admissionregistration "k8s.io/api/admissionregistration/v1beta1"
	"net/http"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/builder"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
)

type amlPipelineDeleteAnnotator struct {
	client  client.Client
	decoder types.Decoder
}

func NewDeleteAnnotator(mgr manager.Manager) (webhook.Webhook, error) {
	wh, err := builder.NewWebhookBuilder().Validating().
		Operations(admissionregistration.Delete).
		ForType(&v1alpha1.AMLPipeline{}).
		Handlers(&amlPipelineDeleteAnnotator{}).
		WithManager(mgr).
		Build()
	if err != nil {
		glog.V(5).Info(errors.Trace(err))
	}
	return wh, err
}

var _ admission.Handler = &amlPipelineDeleteAnnotator{}

func (a *amlPipelineDeleteAnnotator) Handle(ctx context.Context, req types.Request) types.Response {
	amlPipeline := &v1alpha1.AMLPipeline{}
	err := a.decoder.Decode(req, amlPipeline)
	if err != nil {
		return admission.ErrorResponse(http.StatusBadRequest, err)
	}

	pipelineList, err := amlpipeline.GetDevopsPipelineListByLabel(a.client, amlPipeline.Name, amlPipeline.Namespace)
	if err != nil {
		return admission.ErrorResponse(http.StatusBadRequest, err)
	}

	if len(pipelineList.Items) > 1 {
		return admission.ErrorResponse(http.StatusBadRequest, fmt.Errorf("uncorrect pipeline status"))
	}
	for _, pipeline := range pipelineList.Items {
		err := a.client.Delete(ctx, &pipeline)
		glog.V(5).Infof("delete pipeline %s in %s ", amlPipeline.Name, amlPipeline.Namespace)
		if err != nil {
			return admission.ErrorResponse(http.StatusBadRequest, err)
		}
	}

	return admission.ValidationResponse(true, "")

}

// podAnnotator implements inject.Client.
var _ inject.Client = &amlPipelineAnnotator{}

// InjectClient injects the client into the podAnnotator
func (a *amlPipelineDeleteAnnotator) InjectClient(c client.Client) error {
	a.client = c
	return nil
}

// podAnnotator implements inject.Decoder.
var _ inject.Decoder = &amlPipelineAnnotator{}

// InjectDecoder injects the decoder into the podAnnotator
func (a *amlPipelineDeleteAnnotator) InjectDecoder(d types.Decoder) error {
	a.decoder = d
	return nil
}
