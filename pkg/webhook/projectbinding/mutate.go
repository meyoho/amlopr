package projectbinding

import (
	"alauda.io/amlopr/pkg/apis/base"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/utils"
	"context"
	"fmt"
	"github.com/golang/glog"
	"github.com/juju/errors"
	admissionregistration "k8s.io/api/admissionregistration/v1beta1"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"net/http"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/builder"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
	"strconv"
)

type Annotator struct {
	client  client.Client
	decoder types.Decoder
	scheme  *runtime.Scheme
}

func NewAnnotator(mgr manager.Manager) (webhook.Webhook, error) {
	wh, err := builder.NewWebhookBuilder().Mutating().
		Operations(admissionregistration.Create, admissionregistration.Update).
		ForType(&v1alpha1.MLBinding{}).
		FailurePolicy(admissionregistration.Fail).
		Handlers(&Annotator{scheme: mgr.GetScheme()}).
		WithManager(mgr).
		Build()
	if err != nil {
		glog.V(5).Info(errors.ErrorStack(err))
	}
	return wh, err
}

var _ admission.Handler = &Annotator{}

func (a *Annotator) Handle(ctx context.Context, req types.Request) types.Response {
	projectBinding := &v1alpha1.MLBinding{}
	err := a.decoder.Decode(req, projectBinding)
	if err != nil {
		return admission.ErrorResponse(http.StatusBadRequest, err)
	}

	copyBinding := projectBinding.DeepCopy()
	err = a.mutateNamespaceFn(ctx, copyBinding)
	if err != nil {
		return admission.ErrorResponse(http.StatusInternalServerError, err)
	}
	err = a.validate(ctx, copyBinding)
	if err != nil {
		return admission.ErrorResponse(http.StatusInternalServerError, err)
	}
	return admission.ValidationResponse(true, "")

}

const pvcNumber = "pvc.num.ml"

func (a *Annotator) mutateNamespaceFn(ctx context.Context, projectBinding *v1alpha1.MLBinding) error {
	k8sNamespace := &v1.Namespace{}
	err := a.client.Get(ctx, utils.NewNamespacedName(projectBinding.Namespace, ""), k8sNamespace)
	if err != nil {
		glog.V(5).Info(errors.ErrorStack(err))
		return errors.Trace(err)
	}
	if k8sNamespace.Labels == nil {
		k8sNamespace.Labels = make(map[string]string)
	}
	k8sNamespace.Labels[base.MLLabelAppKey] = base.MLLabelAppValue

	if k8sNamespace.Annotations == nil {
		k8sNamespace.Annotations = make(map[string]string)
	}
	k8sNamespace.Annotations[pvcNumber] = strconv.Itoa(len(projectBinding.Spec.Pvc))
	return a.client.Update(ctx, k8sNamespace)
}

func (a *Annotator) validate(ctx context.Context, projectBinding *v1alpha1.MLBinding) error {
	bindingList := &v1alpha1.MLBindingList{}
	err := a.client.List(ctx, &client.ListOptions{Namespace: projectBinding.Namespace}, bindingList)
	if err != nil {
		glog.V(5).Info(errors.ErrorStack(err))
		return errors.Trace(err)
	}
	if len(bindingList.Items) > 0 && bindingList.Items[0].Name != projectBinding.Name {
		return fmt.Errorf("one namespace only one mlbinding")
	}
	return nil
}

// podAnnotator implements inject.Client.
var _ inject.Client = &Annotator{}

// InjectClient injects the client into the podAnnotator
func (a *Annotator) InjectClient(c client.Client) error {
	a.client = c
	return nil
}

// podAnnotator implements inject.Decoder.
var _ inject.Decoder = &Annotator{}

// InjectDecoder injects the decoder into the podAnnotator
func (a *Annotator) InjectDecoder(d types.Decoder) error {
	a.decoder = d
	return nil
}
