package utils

import (
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

func RegisterWebhooksToServer(
	mgr manager.Manager,
	server *webhook.Server,
	webhookFactories []func(manager.Manager) (webhook.Webhook, error)) error {
	var whs []webhook.Webhook
	for _, f := range webhookFactories {
		if wh, err := f(mgr); err == nil {
			whs = append(whs, wh)
		} else {
			return err
		}
	}
	return server.Register(whs...)
}
