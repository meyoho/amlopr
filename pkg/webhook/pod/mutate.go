package pod

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"context"
	"github.com/golang/glog"
	"github.com/juju/errors"
	admissionregistration "k8s.io/api/admissionregistration/v1beta1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"net/http"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/builder"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
	"strings"
)

type podAnnotator struct {
	client  client.Client
	decoder types.Decoder
	scheme  *runtime.Scheme
}

func NewAnnotator(mgr manager.Manager) (webhook.Webhook, error) {
	wh, err := builder.NewWebhookBuilder().Mutating().
		Operations(admissionregistration.Create, admissionregistration.Update).
		ForType(&v1.Pod{}).
		Handlers(&podAnnotator{scheme: mgr.GetScheme()}).
		WithManager(mgr).
		Build()
	if err != nil {
		glog.V(5).Info(errors.Trace(err))
	}
	return wh, err
}

var _ admission.Handler = &podAnnotator{}

func (a *podAnnotator) Handle(ctx context.Context, req types.Request) types.Response {
	pod := &v1.Pod{}
	err := a.decoder.Decode(req, pod)
	if err != nil {
		return admission.ErrorResponse(http.StatusBadRequest, err)
	}
	if !checkMutateRequired(&pod.ObjectMeta) {
		return skipMutate()
	}
	copyPod := pod.DeepCopy()
	hasChanged := a.mutatePodFn(ctx, copyPod)
	if hasChanged {
		return admission.PatchResponse(pod, copyPod)
	}
	return skipMutate()
}

func skipMutate() types.Response {
	return admission.ValidationResponse(true, "")
}

func checkMutateRequired(metadata *metav1.ObjectMeta) bool {
	annos := metadata.GetAnnotations()
	if annos == nil {
		annos = map[string]string{}
	}
	needMutate := annos[v1alpha1.AnnotationsKeyNvidiaGpuVisibilityEnhancement()]
	var required bool
	switch strings.ToLower(needMutate) {
	default:
		required = false
	case "y", "yes", "true", "on":
		required = true
	}
	return required
}

func (a *podAnnotator) mutatePodFn(ctx context.Context, pod *v1.Pod) bool {
	changed := false
	for i := range pod.Spec.Containers {
		container := &pod.Spec.Containers[i]
		if v, ok := container.Resources.Requests[v1alpha1.NvidiaGPUResourceType]; ok && v == resource.MustParse("0") {
			exists := false
			for _, pair := range container.Env {
				if pair.Name == v1alpha1.EnvKeyNvidiaVisibleDevices {
					pair.Value, changed, exists = "none", true, true
					break
				}
			}
			if !exists {
				container.Env = append(container.Env, v1.EnvVar{Name: v1alpha1.EnvKeyNvidiaVisibleDevices, Value: "none"})
				changed = true
			}
		}
	}
	return changed
}

// podAnnotator implements inject.Client.
var _ inject.Client = &podAnnotator{}

// InjectClient injects the client into the podAnnotator
func (a *podAnnotator) InjectClient(c client.Client) error {
	a.client = c
	return nil
}

// podAnnotator implements inject.Decoder.
var _ inject.Decoder = &podAnnotator{}

// InjectDecoder injects the decoder into the podAnnotator
func (a *podAnnotator) InjectDecoder(d types.Decoder) error {
	a.decoder = d
	return nil
}
