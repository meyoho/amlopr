package modeldeployment

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"context"
	"github.com/golang/glog"
	"github.com/juju/errors"
	admissionregistration "k8s.io/api/admissionregistration/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"net/http"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/builder"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
)

type Annotator struct {
	client  client.Client
	decoder types.Decoder
	scheme  *runtime.Scheme
}

func NewAnnotator(mgr manager.Manager) (webhook.Webhook, error) {
	wh, err := builder.NewWebhookBuilder().Mutating().
		Operations(admissionregistration.Create).
		FailurePolicy(admissionregistration.Fail).
		ForType(&v1alpha1.ModelDeployment{}).
		Handlers(&Annotator{scheme: mgr.GetScheme()}).
		WithManager(mgr).
		Build()
	if err != nil {
		glog.V(5).Info(errors.Trace(err))
	}
	return wh, err
}

var _ admission.Handler = &Annotator{}

func (a *Annotator) Handle(ctx context.Context, req types.Request) types.Response {
	modelDeployment := &v1alpha1.ModelDeployment{}
	err := a.decoder.Decode(req, modelDeployment)
	if err != nil {
		return admission.ErrorResponse(http.StatusBadRequest, err)
	}

	copyModelDeployment := modelDeployment.DeepCopy()
	err = a.mutateModelDeploymentFn(ctx, copyModelDeployment)
	if err != nil {
		return admission.ErrorResponse(http.StatusInternalServerError, err)
	}
	return admission.PatchResponse(modelDeployment, copyModelDeployment)
}

func (a *Annotator) mutateModelDeploymentFn(ctx context.Context, modelDeployment *v1alpha1.ModelDeployment) error {
	modelDeployment.Finalizers = append(modelDeployment.Finalizers, v1alpha1.ModelServiceFinalizer)
	return nil
}

// podAnnotator implements inject.Client.
var _ inject.Client = &Annotator{}

// InjectClient injects the client into the podAnnotator
func (a *Annotator) InjectClient(c client.Client) error {
	a.client = c
	return nil
}

// podAnnotator implements inject.Decoder.
var _ inject.Decoder = &Annotator{}

// InjectDecoder injects the decoder into the podAnnotator
func (a *Annotator) InjectDecoder(d types.Decoder) error {
	a.decoder = d
	return nil
}
