package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"fmt"
	"testing"
)

func Test_ambassador(t *testing.T) {
	deploy := mlv1alpha1.ModelDeployment{}
	deploy.Name = "test111"
	deploy.Namespace = "aml-high-publish"
	deploy.Spec.ModelInfo.ModelName = "cat"
	deploy.Spec.ModelInfo.Version = "1"
	deploy.Spec.DeployInfo.Port.Restful = 8501
	deploy.Spec.DeployInfo.Port.Grpc = 8500
	deploy.Spec.DeployInfo.Port.Swagger = 8888

	amb := NewModelAmbassadorMgr(deploy)
	svcName := "test111"
	swaggerSvcName := "test111-swagger"
	fmt.Println(amb.GenSwaggerCfg(swaggerSvcName))
	fmt.Println("restful")
	fmt.Println(amb.GenRestfulCfg(svcName))
	fmt.Println("grpc")
	fmt.Println(amb.GenGRpcCfg(svcName))
}
