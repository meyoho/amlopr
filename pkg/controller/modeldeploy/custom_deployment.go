package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	appv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"strings"
)

type CustomDeployment struct {
	TensorFlowServingDeployment
	deploySetting *mlv1alpha1.ModelDeployment
}

func NewCustomDeployment(deploySetting *mlv1alpha1.ModelDeployment, SwaggerImageWithTag string) *CustomDeployment {
	var swaggerPort = deploySetting.Spec.DeployInfo.Port.Swagger
	var grpcPort = deploySetting.Spec.DeployInfo.Port.Grpc
	var restfulPort = deploySetting.Spec.DeployInfo.Port.Restful

	return &CustomDeployment{
		TensorFlowServingDeployment: TensorFlowServingDeployment{
			//for sidecar
			SwaggerImage: SwaggerImageWithTag,
			swaggerPort:  swaggerPort,
			restfulPort:  restfulPort,
			grpcPort:     grpcPort,
			modelName:    deploySetting.Spec.ModelInfo.ModelName,
		},
		deploySetting: deploySetting,
	}
}
func (c *CustomDeployment) AssembledSpecOnDemand(deploy *appv1.Deployment) {
	c.injectSpecsForDeployment(deploy)
	c.injectSwaggerSideCar(deploy)
}

func (c *CustomDeployment) injectSpecsForDeployment(deployment *appv1.Deployment) {
	// custom labels
	for k, v := range c.deploySetting.Spec.DeployInfo.CustomLabels {
		// just add labels to pod, if need adding to deployment must notice don't change application's labels,
		// or the deployment will be lost connection with application.
		if len(strings.TrimSpace(k)) == 0 {
			continue
		}
		deployment.Spec.Template.Labels[k] = v
	}

	// custom env parameters
	for k, v := range c.deploySetting.Spec.DeployInfo.CustomEnvParams {
		if len(strings.TrimSpace(k)) == 0 {
			continue
		}
		deployment.Spec.Template.Spec.Containers[0].Env = append(deployment.Spec.Template.Spec.Containers[0].Env, corev1.EnvVar{
			Name:  k,
			Value: v,
		})
	}

	// custom args
	for i, arg := range c.deploySetting.Spec.DeployInfo.Args {
		c.deploySetting.Spec.DeployInfo.Args[i] = strings.Trim(strings.TrimSpace(arg), "\"")
	}
	deployment.Spec.Template.Spec.Containers[0].Args = c.deploySetting.Spec.DeployInfo.Args

	// custom command
	for i, cmd := range c.deploySetting.Spec.DeployInfo.Command {
		c.deploySetting.Spec.DeployInfo.Command[i] = strings.TrimSpace(cmd)
	}
	deployment.Spec.Template.Spec.Containers[0].Command = c.deploySetting.Spec.DeployInfo.Command

	// tf ports
	grpcPortName := "grpc"
	restfulPortName := "restful"
	deployment.Spec.Template.Spec.Containers[0].Ports = []corev1.ContainerPort{
		{
			Name:          grpcPortName,
			ContainerPort: c.grpcPort,
			Protocol:      corev1.ProtocolTCP,
		},
		{
			Name:          restfulPortName,
			ContainerPort: c.restfulPort,
			Protocol:      corev1.ProtocolTCP,
		},
	}
	deployment.Spec.Template.Spec.Containers[0].LivenessProbe = &corev1.Probe{
		Handler: corev1.Handler{
			TCPSocket: &corev1.TCPSocketAction{
				Port: intstr.FromString(grpcPortName),
			},
		},
		InitialDelaySeconds: 5,
		PeriodSeconds:       10,
	}
	deployment.Spec.Template.Spec.Containers[0].ReadinessProbe = &corev1.Probe{
		Handler: corev1.Handler{
			TCPSocket: &corev1.TCPSocketAction{
				Port: intstr.FromString(grpcPortName),
			},
		},
		InitialDelaySeconds: 15,
		PeriodSeconds:       20,
	}
	// custom ports TODO
	deployment.Spec.Template.Spec.Containers[0].Ports = append(deployment.Spec.Template.Spec.Containers[0].Ports, c.getContainerPorts()...)

}
func (c *CustomDeployment) getContainerPorts() []corev1.ContainerPort {
	var ports []corev1.ContainerPort
	for _, clusterIp := range c.deploySetting.Spec.DeployInfo.CustomService.ClusterServices {
		if clusterIp.ServiceName == "" {
			continue
		}
		ports = append(ports, corev1.ContainerPort{
			Name:          clusterIp.ServiceName,
			ContainerPort: clusterIp.TargetPort,
			Protocol:      getProtocol(clusterIp.Protocol),
		})
	}
	return ports
}

func getProtocol(protocol string) corev1.Protocol {
	if strings.ToUpper(protocol) == string(corev1.ProtocolTCP) {
		return corev1.ProtocolTCP
	} else {
		return corev1.ProtocolUDP
	}
}
