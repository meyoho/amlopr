package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	appv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/apimachinery/pkg/types"
	"reflect"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"strings"
)

type ModelDeploymentMgr struct {
	ModelDeploy         *mlv1alpha1.ModelDeployment
	ContainerName       string
	IsNeedBuildImage    bool
	SwaggerImageWithTag string
	namespacedName      types.NamespacedName
	reconcile           *ReconcileModelDeploy
}

func NewDeploymentMgr(ins *mlv1alpha1.ModelDeployment, reconcile *ReconcileModelDeploy) *ModelDeploymentMgr {
	return &ModelDeploymentMgr{
		ModelDeploy:      ins,
		ContainerName:    "model",
		IsNeedBuildImage: ins.Spec.ImageBuild.IsNeed,
		//SwaggerImageWithTag: swaggerImage,
		namespacedName: types.NamespacedName{
			Namespace: ins.Namespace,
			Name:      getDeployName(ins),
		},
		reconcile: reconcile,
	}
}
func getDeployName(deploy *mlv1alpha1.ModelDeployment) string {
	return deploy.Name
}

func (mgr *ModelDeploymentMgr) NeedUpdate(from, to *appv1.Deployment) bool {
	var resEqual, imgEqual, replicaEqual bool
	replicaEqual = reflect.DeepEqual(from.Spec.Replicas, to.Spec.Replicas)
	if !replicaEqual {
		return true
	}
	if len(from.Spec.Template.Spec.Containers) > 0 {
		resEqual = reflect.DeepEqual(from.Spec.Template.Spec.Containers[0].Resources.Limits, to.Spec.Template.Spec.Containers[0].Resources.Limits)
		imgEqual = reflect.DeepEqual(from.Spec.Template.Spec.Containers[0].Image, to.Spec.Template.Spec.Containers[0].Image)
		if !resEqual || !imgEqual {
			return true
		}
	}
	return false
}

func (mgr *ModelDeploymentMgr) getGeneralDeployment() *appv1.Deployment {
	deployLabels := map[string]string{}
	podLabels := map[string]string{}

	annotations := common.NewAmlAnnotation().Create().Get()
	annotations[mlv1alpha1.AnnotationsKeyNvidiaGpuVisibilityEnhancement()] = "on"
	labelWithCreatorInfo := common.NewAmlLabel().
		Add(mlv1alpha1.LabelAMLComponentKey(), ComponentName).
		Add(common.GetLabelKeyForCreatorOf(LabelCreatorKey), mgr.ModelDeploy.GetName()).
		Get()
	for k, v := range labelWithCreatorInfo {
		deployLabels[k] = v // add creator for deploy
		podLabels[k] = v    // add creator for pod
	}

	//reqResMap, limitResMap, exractErr := extractResource(mgr.ModelDeploy.Spec.DeployInfo.Resource)
	//if exractErr != nil {
	//	log.V(1).Info(exractErr.Error())
	//}
	dep := &appv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getDeployName(mgr.ModelDeploy),
			Namespace:   mgr.ModelDeploy.GetNamespace(),
			Annotations: annotations,
			Labels:      deployLabels,
		},
		Spec: appv1.DeploymentSpec{
			Replicas: &mgr.ModelDeploy.Spec.DeployInfo.Replica,
			Selector: &metav1.LabelSelector{
				MatchLabels: podLabels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:        getDeployName(mgr.ModelDeploy),
					Namespace:   mgr.ModelDeploy.GetNamespace(),
					Annotations: annotations,
					Labels:      podLabels,
				},
				Spec: corev1.PodSpec{
					ImagePullSecrets: []corev1.LocalObjectReference{
						{
							Name: mgr.ModelDeploy.Spec.ImageRepository.Credential.Id,
						},
					},
					Containers: []corev1.Container{
						{
							Name:            mgr.ContainerName,
							Image:           getImage(mgr.ModelDeploy.Spec.ImageRepository.Url, mgr.ModelDeploy.Spec.ImageRepository.Tag),
							ImagePullPolicy: corev1.PullAlways,
							Resources: corev1.ResourceRequirements{
								//Requests: reqResMap,
								Limits: mgr.ModelDeploy.Spec.DeployInfo.Resource.Limit,
							},
						},
					},
				},
			},
		},
	}
	// inject appCore labels
	appLabels := NewAlaudaAppBuilder().BuildLabels(getAppName(*mgr.ModelDeploy), mgr.ModelDeploy.GetNamespace())
	injectDeploymentLabels(dep, appLabels)
	// add asm label if service type == istio
	if mgr.ModelDeploy.Spec.DeployInfo.ServiceType == mlv1alpha1.IstioService {
		version := mgr.ModelDeploy.Spec.ModelInfo.Version
		injectDeploymentLabels(dep, NewIstioSpecBuilder().BuildLabels(mgr.ModelDeploy.Spec.ModelService.Name, version))
	}
	return dep
}
func injectDeploymentLabels(deployment *appv1.Deployment, m map[string]string) {
	// deploy labels
	if deployment.ObjectMeta.Labels == nil {
		deployment.ObjectMeta.Labels = make(map[string]string)
	}
	// deploy selector
	if deployment.Spec.Selector.MatchLabels == nil {
		deployment.Spec.Selector.MatchLabels = make(map[string]string)
	}
	// pod labels
	if deployment.Spec.Template.ObjectMeta.Labels == nil {
		deployment.Spec.Template.ObjectMeta.Labels = make(map[string]string)
	}
	// add all
	for k, v := range m {
		deployment.ObjectMeta.Labels[k] = v               // deploy labels
		deployment.Spec.Selector.MatchLabels[k] = v       // deploy selector
		deployment.Spec.Template.ObjectMeta.Labels[k] = v //pod labels
	}
}

func (mgr *ModelDeploymentMgr) ParseToDeploy() *appv1.Deployment {
	generalDeployment := mgr.getGeneralDeployment()
	if mgr.IsNeedBuildImage {
		internal := NewInternalDeployment(mgr.ModelDeploy, mgr.SwaggerImageWithTag)
		internal.AssembledSpecOnDemand(generalDeployment)
	} else {
		custom := NewCustomDeployment(mgr.ModelDeploy, mgr.SwaggerImageWithTag)
		custom.AssembledSpecOnDemand(generalDeployment)
	}
	return generalDeployment
}

func getImage(url, tag string) string {
	repo := strings.Replace(strings.Replace(url, "http://", "", -1), "https://", "", -1)
	return fmt.Sprintf("%s:%s", repo, tag)
}

//func getImagePullSecrets(origin string) string {
//	return getLastPartOfString(origin, "-")
//}
//func getLastPartOfString(str, sep string) string {
//	pos := strings.LastIndex(str, sep)
//	if pos == -1 {
//		return str
//	}
//	return str[pos+1:]
//}

func (mgr *ModelDeploymentMgr) prepare() (err error) {
	swaggerImage, err := getSwaggerImage(mgr.reconcile)
	if err != nil {
		log.Error(err, "amlopr configmap not found! stop update model service.")
		return juju.Trace(err)
	}
	mgr.SwaggerImageWithTag = swaggerImage
	return
}

func (mgr *ModelDeploymentMgr) Apply() (err error) {
	if err = mgr.prepare(); err != nil {
		return
	}
	existsDeployment := &appv1.Deployment{}
	err = mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsDeployment)
	deployment := mgr.ParseToDeploy()
	if err != nil {
		if !errors.IsNotFound(err) {
			return
		}
		if err := controllerutil.SetControllerReference(mgr.ModelDeploy, deployment, mgr.reconcile.scheme); err != nil {
			return juju.Trace(err)
		}
		if err = mgr.reconcile.Client.Create(context.Background(), deployment); err != nil {
			log.Info("err in create deployment", "err:", err)
			return juju.Trace(err)
		}
	}
	// update
	if mgr.NeedUpdate(existsDeployment, deployment) {
		if err := controllerutil.SetControllerReference(mgr.ModelDeploy, deployment, mgr.reconcile.scheme); err != nil {
			return juju.Trace(err)
		}
		if err := mgr.reconcile.Client.Update(context.Background(), deployment); err != nil {
			return juju.Trace(err)
		}
	}
	return
}

func (mgr *ModelDeploymentMgr) Delete() (err error) {
	if err = mgr.prepare(); err != nil {
		return
	}
	existsDeployment := &appv1.Deployment{}
	err = mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsDeployment)
	if err != nil {
		if !errors.IsNotFound(err) {
			return err
		}
	}
	if err == nil {
		if err = mgr.reconcile.Client.Delete(context.Background(), existsDeployment); err != nil {
			log.Info("err in delete deployment", "err:", err)
			return juju.Trace(err)
		}
	}
	return nil
}
func (mgr *ModelDeploymentMgr) Update() (err error) {
	if err = mgr.prepare(); err != nil {
		return
	}
	existsDeployment := &appv1.Deployment{}
	err = mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsDeployment)
	deployment := mgr.ParseToDeploy()
	if err != nil {
		if !errors.IsNotFound(err) {
			return err
		}
	}
	//已存在,需要先删除，后创建
	if err == nil {
		if err = mgr.reconcile.Client.Delete(context.Background(), existsDeployment); err != nil {
			log.Info("err in delete deployment", "err:", err)
			return err
		}
	}
	if err := controllerutil.SetControllerReference(mgr.ModelDeploy, deployment, mgr.reconcile.scheme); err != nil {
		return err
	}
	if err = mgr.reconcile.Client.Create(context.Background(), deployment); err != nil {
		log.Info("err in create deployment", "err:", err)
		return err
	}
	return nil
}

func (mgr *ModelDeploymentMgr) UpdateStatusOfDeployment(instanceCopy *mlv1alpha1.ModelDeployment) {
	existsDeploy := &appv1.Deployment{}
	err := mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsDeploy)
	if err == nil {
		podState, podErr := mgr.getPodState(instanceCopy)
		if podErr == nil {
			if podState == string(corev1.PodRunning) {
				instanceCopy.Status.ManagedResourceResult.DeploymentResult.Ready = true
			}
			instanceCopy.Status.ManagedResourceResult.DeploymentResult.ReadyReplicas = existsDeploy.Status.ReadyReplicas
			instanceCopy.Status.ManagedResourceResult.DeploymentResult.Instances = []mlv1alpha1.ResourceInstance{
				{
					ApiVersion: existsDeploy.APIVersion,
					Kind:       existsDeploy.Kind,
					Name:       existsDeploy.Name,
				},
			}
			_ = mgr.UpdatePodStatusOfDeployment(instanceCopy)
			return
		}
	}
	// deployment not ready
	instanceCopy.Status.ManagedResourceResult.DeploymentResult.Ready = false
	instanceCopy.Status.ManagedResourceResult.DeploymentResult.ReadyReplicas = 0
	instanceCopy.Status.ManagedResourceResult.DeploymentResult.Instances = make([]mlv1alpha1.ResourceInstance, 0)
}
func (mgr *ModelDeploymentMgr) getPodState(instanceCopy *mlv1alpha1.ModelDeployment) (string, error) {
	existsPodList := &corev1.PodList{}
	labelSelector := labels.NewSelector()
	// creator.model-deploy: model1-abc
	req, err := labels.NewRequirement(common.GetLabelKeyForCreatorOf(LabelCreatorKey), selection.Equals, []string{mgr.ModelDeploy.GetName()})
	if err != nil {
		return "", err
	}
	ls := labelSelector.Add(*req)
	listOpts := &client.ListOptions{LabelSelector: ls}
	listOpts.Namespace = mgr.namespacedName.Namespace
	if err = mgr.reconcile.Client.List(context.Background(), listOpts, existsPodList); err != nil {
		return "", err
	}
	if len(existsPodList.Items) > 0 {
		pod := existsPodList.Items[0]
		if pod.DeletionTimestamp != nil {
			return "Terminating", nil
		}
		return string(pod.Status.Phase), nil
	}
	return "", fmt.Errorf("pod Not Found")
}

func (mgr *ModelDeploymentMgr) UpdatePodStatusOfDeployment(instanceCopy *mlv1alpha1.ModelDeployment) error {
	existsPodList := &corev1.PodList{}

	labelSelector := labels.NewSelector()
	req, err := labels.NewRequirement(common.GetLabelKeyForCreatorOf(LabelCreatorKey), selection.Equals, []string{mgr.ModelDeploy.GetName()})
	if err != nil {
		return err
	}
	ls := labelSelector.Add(*req)
	listOpts := &client.ListOptions{LabelSelector: ls}
	listOpts.Namespace = mgr.namespacedName.Namespace
	if err = mgr.reconcile.Client.List(context.Background(), listOpts, existsPodList); err != nil {
		return err
	}
	if len(existsPodList.Items) > 0 {
		pod := existsPodList.Items[0]
		instanceCopy.Status.ManagedResourceResult.DeploymentResult.PodStatus = pod.Status
	}
	return nil
}

func getSwaggerImage(r *ReconcileModelDeploy) (string, error) {
	amlCM, err := GetConfigMap(r.Client)
	if err != nil {
		log.Error(err, "amlopr configmap not found!")
		return "", err
	}
	swaggerUIImage := GetModelSwaggerUIImage(amlCM)
	log.Info(fmt.Sprintf("%v", swaggerUIImage))
	return swaggerUIImage, nil
}
