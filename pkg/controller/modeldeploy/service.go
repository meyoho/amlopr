package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"context"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"strings"
)

const (
	ModelServiceIngressUrlPrefix = "/model/service/"
	SwaggerUrlPrefix             = "/swagger/v1/models"
)

type ModelDeployService struct {
	Deploy         *mlv1alpha1.ModelDeployment
	namespacedName types.NamespacedName
	reconcile      *ReconcileModelDeploy
}

func NewServiceMgr(ins *mlv1alpha1.ModelDeployment,
	reconcile *ReconcileModelDeploy) *ModelDeployService {
	return &ModelDeployService{
		Deploy: ins,
		namespacedName: types.NamespacedName{
			Namespace: ins.Namespace,
			Name:      GetServiceName(*ins),
		},
		reconcile: reconcile,
	}
}

func GetServiceName(modelDeployment mlv1alpha1.ModelDeployment) string {
	return modelDeployment.Name
}
func GetSwaggerServiceName(modelDeployment mlv1alpha1.ModelDeployment) string {
	return fmt.Sprintf("%s-swagger", GetServiceName(modelDeployment))
}

func (mgr *ModelDeployService) NeedUpdate(from, to *corev1.Service) bool {
	return false
}

func (mgr *ModelDeployService) GetSwaggerNamespacedName() types.NamespacedName {
	return types.NamespacedName{
		Namespace: mgr.Deploy.Namespace,
		Name:      GetSwaggerServiceName(*mgr.Deploy),
	}
}

func (mgr *ModelDeployService) ParseToService() *corev1.Service {
	var aSvc *corev1.Service
	switch mgr.Deploy.Spec.DeployInfo.ServiceType {
	case mlv1alpha1.IstioService:
		aSvc = mgr.getIstioSvc()
	default:
		aSvc = mgr.getStandardSvc()
	}
	ambassadorGen := NewModelAmbassadorMgr(*mgr.Deploy)
	aSvc.ObjectMeta.Annotations[ambassadorGen.Key()] = ambassadorGen.GenServiceCfg(aSvc.Name)
	// custom image should add extra service port
	if !mgr.Deploy.Spec.ImageBuild.IsNeed {
		mgr.injectCustomServicePorts(aSvc)
	}
	return aSvc
}

/*
	通过Ambassador将swagger-ui 界面进行服务暴露
*/
func (mgr *ModelDeployService) ParseToSwaggerService() *corev1.Service {
	wrappedSvc := mgr.getNamedSvc()
	wrappedSvc.Name = GetSwaggerServiceName(*mgr.Deploy)
	wrappedSvc.Spec.Ports = append(wrappedSvc.Spec.Ports, corev1.ServicePort{
		Name:       "swagger",
		Port:       mgr.Deploy.Spec.DeployInfo.Port.Swagger,
		Protocol:   corev1.ProtocolTCP,
		TargetPort: intstr.FromInt(int(mgr.Deploy.Spec.DeployInfo.Port.Swagger)),
	})
	ambassadorGen := NewModelAmbassadorMgr(*mgr.Deploy)
	wrappedSvc.ObjectMeta.Annotations[ambassadorGen.Key()] = ambassadorGen.GenSwaggerCfg(wrappedSvc.Name)
	return wrappedSvc
}

// getIstioSvc get a istio service which wrap the standard one
func (mgr *ModelDeployService) getIstioSvc() *corev1.Service {
	svc := mgr.getStandardSvc()
	istioSpecBuilder := NewIstioSpecBuilder()
	istioSpecBuilder.EnsureServiceSpec(&svc.Spec)
	return svc
}

// getStandardSvc return k8s service
func (mgr *ModelDeployService) getStandardSvc() *corev1.Service {
	wrappedSvc := mgr.getNamedSvc()
	modelSvcPorts := []corev1.ServicePort{
		{
			Name:       "grpc",
			Port:       mgr.Deploy.Spec.DeployInfo.Port.Grpc,
			Protocol:   corev1.ProtocolTCP,
			TargetPort: intstr.FromInt(int(mgr.Deploy.Spec.DeployInfo.Port.Grpc)),
		},
		{
			Name:       "restful",
			Port:       mgr.Deploy.Spec.DeployInfo.Port.Restful,
			Protocol:   corev1.ProtocolTCP,
			TargetPort: intstr.FromInt(int(mgr.Deploy.Spec.DeployInfo.Port.Restful)),
		},
	}
	wrappedSvc.Spec.Ports = append(wrappedSvc.Spec.Ports, modelSvcPorts...)
	return wrappedSvc
}

func (mgr *ModelDeployService) getNamedSvc() *corev1.Service {
	labels := common.NewAmlLabel().
		Add(mlv1alpha1.LabelAMLComponentKey(), ComponentName).
		Add(common.GetLabelKeyForCreatorOf(LabelCreatorKey), mgr.Deploy.GetName()).Get()
	annotations := common.NewAmlAnnotation().Create().Get()

	service := &corev1.Service{
		TypeMeta: metav1.TypeMeta{
			Kind:       common.KindService,
			APIVersion: common.APIVersionService,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:        GetServiceName(*mgr.Deploy),
			Namespace:   mgr.Deploy.GetNamespace(),
			Annotations: annotations,
			Labels:      labels,
		},
		Spec: corev1.ServiceSpec{
			Type: corev1.ServiceTypeClusterIP,
		},
	}
	// inject appCore labels
	appLabels := NewAlaudaAppBuilder().BuildLabels(getAppName(*mgr.Deploy), mgr.Deploy.GetNamespace())
	injectServiceLabels(service, appLabels)
	return service
}
func injectServiceLabels(svc *corev1.Service, m map[string]string) {
	// service labels
	if svc.ObjectMeta.Labels == nil {
		svc.ObjectMeta.Labels = make(map[string]string)
	}
	// selector
	if svc.Spec.Selector == nil {
		svc.Spec.Selector = make(map[string]string)
	}
	for k, v := range m {
		svc.ObjectMeta.Labels[k] = v
		svc.Spec.Selector[k] = v
	}
}

func (mgr *ModelDeployService) injectCustomServicePorts(origSvc *corev1.Service) {
	var ports []corev1.ServicePort
	for _, clusterIp := range mgr.Deploy.Spec.DeployInfo.CustomService.ClusterServices {
		if clusterIp.ServiceName == "" {
			continue
		}
		ports = append(ports, corev1.ServicePort{
			Name:       clusterIp.ServiceName,
			Port:       clusterIp.SourcePort,
			TargetPort: intstr.FromInt(int(clusterIp.TargetPort)),
			Protocol:   getProtocol(clusterIp.Protocol),
		})
	}
	origSvc.Spec.Ports = append(origSvc.Spec.Ports, ports...)
}

func (mgr *ModelDeployService) applyService(service *corev1.Service) (err error) {
	existsSvc := &corev1.Service{}
	err = mgr.reconcile.Client.Get(context.Background(), types.NamespacedName{Name: service.Name, Namespace: service.Namespace}, existsSvc)
	svc := service
	if err != nil {
		if !errors.IsNotFound(err) {
			return
		}
		if err := controllerutil.SetControllerReference(mgr.Deploy, svc, mgr.reconcile.scheme); err != nil {
			return err
		}
		if err = mgr.reconcile.Client.Create(context.Background(), svc); err != nil {
			log.Info("err in create application", "err:", err)
			return err
		}
	}
	// update
	if mgr.NeedUpdate(existsSvc, svc) {
		if err := controllerutil.SetControllerReference(mgr.Deploy, svc, mgr.reconcile.scheme); err != nil {
			return err
		}
		if err := mgr.reconcile.Client.Update(context.Background(), svc); err != nil {
			return err
		}
	}
	return
}
func (mgr *ModelDeployService) ApplySwaggerService() (err error) {
	return mgr.applyService(mgr.ParseToSwaggerService())
}
func (mgr *ModelDeployService) Apply() (err error) {
	return mgr.applyService(mgr.ParseToService())
}
func (mgr *ModelDeployService) updateService(service *corev1.Service) (err error) {
	existsSvc := &corev1.Service{}
	err = mgr.reconcile.Client.Get(context.Background(), types.NamespacedName{Name: service.Name, Namespace: service.Namespace}, existsSvc)
	svc := service
	if err != nil && !errors.IsNotFound(err) {
		return err
	}
	if err == nil {
		if err := mgr.reconcile.Client.Delete(context.Background(), existsSvc); err != nil {
			return err
		}
	}
	if err := controllerutil.SetControllerReference(mgr.Deploy, svc, mgr.reconcile.scheme); err != nil {
		return err
	}
	if err = mgr.reconcile.Client.Create(context.Background(), svc); err != nil {
		log.Info("err in create service", "err:", err)
		return err
	}
	return nil
}
func (mgr *ModelDeployService) UpdateSwaggerService() (err error) {
	return mgr.updateService(mgr.ParseToSwaggerService())
}
func (mgr *ModelDeployService) Update() (err error) {
	return mgr.updateService(mgr.ParseToService())
}

func (mgr *ModelDeployService) UpdateStatusOfService(instanceCopy *mlv1alpha1.ModelDeployment) {
	mgr.updateServiceStatus(instanceCopy)
	mgr.updateSwaggerServiceStatus(instanceCopy)
}
func (mgr *ModelDeployService) updateSwaggerServiceStatus(instanceCopy *mlv1alpha1.ModelDeployment) {
	existsSvc := &corev1.Service{}
	err := mgr.reconcile.Client.Get(context.TODO(), mgr.GetSwaggerNamespacedName(), existsSvc)
	if err != nil {
		if errors.IsNotFound(err) {
			instanceCopy.Status.ManagedResourceResult.ServiceResult.SwaggerURL = ""
		}
	} else {
		if existsSvc.Spec.ClusterIP != "" {
			instanceCopy.Status.ManagedResourceResult.ServiceResult.SwaggerURL = GetSwaggerServiceURL(*mgr.Deploy)
		}
	}
}

func (mgr *ModelDeployService) updateServiceStatus(instanceCopy *mlv1alpha1.ModelDeployment) {
	existsSvc := &corev1.Service{}
	err := mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsSvc)
	if err != nil {
		if errors.IsNotFound(err) {
			instanceCopy.Status.ManagedResourceResult.ServiceResult.Ready = false
			instanceCopy.Status.ManagedResourceResult.ServiceResult.GRpcURL = ""
			instanceCopy.Status.ManagedResourceResult.ServiceResult.RestfulURL = ""
			instanceCopy.Status.ManagedResourceResult.ServiceResult.SwaggerURL = ""
			instanceCopy.Status.ManagedResourceResult.ServiceResult.Instances = make([]mlv1alpha1.ResourceInstance, 0)
		}
	} else {
		// cluster ip 生成
		if existsSvc.Spec.ClusterIP != "" {
			instanceCopy.Status.ManagedResourceResult.ServiceResult.Ready = true
			var (
				grpcPort    int32
				restfulPort int32
			)
			for _, port := range existsSvc.Spec.Ports {
				if strings.Contains(port.Name, "grpc") {
					grpcPort = port.Port
				}
				if strings.Contains(port.Name, "restful") {
					restfulPort = port.Port
				}
			}
			instanceCopy.Status.ManagedResourceResult.ServiceResult.GRpcURL = fmt.Sprintf("%s:%v", existsSvc.Spec.ClusterIP, grpcPort)
			instanceCopy.Status.ManagedResourceResult.ServiceResult.RestfulURL = fmt.Sprintf("%s:%v", existsSvc.Spec.ClusterIP, restfulPort)
			instanceCopy.Status.ManagedResourceResult.ServiceResult.Instances = []mlv1alpha1.ResourceInstance{
				{
					ApiVersion: existsSvc.APIVersion,
					Kind:       existsSvc.Kind,
					Name:       existsSvc.Name,
				},
			}
		}
	}
}

func GetGRpcBaseUrl(modelDeployment mlv1alpha1.ModelDeployment) string {
	rootURL := fmt.Sprintf("%v%v/", ModelServiceIngressUrlPrefix, "grpc")
	return fmt.Sprintf("%v%v", rootURL, getIdentifierOfURL(modelDeployment))
}
func GetRestfulBaseUrl(modelDeployment mlv1alpha1.ModelDeployment) string {
	rootURL := fmt.Sprintf("%v%v/", ModelServiceIngressUrlPrefix, "restful")
	return fmt.Sprintf("%v%v", rootURL, getIdentifierOfURL(modelDeployment))
}
func GetSwaggerBaseUrl(modelDeployment mlv1alpha1.ModelDeployment) string {
	rootURL := fmt.Sprintf("%v%v/", ModelServiceIngressUrlPrefix, "swagger")
	return fmt.Sprintf("%v%v", rootURL, getIdentifierOfURL(modelDeployment))
}
func GetSwaggerServiceURL(modelDeployment mlv1alpha1.ModelDeployment) (prefix string) {
	return GetClosedUrl(GetSwaggerBaseUrl(modelDeployment))
}
func GetClosedUrl(url string) string {
	if url[len(url)-1:] != "/" {
		return url + "/"
	}
	return url
}

// 在已有镜像模式下，虽然可以指定"模型版本"，但是考虑实际使用时，在没有其他功能辅助的情况下，要了解每一个镜像中的模型版本信息比较困难，
// 所以，不强制认为已有镜像模式下 modelploy中modelversion 与 模型镜像中的真实版本一致。
// 所以在实现 swagger访问时，采用访问模型版本的latest方式，而非指定具体版本进行访问。
func getIdentifierOfURL(modelDeployment mlv1alpha1.ModelDeployment) string {
	// 模型swagger-ui访问路径  /model/service/{namespace}/{modelname}/[{version}/]
	var prefix string
	if modelDeployment.Spec.ModelInfo.Version == "" {
		prefix = fmt.Sprintf("%s/%s/latest/",
			modelDeployment.GetNamespace(),
			modelDeployment.Spec.ModelInfo.ModelName)
	} else {
		prefix = fmt.Sprintf("%s/%s/%s/",
			modelDeployment.GetNamespace(),
			modelDeployment.Spec.ModelInfo.ModelName,
			modelDeployment.Spec.ModelInfo.Version)
	}
	// change the url privately by which  model service operations used
	return fmt.Sprintf("%s%v", prefix, modelDeployment.Name)
}
