package modeldeploy

import (
	"context"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func GetConfigMap(client client.Client) (*v1.ConfigMap, error) {
	configMapName := os.Getenv("CM_NAME")
	configMapNameSpace := os.Getenv("POD_NAMESPACE")
	configMap := &v1.ConfigMap{}
	namespaceName := types.NamespacedName{Name: configMapName, Namespace: configMapNameSpace}
	err := client.Get(context.TODO(), namespaceName, configMap)
	if err != nil {
		return nil, err
	}
	return configMap, nil
}

func GetModelSwaggerUIImage(configMap *v1.ConfigMap) string {
	return configMap.Data[SwaggerImageKeyInConfigMap]
}
