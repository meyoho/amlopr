package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"fmt"
	appv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"strings"
)

const (
	SwaggerPort int32 = 8505
	RestfulPort int32 = 8501
	GrpcPort    int32 = 8500
)

type TensorFlowServingDeployment struct {
	SwaggerImage string
	swaggerPort  int32
	restfulPort  int32
	grpcPort     int32
	modelName    string
}

func (tf *TensorFlowServingDeployment) injectSwaggerSideCar(deploy *appv1.Deployment /*, deploySetting *mlv1alpha1.ModelDeploy*/) {
	serviceUUID := fmt.Sprintf("%v-swagger.%v", deploy.Name, deploy.Namespace)
	args := []string{"server",
		fmt.Sprintf("--port=%v", tf.swaggerPort),
		fmt.Sprintf("--forwardPort=%v", tf.restfulPort),
		fmt.Sprintf("--modelname=%v", tf.modelName),
		fmt.Sprintf("--serviceUUID=%v", serviceUUID),
		fmt.Sprintf("--prefix=%v", "/swagger/"),
	}
	swagger := "swagger"
	c := corev1.Container{
		Name:  swagger,
		Image: strings.TrimSpace(tf.SwaggerImage),
		Args:  args,
		Ports: []corev1.ContainerPort{
			{
				Name:          swagger,
				ContainerPort: tf.swaggerPort,
				Protocol:      corev1.ProtocolTCP,
			},
		},
		LivenessProbe: &corev1.Probe{
			Handler: corev1.Handler{
				TCPSocket: &corev1.TCPSocketAction{
					Port: intstr.FromString(swagger),
				},
			},
			InitialDelaySeconds: 5,
			PeriodSeconds:       10,
		},
		ReadinessProbe: &corev1.Probe{
			Handler: corev1.Handler{
				TCPSocket: &corev1.TCPSocketAction{
					Port: intstr.FromString(swagger),
				},
			},
			InitialDelaySeconds: 15,
			PeriodSeconds:       20,
		},
	}
	deploy.Spec.Template.Spec.Containers = append(deploy.Spec.Template.Spec.Containers, c)
}

//--------- InternalDeployment ---------

//InternalDeployment for model deploy with image building
type InternalDeployment struct {
	TensorFlowServingDeployment
}

func NewInternalDeployment(deploySetting *mlv1alpha1.ModelDeployment, SwaggerImageWithTag string) *InternalDeployment {
	var swaggerPort = SwaggerPort
	var grpcPort = GrpcPort
	var restfulPort = RestfulPort

	if deploySetting.Spec.DeployInfo.Port.Restful != 0 {
		restfulPort = deploySetting.Spec.DeployInfo.Port.Restful
	}
	if deploySetting.Spec.DeployInfo.Port.Grpc != 0 {
		grpcPort = deploySetting.Spec.DeployInfo.Port.Grpc
	}
	if deploySetting.Spec.DeployInfo.Port.Swagger != 0 {
		swaggerPort = deploySetting.Spec.DeployInfo.Port.Swagger
	}

	return &InternalDeployment{
		TensorFlowServingDeployment{
			SwaggerImage: SwaggerImageWithTag,
			swaggerPort:  swaggerPort,
			restfulPort:  restfulPort,
			grpcPort:     grpcPort,
			modelName:    deploySetting.Spec.ModelInfo.ModelName,
		},
	}
}
func (internal *InternalDeployment) AssembledSpecOnDemand(deploy *appv1.Deployment) {
	internal.injectSpecsForDeployment(deploy)
	internal.injectSwaggerSideCar(deploy)
}

func (internal *InternalDeployment) getArgs() []string {
	exec := "/usr/bin/tensorflow_model_server"
	return []string{
		exec,
		fmt.Sprintf("--port=%v", internal.grpcPort),
		fmt.Sprintf("--rest_api_port=%v", internal.restfulPort),
		fmt.Sprintf("--model_name=%v", internal.modelName),
		fmt.Sprintf("--model_base_path=/models/%v", internal.modelName),
	}
}

func (internal *InternalDeployment) injectSpecsForDeployment(deployment *appv1.Deployment) {
	grpcPortName := "grpc"
	restfulPortName := "restful"
	deployment.Spec.Template.Spec.Containers[0].Ports = []corev1.ContainerPort{
		{
			Name:          grpcPortName,
			ContainerPort: internal.grpcPort,
			Protocol:      corev1.ProtocolTCP,
		},
		{
			Name:          restfulPortName,
			ContainerPort: internal.restfulPort,
			Protocol:      corev1.ProtocolTCP,
		},
	}
	deployment.Spec.Template.Spec.Containers[0].LivenessProbe = &corev1.Probe{
		Handler: corev1.Handler{
			TCPSocket: &corev1.TCPSocketAction{
				Port: intstr.FromString(grpcPortName),
			},
		},
		InitialDelaySeconds: 5,
		PeriodSeconds:       10,
	}
	deployment.Spec.Template.Spec.Containers[0].ReadinessProbe = &corev1.Probe{
		Handler: corev1.Handler{
			TCPSocket: &corev1.TCPSocketAction{
				Port: intstr.FromString(grpcPortName),
			},
		},
		InitialDelaySeconds: 15,
		PeriodSeconds:       20,
	}
	deployment.Spec.Template.Spec.Containers[0].Args = internal.getArgs()
}
