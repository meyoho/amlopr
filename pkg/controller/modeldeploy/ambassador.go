package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"fmt"
	"strings"
)

type AmbassadorCfg struct {
	MappingName      string
	Prefix           string
	ReWrite          string
	ServiceName      string
	ServicePort      string
	ServiceNamespace string
	TimeoutMs        int32
}

func GenAmbassadorConfigForService(cfg AmbassadorCfg) string {

	var sArr = []string{
		"---",
		"apiVersion: ambassador/v0",
		"kind:  Mapping",
		"name:  $_mapping_name_",
		"prefix: $_prefix_",
		"rewrite: $_rewrite_",
		"service: $_target_svc_.$_target_ns_:$_svc_port_",
		"timeout_ms: 300000",
		"use_websocket: true", "",
	}
	svcRoute := strings.Join(sArr, "\n")
	svcRoute = strings.Replace(svcRoute, "$_mapping_name_", cfg.MappingName, -1)
	svcRoute = strings.Replace(svcRoute, "$_prefix_", cfg.Prefix, -1)
	svcRoute = strings.Replace(svcRoute, "$_rewrite_", cfg.ReWrite, -1)
	svcRoute = strings.Replace(svcRoute, "$_target_svc_", cfg.ServiceName, -1)
	svcRoute = strings.Replace(svcRoute, "$_target_ns_", cfg.ServiceNamespace, -1)
	svcRoute = strings.Replace(svcRoute, "$_svc_port_", cfg.ServicePort, -1)
	return fmt.Sprintf("%s", svcRoute)

}

func GetAmbassadorAnnotationKey() string {
	return AmbassadorKeyConfig
}

type ModelAmbassadorCfg struct {
	APIVersion   string
	Kind         string
	Name         string
	Prefix       string
	ReWrite      string
	Service      string
	TimeoutMs    int32
	UseWebsocket bool
}

func NewModelAmbassadorCfg() *ModelAmbassadorCfg {
	return &ModelAmbassadorCfg{
		Kind:         "Mapping",
		UseWebsocket: true,
		TimeoutMs:    300000,
	}
}
func (m *ModelAmbassadorCfg) Version0() *ModelAmbassadorCfg {
	m.APIVersion = "ambassador/v0"
	return m
}
func (m *ModelAmbassadorCfg) Version1() *ModelAmbassadorCfg {
	m.APIVersion = "ambassador/v0"
	return m
}
func (m ModelAmbassadorCfg) ToString() string {
	return fmt.Sprintf("---\n"+
		"apiVersion: %v\n"+
		"kind: %v\n"+
		"name: %v\n"+
		"prefix: %v\n"+
		"rewrite: %v\n"+
		"service: %v\n"+
		"timeout_ms: %v\n"+
		"use_websocket: %v",
		m.APIVersion, m.Kind, m.Name, m.Prefix, m.ReWrite, m.Service, m.TimeoutMs, m.UseWebsocket)
}

func (m *ModelAmbassadorCfg) Append(other ModelAmbassadorCfg) string {
	return fmt.Sprintf("%v\n%v", m.ToString(), other.ToString())
}

type ModelAmbassadorMgr struct {
	annoKey string
	mlv1alpha1.ModelDeployment
}

func NewModelAmbassadorMgr(deploy mlv1alpha1.ModelDeployment) ModelAmbassadorMgr {
	return ModelAmbassadorMgr{
		annoKey:         "getambassador.io/config",
		ModelDeployment: deploy,
	}
}
func (mgr *ModelAmbassadorMgr) Key() string {
	return mgr.annoKey
}
func (mgr *ModelAmbassadorMgr) GenSwaggerAnnotation(serviceName string) map[string]string {
	return map[string]string{mgr.annoKey: mgr.GenSwaggerCfg(serviceName)}
}
func (mgr *ModelAmbassadorMgr) GenRestfulAnnotation(serviceName string) map[string]string {
	return map[string]string{mgr.annoKey: mgr.GenRestfulCfg(serviceName)}
}
func (mgr *ModelAmbassadorMgr) GenSwaggerCfg(serviceName string) string {
	route := NewModelAmbassadorCfg().Version0()
	route.Name = fmt.Sprintf("route-%v-%v-%v.%v",
		mgr.ModelDeployment.Spec.ModelInfo.ModelName,
		mgr.ModelDeployment.Spec.ModelInfo.Version,
		serviceName,
		mgr.Namespace)
	route.Prefix = GetSwaggerServiceURL(mgr.ModelDeployment)
	route.Service = fmt.Sprintf("%v.%v:%v", serviceName, mgr.Namespace, mgr.Spec.DeployInfo.Port.Swagger)
	route.ReWrite = "/"
	operation := NewModelAmbassadorCfg().Version0()
	operation.Name = fmt.Sprintf("operations-%v-%v-%v.%v",
		mgr.ModelDeployment.Spec.ModelInfo.ModelName,
		mgr.ModelDeployment.Spec.ModelInfo.Version,
		serviceName,
		mgr.Namespace)
	operation.Prefix = fmt.Sprintf("%v/%v/%v.%v",
		SwaggerUrlPrefix,
		mgr.ModelDeployment.Spec.ModelInfo.ModelName,
		serviceName,
		mgr.Namespace)
	operation.Service = fmt.Sprintf("%v.%v:%v", serviceName, mgr.Namespace, mgr.Spec.DeployInfo.Port.Swagger)
	operation.ReWrite = fmt.Sprintf("%v/%v", SwaggerUrlPrefix, mgr.ModelDeployment.Spec.ModelInfo.ModelName)

	return route.Append(*operation)
}

func (mgr *ModelAmbassadorMgr) GenServiceCfg(serviceName string) string {
	return fmt.Sprintf("%v\n%v", mgr.GenRestfulCfg(serviceName), mgr.GenGRpcCfg(serviceName))
}

func (mgr *ModelAmbassadorMgr) GenRestfulCfg(serviceName string) string {
	operation := NewModelAmbassadorCfg().Version0()
	// /model/service/<namespace>/<modelname>/<version>/<部署名>/restful
	operation.Name = fmt.Sprintf("restful-%v-%v-%v.%v",
		mgr.ModelDeployment.Spec.ModelInfo.ModelName,
		mgr.ModelDeployment.Spec.ModelInfo.Version,
		serviceName,
		mgr.Namespace)
	operation.Prefix = GetRestfulBaseUrl(mgr.ModelDeployment)
	operation.Service = fmt.Sprintf("%v.%v:%v", serviceName, mgr.Namespace, mgr.ModelDeployment.Spec.DeployInfo.Port.Restful)
	operation.ReWrite = fmt.Sprintf("/v1/models/%v", mgr.ModelDeployment.Spec.ModelInfo.ModelName)
	return operation.ToString()
}
func (mgr *ModelAmbassadorMgr) GenGRpcCfg(serviceName string) string {
	operation := NewModelAmbassadorCfg().Version0()
	// /model/service/<namespace>/<modelname>/<version>/<部署名>/restful
	operation.Name = fmt.Sprintf("grpc-%v-%v-%v.%v",
		mgr.ModelDeployment.Spec.ModelInfo.ModelName,
		mgr.ModelDeployment.Spec.ModelInfo.Version,
		serviceName,
		mgr.Namespace)
	operation.Prefix = GetGRpcBaseUrl(mgr.ModelDeployment)
	operation.Service = fmt.Sprintf("%v.%v:%v", serviceName, mgr.Namespace, mgr.ModelDeployment.Spec.DeployInfo.Port.Grpc)
	operation.ReWrite = "/"
	return operation.ToString()
}
