package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"context"
	juju "github.com/juju/errors"
	"github.com/kubernetes-sigs/application/pkg/apis/app/v1beta1"
	appv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

type ModelServiceApplication struct {
	Deploy         *mlv1alpha1.ModelDeployment
	namespacedName types.NamespacedName
	reconcile      *ReconcileModelDeploy
}

func NewApplicationMgr(ins *mlv1alpha1.ModelDeployment,
	reconcile *ReconcileModelDeploy) *ModelServiceApplication {
	return &ModelServiceApplication{
		Deploy: ins,
		namespacedName: types.NamespacedName{
			Namespace: ins.Namespace,
			Name:      getAppName(*ins),
		},
		reconcile: reconcile,
	}
}
func getAppName(modelDeployment mlv1alpha1.ModelDeployment) string {
	return modelDeployment.Spec.ModelService.Name
}

func (mgr *ModelServiceApplication) NeedUpdate(from, to *v1beta1.Application) bool {
	return false
}

func (mgr *ModelServiceApplication) ParseToApplication() *v1beta1.Application {
	componentGroupKinds := []metav1.GroupKind{
		{Group: appv1.GroupName, Kind: "Deployment"},
		{Group: "", Kind: "Service"},
	}
	annotations := common.NewAmlAnnotation().Create().
		Add(AnnotationAppCreateUser(), mgr.Deploy.Spec.ModelService.User).
		Get()
	labels := common.NewAmlLabel().
		Add(mlv1alpha1.LabelAMLComponentKey(), ComponentName).
		Add(common.GetLabelKeyForCreatorOf(LabelCreatorKey), mgr.Deploy.GetName()).
		Get()
	application := &v1beta1.Application{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getAppName(*mgr.Deploy),
			Namespace:   mgr.namespacedName.Namespace,
			Annotations: annotations,
			Labels:      labels,
		},
		Spec: v1beta1.ApplicationSpec{
			AssemblyPhase:       "Succeeded",
			ComponentGroupKinds: componentGroupKinds,
			Selector:            &metav1.LabelSelector{},
		},
	}
	//inject appCore labels
	appLabels := NewAlaudaAppBuilder().BuildLabels(getAppName(*mgr.Deploy), mgr.Deploy.GetNamespace())
	injectLabels(application, appLabels)

	return application
}

func injectLabels(application *v1beta1.Application, l map[string]string) {
	if (*application).Spec.Selector.MatchLabels == nil {
		application.Spec.Selector.MatchLabels = make(map[string]string)
	}
	for k, v := range l {
		application.Spec.Selector.MatchLabels[k] = v
	}
}

func (mgr *ModelServiceApplication) Apply() (err error) {
	existsApp := &v1beta1.Application{}
	err = mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsApp)
	application := mgr.ParseToApplication()
	if err != nil {
		if !errors.IsNotFound(err) {
			return
		}
		if err = mgr.reconcile.Client.Create(context.Background(), application); err != nil {
			log.Info("err in create application", "err:", err)
			return juju.Trace(err)
		}
	}
	// update
	if mgr.NeedUpdate(existsApp, application) {
		if err := mgr.reconcile.Client.Update(context.Background(), application); err != nil {
			return juju.Trace(err)
		}
	}
	return
}
func (mgr *ModelServiceApplication) Delete() (err error) {
	existsApp, err := mgr.getApplication()
	if err != nil {
		if !errors.IsNotFound(err) {
			return err
		}
	}
	if err == nil {
		if err = mgr.reconcile.Client.Delete(context.Background(), existsApp); err != nil {
			log.Info("err in delete application", "err:", err)
			return juju.Trace(err)
		}
	}
	return nil
}

func (mgr *ModelServiceApplication) getApplication() (*v1beta1.Application, error) {
	existsApp := &v1beta1.Application{}
	err := mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsApp)
	return existsApp, err
}

func (mgr *ModelServiceApplication) SetOwnerReference(instanceCopy *mlv1alpha1.ModelDeployment) error {
	application, err := mgr.getApplication()
	if err == nil {
		if !checkHasReferenced(application.APIVersion, application.Kind, application.Name, instanceCopy) {
			if err := controllerutil.SetControllerReference(application, instanceCopy, mgr.reconcile.scheme); err != nil {
				return juju.Trace(err)
			}
		}
	}
	return nil
}

func checkHasReferenced(apiVersion, kind, appName string, instanceCopy *mlv1alpha1.ModelDeployment) bool {
	var hasReferenced = false
	if len(instanceCopy.OwnerReferences) > 0 {
		for _, ref := range instanceCopy.OwnerReferences {
			if ref.Kind == kind &&
				ref.APIVersion == apiVersion &&
				ref.Name == appName {
				hasReferenced = true
				break
			}
		}
	}
	return hasReferenced
}
