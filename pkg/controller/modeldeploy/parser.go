package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"encoding/json"
	"fmt"
	juju "github.com/juju/errors"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"strconv"
	"strings"
)

type AssembledParser struct {
	BooleanGroup             map[string]*bool
	StringGroup              map[string]*string
	Int32Group               map[string]*int32
	NestedStringGroup        map[string]*string
	ModelDeployResourceGroup map[string]*mlv1alpha1.ModelDeploymentResource
	StringArrayGroup         map[string]*[]string
	StringMapGroup           map[string]*map[string]string
	clusterServiceGroup      map[string]*[]mlv1alpha1.ClusterIpInfo
}
type GpuResource struct {
	Cpu    string `json:"cpu"`
	Memory string `json:"memory"`
	Gpu    string `json:"gpu"`
}

type ModelDeploymentResourceDesc struct {
	Request GpuResource `json:"request"`
	Limit   GpuResource `json:"limit"`
}

func NewAssembledParser() *AssembledParser {
	return &AssembledParser{
		BooleanGroup:             make(map[string]*bool),
		StringGroup:              make(map[string]*string),
		Int32Group:               make(map[string]*int32),
		NestedStringGroup:        make(map[string]*string),
		ModelDeployResourceGroup: make(map[string]*mlv1alpha1.ModelDeploymentResource),
		StringArrayGroup:         make(map[string]*[]string),
		StringMapGroup:           make(map[string]*map[string]string),
		clusterServiceGroup:      make(map[string]*[]mlv1alpha1.ClusterIpInfo),
	}
}

func (p *AssembledParser) AddToBooleanGroup(key string, value *bool) {
	p.BooleanGroup[key] = value
}
func (p *AssembledParser) AddToStringGroup(key string, value *string) {
	p.StringGroup[key] = value
}
func (p *AssembledParser) AddToInt32Group(key string, value *int32) {
	p.Int32Group[key] = value
}

func (p *AssembledParser) AddToNestedStringGroup(mapkey, jsonkey string, value *string) {
	p.NestedStringGroup[fmt.Sprintf("%v|%v", mapkey, jsonkey)] = value
}

func (p *AssembledParser) AddToResourceGroup(key string, value *mlv1alpha1.ModelDeploymentResource) {
	p.ModelDeployResourceGroup[key] = value
}
func (p *AssembledParser) AddToClusterService(key string, value *[]mlv1alpha1.ClusterIpInfo) {
	p.clusterServiceGroup[key] = value
}
func (p *AssembledParser) AddToStringArrayGroup(key string, value *[]string) {
	p.StringArrayGroup[key] = value
}
func (p *AssembledParser) AddToMapGroup(key string, value *map[string]string) {
	p.StringMapGroup[key] = value
}

func (p *AssembledParser) doEscape(value string) string {
	return strings.Trim(strings.Trim(value, "'"), "\"")
}

func (p *AssembledParser) AutoParseOrStopOnError(paramsMap map[string]devops.PipelineTemplateArgumentValue) error {
	for k, v := range p.Int32Group {
		s, err := p.parseInt32(k, paramsMap)
		if err != nil {
			return err
		}
		*v = s
	}
	for k, v := range p.StringGroup {
		s, err := p.parseString(k, paramsMap)
		if err != nil {
			return err
		}
		*v = s
	}
	for k, v := range p.StringArrayGroup {
		s, err := p.parseStringArray(k, paramsMap)
		if err != nil {
			return err
		}
		*v = *s
	}

	for k, v := range p.BooleanGroup {
		b, err := p.parseBool(k, paramsMap)
		if err != nil {
			return err
		}
		*v = b
	}
	nestedSep := "|"
	for k, v := range p.NestedStringGroup {
		if len(strings.Split(k, nestedSep)) < 2 {
			return errors.NewBadRequest("register key error in NestedStringGroup,should be ParentKey|AttributeKey")
		}
		mapKey := strings.Split(k, nestedSep)[0]
		jsonKey := strings.Split(k, nestedSep)[1]
		s, err := p.parseNestedString(mapKey, jsonKey, paramsMap)
		if err != nil {
			return err
		}
		*v = s
	}
	for k, v := range p.ModelDeployResourceGroup {
		res, err := p.parseResource(k, paramsMap)
		if err != nil {
			return err
		}
		*v = *res
	}
	for k, v := range p.StringMapGroup {
		res, err := p.parseMap(k, paramsMap)
		if err != nil {
			return err
		}
		*v = res
	}
	for k, v := range p.clusterServiceGroup {
		res, err := p.parseClusterService(k, paramsMap)
		if err != nil {
			return err
		}
		*v = res
	}

	return nil
}

func (p *AssembledParser) checkIfKeyExists(name string, paramsMap map[string]devops.PipelineTemplateArgumentValue) bool {
	_, ok := paramsMap[name]
	return ok
}

func (p *AssembledParser) parseString(name string, paramsMap map[string]devops.PipelineTemplateArgumentValue) (string, error) {
	if !p.checkIfKeyExists(name, paramsMap) {
		return "", errors.NewBadRequest(fmt.Sprintf("lack of params[%v]", name))
	}
	escapedValue := p.doEscape(paramsMap[name].Value)
	return escapedValue, nil
}
func (p *AssembledParser) parseStringArray(name string, paramsMap map[string]devops.PipelineTemplateArgumentValue) (*[]string, error) {
	if !p.checkIfKeyExists(name, paramsMap) {
		return nil, errors.NewBadRequest(fmt.Sprintf("lack of params[%v]", name))
	}
	fn := func(r rune) bool {
		if string(r) == "," || string(r) == " " {
			return true
		}
		return false
	}
	escapedValue := p.doEscape(paramsMap[name].Value)
	arr := strings.FieldsFunc(escapedValue, fn)
	return &arr, nil
}
func (p *AssembledParser) parseInt32(name string, paramsMap map[string]devops.PipelineTemplateArgumentValue) (int32, error) {
	if !p.checkIfKeyExists(name, paramsMap) {
		return 0, errors.NewBadRequest(fmt.Sprintf("lack of params[%v]", name))
	}
	escapedValue := p.doEscape(paramsMap[name].Value)
	v, err := strconv.Atoi(escapedValue)
	if err != nil {
		return 0, errors.NewBadRequest(fmt.Sprintf("wrong value type of params[%v]", name))
	}
	return int32(v), nil
}

func (p *AssembledParser) parseBool(name string, paramsMap map[string]devops.PipelineTemplateArgumentValue) (bool, error) {
	if !p.checkIfKeyExists(name, paramsMap) {
		return false, errors.NewBadRequest(fmt.Sprintf("lack of params[%v]", name))
	}
	escapedValue := p.doEscape(paramsMap[name].Value)
	v, err := strconv.ParseBool(escapedValue)
	if err != nil {
		return false, errors.NewBadRequest(fmt.Sprintf("wrong value type of params[%v]", name))
	}
	return v, nil
}

func (p *AssembledParser) parseNestedString(name, nestedKey string, paramsMap map[string]devops.PipelineTemplateArgumentValue) (string, error) {
	if !p.checkIfKeyExists(name, paramsMap) {
		return "", errors.NewBadRequest(fmt.Sprintf("lack of params[%v]", name))
	}
	escapedValue := p.doEscape(paramsMap[name].Value)
	var aMap map[string]interface{}
	err := json.Unmarshal([]byte(escapedValue), &aMap)
	if err != nil {
		return "", errors.NewBadRequest(fmt.Sprintf("wrong value of params[%v]", name))
	}
	v, ok := aMap[nestedKey]
	if !ok {
		return "", errors.NewBadRequest(fmt.Sprintf("nested key missing of params[%v]", name))
	}
	ss, ok := v.(string)
	if ok {
		return ss, nil
	} else {
		return "", errors.NewBadRequest(fmt.Sprintf("nestedMap of [%v] not implement.", name))
	}
	//return v, nil
}

func (p *AssembledParser) parseResource(name string, paramsMap map[string]devops.PipelineTemplateArgumentValue) (*mlv1alpha1.ModelDeploymentResource, error) {
	if !p.checkIfKeyExists(name, paramsMap) {
		return nil, errors.NewBadRequest(fmt.Sprintf("lack of params[%v]", name))
	}
	escapedValue := p.doEscape(paramsMap[name].Value)
	var stub corev1.ResourceQuotaSpec
	err := json.Unmarshal([]byte(escapedValue), &stub)
	if err != nil {
		return nil, errors.NewBadRequest(fmt.Sprintf("wrong struct of params[%v]", name))
	}
	podResource := parseValidResource(stub)
	return podResource, nil
}
func parseValidResource(stub corev1.ResourceQuotaSpec) *mlv1alpha1.ModelDeploymentResource {
	podResource := &mlv1alpha1.ModelDeploymentResource{}
	podResource.Limit = utils.FilteredLimitsResourceMapByKey(stub.Hard)
	return podResource
}

func (p *AssembledParser) parseClusterService(name string, paramsMap map[string]devops.PipelineTemplateArgumentValue) ([]mlv1alpha1.ClusterIpInfo, error) {
	if !p.checkIfKeyExists(name, paramsMap) {
		return nil, errors.NewBadRequest(fmt.Sprintf("lack of params[%v]", name))
	}
	escapedValue := p.doEscape(paramsMap[name].Value)
	var stub []mlv1alpha1.ClusterIpInfo
	err := json.Unmarshal([]byte(escapedValue), &stub)
	if err != nil {
		return nil, errors.NewBadRequest(fmt.Sprintf("wrong value of params[%v]", name))
	}

	return stub, nil
}

func (p *AssembledParser) parseMap(name string, paramsMap map[string]devops.PipelineTemplateArgumentValue) (map[string]string, error) {
	if !p.checkIfKeyExists(name, paramsMap) {
		return nil, errors.NewBadRequest(fmt.Sprintf("lack of params[%v]", name))
	}
	escapedValue := p.doEscape(paramsMap[name].Value)
	var aMap map[string]string
	err := json.Unmarshal([]byte(escapedValue), &aMap)
	if err != nil {
		return nil, errors.NewBadRequest(fmt.Sprintf("wrong value of params[%v]", name))
	}

	return aMap, nil
}

func (p *AssembledParser) IsNeedBuildStep(paramsMap map[string]devops.PipelineTemplateArgumentValue) bool {
	// this hard code value "needImageBuild" is binding to modelDeploy arguments definition
	// needImageBuild is the value of radio button, means need building image by running jenkins pipeline
	argumentName := UIElementName_needImageBuild //"needImageBuild"
	return getBoolFlag(argumentName, paramsMap)
}

func (p *AssembledParser) IsModelInPvc(paramsMap map[string]devops.PipelineTemplateArgumentValue) bool {
	// this hard code value "code" is binding to modelDeploy arguments definition
	// code is the value of radio button, means model files exists on local pvc stores
	argumentName := UIElementName_code //"code"
	return getBoolFlag(argumentName, paramsMap)
}

func getBoolFlag(name string, paramsMap map[string]devops.PipelineTemplateArgumentValue) bool {
	var v devops.PipelineTemplateArgumentValue
	var ok bool
	if v, ok = paramsMap[name]; !ok {
		return false
	}
	if v.Value != "true" {
		return false
	}
	return true
}

func GetModelDeployMergedParamsGroup(arguments []devops.PipelineTemplateArgumentGroup,
	parameters []devops.PipelineTemplateArgumentGroup) map[string]devops.PipelineTemplateArgumentValue {
	nameValueMap := make(map[string]devops.PipelineTemplateArgumentValue)
	for _, argument := range arguments {
		for _, argument := range argument.Items {
			nameValueMap[argument.Name] = argument
		}
	}
	var result = make(map[string]devops.PipelineTemplateArgumentValue)
	getMergedArgumentValue(arguments, result)
	getMergedArgumentValue(parameters, result)
	return result
}

func getMergedArgumentValue(arguments []devops.PipelineTemplateArgumentGroup, result map[string]devops.PipelineTemplateArgumentValue) {
	for _, value := range arguments {
		for _, item := range value.Items {
			result[item.Name] = item
		}
	}
}

// ----------------

type ModelDeploymentParser struct {
	AMLPipeline mlv1alpha1.AMLPipeline
	Arguments   []devops.PipelineTemplateArgumentGroup
	Namespace   string
}

func NewModelDeploymentParser(amlPipeline mlv1alpha1.AMLPipeline,
	arguments []devops.PipelineTemplateArgumentGroup,
	namespace string) ModelDeploymentParser {
	if arguments == nil {
		arguments = []devops.PipelineTemplateArgumentGroup{}
	}
	return ModelDeploymentParser{amlPipeline, arguments, namespace}
}

func (p ModelDeploymentParser) ToModelDeployment(client client.Client) (*mlv1alpha1.ModelDeployment, error) {
	modelDeploy := &mlv1alpha1.ModelDeployment{}
	paramsMap := GetModelDeployMergedParamsGroup(p.Arguments, p.AMLPipeline.Spec.Parameters)
	action, err := getPipelineAction(paramsMap)
	if err != nil {
		return modelDeploy, err
	}
	customModelDeploymentName, err := getUserDefinedModelDeploymentName(paramsMap)
	if err != nil {
		return modelDeploy, err
	}
	//amlPipelineConfig
	if err = injectPipelineConfigInfo(client,
		types.NamespacedName{Name: p.AMLPipeline.Spec.AMLPipelineConfig.Name, Namespace: p.AMLPipeline.Namespace},
		modelDeploy); err != nil {
		return modelDeploy, err
	}
	//amlPipeline
	modelDeploy.Status.PipelineInfo = mlv1alpha1.PipelineInfo{}
	modelDeploy.Status.PipelineInfo.Behavior = action
	modelDeploy.Status.PipelineInfo.LatestCreator = p.AMLPipeline.Name
	modelDeploy.Status.PipelineInfo.History = []mlv1alpha1.RelationStatus{{Name: p.AMLPipeline.Name, Exists: 1}}
	modelDeploy.Name = customModelDeploymentName
	modelDeploy.Namespace = p.Namespace
	modelServiceName := getModelServiceNameByModelDeploymentName(p.AMLPipeline.Spec.AMLPipelineConfig.Name)
	modelDeploy.Spec.ModelService.Name = modelServiceName
	modelDeploy.Spec.ModelService.AmlPipelineConfigName = p.AMLPipeline.Spec.AMLPipelineConfig.Name
	modelDeploy.Spec.DeployInfo.User = p.AMLPipeline.Spec.User

	var labelMap map[string]string
	labelMap = addModelServiceBinding(labelMap, modelServiceName)
	labelMap = addAmlPipelineBinding(labelMap, p.AMLPipeline.Name)
	modelDeploy.Labels = labelMap
	err = parseToModelDeployFromMap(modelDeploy, paramsMap)
	if err != nil {
		return &mlv1alpha1.ModelDeployment{}, err
	}
	return modelDeploy, nil
}

func injectPipelineConfigInfo(client client.Client, objectKey types.NamespacedName, modelDeployment *mlv1alpha1.ModelDeployment) error {
	amlPipelineCfg := &mlv1alpha1.AMLPipelineConfig{}
	err := client.Get(context.Background(), objectKey, amlPipelineCfg)
	if err != nil {
		return juju.Trace(err)
	}
	modelDeployment.Spec.ModelService.User = amlPipelineCfg.Spec.User
	return nil
}

func (p ModelDeploymentParser) GetModelDeploymentName() (string, error) {
	paramsMap := GetModelDeployMergedParamsGroup([]devops.PipelineTemplateArgumentGroup{}, p.AMLPipeline.Spec.Parameters)
	return getUserDefinedModelDeploymentName(paramsMap)
}

func parseToModelDeployFromMap(modelDeploy *mlv1alpha1.ModelDeployment, paramsMap map[string]devops.PipelineTemplateArgumentValue /*arguments, parameters []devops.PipelineTemplateArgumentGroup*/) error {
	var parser = NewAssembledParser()
	var err error
	//paramsMap := GetModelDeployMergedParamsGroup(arguments, parameters)
	IsNeedBuild := parser.IsNeedBuildStep(paramsMap)
	IsModelInDataMgr := parser.IsModelInPvc(paramsMap)
	modelDeploy.Spec.ImageBuild.IsNeed = IsNeedBuild
	if IsModelInDataMgr {
		modelDeploy.Spec.Source.SourceType = mlv1alpha1.ModelInDataVolume //pvc
	} else {
		modelDeploy.Spec.Source.SourceType = mlv1alpha1.ModelInRepo
	}
	if !IsNeedBuild {
		parseToModelDeployWhenSkipBuild(modelDeploy, parser)
	} else {
		parseToModelDeployWhenNeedBuild(modelDeploy, parser)
		//定义出 场景下所需的参数，如果该参数缺失，运行时会给错误提示
		if IsModelInDataMgr {
			parser.AddToNestedStringGroup(UIElementName_jenkinsNamespace, UIElementName_cluster, &modelDeploy.Spec.ImageBuild.Cluster)
			parser.AddToNestedStringGroup(UIElementName_jenkinsNamespace, UIElementName_namespace, &modelDeploy.Spec.ImageBuild.NameSpace)
			parser.AddToStringGroup(UIElementName_codePath, &modelDeploy.Spec.Source.DataVolume.Path)
		} else {
			parser.AddToStringGroup(UIElementName_Branch, &modelDeploy.Spec.Source.Repository.Branch)
			parser.AddToNestedStringGroup(UIElementName_codeRepository, UIElementName_url, &modelDeploy.Spec.Source.Repository.Url)
			parser.AddToNestedStringGroup(UIElementName_codeRepository, UIElementName_credentialId, &modelDeploy.Spec.Source.Repository.Credential.Id)
			parser.AddToStringGroup(UIElementName_relativeDirectory, &modelDeploy.Spec.Source.Repository.RelativeDirectory)
		}
	}
	if err = parser.AutoParseOrStopOnError(paramsMap); err != nil {
		return err
	}

	fmt.Printf("\nmodelName=%s\n", modelDeploy.Spec.ModelInfo.ModelName)
	defaulter(modelDeploy)
	ruler(modelDeploy)
	return nil
}

func parseToModelDeployWhenSkipBuild(modelDeploy *mlv1alpha1.ModelDeployment, parser *AssembledParser) {
	//模型信息
	parser.AddToStringGroup(UIElementName_modelName, &modelDeploy.Spec.ModelInfo.ModelName)
	parser.AddToStringGroup(UIElementName_modelVersion, &modelDeploy.Spec.ModelInfo.Version)
	parser.AddToStringGroup(UIElementName_framework, &modelDeploy.Spec.ModelInfo.Framework)
	//容器信息
	parser.AddToResourceGroup(UIElementName_containerSize, &modelDeploy.Spec.DeployInfo.Resource)
	parser.AddToInt32Group(UIElementName_instanceNumber, &modelDeploy.Spec.DeployInfo.Replica)
	parser.AddToInt32Group(UIElementName_grpcPort, &modelDeploy.Spec.DeployInfo.Port.Grpc)
	parser.AddToInt32Group(UIElementName_restfulPort, &modelDeploy.Spec.DeployInfo.Port.Restful)
	parser.AddToStringArrayGroup(UIElementName_publishCommand, &modelDeploy.Spec.DeployInfo.Command)
	parser.AddToStringArrayGroup(UIElementName_publishArgs, &modelDeploy.Spec.DeployInfo.Args)
	parser.AddToMapGroup(UIElementName_componentLabels, &modelDeploy.Spec.DeployInfo.CustomLabels)
	parser.AddToMapGroup(UIElementName_env, &modelDeploy.Spec.DeployInfo.CustomEnvParams)
	parser.AddToClusterService(UIElementName_internalNetworkInfos, &modelDeploy.Spec.DeployInfo.CustomService.ClusterServices)
	//输出镜像
	parseOutImage(modelDeploy, parser)
}

func parseToModelDeployWhenNeedBuild(modelDeploy *mlv1alpha1.ModelDeployment, parser *AssembledParser) {
	// arguments
	/**
	  common arguments setting in build step has:
	  1. base image
	  2. output image
	  3. context
	  4. buildArgs
	  5. retry (times)
	  6. timeout (seconds)
	*/
	// 构建信息
	parser.AddToNestedStringGroup(UIElementName_baseImageRepository, UIElementName_repositoryPath, &modelDeploy.Spec.ImageBuild.BaseImage)
	parser.AddToNestedStringGroup(UIElementName_baseImageRepository, UIElementName_tag, &modelDeploy.Spec.ImageBuild.BaseImageTag)
	parser.AddToStringGroup(UIElementName_context, &modelDeploy.Spec.ImageBuild.BuildContext)
	parser.AddToStringGroup(UIElementName_buildArguments, &modelDeploy.Spec.ImageBuild.BuildArguments)
	parser.AddToInt32Group(UIElementName_retry, &modelDeploy.Spec.ImageBuild.Retries)
	//parser.AddToStringGroup(UIElementName_buildTimeout, &modelDeploy.Spec.ImageBuild.Timeout)
	//模型信息
	parser.AddToStringGroup(UIElementName_modelName, &modelDeploy.Spec.ModelInfo.ModelName)
	parser.AddToStringGroup(UIElementName_modelVersion, &modelDeploy.Spec.ModelInfo.Version)
	parser.AddToStringGroup(UIElementName_framework, &modelDeploy.Spec.ModelInfo.Framework)
	//容器信息
	parser.AddToResourceGroup(UIElementName_containerSize, &modelDeploy.Spec.DeployInfo.Resource)
	parser.AddToInt32Group(UIElementName_instanceNumber, &modelDeploy.Spec.DeployInfo.Replica)
	//输出镜像
	parseOutImage(modelDeploy, parser)
}

func parseOutImage(modelDeploy *mlv1alpha1.ModelDeployment, parser *AssembledParser) {
	parser.AddToNestedStringGroup(UIElementName_OutImageRepository, UIElementName_repositoryPath, &modelDeploy.Spec.ImageRepository.Url)
	parser.AddToNestedStringGroup(UIElementName_OutImageRepository, UIElementName_tag, &modelDeploy.Spec.ImageRepository.Tag)
	parser.AddToNestedStringGroup(UIElementName_OutImageRepository, UIElementName_secretName, &modelDeploy.Spec.ImageRepository.OriginSecretName)
	parser.AddToNestedStringGroup(UIElementName_OutImageRepository, UIElementName_secretNamespace, &modelDeploy.Spec.ImageRepository.OriginSecretNamespace)
}

func defaulter(modelDeploy *mlv1alpha1.ModelDeployment) {
	// common settings here, model service default settings
	modelDeploy.Spec.DeployInfo.ServiceType = "istio"
	modelDeploy.Spec.DeployInfo.Port.Swagger = 8888
	if modelDeploy.Spec.DeployInfo.Port.Grpc == 0 {
		modelDeploy.Spec.DeployInfo.Port.Grpc = 8500
	}
	if modelDeploy.Spec.DeployInfo.Port.Restful == 0 {
		modelDeploy.Spec.DeployInfo.Port.Restful = 8501
	}
	if len(strings.TrimSpace(modelDeploy.Spec.ImageBuild.BaseImageTag)) == 0 {
		modelDeploy.Spec.ImageBuild.BaseImageTag = "latest"
	}
	if len(strings.TrimSpace(modelDeploy.Spec.ImageRepository.Tag)) == 0 {
		modelDeploy.Spec.ImageRepository.Tag = "latest"
	}
	if modelDeploy.Spec.ImageBuild.BuildContext == "" {
		modelDeploy.Spec.ImageBuild.BuildContext = "."
	}
	modelDeploy.Spec.ImageBuild.DockerfilePath = "Dockerfile"
	modelDeploy.Spec.DeployInfo.Timeout = "300" //300s
}

func ruler(modelDeploy *mlv1alpha1.ModelDeployment) {
	iRetry := modelDeploy.Spec.ImageBuild.Retries
	// max retry ==  5
	if iRetry < 0 || iRetry > 5 {
		modelDeploy.Spec.ImageBuild.Retries = 3
	}
}

func addModelServiceBinding(label map[string]string, modelServiceName string) map[string]string {
	return common.NewAmlLabel(label).Add(ModelServiceLabelKey, modelServiceName).Get()
}
func addAmlPipelineBinding(label map[string]string, pipelineName string) map[string]string {
	pipelineKey := fmt.Sprintf(BelongToPipelineKeyFormat, pipelineName)
	//return common.NewAmlLabel(label).Add(pipelineKey, strconv.FormatInt(time.Now().UnixNano(), 10)).Get()
	return common.NewAmlLabel(label).Add(pipelineKey, "").Get()
}

func getPipelineAction(paramsMap map[string]devops.PipelineTemplateArgumentValue) (string, error) {
	unknown := fmt.Errorf("unknow pipeline action,should be create/update")
	if v, ok := paramsMap[UIElementName_action]; ok {
		switch v.Value {
		case "", PipelineBehaviorCreate:
			return PipelineBehaviorCreate, nil
		case PipelineBehaviorUpdate:
			return PipelineBehaviorUpdate, nil
		default:
			return "", unknown
		}
	}
	return "", unknown
}

func getModelServiceNameByModelDeploymentName(name string) string {
	// name format likes model01-abc , we expect return service-model01
	pos := strings.Index(name, "-")
	var partName string
	if pos != -1 {
		partName = name[pos+1:]
	} else {
		partName = name
	}
	return fmt.Sprintf("service-%s", partName)
}
func getUserDefinedModelDeploymentName(paramsMap map[string]devops.PipelineTemplateArgumentValue) (string, error) {
	if v, ok := paramsMap[UIElementName_deploymentName]; ok {
		return v.Value, nil
	}
	return "", fmt.Errorf("not found %s in parameters", UIElementName_deploymentName)
}

const (
	ModelServiceLabelKey      = "model-service"
	BelongToPipelineKeyFormat = "belongs-to/%s"
)
