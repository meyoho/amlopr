package test

import (
	mlv1aplha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"encoding/json"
	"github.com/ghodss/yaml"
	"io/ioutil"
)

func LoadYaml2ModelDeploy(filePath string) (*mlv1aplha1.ModelDeployment, error) {

	var err error
	var nb mlv1aplha1.ModelDeployment
	data, err := ioutil.ReadFile(filePath)

	jsonBlob, err := yaml.YAMLToJSON(data)
	if err != nil {
		return &mlv1aplha1.ModelDeployment{}, err
	}
	err = json.Unmarshal(jsonBlob, &nb)
	if err != nil {
		return &mlv1aplha1.ModelDeployment{}, err
	}
	return &nb, nil
}
