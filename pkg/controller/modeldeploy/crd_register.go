package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	mlpipeline "alauda.io/amlopr/pkg/controller/amlpipeline"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"fmt"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"path/filepath"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"strings"
)

func ExecutionKey() string {
	return "ml.model-deploy"
}

type deployGen struct {
}

func (deployGen) ErrorCrd(clusterName string, key types.NamespacedName, amlPipeline *mlv1alpha1.AMLPipeline, err error) error {
	return nil
}

func (deployGen) GenCrd(client client.Client,
	clusterName string,
	namespacedName types.NamespacedName, //crd
	arguments []devops.PipelineTemplateArgumentGroup,
	pipeline *mlv1alpha1.AMLPipeline) (common.Object, error) {

	var err error
	parser := NewModelDeploymentParser(*pipeline, arguments, namespacedName.Namespace)
	modelDeploy, err := parser.ToModelDeployment(client)
	if err != nil {
		return modelDeploy, err
	}
	mgr := NewModelDeploymentGlobalManager(modelDeploy, nil)
	err = mgr.SyncDependentResources(client, clusterName)
	if err != nil {
		return modelDeploy, err
	}
	return modelDeploy, nil
}

func (deployGen) CancelCrd(clusterName string, key types.NamespacedName, amlPipeline *mlv1alpha1.AMLPipeline) error {
	return nil
}

func (deployGen) DeleteCrd(clusterName string, key types.NamespacedName, amlPipeline *mlv1alpha1.AMLPipeline) error {
	clientSet, err := utils.NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		if exist, existError := utils.NewBusinessMgr(clusterName).IsExistCluster(); existError != nil && exist == false {
			return nil
		}
		return juju.Trace(err)
	}
	parser := NewModelDeploymentParser(*amlPipeline, nil, key.Namespace)
	modelDeploymentName, err := parser.GetModelDeploymentName()
	if err != nil {
		return juju.Trace(err)
	}
	existModelDeployment, err := clientSet.MlV1alpha1().ModelDeployments(key.Namespace).Get(modelDeploymentName, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return juju.Trace(err)
	}
	mgr := NewModelDeploymentGlobalManager(existModelDeployment, clientSet)
	return mgr.RemoveRelationRecord(clusterName, amlPipeline.Name)
}

func (deployGen) CreateCrdNotExist(crd common.Object, clusterName string) (common.Object, error) {
	modelDeployment := crd.(*mlv1alpha1.ModelDeployment)
	mgr := NewModelDeploymentGlobalManager(modelDeployment, nil)
	return mgr.CreateOrUpdate(clusterName)
}

func getModelNameFromPath(path string) (string, error) {
	if strings.HasSuffix(path, string(filepath.Separator)) {
		path = strings.TrimRight(path, string(filepath.Separator))
	}
	folders := strings.Split(path, string(filepath.Separator))
	if len(folders) < 2 {
		return "", fmt.Errorf("model path error,should be path/to/model/version")
	}
	return folders[len(folders)-2], nil
}

func init() {
	annotation := mlv1alpha1.Annotation{Key: ExecutionKey(), Value: "true"}
	if err := mlpipeline.RegisterCrd(annotation, ExecutionKey(), deployGen{}); err != nil {
		panic(err)
	}
}
