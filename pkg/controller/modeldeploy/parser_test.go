package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"fmt"
	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"strconv"
	"strings"
	"testing"
	"time"
)

func Test_fieldsFunc(t *testing.T) {
	v := `--port=8500, --rest_api_port=8501, --model_name=half_plus_three, --model_base_path=/models/half_plus_three`
	fn := func(r rune) bool {
		if string(r) == "," || string(r) == " " {
			return true
		}
		return false
	}
	result := strings.FieldsFunc(v, fn)
	wanted := []string{
		"--port=8500",
		"--rest_api_port=8501",
		"--model_name=half_plus_three",
		"--model_base_path=/models/half_plus_three",
	}
	assert.Equal(t, wanted, result, "split by comma and remove space")
}

func Test_parseInt32(t *testing.T) {
	paramsMap := make(map[string]devops.PipelineTemplateArgumentValue)
	var parser = NewAssembledParser()
	v1 := `'1'`
	v2 := `"1"`
	v3 := `1`
	cases := []string{v1, v2, v3}
	desc := []string{"wrapped by ' ", "wrapped by \" ", "not wrapped ' or \" "}
	for i, caseValue := range cases {
		modeldeploy := mlv1alpha1.ModelDeployment{}
		paramsMap["instanceNumber"] = devops.PipelineTemplateArgumentValue{
			Value: caseValue,
		}
		parser.AddToInt32Group("instanceNumber", &modeldeploy.Spec.DeployInfo.Replica)
		err := parser.AutoParseOrStopOnError(paramsMap)
		if err != nil {
			panic(err)
		}
		assert.Equal(t, int32(1), modeldeploy.Spec.DeployInfo.Replica, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
	}
}

func Test_parseStringGroup(t *testing.T) {
	paramsMap := make(map[string]devops.PipelineTemplateArgumentValue)
	var parser = NewAssembledParser()
	v1 := `'model1'`
	v2 := `"model1"`
	v3 := `model1`
	wanted := "model1"
	cases := []string{v1, v2, v3}
	desc := []string{"wrapped by ' ", "wrapped by \" ", "not wrapped ' or \" "}
	for i, caseValue := range cases {
		modeldeploy := mlv1alpha1.ModelDeployment{}
		paramsMap["modelName"] = devops.PipelineTemplateArgumentValue{
			Value: caseValue,
		}
		parser.AddToStringGroup("modelName", &modeldeploy.Spec.ModelInfo.ModelName)
		err := parser.AutoParseOrStopOnError(paramsMap)
		if err != nil {
			panic(err)
		}
		assert.Equal(t, wanted, modeldeploy.Spec.ModelInfo.ModelName, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
	}
}

func Test_parseStringArrayGroup(t *testing.T) {
	paramsMap := make(map[string]devops.PipelineTemplateArgumentValue)
	var parser = NewAssembledParser()
	v1 := `'--port=8500, --rest_api_port=8501, --model_name=half_plus_three, --model_base_path=/models/half_plus_three'`
	v2 := `"--port=8500, --rest_api_port=8501, --model_name=half_plus_three, --model_base_path=/models/half_plus_three"`
	v3 := `--port=8500, --rest_api_port=8501, --model_name=half_plus_three, --model_base_path=/models/half_plus_three`
	wanted := []string{
		"--port=8500",
		"--rest_api_port=8501",
		"--model_name=half_plus_three",
		"--model_base_path=/models/half_plus_three",
	}
	cases := []string{v1, v2, v3}
	desc := []string{"wrapped by ' ", "wrapped by \" ", "not wrapped ' or \" "}
	for i, caseValue := range cases {
		modeldeploy := mlv1alpha1.ModelDeployment{}
		paramsMap["publishArgs"] = devops.PipelineTemplateArgumentValue{
			Value: caseValue,
		}
		parser.AddToStringArrayGroup("publishArgs", &modeldeploy.Spec.DeployInfo.Args)
		err := parser.AutoParseOrStopOnError(paramsMap)
		if err != nil {
			panic(err)
		}
		assert.Equal(t, wanted, modeldeploy.Spec.DeployInfo.Args, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
	}

}

func Test_parseNestedStringGroup(t *testing.T) {
	paramsMap := make(map[string]devops.PipelineTemplateArgumentValue)
	var parser = NewAssembledParser()
	id := "global-credentials-ddddddd"
	url := "http://62.234.104.184:31101/david/amltest"

	v1 := `'{"bindingRepositoryName":"gitlab-enterprise-david-amltest","url":"http://62.234.104.184:31101/david/amltest","credentialId":"global-credentials-ddddddd","sourceType":"GIT","secret":{"namespace":"global-credentials","name":"ddddddd"},"kind":"select"}'`
	v2 := `"{"bindingRepositoryName":"gitlab-enterprise-david-amltest","url":"http://62.234.104.184:31101/david/amltest","credentialId":"global-credentials-ddddddd","sourceType":"GIT","secret":{"namespace":"global-credentials","name":"ddddddd"},"kind":"select"}"`
	v3 := `{"bindingRepositoryName":"gitlab-enterprise-david-amltest","url":"http://62.234.104.184:31101/david/amltest","credentialId":"global-credentials-ddddddd","sourceType":"GIT","secret":{"namespace":"global-credentials","name":"ddddddd"},"kind":"select"}`
	cases := []string{v1, v2, v3}
	desc := []string{"wrapped by ' ", "wrapped by \" ", "not wrapped ' or \" "}
	for i, caseValue := range cases {
		modeldeploy := mlv1alpha1.ModelDeployment{}
		paramsMap["codeRepository"] = devops.PipelineTemplateArgumentValue{
			Value: caseValue,
		}
		parser.AddToNestedStringGroup("codeRepository", "credentialId", &modeldeploy.Spec.Source.Repository.Credential.Id)
		parser.AddToNestedStringGroup("codeRepository", "url", &modeldeploy.Spec.Source.Repository.Url)
		err := parser.AutoParseOrStopOnError(paramsMap)
		if err != nil {
			panic(err)
		}
		assert.Equal(t, id, modeldeploy.Spec.Source.Repository.Credential.Id, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
		assert.Equal(t, url, modeldeploy.Spec.Source.Repository.Url, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
	}
}
func Test_parseNestedStringGroup_outImage(t *testing.T) {
	paramsMap := make(map[string]devops.PipelineTemplateArgumentValue)
	var parser = NewAssembledParser()
	secretName := "pinjufy"
	secretNamespace := "test"
	url := "index.alauda.cn/alaudaorg/testcodemix"
	tag := "create1"

	v1 := `'{"credentialId":"test-pinjufy","repositoryPath":"index.alauda.cn/alaudaorg/testcodemix","type":"input","tag":"create1","secretName":"pinjufy","secretNamespace":"test"}'`
	v2 := `{"credentialId":"test-pinjufy","repositoryPath":"index.alauda.cn/alaudaorg/testcodemix","type":"input","tag":"create1","secretName":"pinjufy","secretNamespace":"test"}`
	cases := []string{v1, v2}
	desc := []string{"wrapped by ' ", "wrapped by \" ", "not wrapped ' or \" "}
	for i, caseValue := range cases {
		modeldeploy := mlv1alpha1.ModelDeployment{}
		paramsMap[UIElementName_OutImageRepository] = devops.PipelineTemplateArgumentValue{
			Value: caseValue,
		}
		parser.AddToNestedStringGroup(UIElementName_OutImageRepository, UIElementName_repositoryPath, &modeldeploy.Spec.ImageRepository.Url)
		parser.AddToNestedStringGroup(UIElementName_OutImageRepository, UIElementName_tag, &modeldeploy.Spec.ImageRepository.Tag)
		parser.AddToNestedStringGroup(UIElementName_OutImageRepository, UIElementName_secretName, &modeldeploy.Spec.ImageRepository.OriginSecretName)
		parser.AddToNestedStringGroup(UIElementName_OutImageRepository, UIElementName_secretNamespace, &modeldeploy.Spec.ImageRepository.OriginSecretNamespace)

		err := parser.AutoParseOrStopOnError(paramsMap)
		if err != nil {
			panic(err)
		}
		assert.Equal(t, secretName, modeldeploy.Spec.ImageRepository.OriginSecretName, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
		assert.Equal(t, secretNamespace, modeldeploy.Spec.ImageRepository.OriginSecretNamespace, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
		assert.Equal(t, url, modeldeploy.Spec.ImageRepository.Url, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
		assert.Equal(t, tag, modeldeploy.Spec.ImageRepository.Tag, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
	}
}

func Test_parseResourceGroup(t *testing.T) {
	paramsMap := make(map[string]devops.PipelineTemplateArgumentValue)
	var parser = NewAssembledParser()
	cpu := resource.MustParse("2")
	mem := resource.MustParse("400M")
	gpu := resource.MustParse("1")
	wanted := mlv1alpha1.ModelDeploymentResource{
		Limit: map[corev1.ResourceName]resource.Quantity{
			corev1.ResourceCPU:    cpu,
			corev1.ResourceMemory: mem,
			"nvidia.com/gpu":      gpu,
		},
	}
	//v1 := "{\"hard\":{\"limits.cpu\":1,\"limits.nvidia.com/gpu\":null,\"limits.memory\":null}}"
	v1 := `{"hard": {"limits.cpu":"2","limits.memory":"400M","limits.nvidia.com/gpu":"1"}}`
	v2 := `{"hard":{"limits.cpu":"2","limits.memory":"400M","limits.nvidia.com/gpu":"1"}}`
	v3 := `{"hard":{"limits.cpu":"2","limits.memory":"400M","limits.nvidia.com/gpu":1}}`
	v4 := `{"hard":{"req.cpu":"1","req.memory":"200M","req.nvidia.com/gpu":0,"limits.cpu":"2","limits.memory":"400M","limits.nvidia.com/gpu":"1"}}`
	v5 := `'{"hard":{"req.cpu":"0","req.nvidia.com/gpu":"0","req.memory":"400Mi","limits.cpu":"2","limits.nvidia.com/gpu":"1","limits.memory":"400M"}}'`
	cases := []string{v1, v2, v3, v4, v5}
	for i, caseValue := range cases {
		fmt.Println(i)
		modeldeploy := mlv1alpha1.ModelDeployment{}
		paramsMap["containerSize"] = devops.PipelineTemplateArgumentValue{
			Value: caseValue,
		}
		parser.AddToResourceGroup("containerSize", &modeldeploy.Spec.DeployInfo.Resource)
		err := parser.AutoParseOrStopOnError(paramsMap)
		if err != nil {
			panic(err)
		}
		assert.Equal(t, wanted.Limit, modeldeploy.Spec.DeployInfo.Resource.Limit)
	}
}

func Test_parseStringMapGroup(t *testing.T) {
	paramsMap := make(map[string]devops.PipelineTemplateArgumentValue)
	var parser = NewAssembledParser()
	wanted := map[string]string{
		"label1": "v1",
		"label2": "v2",
	}
	v1 := `'{"label1":"v1","label2":"v2"}'`
	v2 := `"{"label1":"v1","label2":"v2"}"`
	v3 := `{"label1":"v1","label2":"v2"}`
	cases := []string{v1, v2, v3}
	desc := []string{"wrapped by ' ", "wrapped by \" ", "not wrapped ' or \" "}
	for i, caseValue := range cases {
		modeldeploy := mlv1alpha1.ModelDeployment{}
		paramsMap["componentLabels"] = devops.PipelineTemplateArgumentValue{
			Value: caseValue,
		}
		parser.AddToMapGroup("componentLabels", &modeldeploy.Spec.DeployInfo.CustomLabels)
		err := parser.AutoParseOrStopOnError(paramsMap)
		if err != nil {
			panic(err)
		}
		assert.Equal(t, wanted, modeldeploy.Spec.DeployInfo.CustomLabels, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
	}
}

func Test_parseClusterServiceGroup(t *testing.T) {
	paramsMap := make(map[string]devops.PipelineTemplateArgumentValue)
	var parser = NewAssembledParser()
	wanted := []mlv1alpha1.ClusterIpInfo{
		{
			ServiceName: "port-fy",
			Protocol:    "TCP",
			SourcePort:  int32(9988),
			TargetPort:  int32(7766),
		},
	}
	v1 := `'[{"serviceName": "port-fy","protocol": "TCP","sourcePort": 9988,"targetPort": 7766}]'`
	v2 := `"[{"serviceName": "port-fy","protocol": "TCP","sourcePort": 9988,"targetPort": 7766}]"`
	v3 := `[{"serviceName": "port-fy","protocol": "TCP","sourcePort": 9988,"targetPort": 7766}]`
	cases := []string{v1, v2, v3}
	desc := []string{"wrapped by ' ", "wrapped by \" ", "not wrapped ' or \" "}
	for i, caseValue := range cases {
		modeldeploy := mlv1alpha1.ModelDeployment{}
		paramsMap["internalNetworkInfos"] = devops.PipelineTemplateArgumentValue{
			Value: caseValue,
		}
		parser.AddToClusterService("internalNetworkInfos", &modeldeploy.Spec.DeployInfo.CustomService.ClusterServices)
		err := parser.AutoParseOrStopOnError(paramsMap)
		if err != nil {
			panic(err)
		}
		assert.Equal(t, wanted, modeldeploy.Spec.DeployInfo.CustomService.ClusterServices, fmt.Sprintf("parseMapGroup %v, should be equal", desc[i]))
	}
}

func Test_stringEqual(t *testing.T) {
	svcRoute := `---
apiVersion: ambassador/v0
kind:  Mapping
name:  $_mapping_name_
prefix: $_prefix_
rewrite: $_rewrite_
service: $_target_svc_.$_target_ns_:$_svc_port_
timeout_ms: 300000
use_websocket: true
`
	var sArr = []string{
		"---",
		"apiVersion: ambassador/v0",
		"kind:  Mapping",
		"name:  $_mapping_name_",
		"prefix: $_prefix_",
		"rewrite: $_rewrite_",
		"service: $_target_svc_.$_target_ns_:$_svc_port_",
		"timeout_ms: 300000",
		"use_websocket: true", "",
	}
	assert.Equal(t, svcRoute, strings.Join(sArr, "\n"))
}

func Test_mergeArgumentParas(t *testing.T) {
	var isBuild = "true"
	arguments := []devops.PipelineTemplateArgumentGroup{
		{
			Items: []devops.PipelineTemplateArgumentValue{
				{
					PipelineTemplateArgument: devops.PipelineTemplateArgument{
						Name: UIElementName_needImageBuild,
					},
					Value: isBuild,
				},
				{
					PipelineTemplateArgument: devops.PipelineTemplateArgument{
						Name: "key1",
					},
					Value: "1",
				},
				{
					PipelineTemplateArgument: devops.PipelineTemplateArgument{
						Name: UIElementName_OutImageRepository,
					},
					Value: "outimage-argument",
				},
			},
		},
	}
	paramsMap := []devops.PipelineTemplateArgumentGroup{
		{
			Items: []devops.PipelineTemplateArgumentValue{
				{
					PipelineTemplateArgument: devops.PipelineTemplateArgument{
						Name: "key1",
					},
					Value: "2",
				},
				{
					PipelineTemplateArgument: devops.PipelineTemplateArgument{
						Name: UIElementName_OutImageRepository,
					},
					Value: "outimage-paramter",
				},
			},
		},
	}

	expected := map[string]devops.PipelineTemplateArgumentValue{
		UIElementName_needImageBuild: {
			PipelineTemplateArgument: devops.PipelineTemplateArgument{
				Name: UIElementName_needImageBuild,
			},
			Value: isBuild,
		},
		"key1": {
			PipelineTemplateArgument: devops.PipelineTemplateArgument{
				Name: "key1",
			},
			Value: "2",
		},
		UIElementName_OutImageRepository: {
			PipelineTemplateArgument: devops.PipelineTemplateArgument{
				Name: UIElementName_OutImageRepository,
			},
			Value: "outimage-paramter",
		},
	}
	//var parser = NewAssembledParser()
	actual := GetModelDeployMergedParamsGroup(arguments, paramsMap)
	assert.Equal(t, expected, actual, "should be equal.")
}

func Test_label(t *testing.T) {
	//md := &mlv1alpha1.ModelDeployment{}
	//assert.Nil(t, md.Labels)
	//m := common.NewAmlLabel(md.Labels).Get()
	//
	//println(len(m))

	map1 := map[string]string{"k1": "v1"}

	fmt.Printf("p of map1 = %p\n", map1)
	map2 := common.NewAmlLabel(map1).Get()
	fmt.Printf("p of map2 = %p\n", map2)
	//var labels map[string]string
	//map3:= common.NewAmlLabel(labels).Get()

	fmt.Println(time.Now().Unix())
	fmt.Println(time.Now().Unix())
	fmt.Println(time.Now().UnixNano())
	fmt.Println(strconv.FormatInt(time.Now().UnixNano(), 10))

}

func Test_conv(t *testing.T) {
	//a, err := strconv.Atoi("30")
	//if err != nil {
	//	panic(err)
	//}
	//fmt.Println(a)
	time1 := time.Now()
	fmt.Printf("time1 = %s\n", time1)
	time2 := time1.Add(time.Duration(6) * time.Second)
	fmt.Printf("time2 = %s\n", time2)
	//time.Sleep(time.Duration(3) * time.Second)
	for true {
		if time.Now().Before(time2) {
			fmt.Println("wait.", time.Now())
			time.Sleep(time.Duration(1) * time.Second)
		} else {
			break
		}
	}
	fmt.Println(time.Now())

}
func Test_Conv1(t *testing.T) {

	list2 := []string{}

	list2 = append(list2, "1")

	fmt.Println(list2)

}
