/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	"github.com/kubernetes-sigs/application/pkg/apis/app/v1beta1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/util/retry"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var ctlName = "modelDeployment-controller"
var log = logf.Log.WithName(ctlName)

const (
	ComponentName   = "model-deploy"
	LabelCreatorKey = "model-deploy"
)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new ModelDeploy Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileModelDeploy{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New(ctlName, mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to ModelDeploy
	err = c.Watch(&source.Kind{Type: &mlv1alpha1.ModelDeployment{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	p := common.GetPredicateFuncs(mlv1alpha1.LabelAMLComponentKey(), ComponentName)

	// deploy
	err = c.Watch(&source.Kind{Type: &appsv1.Deployment{}}, &handler.EnqueueRequestsFromMapFunc{
		ToRequests: common.GetMapFnForLabelKey(common.GetLabelKeyForCreatorOf(LabelCreatorKey)),
	}, p)
	if err != nil {
		return err
	}
	//pod
	err = c.Watch(&source.Kind{Type: &corev1.Pod{}}, &handler.EnqueueRequestsFromMapFunc{
		ToRequests: common.GetMapFnForLabelKey(common.GetLabelKeyForCreatorOf(LabelCreatorKey)),
	}, p)
	if err != nil {
		return err
	}
	//svc
	err = c.Watch(&source.Kind{Type: &corev1.Service{}}, &handler.EnqueueRequestsFromMapFunc{
		ToRequests: common.GetMapFnForLabelKey(common.GetLabelKeyForCreatorOf(LabelCreatorKey)),
	}, p)
	if err != nil {
		return err
	}
	// application
	err = c.Watch(&source.Kind{Type: &v1beta1.Application{}}, &handler.EnqueueRequestsFromMapFunc{
		ToRequests: common.GetMapFnForLabelKey(common.GetLabelKeyForCreatorOf(LabelCreatorKey)),
	}, p)
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileModelDeploy{}

// ReconcileModelDeploy reconciles a ModelDeploy object
type ReconcileModelDeploy struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a ModelDeploy object and makes changes based on the state read
// and what is in the ModelDeploy.Spec
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=deployments/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=ml.alauda.io,resources=modeldeploys,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=ml.alauda.io,resources=modeldeploys/status,verbs=get;update;patch
func (r *ReconcileModelDeploy) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	log.Info("start Reconcile")
	log.Info("request::", "Namespace:", request.Namespace, "Name:", request.Name)
	defer log.Info("Over Reconcile")
	return reconcile.Result{}, r.Dispatch(request)
}

func (r *ReconcileModelDeploy) Dispatch(request reconcile.Request) error {
	var err error
	instance := &mlv1alpha1.ModelDeployment{}
	if err := common.ValidateRequest(r.Client, request.NamespacedName, instance); err != nil {
		return juju.Trace(err)
	}
	var instanceCopy = instance.DeepCopy()

	if !instanceCopy.DeletionTimestamp.IsZero() {
		SetDeletingState(instanceCopy)
	}
	switch instanceCopy.Status.State {
	case mlv1alpha1.StateModelDeploymentDeploying, mlv1alpha1.StateModelDeploymentDeploySucceed, mlv1alpha1.StateModelDeploymentUpdating:
		if err = r.syncManagedResources(instanceCopy); err != nil {
			return juju.Trace(err)
		}
	case mlv1alpha1.StateModelDeploymentRedeploying:
		if err = r.reDeployManagedResources(instanceCopy); err != nil {
			return juju.Trace(err)
		}
	case mlv1alpha1.StateModelDeploymentDeleting:
		if err = r.deleteRelatedResources(instanceCopy); err != nil {
			return juju.Trace(err)
		}
	case mlv1alpha1.StateModelDeploymentBuilding,
		mlv1alpha1.StateModelDeploymentBuildError,
		mlv1alpha1.StateModelDeploymentDeployFailed:
		return nil
	}
	if err = r.UpdateModelDeploymentStatus(instance.Status.State, instanceCopy); err != nil {
		return juju.Trace(err)
	}
	return nil
}
func needFillBackState(reconcileState mlv1alpha1.ModelDeploymentState) bool {
	switch reconcileState {
	case mlv1alpha1.StateModelDeploymentDeploySucceed, mlv1alpha1.StateModelDeploymentUpdating:
		return true
	default:
		return false
	}
}

func (r *ReconcileModelDeploy) UpdateModelDeploymentStatus(reconcileState mlv1alpha1.ModelDeploymentState, instanceCopy *mlv1alpha1.ModelDeployment) (err error) {
	appMgr := NewApplicationMgr(instanceCopy, r)
	if err = appMgr.SetOwnerReference(instanceCopy); err != nil {
		return juju.Trace(err)
	}
	if needFillBackState(reconcileState) {
		deployMgr := NewDeploymentMgr(instanceCopy, r)
		deployMgr.UpdateStatusOfDeployment(instanceCopy)

		modelSvcMgr := NewServiceMgr(instanceCopy, r)
		modelSvcMgr.UpdateStatusOfService(instanceCopy)
		if apiErr := modelSvcMgr.UpdateStatusOfApi(instanceCopy); apiErr != nil {
			log.Error(apiErr, "")
		}
		computeModelDeploymentState(instanceCopy)
	}
	retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
		return r.Client.Update(context.Background(), instanceCopy)
	})
	if retryErr != nil {
		return juju.Trace(retryErr)
	}
	return nil
}

func computeModelDeploymentState(instanceCopy *mlv1alpha1.ModelDeployment) {
	if instanceCopy.Status.ManagedResourceResult.ServiceResult.Ready &&
		instanceCopy.Status.ManagedResourceResult.DeploymentResult.Ready {
		instanceCopy.Status.State = mlv1alpha1.StateModelDeploymentRunning
		return
	}
}

func (r *ReconcileModelDeploy) syncManagedResources(modelDeployment *mlv1alpha1.ModelDeployment) error {
	appMgr := NewApplicationMgr(modelDeployment, r)
	appErr := appMgr.Apply()

	deploymentMgr := NewDeploymentMgr(modelDeployment, r)
	deploymentErr := deploymentMgr.Apply()

	svcMgr := NewServiceMgr(modelDeployment, r)
	svcErr := svcMgr.Apply()
	swaggerSvcErr := svcMgr.ApplySwaggerService()
	var errorMsg error
	var hasError bool
	if appErr != nil || deploymentErr != nil || svcErr != nil || swaggerSvcErr != nil {
		hasError = true
		errorMsg = fmt.Errorf("%s", "deploy part of Modeldeplyment error")
	}
	if hasError {
		// set sync failed state
		msg := juju.ErrorStack(errorMsg)
		SetFailedState(modelDeployment, msg, msg)
		return juju.Trace(errorMsg)
	}
	// set sync succeed state
	SetSucceedState(modelDeployment)
	return nil
}
func (r *ReconcileModelDeploy) reDeployManagedResources(modelDeployment *mlv1alpha1.ModelDeployment) error {
	deploymentMgr := NewDeploymentMgr(modelDeployment, r)
	deploymentErr := deploymentMgr.Delete()

	var errorMsg error
	if deploymentErr != nil {
		// set update failed state
		errorMsg = fmt.Errorf("%s", "redeploy part of Modeldeplyment error")
		msg := juju.ErrorStack(errorMsg)
		SetFailedState(modelDeployment, msg, msg)
		return juju.Trace(errorMsg)
	}
	// set update succeed state
	SetSucceedState(modelDeployment)
	return nil
}

func (r *ReconcileModelDeploy) deleteRelatedResources(modelDeployment *mlv1alpha1.ModelDeployment) error {
	modelDeploymentMgr := NewModelDeploymentManager(modelDeployment, r)
	empty, err := modelDeploymentMgr.IsModelServiceEmpty()
	if err != nil {
		return juju.Trace(err)
	}
	if !empty {
		modelDeploymentMgr.RemoveFinalizer()
		return nil
	}
	appMgr := NewApplicationMgr(modelDeployment, r)
	err = appMgr.Delete()
	if err != nil {
		return juju.Trace(err)
	}
	modelDeploymentMgr.RemoveFinalizer()
	return nil
}

func setState(notebook *mlv1alpha1.ModelDeployment, state mlv1alpha1.ModelDeploymentState) {
	notebook.Status.State = state

}
func setMessage(notebook *mlv1alpha1.ModelDeployment, reason, message string) {
	notebook.Status.Message = message
	notebook.Status.Reason = reason
}

func SetSucceedState(modelDeployment *mlv1alpha1.ModelDeployment) {
	setState(modelDeployment, mlv1alpha1.StateModelDeploymentDeploySucceed)
	setMessage(modelDeployment, "", "")
}
func SetFailedState(modelDeployment *mlv1alpha1.ModelDeployment, reason, message string) {
	setMessage(modelDeployment, reason, message)
}
func SetDeletingState(modelDeployment *mlv1alpha1.ModelDeployment) {
	setState(modelDeployment, mlv1alpha1.StateModelDeploymentDeleting)
}
