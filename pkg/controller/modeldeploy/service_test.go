package modeldeploy

import (
	"fmt"
	"testing"
)

func Test_GenAmbassadorConfig(t *testing.T) {

	ModelName := "cat-prediction"
	ModelVersion := "12"
	namespace := "default"
	lvl1mappingName := fmt.Sprintf("%v-%v-router", ModelName, ModelVersion)
	lvl1prefix := fmt.Sprintf("/model/service/%v/%v/%s/", namespace, ModelName, ModelVersion)
	lvl1Cfg := AmbassadorCfg{
		MappingName:      lvl1mappingName,
		Prefix:           lvl1prefix,
		ReWrite:          "/",
		ServiceName:      "serviceSwagger",
		ServicePort:      "8888",
		ServiceNamespace: namespace,
		TimeoutMs:        300000,
	}

	_ = "/model/service/default/cat-prediction/20/"
	_ = "/alauda/v1/models/cat-prediction"

	lvl2mappingName := fmt.Sprintf("%v-%v-operations", ModelName, ModelVersion)
	lvl2prefix := fmt.Sprintf("/alauda/v1/models/%v", ModelName)
	lvl2Cfg := AmbassadorCfg{
		MappingName:      lvl2mappingName,
		Prefix:           lvl2prefix,
		ReWrite:          lvl2prefix,
		ServiceName:      "serviceSwagger",
		ServicePort:      "8888",
		ServiceNamespace: namespace,
		TimeoutMs:        300000,
	}
	fmt.Print(fmt.Sprintf("%s%s", GenAmbassadorConfigForService(lvl1Cfg), GenAmbassadorConfigForService(lvl2Cfg)))

}
