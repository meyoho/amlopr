package modeldeploy

import (
	"alauda.io/amlopr/pkg/controller/utils"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func GetSyncedSecretName(name string) string {
	return fmt.Sprintf("%s%s", PrefixOfSyncedSecret, name)
}
func copySecretToBusiness(secretName, secretNamespace string, globalClient client.Client, namespace string, clusterName string) (*v1.Secret, error) {
	secret := &v1.Secret{}
	secretKey := utils.NewNamespacedName(secretName, secretNamespace)
	err := globalClient.Get(context.TODO(), secretKey, secret)
	if err != nil {
		return nil, juju.Trace(err)
	}
	businessSecret := &v1.Secret{}
	businessSecret.Name = GetSyncedSecretName(secret.Name)
	businessSecret.Namespace = namespace
	businessSecret.Data = secret.Data
	businessSecret.Type = secret.Type
	clientSet, err := utils.NewBusinessMgr(clusterName).K8sClient()
	existed, err := clientSet.CoreV1().Secrets(businessSecret.Namespace).Get(businessSecret.Name, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			return clientSet.CoreV1().Secrets(businessSecret.Namespace).Create(businessSecret)
		}
		return nil, juju.Trace(err)
	}
	existed.Data = businessSecret.Data
	return clientSet.CoreV1().Secrets(businessSecret.Namespace).Update(existed)
}
