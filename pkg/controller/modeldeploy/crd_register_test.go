package modeldeploy

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func Test_getModelNameFromPath(t *testing.T) {
	orgStr := "docker/half_plus_three/version1"
	expected := "half_plus_three"
	got, err := getModelNameFromPath(orgStr)
	assert.NoError(t, err)
	assert.Equal(t, expected, got)

	orgStr2 := "docker/half_plus_three/version1/"
	expected2 := "half_plus_three"
	got2, err := getModelNameFromPath(orgStr2)
	assert.NoError(t, err)
	assert.Equal(t, expected2, got2)

	orgStr3 := "half_plus_three/version1/"
	expected3 := "half_plus_three"
	got3, err := getModelNameFromPath(orgStr3)
	assert.NoError(t, err)
	assert.Equal(t, expected3, got3)

	orgStr4 := "half_plus_three/"
	expected4 := ""
	got4, err := getModelNameFromPath(orgStr4)
	assert.Errorf(t, err, "model path error,should be path/to/model/version")
	assert.Equal(t, expected4, got4)

}

func Test_encodeContainerSizeStr(t *testing.T) {
	res := ModelDeploymentResourceDesc{
		Request: GpuResource{
			Cpu:    "1",
			Memory: "200M",
			Gpu:    "{nvidia.com/gpu,0}",
		},
		Limit: GpuResource{
			Cpu:    "2",
			Memory: "400M",
			Gpu:    "{nvidia.com/gpu,1}",
		},
	}
	wanted := `{"request":{"cpu":"1","memory":"200M","gpu":"{nvidia.com/gpu,0}"},"limit":{"cpu":"2","memory":"400M","gpu":"{nvidia.com/gpu,1}"}}`
	buf := &bytes.Buffer{}
	if err := json.NewEncoder(buf).Encode(res); err != nil {
		panic(err)
	}
	assert.Equal(t, wanted, strings.Replace(buf.String(), "\n", "", -1), "")
}
