package modeldeploy

import "alauda.io/amlopr/pkg/config"

const (
	UIElementName_needImageBuild = "needImageBuild"
	UIElementName_code           = "code"
	// 构建镜像 begin
	//数据管理
	UIElementName_codePath = "codePath"
	//代码仓库
	UIElementName_Branch            = "Branch"
	UIElementName_codeRepository    = "codeRepository"
	UIElementName_url               = "url"
	UIElementName_credentialId      = "credentialId"
	UIElementName_relativeDirectory = "relativeDirectory"

	//基础镜像
	UIElementName_baseImageRepository = "baseImageRepository"
	UIElementName_repositoryPath      = "repositoryPath"
	UIElementName_tag                 = "tag"

	UIElementName_context          = "context"
	UIElementName_buildArguments   = "buildArguments"
	UIElementName_retry            = "retry"
	UIElementName_jenkinsNamespace = "jenkinsNamespace"
	UIElementName_cluster          = "cluster"
	UIElementName_namespace        = "namespace"

	// 构建镜像 end

	//模型信息
	UIElementName_modelName    = "modelName"
	UIElementName_modelVersion = "modelVersion"
	UIElementName_framework    = "framework"
	//容器信息
	UIElementName_containerSize        = "containerSize"
	UIElementName_instanceNumber       = "instanceNumber"
	UIElementName_grpcPort             = "grpcPort"
	UIElementName_restfulPort          = "restfulPort"
	UIElementName_publishCommand       = "publishCommand"
	UIElementName_publishArgs          = "publishArgs"
	UIElementName_componentLabels      = "componentLabels"
	UIElementName_env                  = "env"
	UIElementName_internalNetworkInfos = "internalNetworkInfos"

	//
	UIElementName_OutImageRepository = "outImageRepository"
	UIElementName_secretName         = "secretName"
	UIElementName_secretNamespace    = "secretNamespace"
	//
	UIElementName_action         = "action"
	UIElementName_deploymentName = "deploymentName"
)
const (
	AmbassadorKeyConfig        = "getambassador.io/config"
	PrefixOfSyncedSecret       = "ml-copy-"
	SwaggerImageKeyInConfigMap = "ModelSwaggerUIImage"
	PipelineBehaviorCreate     = "create"
	PipelineBehaviorUpdate     = "update"
)

func AnnotationAppCreateUser() string {
	return "ml." + config.GetDomain() + "/user"
}
