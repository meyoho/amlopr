package modeldeploy

import (
	"strings"
	"testing"
)

func Test_extract_gpu(t *testing.T) {
	var gpustr1 = "{'nvidia.com/gpu','1'}"
	gpustr1 = strings.Replace(gpustr1, "'", "", -1)
	if strings.Contains(gpustr1, "{") {
		gpustr1 = strings.Replace(gpustr1, "{", "", -1)
	}
	if strings.Contains(gpustr1, "}") {
		gpustr1 = strings.Replace(gpustr1, "}", "", -1)
	}
	gpukv := strings.Split(gpustr1, ",")
	println(gpukv[0], gpukv[1])
}

//func Test_getImagePullSecrets(t *testing.T) {
//	org := "default-dockerpush"
//	expected := "dockerpush"
//	assert.Equal(t, expected, getImagePullSecrets(org), "it should be equal.")
//
//	nothasstr := "dockerpush"
//	assert.Equal(t, expected, getImagePullSecrets(nothasstr), "it should be equal.")
//}

func Test_extractResource(t *testing.T) {
	/*
		gpu1Res := GpuResource{Cpu: "1", Memory: "512Mi", Gpu: "{'nvidia.com/gpu','1'}"}
		gpuMinRes := GpuResource{Cpu: "0.5", Memory: "256Mi", Gpu: "{'nvidia.com/gpu','0'}"}
		gpuMaxRes := GpuResource{Cpu: "2", Memory: "1024Mi", Gpu: "{'nvidia.com/gpu','5'}"}
		gpuEmptyRes := GpuResource{Cpu: "", Memory: "", Gpu: ""}
		var req, limit map[corev1.ResourceName]resource.Quantity
		var extractErr error

		// req empty
		req, limit, extractErr = extractResource(mlv1alpha1.ModelDeploymentResource{Request: gpuEmptyRes, Limit: gpuEmptyRes})
		assert.Error(t, extractErr, "request resource is required")
		// limit empty
		req, limit, extractErr = extractResource(mlv1alpha1.ModelDeploymentResource{Request: gpu1Res, Limit: gpuEmptyRes})
		assert.NoError(t, extractErr, "limit resource is optional")
		assert.Equal(t, req[corev1.ResourceMemory], limit[corev1.ResourceMemory], "when limit is empty , limit will equal request")
		assert.Equal(t, req[corev1.ResourceCPU], limit[corev1.ResourceCPU], "when limit is empty , limit will equal request")

		// limit values exists
		// limit <= req , real limit value should use request's
		req, limit, extractErr = extractResource(mlv1alpha1.ModelDeploymentResource{Request: gpu1Res, Limit: gpuMinRes})
		assert.NoError(t, extractErr)
		assert.Equal(t, req[corev1.ResourceMemory], limit[corev1.ResourceMemory], "when limit is less than req, limit will equal request")
		assert.Equal(t, req[corev1.ResourceCPU], limit[corev1.ResourceCPU], "when limit is less than req, limit will equal request")
		// gpu value should use bigger one
		assert.Equal(t, limit[mlv1alpha1.NvidiaGPUResourceType], resource.MustParse("1"))

		// limit > req , real limit value should take effect
		req, limit, extractErr = extractResource(mlv1alpha1.ModelDeploymentResource{Request: gpu1Res, Limit: gpuMaxRes})
		assert.NoError(t, extractErr)
		assert.NotEqual(t, req[corev1.ResourceMemory], limit[corev1.ResourceMemory], "when limit is greater than req, limit will not equal request")
		assert.NotEqual(t, req[corev1.ResourceCPU], limit[corev1.ResourceCPU], "when limit is less greater req, limit will not equal request")
		assert.Equal(t, limit[mlv1alpha1.NvidiaGPUResourceType], resource.MustParse("5"))

	*/
}
