package modeldeploy

import (
	"alauda.io/amlopr/pkg/config"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	"strings"
)

/**
    //----------------------------------
    application rules:
    acp ace application 的label 标签

    app.alauda.io/name: nginx.aml-m
    service.alauda.io/name: deploy-nginx-aml-m

	service mesh 的service 也用这两个标签。
	///// 下边是实例

	1）service 所需要的label 有：
    app.alauda.io/name: nginx.aml-m
    service.alauda.io/name: deploy-nginx-aml-m

	2）deploy labels 有：
    app.alauda.io/name: nginx.aml-m
    service.alauda.io/name: deploy-nginx-aml-m

	3) pod labels 有：
    app.alauda.io/name: nginx.aml-m
    service.alauda.io/name: deploy-nginx-aml-m
	app: nginx
	version: v1
	//----------------------------------
	service mesh rules:
	"version:<v1>" and "app:<app-name>"

*/

// service mesh requirements start
type IstioFeatures struct {
	httpServicePrefix string
	grpcServicePrefix string
	appPrefix         string
	versionPrefix     string
}

func NewIstioSpecBuilder() *IstioFeatures {

	return &IstioFeatures{
		httpServicePrefix: "http-",
		grpcServicePrefix: "grpc-",
		appPrefix:         "app",
		versionPrefix:     "version",
	}
}

func (f *IstioFeatures) BuildLabels(appName, version string) map[string]string {
	return map[string]string{
		f.appPrefix:     appName,
		f.versionPrefix: version,
	}
}

func (f *IstioFeatures) EnsureServiceSpec(svcSpec *corev1.ServiceSpec) {
	for i, port := range svcSpec.Ports {
		if strings.Contains(port.Name, "grpc") {
			if !strings.HasSuffix(port.Name, f.grpcServicePrefix) {
				svcSpec.Ports[i].Name = f.grpcServicePrefix + port.Name
			}
		} else {
			if !strings.HasSuffix(port.Name, f.httpServicePrefix) {
				svcSpec.Ports[i].Name = f.httpServicePrefix + port.Name
			}
		}
	}
}

type AlaudaAppSpecBuilder struct {
	appNameAsLabelKey     string
	serviceNameAsLabelKey string
}

func NewAlaudaAppBuilder() *AlaudaAppSpecBuilder {
	return &AlaudaAppSpecBuilder{
		appNameAsLabelKey:     "app." + config.GetDomain() + "/name",
		serviceNameAsLabelKey: "service." + config.GetDomain() + "/name",
	}
}
func (app *AlaudaAppSpecBuilder) BuildLabels(appName, namespace string) map[string]string {
	m := make(map[string]string)
	m[app.appNameAsLabelKey] = fmt.Sprintf("%s.%s", appName, namespace)
	m[app.serviceNameAsLabelKey] = fmt.Sprintf("deploy-%s-%s", appName, namespace)
	return m
}
