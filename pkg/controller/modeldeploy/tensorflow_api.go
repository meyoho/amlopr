package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"fmt"
	"strings"
)

func (mgr *ModelDeployService) UpdateStatusOfApi(instanceCopy *mlv1alpha1.ModelDeployment) error {
	switch strings.ToLower(mgr.Deploy.Spec.ModelInfo.Framework) {
	case "tensorflow":
		NewTensorFlowApis().Inject(instanceCopy)
	default:
		return fmt.Errorf("unknown framework when inject apis")
	}
	return nil
}

type Api struct {
	Name      string
	Url       string
	Desc      string
	Group     string
	Kind      string
	Verb      string
	partOfUrl string
}
type TensorFlowApis struct {
	Apis []Api
}

func NewTensorFlowApis() TensorFlowApis {
	api := []Api{
		{Kind: "Restful", Name: "GetModelStatus", Verb: "GET", Desc: "get Model Status", partOfUrl: ""},
		{Kind: "Restful", Name: "Metadata", Verb: "GET", Desc: "get Model Metadata", partOfUrl: "/metadata"},
		{Kind: "Restful", Name: "Classify", Verb: "POST", Desc: "call Classify Method", partOfUrl: ":classify"},
		{Kind: "Restful", Name: "Predict", Verb: "POST", Desc: "call Predict Method", partOfUrl: ":predict"},
		{Kind: "Restful", Name: "Regress", Verb: "POST", Desc: "call Regress Method", partOfUrl: ":regress"},
	}
	tfApi := TensorFlowApis{api}
	return tfApi
}

func (tf TensorFlowApis) Inject(modelDeployResource *mlv1alpha1.ModelDeployment) {
	if modelDeployResource.Status.Apis != nil {
		return
	}
	baseUrl := GetRestfulBaseUrl(*modelDeployResource)
	var serviceApis []mlv1alpha1.ServiceApi
	for _, api := range tf.Apis {
		api.Url = fmt.Sprintf("%v%v", baseUrl, api.partOfUrl)
		serviceApis = append(serviceApis, mlv1alpha1.ServiceApi{Kind: api.Kind, Name: api.Name, Url: api.Url, Verb: api.Verb, Desc: api.Desc})
	}
	modelDeployResource.Status.Apis = append(modelDeployResource.Status.Apis, serviceApis...)
}
