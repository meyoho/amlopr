package modeldeploy

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
	"context"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ModelDeploymentGlobalManager struct {
	modelDeploy *mlv1alpha1.ModelDeployment
	clientSet   *versioned.Clientset
}

func NewModelDeploymentGlobalManager(modelDeploy *mlv1alpha1.ModelDeployment, clientSet *versioned.Clientset) ModelDeploymentGlobalManager {
	return ModelDeploymentGlobalManager{modelDeploy, clientSet}
}

func (mgr *ModelDeploymentGlobalManager) initClientSet(clusterName string) error {
	if mgr.clientSet == nil {
		clientSet, err := utils.NewBusinessMgr(clusterName).MLClient()
		if err != nil {
			return juju.Trace(err)
		}
		mgr.clientSet = clientSet
	}
	return nil
}

func (mgr *ModelDeploymentGlobalManager) SyncDependentResources(globalClient client.Client, clusterName string) error {
	return syncSecret2BusinessCluster(mgr.modelDeploy, globalClient, clusterName, mgr.modelDeploy.Namespace)
}

func syncSecret2BusinessCluster(modelDeploy *mlv1alpha1.ModelDeployment, globalClient client.Client, clusterName string, namespace string) error {
	secretName := modelDeploy.Spec.ImageRepository.OriginSecretName
	if secretName != "" {
		secret, err := copySecretToBusiness(
			modelDeploy.Spec.ImageRepository.OriginSecretName,
			modelDeploy.Spec.ImageRepository.OriginSecretNamespace,
			globalClient,
			namespace,
			clusterName)
		if err != nil {
			return err
		}
		modelDeploy.Spec.ImageRepository.Credential.Id = secret.Name
	}
	return nil
}

func (mgr *ModelDeploymentGlobalManager) CreateOrUpdate(clusterName string) (common.Object, error) {
	modelDeployment := mgr.modelDeploy
	if err := mgr.initClientSet(clusterName); err != nil {
		return nil, juju.Trace(err)
	}
	existCrd, err := mgr.clientSet.MlV1alpha1().ModelDeployments(modelDeployment.Namespace).Get(modelDeployment.Name, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			return mgr.clientSet.MlV1alpha1().ModelDeployments(modelDeployment.Namespace).Create(modelDeployment)
		}
		return existCrd, juju.Trace(err)
	}
	if modelDeployment.Status.PipelineInfo.Behavior == PipelineBehaviorUpdate {
		/*
			var mergedLabels = make(map[string]string)
			for k, v := range created.Labels {
				mergedLabels[k] = v
			}
			for k, v := range modelDeployment.Labels {
				mergedLabels[k] = v
			}
			modelDeployment.Labels = mergedLabels
		*/
		modelDeployment.ObjectMeta.ResourceVersion = existCrd.ResourceVersion
		if existCrd.Finalizers != nil {
			modelDeployment.Finalizers = append(modelDeployment.Finalizers, existCrd.Finalizers...)
		}
		var hasBuildRelation = false
		for _, v := range existCrd.Status.PipelineInfo.History {
			if v.Name == modelDeployment.Status.PipelineInfo.LatestCreator {
				hasBuildRelation = true
				break
			}
		}
		if !hasBuildRelation {
			modelDeployment.Status.PipelineInfo.History = append(existCrd.Status.PipelineInfo.History, modelDeployment.Status.PipelineInfo.History...)
		} else {
			modelDeployment.Status.PipelineInfo.History = existCrd.Status.PipelineInfo.History
		}
		modelDeployment.Status.State = mlv1alpha1.StateModelDeploymentUnknown
		return mgr.clientSet.MlV1alpha1().ModelDeployments(modelDeployment.Namespace).Update(modelDeployment)
	}
	return existCrd, err
}

func (mgr *ModelDeploymentGlobalManager) RemoveRelationRecord(clusterName, amlPipelineName string) error {
	if err := mgr.initClientSet(clusterName); err != nil {
		return juju.Trace(err)
	}
	for i, v := range mgr.modelDeploy.Status.PipelineInfo.History {
		if v.Name == amlPipelineName {
			mgr.modelDeploy.Status.PipelineInfo.History[i].Exists = 0
			break
		}
	}
	_, err := mgr.clientSet.MlV1alpha1().ModelDeployments(mgr.modelDeploy.Namespace).Update(mgr.modelDeploy)
	return err
}

//----------
type ModelDeploymentManager struct {
	modelDeploy *mlv1alpha1.ModelDeployment
	reconcile   *ReconcileModelDeploy
}

func NewModelDeploymentManager(modelDeploy *mlv1alpha1.ModelDeployment, reconcile *ReconcileModelDeploy) ModelDeploymentManager {
	return ModelDeploymentManager{modelDeploy, reconcile}
}
func (mgr *ModelDeploymentManager) RemoveFinalizer() {
	finalizers := mgr.modelDeploy.Finalizers
	mgr.modelDeploy.Finalizers = utils.Delete(finalizers, mlv1alpha1.ModelServiceFinalizer)
	return
}
func (mgr *ModelDeploymentManager) IsModelServiceEmpty() (bool, error) {
	applicationName := mgr.modelDeploy.Spec.ModelService.Name
	labelSelector := labels.NewSelector()
	req, err := labels.NewRequirement(ModelServiceLabelKey, selection.Equals, []string{applicationName})
	if err != nil {
		return false, juju.Trace(err)
	}
	ls := labelSelector.Add(*req)
	listOpts := &client.ListOptions{LabelSelector: ls}
	var modelDeploymentList = &mlv1alpha1.ModelDeploymentList{}
	err = mgr.reconcile.List(context.Background(), listOpts, modelDeploymentList)
	if err != nil {
		return false, juju.Trace(err)
	}
	if len(modelDeploymentList.Items) == 1 && modelDeploymentList.Items[0].Name == mgr.modelDeploy.Name {
		return true, nil
	}
	return false, nil
}
