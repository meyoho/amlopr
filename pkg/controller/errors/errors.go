package errors

import (
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
)

// NewParametersMissing returns an error indicating the item requested exists by that identifier.
func NewParametersMissing(reason string) *errors.StatusError {
	message := reason
	if len(message) == 0 {
		message = "parameters missing"
	}
	return &errors.StatusError{
		ErrStatus: metav1.Status{
			Status:  metav1.StatusFailure,
			Code:    http.StatusBadRequest,
			Reason:  metav1.StatusReasonBadRequest,
			Message: message,
		}}
}
