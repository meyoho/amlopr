package common

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
)

var AMLAnnotationsMap = map[string]string{
	mlv1alpha1.AnnotationProductAMLKey():     mlv1alpha1.AnnotationProductAMLValue,
	mlv1alpha1.AnnotationProductVersionKey(): mlv1alpha1.AnnotationProductVersionValue,
}

type AmlAnnotation struct {
	annos map[string]string
}

func NewAmlAnnotation() *AmlAnnotation {
	return &AmlAnnotation{
		annos: make(map[string]string),
	}
}
func (f *AmlAnnotation) Get() map[string]string {
	return f.annos
}
func (f *AmlAnnotation) Create() *AmlAnnotation {
	if _, ok := f.annos[mlv1alpha1.AnnotationProductAMLKey()]; !ok {
		f.annos[mlv1alpha1.AnnotationProductAMLKey()] = mlv1alpha1.AnnotationProductAMLValue
	}
	if _, ok := f.annos[mlv1alpha1.AnnotationProductVersionKey()]; !ok {
		f.annos[mlv1alpha1.AnnotationProductVersionKey()] = mlv1alpha1.AnnotationProductVersionValue
	}
	return f
}
func (f *AmlAnnotation) Add(k, v string) *AmlAnnotation {
	if _, ok := f.annos[k]; !ok {
		f.annos[k] = v
	}
	return f
}
func (f *AmlAnnotation) AddForce(k, v string) *AmlAnnotation {
	f.annos[k] = v
	return f
}
func (f *AmlAnnotation) AddMap(annotations map[string]string) *AmlAnnotation {
	if annotations != nil {
		for k, v := range annotations {
			f.annos[k] = v
		}
	}
	return f
}
