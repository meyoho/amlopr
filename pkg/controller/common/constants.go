package common

const (
	KindService       = "Service"
	APIVersionService = "v1"

	KindStatefulSet       = "StatefulSet"
	APIVersionStatefulSet = "apps/v1"
)
