package common

import (
	"fmt"
)

const (
	AmlLabelAppKey        = "app"
	AmlLabelCreatorPrefix = "creator"
)

func GetLabelKeyForCreatorOf(crdName string) string {
	return fmt.Sprintf("%v.%v", AmlLabelCreatorPrefix, crdName)
}

type AmlLabel struct {
	annos map[string]string
}

func NewAmlLabel(annos ...map[string]string) *AmlLabel {
	if len(annos) > 0 {
		return &AmlLabel{
			annos: merge(annos...),
		}
	}
	return &AmlLabel{
		annos: make(map[string]string),
	}
}

func merge(ms ...map[string]string) map[string]string {
	res := map[string]string{}
	for _, m := range ms {
		for k, v := range m {
			res[k] = v
		}
	}
	return res
}

func (f *AmlLabel) Get() map[string]string {
	return f.annos
}

func (f *AmlLabel) AddForce(k, v string) *AmlLabel {
	f.annos[k] = v
	return f
}

func (f *AmlLabel) Add(k, v string) *AmlLabel {
	if _, ok := f.annos[k]; !ok {
		f.annos[k] = v
	}
	return f
}

func (f *AmlLabel) GetSelectorString() string {
	var result string
	for key, value := range f.annos {
		result = fmt.Sprintf("%s=%s,%s", key, value, result)
	}
	return result[0 : len(result)-1]
}
