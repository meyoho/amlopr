package common

import (
	"github.com/onsi/gomega"
	"testing"
)

func TestNewAmlLabel(t *testing.T) {
	label := NewAmlLabel(nil).Add("a", "b").Get()
	g := gomega.NewGomegaWithT(t)
	g.Expect(label).To(gomega.Equal(map[string]string{"a": "b"}))

	label = NewAmlLabel().Add("a", "b").Get()
	g.Expect(label).To(gomega.Equal(map[string]string{"a": "b"}))

	label2 := NewAmlLabel(label).Add("c", "d").Get()
	g.Expect(label2).To(gomega.Equal(map[string]string{"a": "b", "c": "d"}))

	label = NewAmlLabel(label, nil).Add("e", "f").Get()
	g.Expect(label).To(gomega.Equal(map[string]string{"a": "b", "e": "f"}))

	label = NewAmlLabel(label, label2).Add("g", "h").Get()
	g.Expect(label).To(gomega.Equal(map[string]string{"a": "b", "c": "d", "e": "f", "g": "h"}))
}
