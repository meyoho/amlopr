package common

import (
	"fmt"
	"strings"
)

func MergeErrors(amlErrs []*AmlError) *AmlError {
	var sliceR, sliceM []string
	var sep = ";;"
	for _, amlErr := range amlErrs {
		sliceR = append(sliceR, amlErr.Reason())
		sliceM = append(sliceM, amlErr.Message())
	}
	return &AmlError{
		strings.Join(sliceR, sep),
		strings.Join(sliceM, sep),
	}

}

func MergeSimpleErrors(errs []error) error {
	var slice []string
	var sep = ";;"
	for _, amlErr := range errs {
		if amlErr != nil {
			slice = append(slice, amlErr.Error())
		}
	}
	if len(slice) == 0 {
		return nil
	}
	return fmt.Errorf(strings.Join(slice, sep))
}
