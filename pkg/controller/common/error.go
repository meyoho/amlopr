package common

import "fmt"

// New returns an error that formats as the given text.
func NewAmlError(reason, message string) *AmlError {
	return &AmlError{reason, message}
}

// errorString is a trivial implementation of error.
type AmlError struct {
	r string
	s string
}

func (e *AmlError) Reason() string {
	return e.r
}

func (e *AmlError) Message() string {
	return e.s
}
func (e *AmlError) Error() string {
	return fmt.Sprintf("Reason: %v\nMessage:%v", e.r, e.s)
}
