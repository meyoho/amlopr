package common

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"context"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

func GetMapFnForLabelKey(subject string) handler.ToRequestsFunc {

	return handler.ToRequestsFunc(
		func(a handler.MapObject) []reconcile.Request {
			for k, v := range a.Meta.GetLabels() {
				if k == subject {
					return []reconcile.Request{
						{NamespacedName: types.NamespacedName{
							Name:      v,
							Namespace: a.Meta.GetNamespace(),
						}},
					}
				}
			}
			return []reconcile.Request{}
		})
}

func GetPredicateFuncs(labelK, labelV string) predicate.Funcs {
	return predicate.Funcs{
		UpdateFunc: func(e event.UpdateEvent) bool {
			// The object doesn't contain label "app" and app!=notebook, so the event will be
			// ignored.
			product, ok := e.MetaOld.GetAnnotations()[mlv1alpha1.AnnotationProductAMLKey()]
			if !ok || product != mlv1alpha1.AnnotationProductAMLValue {
				return false
			}
			appV, ok := e.MetaOld.GetLabels()[labelK]
			if !ok || appV != labelV {
				return false
			}

			return e.ObjectOld != e.ObjectNew
		},
		CreateFunc: func(e event.CreateEvent) bool {
			product, ok := e.Meta.GetAnnotations()[mlv1alpha1.AnnotationProductAMLKey()]
			if !ok || product != mlv1alpha1.AnnotationProductAMLValue {
				return false
			}
			appV, ok := e.Meta.GetLabels()[labelK]
			if !ok || appV != labelV {
				return false
			}
			return true
		},
		DeleteFunc: func(e event.DeleteEvent) bool {
			product, ok := e.Meta.GetAnnotations()[mlv1alpha1.AnnotationProductAMLKey()]
			if !ok || product != mlv1alpha1.AnnotationProductAMLValue {
				return false
			}
			appV, ok := e.Meta.GetLabels()[labelK]
			if !ok || appV != labelV {
				return false
			}
			return true
		},
	}
}

func ValidateRequest(client client.Client, name types.NamespacedName, instance runtime.Object) (err error) {
	err = client.Get(context.Background(), name, instance)
	if err != nil && errors.IsNotFound(err) {
		err = nil
	}
	return
}
