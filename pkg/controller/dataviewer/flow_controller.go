package dataviewer

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"context"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type viewer interface {
	ConvertToReady(parent *Subject, client client.Client, scheme *runtime.Scheme) error
	ConvertToStop(parent *Subject, client client.Client, scheme *runtime.Scheme) error
}

var funcMap = make(map[v1alpha1.Type][]viewer)

func Register(key v1alpha1.Type, viewer viewer) {
	funcMap[key] = append(funcMap[key], viewer)
}

type FlowControlProvider struct {
}

func (fc *FlowControlProvider) Dispatch(reconcileDataViewer *ReconcileDataViewer, request reconcile.Request) error {
	parent := &v1alpha1.DataViewer{}
	err := reconcileDataViewer.Get(context.TODO(), request.NamespacedName, parent)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		} else {
			return err
		}
	}

	subject := &Subject{SubjectInstance: parent}
	var phase = subject.GetCurrentStage()
	switch phase {
	case v1alpha1.DataViewerPhaseEnum.Running:
		err = fc.processRunningPhase(reconcileDataViewer, subject)
		break
	case v1alpha1.DataViewerPhaseEnum.Stopped:
		err = fc.processStoppedPhase(reconcileDataViewer, subject)
		break
	}

	updateErr := reconcileDataViewer.Client.Update(context.TODO(), subject.SubjectInstance)
	if err == nil && updateErr != nil {
		return updateErr
	}

	return err
}

func (fc *FlowControlProvider) processRunningPhase(reconcileDataViewer *ReconcileDataViewer, subject *Subject) error {
	viewers := funcMap[subject.GetReadableInstance().Spec.Type]
	for _, viewer := range viewers {
		err := viewer.ConvertToReady(subject, reconcileDataViewer.Client, reconcileDataViewer.scheme)
		if err != nil {
			return juju.Trace(err)
		}
	}
	return nil
}

func (fc *FlowControlProvider) processStoppedPhase(reconcileDataViewer *ReconcileDataViewer, subject *Subject) error {
	viewers := funcMap[subject.GetReadableInstance().Spec.Type]
	for _, viewer := range viewers {
		err := viewer.ConvertToStop(subject, reconcileDataViewer.Client, reconcileDataViewer.scheme)
		if err != nil {
			return juju.Trace(err)
		}
	}
	return nil
}
