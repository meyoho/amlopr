package dataviewer

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/base"
	"context"
	juju "github.com/juju/errors"
	"k8s.io/api/apps/v1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"reflect"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type storageUIPodComp struct {
	storageUI
	Client client.Client
	scheme *runtime.Scheme
}

func (c *storageUIPodComp) init(parent *Subject, client client.Client, scheme *runtime.Scheme) {
	c.Parent = parent
	c.Client = client
	c.scheme = scheme
}

func (c *storageUIPodComp) isReady(deploy *v1.Deployment) bool {
	if deploy.Status.AvailableReplicas > 0 {
		return true
	}
	return false
}

func (c *storageUIPodComp) ConvertToReady(parent *Subject, client client.Client, scheme *runtime.Scheme) error {
	c.init(parent, client, scheme)
	deployment, err := c.createNotExist()
	if err != nil {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Error, v1alpha1.DataViewerPhaseEnum.Running, "create deploy failed", err.Error())
		return juju.Trace(err)
	}
	if c.isReady(deployment) {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Running, v1alpha1.DataViewerPhaseEnum.Running, "", "")
	} else {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Created, v1alpha1.DataViewerPhaseEnum.Running, "", "")
	}
	return nil
}

func (c *storageUIPodComp) ConvertToStop(parent *Subject, client client.Client, scheme *runtime.Scheme) error {
	c.init(parent, client, scheme)
	err := c.delete()
	if err != nil {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Error, v1alpha1.DataViewerPhaseEnum.Stopped, "delete pod failed", err.Error())
		return juju.Trace(err)
	}
	return nil
}

func (c *storageUIPodComp) createNotExist() (*v1.Deployment, error) {
	foundeDeploy := &v1.Deployment{}

	parsedDeploy, err := c.parseTo()
	if err != nil {
		return nil, juju.Trace(err)
	}
	isFounded, err := c.isExist(foundeDeploy)
	if err != nil || IsEqualUIDeploy(foundeDeploy, parsedDeploy) {
		return foundeDeploy, juju.Trace(err)
	}

	if isFounded {
		foundeDeploy.Spec = parsedDeploy.Spec
		err = c.Client.Update(context.TODO(), foundeDeploy)
		return foundeDeploy, juju.Trace(err)
	}
	err = c.Client.Create(context.TODO(), parsedDeploy)
	return parsedDeploy, juju.Trace(err)
}

func (c *storageUIPodComp) delete() error {
	deploy := &v1.Deployment{}
	founded, err := c.isExist(deploy)
	if !founded || err != nil {
		return juju.Trace(err)
	}

	err = c.Client.Delete(context.TODO(), deploy)
	return juju.Trace(err)
}

func (c *storageUIPodComp) isExist(deploy *v1.Deployment) (bool, error) {
	newRequest := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Namespace: c.Parent.SubjectInstance.GetNamespace(),
			Name:      c.GetDeploymentName(),
		}}
	return base.FindAndInitInstance(c.Client, newRequest, deploy)
}

const (
	DataMgrCreator = "dataManager"
)

func IsEqualUIDeploy(deploy *appsv1.Deployment, other *appsv1.Deployment) bool {
	return reflect.DeepEqual(deploy.Name, other.Name) &&
		reflect.DeepEqual(deploy.Namespace, other.Namespace) &&
		reflect.DeepEqual(deploy.Spec.Replicas, other.Spec.Replicas) &&
		reflect.DeepEqual(deploy.Spec.Selector, other.Spec.Selector) &&
		reflect.DeepEqual(deploy.Spec.Template.ObjectMeta, other.Spec.Template.ObjectMeta) &&
		reflect.DeepEqual(deploy.Spec.Template.Spec.Containers[0].VolumeMounts, other.Spec.Template.Spec.Containers[0].VolumeMounts) &&
		reflect.DeepEqual(deploy.Spec.Template.Spec.Volumes, other.Spec.Template.Spec.Volumes)
}

func (c *storageUIPodComp) GetUIDeployment() *appsv1.Deployment {
	replica := int32(1)
	podTemplateSpec := c.Parent.GetReadableInstance().Spec.PodTemplateSpec
	c.setPodTemplate(&podTemplateSpec)

	return &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      c.GetDeploymentName(),
			Namespace: c.GetNamespace(),
		},

		Spec: appsv1.DeploymentSpec{
			Replicas: &replica,
			Selector: &metav1.LabelSelector{
				MatchLabels: c.getLabelMap(),
			},
			Template: podTemplateSpec,
		},
	}
}

func (c *storageUIPodComp) setPodTemplate(podTemplateSpec *corev1.PodTemplateSpec) {
	podTemplateSpec.ObjectMeta = metav1.ObjectMeta{Labels: c.getLabelMap()}
	spec := &podTemplateSpec.Spec
	if len(spec.Containers) == 0 {
		spec.Containers = append(spec.Containers, corev1.Container{})
	}
	spec.Containers[0].VolumeMounts = c.getVolumeMount()
	spec.Containers[0].Command = []string{"/filebrowser", "--baseurl", c.getBaseUrl()}
	spec.Volumes = c.getVolumes()
}

func (c *storageUIPodComp) getVolumes() []corev1.Volume {
	pvcSlice := c.Parent.GetReadableInstance().Spec.Pvc
	var volumes []corev1.Volume
	for _, pvc := range pvcSlice {
		volumes = append(volumes, pvc.Volume)
	}
	return volumes
}

func (c *storageUIPodComp) getVolumeMount() []corev1.VolumeMount {
	pvcSlice := c.Parent.GetReadableInstance().Spec.Pvc
	var volumeMounts []corev1.VolumeMount
	for _, pvc := range pvcSlice {
		volumeMounts = append(volumeMounts, corev1.VolumeMount{
			Name:      pvc.Volume.Name,
			MountPath: "/srv/" + pvc.Volume.Name,
		})

	}
	return volumeMounts
}

func (c *storageUIPodComp) parseTo() (*v1.Deployment, error) {
	result := c.GetUIDeployment()

	err := controllerutil.SetControllerReference(c.Parent.SubjectInstance, result, c.scheme)
	return result, juju.Trace(err)
}
