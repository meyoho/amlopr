package dataviewer

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/base"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	"k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type TensorboardPodComp struct {
	Parent *Subject
	Client client.Client
	scheme *runtime.Scheme
}

func (c *TensorboardPodComp) init(parent *Subject, client client.Client, scheme *runtime.Scheme) {
	c.Parent = parent
	c.Client = client
	c.scheme = scheme
}

func (c *TensorboardPodComp) GetName() string {
	return fmt.Sprintf("%s-%s-deploy", c.Parent.SubjectInstance.Name, "tensorboard")
}

func (c *TensorboardPodComp) isReady(deploy *v1.Deployment) bool {
	if deploy.Status.AvailableReplicas > 0 {
		return true
	}
	return false
}

func (c *TensorboardPodComp) ConvertToReady(parent *Subject, client client.Client, scheme *runtime.Scheme) error {
	c.init(parent, client, scheme)
	deployment, err := c.createNotExist()
	if err != nil {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Error, v1alpha1.DataViewerPhaseEnum.Running, "create deploy failed", err.Error())
		return juju.Trace(err)
	}
	if c.isReady(deployment) {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Running, v1alpha1.DataViewerPhaseEnum.Running, "", "")
	} else {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Created, v1alpha1.DataViewerPhaseEnum.Running, "", "")
	}
	return nil
}

func (c *TensorboardPodComp) ConvertToStop(parent *Subject, client client.Client, scheme *runtime.Scheme) error {
	c.init(parent, client, scheme)
	err := c.delete()
	if err != nil {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Error, v1alpha1.DataViewerPhaseEnum.Stopped, "delete pod failed", err.Error())
		return juju.Trace(err)
	}
	return nil
}

func (c *TensorboardPodComp) createNotExist() (*v1.Deployment, error) {
	deploy := &v1.Deployment{}
	founded, err := c.isExist(deploy)
	if founded || err != nil {
		return deploy, juju.Trace(err)
	}
	deploy, err = c.parseTo()
	if err != nil {
		return nil, juju.Trace(err)
	}

	err = c.Client.Create(context.TODO(), deploy)
	return deploy, juju.Trace(err)
}

func (c *TensorboardPodComp) delete() error {
	deploy := &v1.Deployment{}
	founded, err := c.isExist(deploy)
	if !founded || err != nil {
		return juju.Trace(err)
	}

	err = c.Client.Delete(context.TODO(), deploy)
	return juju.Trace(err)
}

func (c *TensorboardPodComp) isExist(deploy *v1.Deployment) (bool, error) {
	newRequest := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Namespace: c.Parent.SubjectInstance.GetNamespace(),
			Name:      c.GetName(),
		}}
	return base.FindAndInitInstance(c.Client, newRequest, deploy)
}

const (
	TensorboardCreator = "tensorboard"
)

func (c *TensorboardPodComp) parseTo() (*v1.Deployment, error) {
	labels := common.NewAmlLabel().
		Add(common.GetLabelKeyForCreatorOf(TensorboardCreator), c.Parent.SubjectInstance.GetName()).
		Get()
	result := &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      c.GetName(),
			Namespace: c.Parent.SubjectInstance.Namespace,
		},
		Spec: v1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: c.Parent.SubjectInstance.Spec.PodTemplateSpec,
		},
	}

	result.Spec.Template.ObjectMeta.Labels = common.NewAmlLabel(result.Spec.Template.ObjectMeta.Labels, labels).
		Get()

	err := c.setPodSpecForTensorboard(c.Parent.SubjectInstance, &result.Spec.Template.Spec)
	if err != nil {
		return nil, juju.Trace(err)
	}

	err = controllerutil.SetControllerReference(c.Parent.SubjectInstance, result, c.scheme)

	return result, juju.Trace(err)
}

const MountBaseUrl = "/srv"

func (c *TensorboardPodComp) setPodSpecForTensorboard(viewer *v1alpha1.DataViewer, s *corev1.PodSpec) error {
	if len(s.Containers) == 0 {
		s.Containers = append(s.Containers, corev1.Container{})
	}

	namespacedName := fmt.Sprintf("%s/%s", c.Parent.SubjectInstance.Namespace, c.Parent.SubjectInstance.GetName())
	routePrefix := fmt.Sprintf("/%s/%s/", "tensorboard", namespacedName)

	container := &s.Containers[0]
	container.Args = []string{
		"tensorboard",
		fmt.Sprintf("--logdir=%s", MountBaseUrl+viewer.Spec.DataPath),
		fmt.Sprintf("--path_prefix=%s", routePrefix),
	}
	container.Ports = []corev1.ContainerPort{
		{
			ContainerPort: 6006,
		},
	}
	binding, err := utils.GetMLBinding(c.Client, c.Parent.SubjectInstance.Namespace)
	if err != nil {
		return err
	}

	container.VolumeMounts = utils.GetAMLFixedVolumeMount(binding, MountBaseUrl)

	s.Volumes = utils.GetVolumes(binding)

	return nil
}
