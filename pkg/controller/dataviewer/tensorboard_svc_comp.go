package dataviewer

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/base"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils/ambassador"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type TensorboardSvcComp struct {
	Parent *Subject
	Client client.Client
	scheme *runtime.Scheme
}

func (c *TensorboardSvcComp) init(parent *Subject, client client.Client, scheme *runtime.Scheme) {
	c.Parent = parent
	c.Client = client
	c.scheme = scheme
}

func (c *TensorboardSvcComp) GetName() string {
	return fmt.Sprintf("%s-%s-svc", c.Parent.SubjectInstance.Name, "tensorboard")
}

func (c *TensorboardSvcComp) ConvertToReady(parent *Subject, client client.Client, scheme *runtime.Scheme) error {
	c.init(parent, client, scheme)
	_, err := c.createNotExist()
	if err != nil {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Error, v1alpha1.DataViewerPhaseEnum.Running, "create deploy failed", err.Error())
		return juju.Trace(err)
	}
	return nil
}

func (c *TensorboardSvcComp) ConvertToStop(parent *Subject, client client.Client, scheme *runtime.Scheme) error {
	c.init(parent, client, scheme)
	err := c.delete()
	if err != nil {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Error, v1alpha1.DataViewerPhaseEnum.Stopped, "delete pod failed", err.Error())
		return juju.Trace(err)
	}
	return nil
}

func (c *TensorboardSvcComp) createNotExist() (*corev1.Service, error) {
	service := &corev1.Service{}
	founded, err := c.isExist(service)
	if founded || err != nil {
		return service, juju.Trace(err)
	}

	service, err = c.parseTo()
	if err != nil {
		return service, juju.Trace(err)
	}

	err = c.Client.Create(context.TODO(), service)
	return service, juju.Trace(err)
}

func (c *TensorboardSvcComp) delete() error {
	deploy := &corev1.Service{}
	founded, err := c.isExist(deploy)
	if !founded || err != nil {
		return juju.Trace(err)
	}

	err = c.Client.Delete(context.TODO(), deploy)
	return juju.Trace(err)
}

func (c *TensorboardSvcComp) isExist(deploy *corev1.Service) (bool, error) {
	newRequest := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Namespace: c.Parent.SubjectInstance.GetNamespace(),
			Name:      c.GetName(),
		}}
	return base.FindAndInitInstance(c.Client, newRequest, deploy)
}

func (c *TensorboardSvcComp) parseTo() (*corev1.Service, error) {
	ns := c.Parent.SubjectInstance.Namespace
	namespacedName := fmt.Sprintf("%s/%s", ns, c.Parent.SubjectInstance.GetName())
	routePrefix := fmt.Sprintf("/%s/%s/", "tensorboard", namespacedName)
	svcName := c.GetName()
	svcPort := intstr.FromInt(8000)

	ambassadorAnnotation := ambassador.AmbassadorAnnotation{
		Name:    fmt.Sprintf("%s_%s", ns, c.Parent.SubjectInstance.GetName()),
		Prefix:  routePrefix,
		Rewrite: routePrefix,
		Service: fmt.Sprintf("%s.%s:%d", svcName, ns, svcPort.IntVal),
		Timeout: 300000,
		RetryPolicy: ambassador.RetryPolicy{
			RetryOn:    "5xx",
			NumRetries: 10,
		},
	}
	annotations, err := ambassadorAnnotation.GetAnnotation()
	if err != nil {
		return nil, juju.Trace(err)
	}

	c.Parent.SubjectInstance.Status.URL = routePrefix

	labels := common.NewAmlLabel().
		Add(common.GetLabelKeyForCreatorOf(TensorboardCreator), c.Parent.SubjectInstance.GetName()).
		Get()
	result := &corev1.Service{
		ObjectMeta: v1.ObjectMeta{
			Name:        c.GetName(),
			Namespace:   c.Parent.SubjectInstance.Namespace,
			Annotations: annotations,
		},
		Spec: corev1.ServiceSpec{
			Selector: labels,
			Ports: []corev1.ServicePort{
				{
					Protocol:   corev1.ProtocolTCP,
					Port:       int32(svcPort.IntValue()),
					TargetPort: intstr.FromInt(6006),
				},
			},
		},
	}

	err = controllerutil.SetControllerReference(c.Parent.SubjectInstance, result, c.scheme)
	return result, juju.Trace(err)
}
