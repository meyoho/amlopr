package dataviewer

import "alauda.io/amlopr/pkg/apis/ml/v1alpha1"

func init() {
	Register(v1alpha1.Tensorboard, &TensorboardSvcComp{})
	Register(v1alpha1.Tensorboard, &TensorboardPodComp{})

	Register(v1alpha1.DataManager, &storageUIPodComp{})
	Register(v1alpha1.DataManager, &storageUISvcComp{})
}
