package dataviewer

type storageUI struct {
	Parent *Subject
}

func (storageUI *storageUI) GetDeploymentName() string {
	return storageUI.Parent.GetReadableInstance().Name
}

func (storageUI *storageUI) getBaseUrl() string {
	return storageUI.Parent.GetReadableInstance().Spec.BaseUrl + storageUI.GetNamespace() + "/"
}

func (storageUI *storageUI) getLabelMap() map[string]string {
	return map[string]string{"deployment": storageUI.GetDeploymentName()}
}

func (c *storageUI) GetNamespace() string {
	return c.Parent.GetReadableInstance().Namespace
}
