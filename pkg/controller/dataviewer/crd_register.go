package dataviewer

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/config/template"
	"alauda.io/amlopr/pkg/controller/amlpipeline"
	"alauda.io/amlopr/pkg/controller/common"
	utils2 "alauda.io/amlopr/pkg/controller/tensorflowtraining/utils"
	"alauda.io/amlopr/pkg/controller/utils"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type DataViewerGen struct {
}

func (DataViewerGen) ErrorCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline, err error) error {
	return nil
}

func (DataViewerGen) DeleteCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline) error {
	clientset, err := utils.NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		if exist, existError := utils.NewBusinessMgr(clusterName).IsExistCluster(); existError != nil && exist == false {
			return nil
		}
		return juju.Trace(err)
	}
	err = clientset.MlV1alpha1().DataViewers(key.Namespace).Delete(key.Name, &metav1.DeleteOptions{})
	if err != nil && errors.IsNotFound(err) {
		return nil
	}
	return juju.Trace(err)
}

func (DataViewerGen) GenCrd(client client.Client,
	_ string,
	namespacedName types.NamespacedName,
	arguments []devops.PipelineTemplateArgumentGroup,
	pipeline *v1alpha1.AMLPipeline) (common.Object, error) {
	dataViewer := &v1alpha1.DataViewer{}
	dataViewer.Status.Phase = v1alpha1.DataViewerPhaseEnum.Stopped
	dataViewer.Spec.Type = v1alpha1.Tensorboard
	dataViewer.Namespace = namespacedName.Namespace
	dataViewer.Name = namespacedName.Name
	valueMap := utils.GetMergedMapGroup(arguments, pipeline.Spec.Parameters)
	_, path, err := utils2.DecodePath(valueMap["tensorboardPath"])
	if err != nil {
		return nil, juju.Trace(err)
	}
	dataViewer.Spec.DataPath = path
	dataViewer.Spec.PodTemplateSpec = *template.GetTensorboardDefaultTemplate()

	return dataViewer, nil
}

func (d DataViewerGen) CancelCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline) error {
	return d.DeleteCrd(clusterName, key, amlPipeline)
}

func (DataViewerGen) CreateCrdNotExist(crd common.Object, clusterName string) (common.Object, error) {
	training := crd.(*v1alpha1.DataViewer)
	clientset, err := utils.NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		return nil, juju.Trace(err)
	}
	created, err := clientset.MlV1alpha1().DataViewers(training.Namespace).Get(training.Name, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			return clientset.MlV1alpha1().DataViewers(training.Namespace).Create(training)
		}
		return created, juju.Trace(err)
	}
	return created, err
}

func init() {
	registerForTensorflow()
	registerForPyTorch()
}

func registerForPyTorch() {
	annotation := v1alpha1.Annotation{Key: "ml.training", Value: "PyTorch"}
	err := amlpipeline.RegisterCrd(annotation, "tensorboard", &DataViewerGen{})
	if err != nil {
		os.Exit(1)
	}
}

func registerForTensorflow() {
	annotation := v1alpha1.Annotation{Key: "ml.training", Value: "tensorflow"}
	err := amlpipeline.RegisterCrd(annotation, "tensorboard", DataViewerGen{})
	if err != nil {
		os.Exit(1)
	}
}
