package dataviewer

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/base"
	"alauda.io/amlopr/pkg/controller/utils/ambassador"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type storageUISvcComp struct {
	storageUI
	Client client.Client
	scheme *runtime.Scheme
}

func (c *storageUISvcComp) init(parent *Subject, client client.Client, scheme *runtime.Scheme) {
	c.Parent = parent
	c.Client = client
	c.scheme = scheme
}

func (c *storageUISvcComp) GetServiceName() string {
	return fmt.Sprintf("%s-svc", c.Parent.SubjectInstance.Name)
}

func (c *storageUISvcComp) ConvertToReady(parent *Subject, client client.Client, scheme *runtime.Scheme) error {
	c.init(parent, client, scheme)
	_, err := c.createNotExist()
	if err != nil {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Error, v1alpha1.DataViewerPhaseEnum.Running, "create deploy failed", err.Error())
		return juju.Trace(err)
	}
	return nil
}

func (c *storageUISvcComp) ConvertToStop(parent *Subject, client client.Client, scheme *runtime.Scheme) error {
	c.init(parent, client, scheme)
	err := c.delete()
	if err != nil {
		c.Parent.SetCurrentState(v1alpha1.DataViewerStateEnum.Error, v1alpha1.DataViewerPhaseEnum.Stopped, "delete pod failed", err.Error())
		return juju.Trace(err)
	}
	return nil
}

func (c *storageUISvcComp) createNotExist() (*corev1.Service, error) {
	service := &corev1.Service{}
	founded, err := c.isExist(service)
	if founded || err != nil {
		return service, juju.Trace(err)
	}

	service, err = c.parseTo()
	if err != nil {
		return service, juju.Trace(err)
	}

	err = c.Client.Create(context.TODO(), service)
	return service, juju.Trace(err)
}

func (c *storageUISvcComp) delete() error {
	deploy := &corev1.Service{}
	founded, err := c.isExist(deploy)
	if !founded || err != nil {
		return juju.Trace(err)
	}

	err = c.Client.Delete(context.TODO(), deploy)
	return juju.Trace(err)
}

func (c *storageUISvcComp) isExist(deploy *corev1.Service) (bool, error) {
	newRequest := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Namespace: c.Parent.SubjectInstance.GetNamespace(),
			Name:      c.GetServiceName(),
		}}
	return base.FindAndInitInstance(c.Client, newRequest, deploy)
}

func (c *storageUISvcComp) parseTo() (*corev1.Service, error) {

	//下载文件过大，需要超时时间久一些
	const timeoutMS = 10 * 60 * 60 * 1000

	ambassadorAnnotation := ambassador.AmbassadorAnnotation{
		Name:    fmt.Sprintf("%s_%s", c.GetNamespace(), c.Parent.GetReadableInstance().Name),
		Prefix:  c.getBaseUrl(),
		Rewrite: c.getBaseUrl(),
		Service: c.GetServiceName() + "." + c.GetNamespace(),
		Timeout: timeoutMS,
		RetryPolicy: ambassador.RetryPolicy{
			RetryOn:    "5xx",
			NumRetries: 10,
		},
	}
	annotation, err := ambassadorAnnotation.GetAnnotation()
	if err != nil {
		return nil, juju.Trace(err)
	}
	service := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:        c.GetServiceName(),
			Namespace:   c.GetNamespace(),
			Annotations: annotation,
		},

		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{
				{Port: c.GetUIServicePort(),
					TargetPort: c.GetUIServiceTargetPort()},
			},
			Selector: c.getLabelMap(),
		},
	}
	err = controllerutil.SetControllerReference(c.Parent.SubjectInstance, service, c.scheme)
	return service, juju.Trace(err)
}

func (c *storageUISvcComp) GetUIServicePort() int32 {
	return 80
}

func (c *storageUISvcComp) GetUIServiceTargetPort() intstr.IntOrString {
	return intstr.FromInt(80)
}
