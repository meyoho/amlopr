package tensorflowtraining

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/tensorflowtraining/utils"
	"encoding/json"
	"fmt"
	"github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta1"
	"github.com/onsi/gomega"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"testing"
)

func TestGetSelfDefineArgs(t *testing.T) {
	selfDefineArgs, err := utils.GetSelfDefineArgs(nil, `{"key1": "value1","key2": "value3"}`)
	if err != nil {
		t.Error(err)
	}
	g := gomega.NewGomegaWithT(t)

	g.Expect(selfDefineArgs[0] + selfDefineArgs[1]).To(gomega.ContainSubstring("--key1=value1"))
	g.Expect(selfDefineArgs[0] + selfDefineArgs[1]).To(gomega.ContainSubstring("--key2=value3"))
}

func TestGetTensorflowQuota(t *testing.T) {
	tensorflowQuota := getTensorflowQuota()
	bytes, _ := json.Marshal(tensorflowQuota)
	fmt.Print(string(bytes))
	quota, err := utils.GetTensorflowQuota(`{"PS":{"replica":1,"quota":{"hard":{"limits.cpu":1,"limits.memory":1}}},"MASTER":{"replica":1,"quota":{"hard":{"limits.cpu":1,"limits.memory":1}}},"WORKER":{"replica":1,"quota":{"hard":{"limits.cpu":1,"limits.memory":1}}}}`)
	//quota, err := GetTensorflowQuota(string(bytes))
	if err != nil {
		t.Error(err)
	}
	g := gomega.NewGomegaWithT(t)
	g.Expect(quota[v1beta1.TFReplicaTypePS]).To(gomega.Equal(tensorflowQuota[v1beta1.TFReplicaTypePS]))

}

func getTensorflowQuota() v1alpha1.TensorflowQuota {
	return v1alpha1.TensorflowQuota{
		v1beta1.TFReplicaTypePS: v1alpha1.ResourceQuota{
			Replica: int32(1),
			Quota: v1.ResourceQuotaSpec{
				Hard: v1.ResourceList{
					"limits.cpu":    resource.MustParse("1"),
					"limits.memory": resource.MustParse("1"),
				},
			},
		},
		v1beta1.TFReplicaTypeMaster: v1alpha1.ResourceQuota{
			Replica: int32(1),
			Quota: v1.ResourceQuotaSpec{
				Hard: v1.ResourceList{
					"limits.cpu":    resource.MustParse("1"),
					"limits.memory": resource.MustParse("1"),
				},
			},
		},
		v1beta1.TFReplicaTypeWorker: v1alpha1.ResourceQuota{
			Replica: int32(1),
			Quota: v1.ResourceQuotaSpec{
				Hard: v1.ResourceList{
					"limits.cpu":    resource.MustParse("1"),
					"limits.memory": resource.MustParse("1"),
				},
			},
		},
	}
}
