package tensorflowtraining

import (
	trainingutil "alauda.io/amlopr/pkg/controller/tensorflowtraining/utils"
	utils2 "alauda.io/amlopr/pkg/controller/utils"
	"context"
	"fmt"
	juju "github.com/juju/errors"

	commonv1beta1 "github.com/kubeflow/tf-operator/pkg/apis/common/v1beta1"
	"github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

type TFJobComp struct {
	Parent *Subject
	Client client.Client
	scheme *runtime.Scheme
}

func NewTFJobComp(parent *Subject, client client.Client, scheme *runtime.Scheme) *TFJobComp {
	return &TFJobComp{
		Parent: parent,
		Client: client,
		scheme: scheme,
	}
}
func (c *TFJobComp) isExist() (bool, *v1beta1.TFJob, error) {
	pipelineConfig := &v1beta1.TFJob{}
	namespacedName := utils2.NewNamespacedName(c.GetName(), c.GetNameSpace())
	err := c.Client.Get(context.TODO(), namespacedName, pipelineConfig)
	if err != nil {
		if errors.IsNotFound(err) {
			return false, pipelineConfig, nil
		}
		return false, pipelineConfig, err
	}

	return true, pipelineConfig, nil
}

func (c *TFJobComp) createNotExist() (*v1beta1.TFJob, error) {
	exist, pipelineConfig, err := c.isExist()
	if err != nil || exist {
		return pipelineConfig, juju.Trace(err)
	}

	tfJob, err := c.parseTo()
	if err != nil {
		return nil, juju.Trace(err)
	}

	err = controllerutil.SetControllerReference(c.Parent.SubjectInstance, tfJob,
		c.scheme)
	if err != nil {
		return nil, juju.Trace(err)
	}

	err = c.Client.Create(context.TODO(), tfJob)
	if err != nil {
		return nil, juju.Trace(err)
	}

	return tfJob, nil
}

func (c *TFJobComp) GetName() string {
	return fmt.Sprintf("%s", c.Parent.SubjectInstance.Name)
}

func (c *TFJobComp) GetNameSpace() string {
	return c.Parent.SubjectInstance.Namespace
}

func (c *TFJobComp) IsFinished(resource *v1beta1.TFJob) (bool, commonv1beta1.JobConditionType) {
	if resource.Status.Conditions != nil && len(resource.Status.Conditions) != 0 {
		jobCondition := resource.Status.Conditions[len(resource.Status.Conditions)-1]
		if jobCondition.Type == commonv1beta1.JobSucceeded ||
			jobCondition.Type == commonv1beta1.JobFailed {
			return true, jobCondition.Type
		}
	}
	return false, ""
}

func (c *TFJobComp) IsSucceeded(jobConditionType commonv1beta1.JobConditionType) bool {
	return jobConditionType == commonv1beta1.JobSucceeded
}

func (c *TFJobComp) parseTo() (*v1beta1.TFJob, error) {
	binding, err := utils2.GetMLBinding(c.Client, c.Parent.SubjectInstance.Namespace)
	if err != nil {
		return nil, juju.Trace(err)
	}
	training := trainingutil.CompleteTFJobSpec(c.Parent.SubjectInstance, binding)
	return &v1beta1.TFJob{
		ObjectMeta: v1.ObjectMeta{
			Name:      c.GetName(),
			Namespace: c.GetNameSpace(),
			Labels:    map[string]string{"tensorflowtraining": c.Parent.SubjectInstance.Name},
		},
		Spec: *training.Spec.TFJob.TFJobSpec,
	}, nil
}
