package utils

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta1"
	"github.com/onsi/gomega"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"testing"
)

func getCpuQuota() v1alpha1.ResourceQuota {
	return v1alpha1.ResourceQuota{
		Replica: int32(1),
		Quota: v1.ResourceQuotaSpec{
			Hard: v1.ResourceList{
				"limits.cpu":    resource.MustParse("1"),
				"limits.memory": resource.MustParse("1"),
			},
		},
	}
}

func getGpuQuota() v1alpha1.ResourceQuota {
	return v1alpha1.ResourceQuota{
		Replica: int32(1),
		Quota: v1.ResourceQuotaSpec{
			Hard: v1.ResourceList{
				"limits.cpu":            resource.MustParse("1"),
				"limits.memory":         resource.MustParse("1"),
				"limits.nvidia.com/gpu": resource.MustParse("1"),
			},
		},
	}
}

func getCpuTensorflowQuota() v1alpha1.TensorflowQuota {
	return v1alpha1.TensorflowQuota{
		v1beta1.TFReplicaTypePS:     getCpuQuota(),
		v1beta1.TFReplicaTypeMaster: getCpuQuota(),
		v1beta1.TFReplicaTypeWorker: getCpuQuota(),
	}
}

func getGpuTensorflowQuota() v1alpha1.TensorflowQuota {
	return v1alpha1.TensorflowQuota{
		v1beta1.TFReplicaTypePS:     getCpuQuota(),
		v1beta1.TFReplicaTypeMaster: getCpuQuota(),
		v1beta1.TFReplicaTypeWorker: getGpuQuota(),
	}
}

func TestValidate(t *testing.T) {
	g := gomega.NewGomegaWithT(t)
	training := getTraining(getCpuTensorflowQuota(), "", false)

	g.Expect(Validate(training)).ShouldNot(gomega.HaveOccurred())

	training = getTraining(getGpuTensorflowQuota(), "", false)
	g.Expect(Validate(training)).Should(gomega.HaveOccurred())

	training = getTraining(getGpuTensorflowQuota(), "gpu", true)
	g.Expect(Validate(training)).ShouldNot(gomega.HaveOccurred())
}

func getTraining(tensorflowQuota v1alpha1.TensorflowQuota, repo string, useGpuImage bool) *v1alpha1.TensorflowTraining {
	return &v1alpha1.TensorflowTraining{
		Spec: v1alpha1.TensorflowTrainingSpec{
			TFJob: v1alpha1.TFJob{
				Origin: v1alpha1.OriginTFJobSpec{
					TensorflowQuota: tensorflowQuota,
					GpuImage: v1alpha1.Image{
						RepositoryPath: repo,
					},
					UseGpuImage: useGpuImage,
				},
			},
		},
	}
}
