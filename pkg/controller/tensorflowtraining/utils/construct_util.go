package utils

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/utils"
	"context"
	"encoding/json"
	"fmt"
	juju "github.com/juju/errors"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	v12 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func GetImage(globalClient client.Client, image string, clusterName string, namespace string) (*v1alpha1.Image, error) {
	var result v1alpha1.Image
	err := json.Unmarshal([]byte(image), &result)
	if err != nil {
		return nil, juju.Trace(err)
	}

	if result.SecretName != "" {
		secret, err := copySecretToBusiness(result, globalClient, namespace, clusterName)
		if err != nil {
			return nil, juju.Trace(err)
		}

		result.SecretName = secret.Name
		result.SecretNamespace = secret.Namespace
		if err != nil {
			return nil, juju.Trace(err)
		}
	}

	//if !strings.Contains(result.CredentialId, namespace) {
	//	glog.V(5).Infof("take care: %s not contain namespace, namespace: %s", image, namespace)
	//	return &result, nil
	//}
	//
	//result.CredentialId = result.CredentialId[len(namespace)+1 : len(result.CredentialId)]
	return &result, nil
}

func copySecretToBusiness(result v1alpha1.Image, globalClient client.Client, namespace string, clusterName string) (*v1.Secret, error) {
	secret := &v1.Secret{}
	secretKey := utils.NewNamespacedName(result.SecretName, result.SecretNamespace)
	err := globalClient.Get(context.TODO(), secretKey, secret)
	if err != nil {
		return nil, juju.Trace(err)
	}
	bussinessSecret := &v1.Secret{}
	bussinessSecret.Name = "ml-copy-" + secret.Name
	bussinessSecret.Namespace = namespace
	bussinessSecret.Data = secret.Data
	bussinessSecret.Type = secret.Type
	clientset, err := utils.NewBusinessMgr(clusterName).K8sClient()

	existed, err := clientset.CoreV1().Secrets(bussinessSecret.Namespace).Get(bussinessSecret.Name, v12.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			return clientset.CoreV1().Secrets(bussinessSecret.Namespace).Create(bussinessSecret)
		}
		return nil, juju.Trace(err)
	}
	existed.Data = bussinessSecret.Data
	return clientset.CoreV1().Secrets(bussinessSecret.Namespace).Update(existed)
}

func GetTensorflowQuota(quota string) (v1alpha1.TensorflowQuota, error) {
	var result v1alpha1.TensorflowQuota
	err := json.Unmarshal([]byte(quota), &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

//TODO zlc refactor
func GetDefineArgs(clusterName string, namespace string, tensorboardPath string, modelPath string) ([]string, error) {
	var result []string
	binding, err := utils.GetClusterProjectBinding(clusterName, namespace)
	if err != nil {
		return nil, juju.Trace(err)
	}
	tensorboardKey, tensorboardRealPath, err := getTrainingPath(binding, tensorboardPath)
	if err != nil {
		return nil, juju.Trace(err)
	}
	modelKey, modelRealPath, err := getTrainingPath(binding, modelPath)
	if err != nil {
		return nil, juju.Trace(err)
	}

	result = append(result, fmt.Sprintf("--%s=%s", tensorboardKey, tensorboardRealPath))
	result = append(result, fmt.Sprintf("--%s=%s", modelKey, modelRealPath))
	return result, nil
}

func getTrainingPath(binding *v1alpha1.MLBinding, mapString string) (string, string, error) {
	key, path, err := DecodePath(mapString)
	if err != nil {
		return "", "", juju.Trace(err)
	}
	pvcPath, err := decodePvcPath(binding, path)
	if err != nil {
		return "", "", juju.Trace(err)
	}
	return key, pvcPath, err

}

func DecodePath(mapString string) (string, string, error) {
	pathMap, err := utils.UnmarshalJsonToMap(mapString)
	if err != nil {
		return "", "", juju.Trace(err)
	}
	path, ok := pathMap["value"]
	key, keyOk := pathMap["key"]
	if !(ok && keyOk) {
		return "", "", fmt.Errorf("not valid Path for tensorboard or model: %s", pathMap)
	}
	return key, path, err

}

func decodePvcPath(binding *v1alpha1.MLBinding, path string) (string, error) {

	if binding == nil {
		return path, nil
	}

	realPath, err := utils.GetMountPath(path, binding)
	if err != nil {
		return "", juju.Trace(err)
	}
	return realPath, nil
}

func GetSelfDefineArgs(defineArgs []string, entrypointArgs string) ([]string, error) {
	args, err := utils.UnmarshalJsonToMap(entrypointArgs)
	if err != nil {
		return nil, juju.Trace(err)
	}

	for key, value := range args {
		if value == "" {
			defineArgs = append(defineArgs, fmt.Sprintf("--%s", key))
		} else {
			defineArgs = append(defineArgs, fmt.Sprintf("--%s=%s", key, value))
		}
	}
	return defineArgs, nil
}

func GetEnvVars(envVars string) ([]v1.EnvVar, error) {
	var result []v1.EnvVar
	args, err := utils.UnmarshalJsonToMap(envVars)
	if err != nil {
		return nil, juju.Trace(err)
	}

	for key, value := range args {
		result = append(result, v1.EnvVar{Name: key, Value: value})
	}
	return result, nil
}
