package utils

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/utils"
	juju "github.com/juju/errors"
	common "github.com/kubeflow/tf-operator/pkg/apis/common/v1beta1"
	"github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta1"
	"k8s.io/api/core/v1"
	corev1 "k8s.io/api/core/v1"
)

func Validate(training *v1alpha1.TensorflowTraining) error {
	return validateGpuOption(training.Spec.TFJob.Origin)
}

func validateGpuOption(originTFJobSpec v1alpha1.OriginTFJobSpec) error {
	if originTFJobSpec.GpuImage.RepositoryPath != "" &&
		originTFJobSpec.UseGpuImage == true {
		return nil
	}

	tensorflowQuotas := originTFJobSpec.TensorflowQuota
	for _, tensorflowQuota := range tensorflowQuotas {
		if needGpuImage(tensorflowQuota) {
			return juju.Errorf("please config gpu image when use gpu")
		}
	}
	return nil
}

func needGpuImage(resourceQuota v1alpha1.ResourceQuota) bool {
	if resourceQuota.Replica == int32(0) {
		return false
	}

	hard := resourceQuota.Quota.Hard
	for key, value := range hard {
		if !value.IsZero() && (key.String() == "limits.nvidia.com/gpu" || key.String() == "limits.amd.com/gpu") {
			return true
		}
	}
	return false
}

func CompleteTFJobSpec(training *v1alpha1.TensorflowTraining, mlBinding *v1alpha1.MLBinding) *v1alpha1.TensorflowTraining {
	result := training.DeepCopy()
	if result.Spec.TFJob.TFJobSpec != nil {
		return result
	}

	specs := make(map[v1beta1.TFReplicaType]*common.ReplicaSpec)
	originTFJobSpec := training.Spec.TFJob.Origin
	quota := originTFJobSpec.TensorflowQuota
	for tfReplicaType, resurceQuota := range quota {
		if resurceQuota.Replica != int32(0) {
			specs[tfReplicaType] = getReplica(resurceQuota, originTFJobSpec, mlBinding)
			//TODO this should be deleted after the py-operator upgrade https://github.com/kubeflow/pytorch-operator/pull/181
			specs[tfReplicaType].Template.Annotations = map[string]string{"scheduling.k8s.io/group-name": training.Name}
		}

	}
	tfJobSpec := v1beta1.TFJobSpec{
		TFReplicaSpecs: specs,
	}
	result.Spec.TFJob.TFJobSpec = &tfJobSpec
	return result
}

func getReplica(quota v1alpha1.ResourceQuota, originTFJobSpec v1alpha1.OriginTFJobSpec, mlBinding *v1alpha1.MLBinding) *common.ReplicaSpec {
	requestsMap := utils.FilteredRequestsResourceMapByKey(quota.Quota.Hard)
	limitsMap := utils.FilteredLimitsResourceMapByKey(quota.Quota.Hard)
	needGpuImage := needGpuImage(quota)
	var originImage v1alpha1.Image
	if needGpuImage {
		originImage = originTFJobSpec.GpuImage
	} else {
		originImage = originTFJobSpec.Image
	}
	var image string
	if originImage.Tag != "" {
		image = originImage.RepositoryPath + ":" + originImage.Tag
	} else {
		image = originImage.RepositoryPath
	}
	podTemplateSpec := &v1.PodTemplateSpec{
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				{
					Name:            "tensorflow",
					Image:           image,
					ImagePullPolicy: corev1.PullAlways,
					Resources: corev1.ResourceRequirements{
						Limits:   limitsMap,
						Requests: requestsMap,
					},
					Args:         originTFJobSpec.Args,
					Env:          originTFJobSpec.Env,
					VolumeMounts: utils.GetVolumeMount(mlBinding),
				},
			},
			Volumes: utils.GetVolumes(mlBinding),
			ImagePullSecrets: []corev1.LocalObjectReference{
				{Name: originImage.SecretName},
			},
		},
	}
	return &common.ReplicaSpec{
		Replicas: &quota.Replica,
		Template: *utils.InjectEnvForNvidia(podTemplateSpec),
	}
}
