package tensorflowtraining

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/amlpipeline"
	"alauda.io/amlopr/pkg/controller/common"
	utils2 "alauda.io/amlopr/pkg/controller/tensorflowtraining/utils"
	"alauda.io/amlopr/pkg/controller/utils"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type TrainingGen struct {
}

func (t TrainingGen) ErrorCrd(clusterName string, key types.NamespacedName, amlpipeline *v1alpha1.AMLPipeline, pipelineErr error) error {
	clientset, err := utils.NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		return juju.Trace(err)
	}
	created, err := clientset.MlV1alpha1().TensorflowTrainings(key.Namespace).Get(key.Name, v1.GetOptions{})

	if created.Status.Phase != v1alpha1.TrainingPhaseEnum.Finished {
		created.Status.Phase = v1alpha1.TrainingPhaseEnum.Finished
		created.Status.State = v1alpha1.TrainingStateEnum.Error
		created.Status.Reason = pipelineErr.Error()
		created.Status.Message = pipelineErr.Error()
		_, err = clientset.MlV1alpha1().TensorflowTrainings(key.Namespace).Update(created)
		return err
	}

	return nil
}

func (TrainingGen) DeleteCrd(clusterName string, key types.NamespacedName, amlpipeline *v1alpha1.AMLPipeline) error {
	clientset, err := utils.NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		if exist, existError := utils.NewBusinessMgr(clusterName).IsExistCluster(); existError != nil && exist == false {
			return nil
		}
		return juju.Trace(err)
	}
	err = clientset.MlV1alpha1().TensorflowTrainings(key.Namespace).Delete(key.Name, &v1.DeleteOptions{})
	if err != nil && errors.IsNotFound(err) {
		return nil
	}
	return juju.Trace(err)
}

func (TrainingGen) GenCrd(client client.Client,
	clusterName string,
	namespacedName types.NamespacedName,
	arguments []devops.PipelineTemplateArgumentGroup,
	pipeline *v1alpha1.AMLPipeline) (common.Object, error) {
	training := &v1alpha1.TensorflowTraining{}
	training.Namespace = namespacedName.Namespace
	training.Name = namespacedName.Name
	valueMap := utils.GetMergedMapGroup(arguments, pipeline.Spec.Parameters)

	image, err := utils2.GetImage(client, valueMap["outImageRepository"], clusterName, namespacedName.Namespace)
	if err != nil {
		return nil, juju.Trace(err)
	}
	training.Spec.TFJob.Origin.Image = *image

	needBuildGpuImage := valueMap["needBuildGpuImage"]
	training.Spec.TFJob.Origin.UseGpuImage = false
	if needBuildGpuImage == "true" {
		gpuImage, err := utils2.GetImage(client, valueMap["gpuOutImageRepository"], clusterName, namespacedName.Namespace)
		if err != nil {
			return nil, juju.Trace(err)
		}
		training.Spec.TFJob.Origin.GpuImage = *gpuImage
		training.Spec.TFJob.Origin.UseGpuImage = true
	}

	defineArgs, err := utils2.GetDefineArgs(clusterName, namespacedName.Namespace, valueMap["tensorboardPath"], valueMap["modelPath"])
	if err != nil {
		return nil, juju.Trace(err)
	}
	defineArgs, err = utils2.GetSelfDefineArgs(defineArgs, valueMap["entrypointArgs"])
	if err != nil {
		return nil, juju.Trace(err)
	}
	training.Spec.TFJob.Origin.Args = defineArgs

	envVars, err := utils2.GetEnvVars(valueMap["envVar"])
	if err != nil {
		return nil, juju.Trace(err)
	}
	training.Spec.TFJob.Origin.Env = envVars

	quota, err := utils2.GetTensorflowQuota(valueMap["resourceQuota"])
	if err != nil {
		return nil, juju.Trace(err)
	}
	training.Spec.TFJob.Origin.TensorflowQuota = quota

	return training, utils2.Validate(training)
}

func (t TrainingGen) CancelCrd(clusterName string, key types.NamespacedName, amlpipeline *v1alpha1.AMLPipeline) error {
	return t.DeleteCrd(clusterName, key, amlpipeline)
}

func (TrainingGen) CreateCrdNotExist(crd common.Object, clusterName string) (common.Object, error) {
	training := crd.(*v1alpha1.TensorflowTraining)
	clientset, err := utils.NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		return nil, juju.Trace(err)
	}
	created, err := clientset.MlV1alpha1().TensorflowTrainings(training.Namespace).Get(training.Name, v1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			return clientset.MlV1alpha1().TensorflowTrainings(training.Namespace).Create(training)
		}
		return created, juju.Trace(err)
	}
	return created, err
}

func init() {
	annotation := v1alpha1.Annotation{Key: "ml.training", Value: "tensorflow"}
	err := amlpipeline.RegisterCrd(annotation, "tensorflowTraining", TrainingGen{})
	if err != nil {
		os.Exit(1)
	}
}
