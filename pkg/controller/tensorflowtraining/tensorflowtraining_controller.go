/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tensorflowtraining

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	juju "github.com/juju/errors"
	"github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new TensorflowTraining Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
// USER ACTION REQUIRED: update cmd/manager/main.go to call this ml.Add(mgr) to install this Controller
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileTensorflowTraining{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("tensorflowtraining-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to TensorflowTraining
	err = c.Watch(&source.Kind{Type: &mlv1alpha1.TensorflowTraining{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &v1beta1.TFJob{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &mlv1alpha1.TensorflowTraining{},
	})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileTensorflowTraining{}

// ReconcileTensorflowTraining reconciles a TensorflowTraining object
type ReconcileTensorflowTraining struct {
	client.Client
	scheme *runtime.Scheme
}

var ctlName = "TFTraining-controller"

// Reconcile reads that state of the cluster for a TensorflowTraining object and makes changes based on the state read
// and what is in the TensorflowTraining.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  The scaffolding writes
// a Deployment as an example
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=ml.alauda.io,resources=tensorflowtrainings,verbs=get;list;watch;create;update;patch;delete
func (r *ReconcileTensorflowTraining) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	logf.SetLogger(logf.ZapLogger(false))
	log := logf.Log.WithName(ctlName)
	log.Info("start Reconcile, request::", "Namespace:", request.Namespace, "Name:", request.Name)

	defer log.Info("Over Reconcile::", "Namespace:", request.Namespace, "Name:", request.Name)
	flowControlProvider := FlowControlProvider{}
	err := flowControlProvider.Dispatch(r, request)
	if err != nil {
		log.Error(err, "reconcile fail:::", "Namespace:", request.Namespace, "Name:", request.Name, "error:", juju.ErrorStack(err))
	}
	return reconcile.Result{}, err
}
