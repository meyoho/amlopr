package namespace

import (
	"alauda.io/amlopr/pkg/apis/base"
	"github.com/onsi/gomega"
	"k8s.io/api/core/v1"
	"testing"
)

func TestIsMLNamespace(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	namespace := &v1.Namespace{}
	g.Expect(IsMLNamespace(namespace), false)

	namespace.Labels = make(map[string]string)
	namespace.Labels[base.MLLabelAppValue] = base.MLLabelAppValue
	g.Expect(IsMLNamespace(namespace), true)

	namespace.Labels[base.MLLabelAppValue] = "d"
	g.Expect(IsMLNamespace(namespace), false)
}
