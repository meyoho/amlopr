package namespace

import (
	"testing"
	"time"

	"alauda.io/amlopr/pkg/apis/base"
	"alauda.io/amlopr/pkg/controller/utils"
	"github.com/onsi/gomega"
	"golang.org/x/net/context"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sync"
)

var c client.Client

var expectedRequest = reconcile.Request{NamespacedName: instanceNamespacedName}

const timeout = time.Second * 5

const instanceName = "test"

var instanceNamespacedName = types.NamespacedName{Name: instanceName}

func TestReconcile(t *testing.T) {
	g, err, requests, stopMgr, mgrStopped := setUp(t)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	namespace := getNamespace()
	err = c.Create(context.TODO(), namespace)
	// The instance object may not be a valid object because it might be missing some required fields.
	// Please modify the instance object by adding required fields and then remove the following if statement.
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}

	g.Expect(err).NotTo(gomega.HaveOccurred())
	g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))

	//testCreateInstance(g)
	//testDeleteLable(namespace, g)

}

func testDeleteLable(namespace *corev1.Namespace, g *gomega.GomegaWithT) {
	key := utils.NewNamespacedName(instanceName, "")
	err := c.Get(context.TODO(), key, namespace)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	namespace.Labels = nil
	err = c.Update(context.TODO(), namespace)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	serviceAccount := &corev1.ServiceAccount{}
	key = utils.NewNamespacedName(ServiceAccountName, instanceName)
	g.Eventually(apierrors.IsNotFound(c.Get(context.TODO(), key, serviceAccount)))
}

func testCreateInstance(g *gomega.GomegaWithT) *corev1.ServiceAccount {
	serviceAccount := &corev1.ServiceAccount{}
	key := utils.NewNamespacedName(ServiceAccountName, instanceName)
	g.Eventually(func() error { return c.Get(context.TODO(), key, serviceAccount) }, timeout).
		Should(gomega.Succeed())
	return serviceAccount
}

func setUp(t *testing.T) (*gomega.GomegaWithT, error, chan reconcile.Request, chan struct{}, *sync.WaitGroup) {
	g := gomega.NewGomegaWithT(t)
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())
	c = mgr.GetClient()
	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())
	stopMgr, mgrStopped := StartTestManager(mgr, g)
	return g, err, requests, stopMgr, mgrStopped
}

func getNamespace() *corev1.Namespace {
	return &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: instanceName,
			Labels: map[string]string{
				base.MLLabelAppKey: base.MLLabelAppValue,
			},
		},
	}
}
