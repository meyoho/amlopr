package namespace

import (
	"alauda.io/amlopr/pkg/apis/base"
	"k8s.io/api/core/v1"
)

func IsMLNamespace(namespace *v1.Namespace) bool {
	value, ok := namespace.Labels[base.MLLabelAppKey]
	if !ok || value != base.MLLabelAppValue {
		return false
	}
	return true
}
