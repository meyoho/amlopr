package namespace

import (
	"alauda.io/amlopr/pkg/controller/utils"
	"context"
	juju "github.com/juju/errors"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	RoleName           = "aml-auto-role"
	ServiceAccountName = "aml-auto"
	RoleBindingName    = "aml-auto-RoleBinding"
)

type FlowControlProvider struct {
}

func (fc *FlowControlProvider) Dispatch(reconcileNamespace *ReconcileNamespace, request reconcile.Request) error {
	return nil
	//parent := &corev1.Namespace{}
	//namespacedName := utils.NewNamespacedName(request.Name, "")
	//err := reconcileNamespace.Get(context.TODO(), namespacedName, parent)
	//namespace := request.Name
	//if err != nil {
	//	if errors.IsNotFound(err) {
	//		return nil
	//	} else {
	//		return err
	//	}
	//}
	//
	//// TODO refactor
	//if IsMLNamespace(parent) {
	//	err = createRole(reconcileNamespace, parent)
	//	if err != nil {
	//		return juju.Trace(err)
	//	}
	//
	//	err = createRoleBinding(reconcileNamespace, parent)
	//	if err != nil {
	//		return juju.Trace(err)
	//	}
	//
	//	err = createServiceAccount(reconcileNamespace, parent)
	//	if err != nil {
	//		return juju.Trace(err)
	//	}
	//} else {
	//	err = DeleteRole(reconcileNamespace, namespace)
	//	if err != nil {
	//		return juju.Trace(err)
	//	}
	//
	//	err = DeleteRoleBinding(reconcileNamespace, namespace)
	//	if err != nil {
	//		return juju.Trace(err)
	//	}
	//
	//	err = DeleteServiceAccount(reconcileNamespace, namespace)
	//	if err != nil {
	//		return juju.Trace(err)
	//	}
	//}
	//
	//return err
}

func DeleteRole(reconcileNamespace *ReconcileNamespace, namespace string) error {
	getRole := &v1.Role{}
	namespacedName := utils.NewNamespacedName(RoleName, namespace)
	err := reconcileNamespace.Get(context.TODO(), namespacedName, getRole)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return juju.Trace(err)
	}
	return reconcileNamespace.Delete(context.TODO(), getRole)
}

func createRole(reconcileNamespace *ReconcileNamespace, k8sNamespace *corev1.Namespace) error {
	getRole := &v1.Role{}
	namespace := k8sNamespace.Name
	namespacedName := utils.NewNamespacedName(RoleName, namespace)
	err := reconcileNamespace.Get(context.TODO(), namespacedName, getRole)
	if err != nil {
		if errors.IsNotFound(err) {
			created := &v1.Role{
				ObjectMeta: metav1.ObjectMeta{
					Name:      RoleName,
					Namespace: namespace,
				},
				Rules: []v1.PolicyRule{
					{
						APIGroups: []string{"*"},
						Resources: []string{"*"},
						Verbs:     []string{"*"},
					},
				},
			}
			err := controllerutil.SetControllerReference(k8sNamespace, created, reconcileNamespace.scheme)
			if err != nil {
				return juju.Trace(err)
			}
			return reconcileNamespace.Create(context.TODO(), created)
		}
	}

	return nil
}

func createRoleBinding(reconcileNamespace *ReconcileNamespace, k8sNamespace *corev1.Namespace) error {
	getRole := &v1.RoleBinding{}
	namespace := k8sNamespace.Name
	namespacedName := utils.NewNamespacedName(RoleBindingName, namespace)
	err := reconcileNamespace.Get(context.TODO(), namespacedName, getRole)
	if err != nil {
		if errors.IsNotFound(err) {
			created := &v1.RoleBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      RoleBindingName,
					Namespace: namespace,
				},
				RoleRef: v1.RoleRef{
					APIGroup: "rbac.authorization.k8s.io",
					Kind:     "Role",
					Name:     RoleName,
				},
				Subjects: []v1.Subject{
					{
						Name: ServiceAccountName,
						Kind: "ServiceAccount",
					},
				},
			}
			err := controllerutil.SetControllerReference(k8sNamespace, created, reconcileNamespace.scheme)
			if err != nil {
				return juju.Trace(err)
			}
			return reconcileNamespace.Create(context.TODO(), created)
		}
	}

	return nil
}

func DeleteRoleBinding(reconcileNamespace *ReconcileNamespace, namespace string) error {
	getRoleBinding := &v1.RoleBinding{}
	namespacedName := utils.NewNamespacedName(RoleBindingName, namespace)
	err := reconcileNamespace.Get(context.TODO(), namespacedName, getRoleBinding)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return juju.Trace(err)
	}
	return reconcileNamespace.Delete(context.TODO(), getRoleBinding)
}

func createServiceAccount(reconcileNamespace *ReconcileNamespace, k8sNamespace *corev1.Namespace) error {
	getRole := &corev1.ServiceAccount{}
	namespace := k8sNamespace.Name
	namespacedName := utils.NewNamespacedName(ServiceAccountName, namespace)
	err := reconcileNamespace.Get(context.TODO(), namespacedName, getRole)
	if err != nil {
		if errors.IsNotFound(err) {
			created := &corev1.ServiceAccount{
				ObjectMeta: metav1.ObjectMeta{
					Name:      ServiceAccountName,
					Namespace: namespace,
				},
			}
			err := controllerutil.SetControllerReference(k8sNamespace, created, reconcileNamespace.scheme)
			if err != nil {
				return juju.Trace(err)
			}
			return reconcileNamespace.Create(context.TODO(), created)
		}
	}

	return nil
}

func DeleteServiceAccount(reconcileNamespace *ReconcileNamespace, namespace string) error {
	getServiceAccount := &corev1.ServiceAccount{}
	namespacedName := utils.NewNamespacedName(ServiceAccountName, namespace)
	err := reconcileNamespace.Get(context.TODO(), namespacedName, getServiceAccount)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return juju.Trace(err)
	}
	return reconcileNamespace.Delete(context.TODO(), getServiceAccount)
}
