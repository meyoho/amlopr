package mlbinding

import "alauda.io/amlopr/pkg/apis/ml/v1alpha1"

type Subject struct {
	SubjectInstance     *v1alpha1.MLBinding
	subjectInstanceCopy *v1alpha1.MLBinding
}

func (s *Subject) GetReadableInstance() *v1alpha1.MLBinding {
	if s.subjectInstanceCopy == nil {
		s.subjectInstanceCopy = s.SubjectInstance.DeepCopy()
	}
	return s.subjectInstanceCopy
}

func (s *Subject) GetCurrentStage() v1alpha1.BaseStage {
	return s.GetReadableInstance().Status.AmlBaseStatus.Phase
}

func (s *Subject) SetCurrentState(state v1alpha1.BaseState, phase v1alpha1.BaseStage, reason string, message string) {
	s.SubjectInstance.Status.AmlBaseStatus.State = state
	s.SubjectInstance.Status.AmlBaseStatus.Phase = phase
	s.SubjectInstance.Status.Reason = reason
	s.SubjectInstance.Status.Message = reason
	return
}
