package mlbinding

import (
	mlbase "alauda.io/amlopr/pkg/apis/base"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/config"
	"alauda.io/amlopr/pkg/config/template"
	"alauda.io/amlopr/pkg/controller/base"
	"alauda.io/amlopr/pkg/controller/common"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"reflect"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type dataviewerPodComp struct {
	Parent *Subject
	Client client.Client
	scheme *runtime.Scheme
}

func NewComp(parent *Subject, client client.Client, scheme *runtime.Scheme) *dataviewerPodComp {
	return &dataviewerPodComp{
		Parent: parent,
		Client: client,
		scheme: scheme,
	}
}

func (c *dataviewerPodComp) getName() string {
	return fmt.Sprintf("%s-%s", c.Parent.SubjectInstance.Name, "dm")
}

func (c *dataviewerPodComp) getNamespace() string {
	return c.Parent.SubjectInstance.GetNamespace()
}

func (c *dataviewerPodComp) Sync() error {
	dataViewer, err := c.parseTo()
	if err != nil {
		return juju.Trace(err)
	}

	existDataViewer := &v1alpha1.DataViewer{}
	isExist, err := c.isExist(existDataViewer)
	if err != nil {
		return juju.Trace(err)
	}

	if isExist {
		if !reflect.DeepEqual(existDataViewer.Spec.Pvc, dataViewer.Spec.Pvc) {
			existDataViewer.Spec.Pvc = dataViewer.Spec.Pvc
			return c.Client.Update(context.TODO(), existDataViewer)
		}
		return nil
	}
	return c.Client.Create(context.TODO(), dataViewer)
}

func (c *dataviewerPodComp) isExist(dataViewer *v1alpha1.DataViewer) (bool, error) {
	newRequest := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Namespace: c.getNamespace(),
			Name:      c.getName(),
		}}
	return base.FindAndInitInstance(c.Client, newRequest, dataViewer)
}

const (
	DataviewerCreator = "tensorboard"
)

func (c *dataviewerPodComp) parseTo() (*v1alpha1.DataViewer, error) {
	result := &v1alpha1.DataViewer{}
	result.Name = c.getName()
	result.Namespace = c.getNamespace()
	result.Labels = mlbase.NewMLAppLabel().
		Add(common.GetLabelKeyForCreatorOf(DataviewerCreator), c.Parent.SubjectInstance.GetName()).
		Add("dataManage.ml", "true").
		Get()
	result.Spec.Type = v1alpha1.DataManager
	result.Spec.Pvc = c.Parent.GetReadableInstance().Spec.Pvc

	configMap, err := config.GetGlobalConfigMap(c.Client)
	if err != nil {
		return nil, juju.Trace(err)
	}
	result.Spec.BaseUrl = config.GetBaseUrl(configMap)
	result.Spec.PodTemplateSpec = *template.GetDataManagerTemplate()
	result.Status.Phase = v1alpha1.DataViewerPhaseEnum.Running

	err = controllerutil.SetControllerReference(c.Parent.SubjectInstance, result, c.scheme)
	return result, juju.Trace(err)
}
