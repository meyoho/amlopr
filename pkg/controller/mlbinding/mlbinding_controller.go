/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package mlbinding

import (
	amlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"github.com/juju/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new MLBinding Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
// USER ACTION REQUIRED: update cmd/manager/main.go to call this ml.Add(mgr) to install this Controller
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileMLBinding{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("mlbinding-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to MLBinding
	err = c.Watch(&source.Kind{Type: &amlv1alpha1.MLBinding{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// Modify this to be the types you create
	// Uncomment watch a Deployment created by MLBinding - change this for objects you create
	err = c.Watch(&source.Kind{Type: &amlv1alpha1.DataViewer{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &amlv1alpha1.MLBinding{},
	})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileMLBinding{}

// ReconcileMLBinding reconciles a MLBinding object
type ReconcileMLBinding struct {
	client.Client
	scheme *runtime.Scheme
}

var ctlName = "mlbinding-controller"

// Reconcile reads that state of the cluster for a MLBinding object and makes changes based on the state read
// and what is in the MLBinding.Spec
// Modify this Reconcile function to implement your Controller logic.  The scaffolding writes
// a Deployment as an example
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=ml.alauda.io,resources=mlbindings,verbs=get;list;watch;create;update;patch;delete
func (r *ReconcileMLBinding) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the MLBinding instance
	logf.SetLogger(logf.ZapLogger(false))
	log := logf.Log.WithName(ctlName)
	log.Info("start Reconcile")
	log.Info("request::", "Namespace:", request.Namespace, "Name:", request.Name)

	defer log.Info("Over Reconcile::", "Namespace:", request.Namespace, "Name:", request.Name)
	flowControlProvider := FlowControlProvider{}
	err := flowControlProvider.Dispatch(r, request)
	if err != nil {
		log.Error(err, "reconcile fail:::", "Namespace:", request.Namespace, "Name:", request.Name, "error:", errors.ErrorStack(err))
	}
	return reconcile.Result{}, err
}
