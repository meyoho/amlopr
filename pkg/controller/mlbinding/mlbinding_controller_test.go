/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package mlbinding

import (
	"testing"
	"time"

	. "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"github.com/onsi/gomega"
	"golang.org/x/net/context"
	"k8s.io/api/core/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"reflect"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sync"
)

var c client.Client

var expectedRequest = reconcile.Request{NamespacedName: instanceNamespacedName}

const timeout = time.Second * 5

const instanceName = "foo"
const instanceNameSpace = "default"
const configMapName = "amlopr-cm"
const configMapNameSpace = "default"
const imageName = "imageName"
const baseUrl = "/aml/storageui/"

var instanceNamespacedName = types.NamespacedName{Name: instanceName, Namespace: instanceNameSpace}

func TestReconcile(t *testing.T) {
	g, instance, err, requests, stopMgr, mgrStopped := setUp(t)

	os.Setenv("CM_NAME", configMapName)
	os.Setenv("POD_NAMESPACE", configMapNameSpace)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	configMap := getConfigMap()
	err = c.Create(context.TODO(), configMap)
	// The instance object may not be a valid object because it might be missing some required fields.
	// Please modify the instance object by adding required fields and then remove the following if statement.
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}

	// Create the MLBinding object and expect the Reconcile and Deployment to be created
	err = c.Create(context.TODO(), instance)
	// The instance object may not be a valid object because it might be missing some required fields.
	// Please modify the instance object by adding required fields and then remove the following if statement.
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}

	g.Expect(err).NotTo(gomega.HaveOccurred())
	defer c.Delete(context.TODO(), instance)
	g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))

	testCreateInstance(instance, g)

	testModifyInstance(g, requests)

}

func testCreateInstance(instance *MLBinding, g *gomega.GomegaWithT) *DataViewer {
	depKey := getDataViewerKey(instance)
	dataViewer := &DataViewer{}
	g.Eventually(func() error { return c.Get(context.TODO(), depKey, dataViewer) }, timeout).
		Should(gomega.Succeed())
	return dataViewer
}

func getDataViewerKey(instance *MLBinding) types.NamespacedName {
	subject := &Subject{SubjectInstance: instance}
	comp := NewComp(subject, nil, nil)
	var depKey = types.NamespacedName{Name: comp.getName(), Namespace: comp.getNamespace()}
	return depKey
}

func setUp(t *testing.T) (*gomega.GomegaWithT, *MLBinding, error, chan reconcile.Request, chan struct{}, *sync.WaitGroup) {
	g := gomega.NewGomegaWithT(t)
	instance := getMLBindingInstance()
	// Setup the Manager and Controller.  Wrap the Controller Reconcile function so it writes each request to a
	// channel when it is finished.
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())
	c = mgr.GetClient()
	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())
	stopMgr, mgrStopped := StartTestManager(mgr, g)
	return g, instance, err, requests, stopMgr, mgrStopped
}

func testModifyInstance(g *gomega.GomegaWithT, requests chan reconcile.Request) {
	instance := &MLBinding{}
	err := c.Get(context.TODO(), instanceNamespacedName, instance)
	g.Expect(err).ShouldNot(gomega.HaveOccurred())
	appendPvc(instance)
	err = c.Update(context.TODO(), instance)
	g.Expect(err).ShouldNot(gomega.HaveOccurred())
	g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))
	depKey := getDataViewerKey(instance)
	compareFunc := func() bool {
		dataViewer := &DataViewer{}
		c.Get(context.TODO(), depKey, dataViewer)
		return reflect.DeepEqual(dataViewer.Spec.Pvc, instance.Spec.Pvc)
	}
	g.Eventually(compareFunc, timeout).Should(gomega.BeTrue())
}

func getMLBindingInstance() *MLBinding {
	return &MLBinding{
		ObjectMeta: metav1.ObjectMeta{Name: "foo", Namespace: "default"},
		Spec: MLBindingSpec{
			Pvc: []PVC{
				{
					Volume: v1.Volume{
						Name: "pvc1",
						VolumeSource: v1.VolumeSource{
							PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
								ClaimName: "pvc1",
							},
						},
					},
					MountPath: "/aml",
				},
			},
		},
		Status: MLBindingStatus{
			AmlBaseStatus: AmlBaseStatus{
				State: StateCreating,
			},
		}}
}

func getConfigMap() *corev1.ConfigMap {
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{Name: configMapName, Namespace: configMapNameSpace},
		Data:       map[string]string{"UIStorageImage": imageName, "UIBaseUrl": baseUrl},
	}
}

func appendPvc(instance *MLBinding) {
	pvc := PVC{
		Volume: v1.Volume{
			Name: "pvc2",
			VolumeSource: v1.VolumeSource{
				PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
					ClaimName: "pvc2",
				},
			},
		},
		MountPath: "/aml",
	}
	instance.Spec.Pvc = append(instance.Spec.Pvc, pvc)

}
