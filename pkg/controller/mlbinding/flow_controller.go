package mlbinding

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"context"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type FlowControlProvider struct {
}

func (fc *FlowControlProvider) Dispatch(reconcileMLBinding *ReconcileMLBinding, request reconcile.Request) error {
	parent := &v1alpha1.MLBinding{}
	err := reconcileMLBinding.Get(context.TODO(), request.NamespacedName, parent)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		} else {
			return err
		}
	}
	subject := &Subject{SubjectInstance: parent}
	return NewComp(subject, reconcileMLBinding.Client, reconcileMLBinding.scheme).Sync()

}
