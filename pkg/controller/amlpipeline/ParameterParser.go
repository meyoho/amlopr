package amlpipeline

import (
	v1alpha12 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	//logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"github.com/juju/errors"
)

//var log = logf.Log.WithName(ctlName)

type ParameterParser = func(v1alpha1.PipelineTemplateArgumentValue) string

var ParameterParserMap = make(map[string]ParameterParser)

func RegisterParameterParser(parameterType string, parameterPaser ParameterParser) {
	ParameterParserMap[parameterType] = parameterPaser
}

func ParseParameterItem(value devopsv1alpha1.PipelineTemplateArgumentValue) string {
	parser, ok := ParameterParserMap[value.Schema.Type]
	if ok {
		return parser(value)
	} else {
		return value.Value
	}
}

func ParseParameter(amlPipeline *v1alpha12.AMLPipeline) ([]devopsv1alpha1.PipelineParameter, error) {
	var result []devopsv1alpha1.PipelineParameter
	result = addParaFromAMlPipeline(amlPipeline, result)

	result = addParaFromCrd(amlPipeline, result)

	return addNamespacePara(result, amlPipeline)
}

func addNamespacePara(result []devopsv1alpha1.PipelineParameter, amlPipeline *v1alpha12.AMLPipeline) ([]devopsv1alpha1.PipelineParameter, error) {
	cluster, namespace, err := getCrdNamespace(amlPipeline.Spec.Parameters)
	if err != nil {
		return nil, errors.Trace(err)
	}
	result = append(
		result,
		devopsv1alpha1.PipelineParameter{
			Name:  "namespace",
			Type:  devopsv1alpha1.PipelineParameterType("string"),
			Value: namespace,
		},
		devopsv1alpha1.PipelineParameter{
			Name:  "cluster",
			Type:  devopsv1alpha1.PipelineParameterType("string"),
			Value: cluster,
		},
	)

	return result, nil
}

func addParaFromCrd(amlPipeline *v1alpha12.AMLPipeline, result []devopsv1alpha1.PipelineParameter) []devopsv1alpha1.PipelineParameter {
	crdList := amlPipeline.Status.CRDList
	var i = 0
	for _, crd := range crdList {
		i++
		result = append(result, devopsv1alpha1.PipelineParameter{
			Name:  crd.Kind,
			Type:  devopsv1alpha1.PipelineParameterType("string"),
			Value: crd.Name,
		})
	}
	//log.Info("crd parameters", "run in crd", i)
	return result
}

func addParaFromAMlPipeline(amlPipeline *v1alpha12.AMLPipeline, result []devopsv1alpha1.PipelineParameter) []devopsv1alpha1.PipelineParameter {
	parameters := amlPipeline.Spec.Parameters
	for _, parameter := range parameters {
		for _, item := range parameter.Items {
			value := ParseParameterItem(item)
			result = append(result, devopsv1alpha1.PipelineParameter{
				Name:  item.Name,
				Type:  devopsv1alpha1.PipelineParameterType("string"),
				Value: value,
			})
		}
	}
	return result
}
