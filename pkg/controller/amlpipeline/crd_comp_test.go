package amlpipeline

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"testing"
)

var tensorflowTraining = getTensorflowTraining()

type simpleTFGen struct {
}

func (simpleTFGen) GenCrd(client client.Client,
	clustrName string,
	namespacedName types.NamespacedName,
	arguments []devops.PipelineTemplateArgumentGroup,
	amlPipeline *v1alpha1.AMLPipeline) (common.Object, error) {
	return tensorflowTraining, nil
}

func (simpleTFGen) ErrorCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline, err error) error {
	panic("implement me")
}

func (simpleTFGen) DeleteCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline) error {
	panic("implement me")
}

func (simpleTFGen) CreateCrdNotExist(crd common.Object, clusterName string) (common.Object, error) {
	panic("implement me")
}

func (simpleTFGen) CancelCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline) error {
	panic("implement me")
}

func TestRegisterCrdGenerator(t *testing.T) {
	annotation := v1alpha1.Annotation{Key: "Key", Value: "Value"}
	RegisterCrd(annotation, "tensorflow", &simpleTFGen{})
	created, _ := crdGenMap[annotation]["tensorflow"].GenCrd(ReconcileAMLPipeline{}, "", utils.NewNamespacedName("test", "default"), nil, nil)

	g := gomega.NewGomegaWithT(t)

	g.Expect(tensorflowTraining).To(gomega.Equal(created))
	notFoundAnnotation := v1alpha1.Annotation{Key: "key1", Value: "Value"}
	_, ok := crdGenMap[notFoundAnnotation]
	g.Expect(ok, false)
}

func getTensorflowTraining() *v1alpha1.TensorflowTraining {
	return &v1alpha1.TensorflowTraining{
		ObjectMeta: v1.ObjectMeta{
			Name:      "test",
			Namespace: "default",
		},
	}
}
