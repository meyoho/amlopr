package amlpipeline

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"fmt"
	"github.com/juju/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/apiutil"
)

type CrdGenerator interface {
	GenCrd(client client.Client,
		clustrName string,
		namespacedName types.NamespacedName,
		arguments []devops.PipelineTemplateArgumentGroup,
		amlPipeline *v1alpha1.AMLPipeline) (common.Object, error)

	CancelCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline) error

	DeleteCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline) error

	ErrorCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline, err error) error

	CreateCrdNotExist(crd common.Object, clusterName string) (common.Object, error)
}

var crdGenMap = make(map[v1alpha1.Annotation]map[string]CrdGenerator)

type crdWithAnnotation struct {
	annotation v1alpha1.Annotation
	uuid       string
	crd        common.Object
}

func RegisterCrd(annotation v1alpha1.Annotation, uuid string, generator CrdGenerator) error {
	crdGens, ok := crdGenMap[annotation]
	if !ok {
		crdGens = make(map[string]CrdGenerator)
	}

	_, ok = crdGens[uuid]
	if ok {
		return fmt.Errorf("the uuid is duplicated: %s with annotation: %s, %s", uuid, annotation.Key, annotation.Value)
	}

	crdGens[uuid] = generator
	crdGenMap[annotation] = crdGens
	return nil
}

func NewAnnotation(key string, value string) v1alpha1.Annotation {
	return v1alpha1.Annotation{
		Key:   key,
		Value: value,
	}
}

func (crdComp *CrdComp) CreateCrd(client client.Client, annotation v1alpha1.Annotation, namespacedName types.NamespacedName) ([]crdWithAnnotation, error) {
	crdGenerators, _ := crdGenMap[annotation]
	var result []crdWithAnnotation
	for key, crdGenerator := range crdGenerators {
		crd, err := crdGenerator.GenCrd(client, crdComp.clusterName, namespacedName, crdComp.amlPipelineConfig.Spec.Arguments, crdComp.Parent.SubjectInstance)
		if err != nil {
			return nil, errors.Trace(err)
		}

		setLabelAnnotation(crdComp, crd)
		created, err := crdGenerator.CreateCrdNotExist(crd, crdComp.clusterName)
		if err != nil {
			return nil, errors.Trace(err)
		}
		result = append(result, crdWithAnnotation{annotation, key, created})
	}
	return result, nil
}

func (crdComp *CrdComp) CreateCrds(client client.Client, annotations map[string]string, namespacedName types.NamespacedName) ([]crdWithAnnotation, error) {
	var result []crdWithAnnotation
	for key, value := range annotations {
		annotation := NewAnnotation(key, value)
		crds, err := crdComp.CreateCrd(client, annotation, namespacedName)

		if err != nil {
			return nil, err
		}
		result = append(result, crds...)
	}
	return result, nil
}

type CrdComp struct {
	client client.Client
	scheme *runtime.Scheme
	Parent *AMLPipelineSubject

	amlPipelineConfig   *v1alpha1.AMLPipelineConfig
	amlPipelineTemplate *v1alpha1.AMLPipelineTemplate
	clusterName         string
	namespace           string
}

func (crdComp *CrdComp) checkAndInit() error {
	if crdComp.amlPipelineConfig == nil {
		amlPipelineConfigName := crdComp.Parent.SubjectInstance.Spec.AMLPipelineConfig.Name
		namespacedName := utils.NewNamespacedName(amlPipelineConfigName, crdComp.Parent.SubjectInstance.Namespace)
		amlPipelineConfig := &v1alpha1.AMLPipelineConfig{}
		err := crdComp.client.Get(context.TODO(), namespacedName, amlPipelineConfig)
		if err != nil {
			return errors.Trace(err)
		}
		crdComp.amlPipelineConfig = amlPipelineConfig
	}

	if crdComp.amlPipelineTemplate == nil {
		amlPipelineTemplateName := crdComp.amlPipelineConfig.Spec.AMLPipelineTemplate.Name
		amlPipelineTemplateNamespace := crdComp.amlPipelineConfig.Spec.AMLPipelineTemplate.Namespace
		namespacedName := utils.NewNamespacedName(amlPipelineTemplateName, amlPipelineTemplateNamespace)
		amlPipelineTemplate := &v1alpha1.AMLPipelineTemplate{}
		err := crdComp.client.Get(context.TODO(), namespacedName, amlPipelineTemplate)
		if err != nil {
			return errors.Trace(err)
		}
		crdComp.amlPipelineTemplate = amlPipelineTemplate
	}

	if crdComp.clusterName == "" || crdComp.namespace == "" {
		crdCluster, namespace, err := getCrdNamespace(crdComp.Parent.GetReadableInstance().Spec.Parameters)
		if err != nil {
			return errors.Trace(err)
		}
		crdComp.clusterName = crdCluster
		crdComp.namespace = namespace
	}

	return nil
}

func getCrdNamespace(Parameters []devops.PipelineTemplateArgumentGroup) (string, string, error) {
	mapGroup := utils.GetMergedMapGroup(Parameters, nil)
	crdNamespace, ok := mapGroup["processNamespace"]
	if !ok {
		return "", "", fmt.Errorf("no process namespace config")
	}
	crdNamespaceMap, err := utils.UnmarshalJsonToMap(crdNamespace)
	if err != nil {
		return "", "", errors.Trace(err)
	}
	cluster, ok := crdNamespaceMap["cluster"]
	if !ok {
		return "", "", fmt.Errorf("no process namespace config")
	}

	namespace, ok := crdNamespaceMap["namespace"]
	if !ok {
		return "", "", fmt.Errorf("no process namespace config")
	}
	return cluster, namespace, nil
}

func NewCrdComp(reconcileAMLPipeline *ReconcileAMLPipeline, Parent *AMLPipelineSubject) *CrdComp {
	crdComp := &CrdComp{
		client: reconcileAMLPipeline.Client,
		scheme: reconcileAMLPipeline.Scheme,
		Parent: Parent,
	}

	return crdComp
}

func (crdComp *CrdComp) doFuncOnCRDs(function string, option interface{}) error {
	var errorSlice []error
	crdList := crdComp.Parent.SubjectInstance.Status.CRDList
	for _, crdReference := range crdList {
		annotation := crdReference.Annotation
		crdGenerators, ok := crdGenMap[annotation]
		if !ok {
			errorSlice = append(errorSlice, fmt.Errorf("cannot find cannel fun for annotation: %s, %s", annotation.Key, annotation.Value))
		}

		crdGenerator, ok := crdGenerators[crdReference.UUID]
		if !ok {
			errorSlice = append(errorSlice, fmt.Errorf("cannot find cannel fun for uuid: %s, %s, %s", annotation.Key, annotation.Value, crdReference.UUID))
		} else {
			err := crdComp.DoFuncOnCRD(function, crdGenerator, crdReference, option)

			if err != nil {
				errorSlice = append(errorSlice, err)
			}
		}
	}
	return common.MergeSimpleErrors(errorSlice)
}

const (
	CancelFunctionName = "cancel"
	DeleteFunctionName = "delete"
	ErrorFunctionName  = "error"
)

func (crdComp *CrdComp) DoFuncOnCRD(function string, crdGenerator CrdGenerator, crdReference v1alpha1.CRDReference, option interface{}) error {
	var err error
	switch function {
	case CancelFunctionName:
		err = crdGenerator.CancelCrd(crdReference.ClusterName, utils.NewNamespacedName(crdReference.Name, crdReference.Namespace), crdComp.Parent.GetReadableInstance())
	case DeleteFunctionName:
		err = crdGenerator.DeleteCrd(crdReference.ClusterName, utils.NewNamespacedName(crdReference.Name, crdReference.Namespace), crdComp.Parent.GetReadableInstance())
	case ErrorFunctionName:
		pipelineErr, ok := option.(error)
		if !ok {
			err = fmt.Errorf("cannot cast to Error")
			break
		}
		err = crdGenerator.ErrorCrd(crdReference.ClusterName, utils.NewNamespacedName(crdReference.Name, crdReference.Namespace), crdComp.Parent.GetReadableInstance(), pipelineErr)
	default:
		err = fmt.Errorf("no function %s defined", function)
	}
	return err
}

func (crdComp *CrdComp) error(err error) error {
	return crdComp.doFuncOnCRDs(ErrorFunctionName, err)
}

func (crdComp *CrdComp) delete() error {
	return crdComp.doFuncOnCRDs(DeleteFunctionName, nil)
}

func (crdComp *CrdComp) cancel() error {
	return crdComp.doFuncOnCRDs(CancelFunctionName, nil)
}

func (crdComp *CrdComp) SyncCrdByAnnotation() ([]crdWithAnnotation, error) {
	err := crdComp.checkAndInit()
	if err != nil {
		return nil, errors.Trace(err)
	}

	var result []crdWithAnnotation
	namespacedName := utils.NewNamespacedName(crdComp.Parent.SubjectInstance.Name, crdComp.namespace)
	crdWithAnnotations, err := crdComp.CreateCrds(crdComp.client, crdComp.amlPipelineTemplate.ObjectMeta.Annotations, namespacedName)
	if err != nil {
		return crdWithAnnotations, err
	}

	crdComp.updateAMLPipeline(crdWithAnnotations)
	return result, nil
}

func setLabelAnnotation(crdMgr *CrdComp, crd common.Object) {
	crd.SetAnnotations(common.NewAmlAnnotation().Create().AddMap(crd.GetAnnotations()).Get())
	crd.SetLabels(common.NewAmlLabel(crd.GetLabels()).
		Add(AMLPipelineLabelKey, AMLPipelineLabelValue).
		Add(common.GetLabelKeyForCreatorOf(AMLPipelineCreator), crdMgr.Parent.SubjectInstance.Name).
		Get())
}

func (crdComp *CrdComp) updateAMLPipeline(crdWithAnnotations []crdWithAnnotation) error {
	var crdArray []v1alpha1.CRDReference
	for _, crdWithAnnotation := range crdWithAnnotations {
		item := crdWithAnnotation.crd
		gvk, err := apiutil.GVKForObject(item, crdComp.scheme)
		if err != nil {
			return err
		}
		kind := gvk.Kind
		name := item.GetName()
		crdArray = append(crdArray, v1alpha1.CRDReference{
			Kind:        kind,
			Name:        name,
			Namespace:   item.GetNamespace(),
			ClusterName: crdComp.clusterName,
			UUID:        crdWithAnnotation.uuid,
			Annotation:  crdWithAnnotation.annotation,
		})
	}
	crdComp.Parent.SubjectInstance.Status.CRDList = crdArray
	return nil
}
