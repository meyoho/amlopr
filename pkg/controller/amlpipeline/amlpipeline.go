package amlpipeline

import (
	"alauda.io/amlopr/pkg/apis/base"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
)

type AMLPipelineSubject struct {
	SubjectInstance     *v1alpha1.AMLPipeline
	subjectInstanceCopy *v1alpha1.AMLPipeline
}

func (s *AMLPipelineSubject) GetReadableInstance() *v1alpha1.AMLPipeline {
	if s.subjectInstanceCopy == nil {
		s.subjectInstanceCopy = s.SubjectInstance.DeepCopy()
	}
	return s.subjectInstanceCopy
}

func (s *AMLPipelineSubject) GetWriteableInstance() *v1alpha1.AMLPipeline {
	return s.SubjectInstance
}

func (s *AMLPipelineSubject) GetCurrentState() v1alpha1.BaseState {
	return s.SubjectInstance.Status.AmlBaseStatus.State
}

func (s *AMLPipelineSubject) GetCurrentPhase() v1alpha1.BaseStage {
	return s.SubjectInstance.Status.AmlBaseStatus.Phase
}

func (s *AMLPipelineSubject) SetCurrentPhase(stage v1alpha1.BaseStage) {
	if stage == v1alpha1.AMLPipelinePhaseEnum.Deleting {
		s.SubjectInstance.Labels = common.NewAmlLabel(s.SubjectInstance.Labels).
			Add("deleting", "true").Get()
	}
	s.SubjectInstance.Status.AmlBaseStatus.Phase = stage
	return
}

func (s *AMLPipelineSubject) SetCurrentState(state v1alpha1.BaseState, reason string, message string) {
	s.SubjectInstance.Status.AmlBaseStatus.State = state
	s.SubjectInstance.Status.Reason = reason
	s.SubjectInstance.Status.Message = reason
	return
}

func (s *AMLPipelineSubject) RemoveMLFinalizer() {
	finalizers := s.SubjectInstance.Finalizers
	s.SubjectInstance.Finalizers = utils.Delete(finalizers, base.PipelineFinalizer)
	return
}

func (s *AMLPipelineSubject) GetUpdatePipeline() *v1alpha1.AMLPipeline {
	deepCopy := s.SubjectInstance.DeepCopy()
	switch deepCopy.Status.State {
	case v1alpha1.AMLPipelineStateEnum.Creating,
		v1alpha1.AMLPipelineStateEnum.Unknown,
		v1alpha1.AMLPipelineStateEnum.Error:
		deepCopy.Status.AmlBaseStatus.Phase = v1alpha1.AMLPipelinePhaseEnum.Creating
		break
	case v1alpha1.AMLPipelineStateEnum.Running:
		deepCopy.Status.AmlBaseStatus.Phase = v1alpha1.AMLPipelinePhaseEnum.Running
		break
	case v1alpha1.AMLPipelineStateEnum.Cancelled:
		deepCopy.Status.AmlBaseStatus.Phase = v1alpha1.AMLPipelinePhaseEnum.Cancelled
		break
	case v1alpha1.AMLPipelineStateEnum.Success:
		deepCopy.Status.AmlBaseStatus.Phase = v1alpha1.AMLPipelinePhaseEnum.Success
		break
	case v1alpha1.AMLPipelineStateEnum.Failed:
		deepCopy.Status.AmlBaseStatus.Phase = v1alpha1.AMLPipelinePhaseEnum.Failed
		break
	}

	return deepCopy
}
