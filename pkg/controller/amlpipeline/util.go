package amlpipeline

import (
	"alauda.io/amlopr/pkg/controller/common"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"fmt"
	pkgclient "sigs.k8s.io/controller-runtime/pkg/client"
)

func GetDevopsPipelineLabel(name string) *common.AmlLabel {
	return common.NewAmlLabel().
		Add(common.GetLabelKeyForCreatorOf(AMLPipelineCreator), name)
}

func GetDevopsPipelineListByLabel(client pkgclient.Client, name string, namespace string) (*devopsv1alpha1.PipelineList, error) {
	if name == "" || namespace == "" {
		return nil, fmt.Errorf("name and namespace should not be nil")
	}
	pipelineList := &devopsv1alpha1.PipelineList{}
	listOptions := &pkgclient.ListOptions{}
	labelSelector := GetDevopsPipelineLabel(name).GetSelectorString()
	listOptions.SetLabelSelector(labelSelector)
	listOptions.Namespace = namespace
	err := client.List(context.TODO(), listOptions, pipelineList)
	return pipelineList, err
}
