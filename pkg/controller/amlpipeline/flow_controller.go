package amlpipeline

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"context"
	"fmt"
	"github.com/golang/glog"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"time"
)

type FlowControlProvider struct {
}

func (fc *FlowControlProvider) Dispatch(reconcileAMLPipeline *ReconcileAMLPipeline, request reconcile.Request) error {
	parent := &v1alpha1.AMLPipeline{}
	err := reconcileAMLPipeline.Get(context.TODO(), request.NamespacedName, parent)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		} else {
			return err
		}
	}
	subject := &AMLPipelineSubject{SubjectInstance: parent}

	if parent.DeletionTimestamp != nil {
		subject.SetCurrentPhase(v1alpha1.AMLPipelinePhaseEnum.Deleting)
	}
	var stage = subject.GetCurrentPhase()
	switch stage {
	case v1alpha1.AMLPipelinePhaseEnum.Creating:
		err = fc.convertToRunning(reconcileAMLPipeline, subject)
		if err != nil {
			fc.convertToError(reconcileAMLPipeline, subject, err)
		}
	case v1alpha1.AMLPipelinePhaseEnum.Running:
		err = fc.processRunningState(reconcileAMLPipeline, subject)
	case v1alpha1.AMLPipelinePhaseEnum.Cancelled:
		err = fc.processCancelState(reconcileAMLPipeline, subject)
	case v1alpha1.AMLPipelinePhaseEnum.Deleting:
		err = fc.processDeleteState(reconcileAMLPipeline, subject)
	case v1alpha1.AMLPipelinePhaseEnum.Success,
		v1alpha1.AMLPipelinePhaseEnum.Failed:
		return nil
	}

	updateErr := reconcileAMLPipeline.Client.Update(context.TODO(), subject.GetUpdatePipeline())
	if err == nil && updateErr != nil {
		return updateErr
	}

	return err
}

func (fc *FlowControlProvider) processDeleteState(reconcileAMLPipeline *ReconcileAMLPipeline, subject *AMLPipelineSubject) error {
	var errs []error

	pipelineComp, err := NewDevopsPipelineGenerator(reconcileAMLPipeline, subject)
	errs = handleError(err, errs)

	if err == nil {
		err = pipelineComp.delete()
		errs = handleError(err, errs)
	}

	crdComp := NewCrdComp(reconcileAMLPipeline, subject)

	err = crdComp.delete()
	errs = handleError(err, errs)

	if common.MergeSimpleErrors(errs) == nil {
		subject.RemoveMLFinalizer()
		return nil
	}

	subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Deleting, common.MergeSimpleErrors(errs).Error(), "")
	return common.MergeSimpleErrors(errs)
}

func (fc *FlowControlProvider) processCancelState(reconcileAMLPipeline *ReconcileAMLPipeline, subject *AMLPipelineSubject) error {
	var errs []error
	pipelineComp, err := NewDevopsPipelineGenerator(reconcileAMLPipeline, subject)
	errs = handleError(err, errs)

	if err == nil {
		err = pipelineComp.cancel()
		errs = handleError(err, errs)
	}

	crdComp := NewCrdComp(reconcileAMLPipeline, subject)

	err = crdComp.cancel()
	errs = handleError(err, errs)

	mergeSimpleErrors := common.MergeSimpleErrors(errs)
	if mergeSimpleErrors != nil {
		subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Cancelled, mergeSimpleErrors.Error(), "")
	} else {
		subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Cancelled, "", "")
	}

	return mergeSimpleErrors
}

func handleError(err error, errs []error) []error {
	if err != nil {
		glog.V(5).Info(juju.ErrorStack(err))
		errs = append(errs, err)
	}
	return errs
}

func (fc *FlowControlProvider) convertToError(reconcileAMLPipeline *ReconcileAMLPipeline, subject *AMLPipelineSubject, err error) {
	subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Error, juju.Cause(err).Error(), "")
}

func (fc *FlowControlProvider) processRunningState(reconcileAMLPipeline *ReconcileAMLPipeline, subject *AMLPipelineSubject) error {
	pipelineComp, err := NewDevopsPipelineGenerator(reconcileAMLPipeline, subject)
	if err != nil {
		return juju.Trace(err)
	}

	isExist, pipeline, err := pipelineComp.IsExist()
	if err != nil {
		subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Error, juju.Cause(err).Error(), "")
		return juju.Trace(err)
	}
	if !isExist {
		err := fc.errorCrd(reconcileAMLPipeline, subject, fmt.Errorf("devops pipepline is deleted"))
		if err != nil {
			subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Error, juju.Cause(err).Error(), "")
			return juju.Trace(err)
		}

		subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Failed, "devops pipepline is deleted", "")
		subject.SubjectInstance.Status.FinishTime = &metav1.Time{Time: time.Now()}
		return fmt.Errorf("devops pipepline is deleted")
	}

	isFinalPhase, amlError := pipelineComp.GetDevopsPipelineState(pipeline)
	if !isFinalPhase {
		return nil
	}
	if amlError != nil {
		err := fc.errorCrd(reconcileAMLPipeline, subject, amlError)
		if err != nil {
			subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Error, juju.Cause(err).Error(), "")
			return juju.Trace(err)
		}

		subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Failed, amlError.Reason(), amlError.Message())
		subject.SubjectInstance.Status.FinishTime = &metav1.Time{Time: time.Now()}
		return nil
	}
	subject.SubjectInstance.Status.FinishTime = &metav1.Time{Time: time.Now()}
	subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Success, "", "")
	return nil
}

func (fc *FlowControlProvider) errorCrd(reconcileAMLPipeline *ReconcileAMLPipeline, subject *AMLPipelineSubject, amlError error) error {
	crdComp := NewCrdComp(reconcileAMLPipeline, subject)
	return crdComp.error(amlError)
}

func (fc *FlowControlProvider) convertToRunning(reconcileAMLPipeline *ReconcileAMLPipeline, subject *AMLPipelineSubject) error {
	comp := NewCrdComp(reconcileAMLPipeline, subject)

	_, err := comp.SyncCrdByAnnotation()
	if err != nil {
		return juju.Trace(err)
	}

	pipelineComp, err := NewDevopsPipelineGenerator(reconcileAMLPipeline, subject)
	if err != nil {
		return juju.Trace(err)
	}
	err = pipelineComp.SyncDevopsPipeline()
	if err != nil {
		return juju.Trace(err)
	}

	subject.SetCurrentState(v1alpha1.AMLPipelineStateEnum.Running, "", "")
	return nil
}
