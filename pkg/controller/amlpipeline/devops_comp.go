package amlpipeline

//noinspection SpellCheckingInspection
import (
	"alauda.io/amlopr/pkg/controller/amlpipelineconfig"
	"alauda.io/amlopr/pkg/controller/common"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"github.com/juju/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type DevopsPipelineComp struct {
	client client.Client
	scheme *runtime.Scheme
	Parent *AMLPipelineSubject

	devopsPipelineConfig *devopsv1alpha1.PipelineConfig
}

func NewDevopsPipelineGenerator(reconcileAMLPipeline *ReconcileAMLPipeline, Parent *AMLPipelineSubject) (*DevopsPipelineComp, error) {
	pipelineMgr := &DevopsPipelineComp{
		client: reconcileAMLPipeline.Client,
		scheme: reconcileAMLPipeline.Scheme,
		Parent: Parent,
	}

	err := pipelineMgr.checkAndInit()
	if err != nil {
		return pipelineMgr, errors.Trace(err)
	}
	return pipelineMgr, nil
}

func (c *DevopsPipelineComp) IsExist() (bool, *devopsv1alpha1.Pipeline, error) {
	pipelineList, err := GetDevopsPipelineListByLabel(c.client, c.Parent.SubjectInstance.Name, c.Parent.SubjectInstance.Namespace)
	if err != nil {
		return false, nil, errors.Trace(err)
	}
	if len(pipelineList.Items) > 0 {
		return true, &pipelineList.Items[0], nil
	}
	return false, nil, nil
}

func (c *DevopsPipelineComp) delete() error {
	isExist, pipeline, err := c.IsExist()
	if err != nil {
		return errors.Trace(err)
	}
	if isExist {
		return c.client.Delete(context.TODO(), pipeline)
	}
	return nil
}

func (c *DevopsPipelineComp) cancel() error {
	return c.delete()
}

func (c *DevopsPipelineComp) checkAndInit() error {
	if c.devopsPipelineConfig == nil {
		templateName := types.NamespacedName{Name: amlpipelineconfig.GetGeneratedDevopsPipelineConfigName(c.Parent.SubjectInstance.Spec.AMLPipelineConfig.Name), Namespace: c.Parent.SubjectInstance.Namespace}
		devopsPipelineConfig := &devopsv1alpha1.PipelineConfig{}
		err := c.client.Get(context.TODO(), templateName, devopsPipelineConfig)
		if err != nil {
			return errors.Trace(err)
		}
		c.devopsPipelineConfig = devopsPipelineConfig
	}
	return nil
}

func (c *DevopsPipelineComp) GetDevopsPipeline() (*devopsv1alpha1.Pipeline, error) {
	devopsPipeline := generatePipelineFromConfig(c.devopsPipelineConfig)

	devopsPipeline.Annotations = common.NewAmlAnnotation().Create().AddMap(devopsPipeline.Annotations).Get()
	devopsPipeline.Labels = common.NewAmlLabel(devopsPipeline.Labels).
		Add(AMLPipelineLabelKey, AMLPipelineLabelValue).
		Add(common.GetLabelKeyForCreatorOf(AMLPipelineCreator), c.Parent.SubjectInstance.Name).
		Get()

	devopsPipeline.Spec.Parameters = c.filterParas()
	return devopsPipeline, nil
}

func (c *DevopsPipelineComp) filterParas() []devopsv1alpha1.PipelineParameter {
	var result []devopsv1alpha1.PipelineParameter
	parameters, err := ParseParameter(c.Parent.SubjectInstance)
	if err != nil {
		//return nil, errors.Trace(err)
	}
	paraMap := make(map[string]devopsv1alpha1.PipelineParameter)
	for _, value := range c.devopsPipelineConfig.Spec.Parameters {
		paraMap[value.Name] = value
	}
	for _, value := range parameters {
		if _, ok := paraMap[value.Name]; ok {
			result = append(result, value)
		}
	}
	return result
}

func generatePipelineFromConfig(config *devopsv1alpha1.PipelineConfig) (pipe *devopsv1alpha1.Pipeline) {
	pipe = &devopsv1alpha1.Pipeline{
		ObjectMeta: v1.ObjectMeta{
			Namespace: config.Namespace,
		},
		Spec: devopsv1alpha1.PipelineSpec{
			JenkinsBinding: config.Spec.JenkinsBinding,
			PipelineConfig: devopsv1alpha1.LocalObjectReference{
				Name: config.GetName(),
			},
			Cause: devopsv1alpha1.PipelineCause{
				Type:    devopsv1alpha1.PipelineCauseTypeManual,
				Message: "Triggered by AMLPipeline",
			},
			RunPolicy: config.Spec.RunPolicy,
			Triggers:  config.Spec.Triggers,
			Strategy:  config.Spec.Strategy,
			Hooks:     config.Spec.Hooks,
			Source:    config.Spec.Source,
		},
	}
	return
}

func (c *DevopsPipelineComp) SyncDevopsPipeline() error {
	pipeline, err := c.GetDevopsPipeline()
	if err != nil {
		return errors.Trace(err)
	}

	isExist, _, err := c.IsExist()
	if err != nil {
		return errors.Trace(err)
	}

	if !isExist {
		//if err = controllerutil.SetControllerReference(c.Parent.SubjectInstance, pipeline, c.scheme); err == nil {
		// devops创建时会自动设置owner为pipelineconfig，所以先创建再update woner
		err = c.client.Create(context.TODO(), pipeline)
		if err != nil {
			return errors.Trace(err)
		}
	}

	return nil
}

func (c *DevopsPipelineComp) GetDevopsPipelineState(pipeline *devopsv1alpha1.Pipeline) (bool, *common.AmlError) {
	phase := pipeline.Status.Phase
	isFinalPhase := phase.IsFinalPhase()

	switch phase {
	case devopsv1alpha1.PipelinePhasePending,
		devopsv1alpha1.PipelinePhaseQueued,
		devopsv1alpha1.PipelinePhaseRunning,
		devopsv1alpha1.PipelinePhaseComplete:
		return isFinalPhase, nil
	case devopsv1alpha1.PipelinePhaseFailed:
		return isFinalPhase, common.NewAmlError("devops pipeline failed", "devops pipeline failed")
	case devopsv1alpha1.PipelinePhaseError:
		return isFinalPhase, common.NewAmlError("devops pipeline error", "devops pipeline error")
	case devopsv1alpha1.PipelinePhaseCancelled:
		return isFinalPhase, common.NewAmlError("devops pipeline cancelled", "devops pipeline cancelled")
	case devopsv1alpha1.PipelinePhaseAborted:
		return isFinalPhase, common.NewAmlError("devops pipeline aborted", "devops pipeline aborted")
	default:
		return isFinalPhase, common.NewAmlError("unknown devops pipeline state", "unknown devops pipeline state")
	}
}
