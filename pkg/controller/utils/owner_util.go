package utils

import (
	"alauda.io/amlopr/pkg/controller/common"
	"context"
	"fmt"
	"github.com/juju/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/apiutil"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

func CheckAndSetOwner(Client client.Client, scheme *runtime.Scheme, owner common.Object, child common.Object) error {
	needSet := true

	for _, value := range child.GetOwnerReferences() {
		ownerGvk, err := apiutil.GVKForObject(owner, scheme)
		if err != nil {
			return errors.Trace(err)
		}

		childGvk, err := apiutil.GVKForObject(child, scheme)
		if err != nil {
			return errors.Trace(err)
		}

		if value.Name == owner.GetName() && ownerGvk.Kind == childGvk.Kind {
			needSet = false
			return nil
		}
	}

	if needSet {
		child.SetOwnerReferences(nil)
		err := controllerutil.SetControllerReference(owner, child, scheme)
		if err != nil {
			return errors.Trace(err)
		}

		err = Client.Update(context.TODO(), child)
		if err != nil {
			fmt.Printf("error when update owner: %s", err)
			return errors.Trace(err)
		}
	}

	return nil
}
