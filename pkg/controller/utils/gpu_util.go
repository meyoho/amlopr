package utils

import (
	"k8s.io/api/core/v1"
)

var emptyEnvVar = v1.EnvVar{
	Name:  "NVIDIA_VISIBLE_DEVICES",
	Value: "none",
}

func InjectEnvForNvidia(spec *v1.PodTemplateSpec) *v1.PodTemplateSpec {
	for index := range spec.Spec.Containers {
		injectEnvNvidiaForContainer(&spec.Spec.Containers[index])
	}

	for index := range spec.Spec.InitContainers {
		injectEnvNvidiaForContainer(&spec.Spec.InitContainers[index])
	}

	return spec
}

func injectEnvNvidiaForContainer(container *v1.Container) {
	quantity, ok := container.Resources.Limits["nvidia.com/gpu"]
	if !ok || quantity.IsZero() {
		container.Env = append(container.Env, emptyEnvVar)
	}
}
