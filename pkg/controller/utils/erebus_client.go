package utils

import (
	ml "alauda.io/amlopr/pkg/client/clientset/versioned"
	"fmt"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/cluster-registry/pkg/apis/clusterregistry/v1alpha1"
	clusterregistryclient "k8s.io/cluster-registry/pkg/client/clientset/versioned"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

var multiClusterHost = "https://erebus"
var clusterRegistryNamespace = "alauda-system"

func SetBusinessCfg(multiClusterHostArg string, clusterRegistryNamespaceArg string) {
	multiClusterHost = multiClusterHostArg
	clusterRegistryNamespace = clusterRegistryNamespaceArg
}

type businessMgr struct {
	clusterName string
}

func NewBusinessMgr(clusterName string) *businessMgr {
	return &businessMgr{clusterName: clusterName}
}

func (businessClient *businessMgr) MLClient() (*ml.Clientset, error) {
	err, cfg := businessClient.getCfg()
	if err != nil {
		return nil, err
	}

	clientSet, err := ml.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}
	return clientSet, nil
}

func (businessClient *businessMgr) K8sClient() (*kubernetes.Clientset, error) {
	err, cfg := businessClient.getCfg()
	if err != nil {
		return nil, err
	}

	clientSet, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}
	return clientSet, nil
}

func (businessClient *businessMgr) getCfg() (error, *rest.Config) {
	bearToken, err := businessClient.getBearToken()
	if err != nil {
		return juju.Trace(err), nil
	}
	cfg := &rest.Config{
		Host:        fmt.Sprintf("%s/kubernetes/%s", multiClusterHost, businessClient.clusterName),
		BearerToken: bearToken,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}
	return err, cfg
}

func getConfig() (*rest.Config, error) {
	cfg, err := config.GetConfig()
	if err != nil {
		cfg, err = clientcmd.BuildConfigFromFlags("", "~/.kube/config")
	}
	return cfg, err
}

func (businessClient *businessMgr) IsExistCluster() (bool, error) {
	cfg, err := getConfig()
	if err != nil {
		return false, juju.Trace(err)
	}

	_, err = businessClient.getClusterInfo(cfg)
	if err != nil {
		if errors.IsNotFound(err) {
			return false, nil
		}
		return false, juju.Trace(err)
	}
	return true, nil

}

func (businessClient *businessMgr) getBearToken() (string, error) {
	cfg, err := getConfig()
	if err != nil {
		return "", juju.Trace(err)
	}

	cluster, err := businessClient.getClusterInfo(cfg)
	if err != nil {
		return "", juju.Trace(err)
	}

	controller := cluster.Spec.AuthInfo.Controller
	switch controller.Kind {
	case "Secret":
		return getTokenFromSecret(cfg, controller)
	default:
		return "", fmt.Errorf("unknown cluster kind: %s \n", controller.Kind)
	}
}

func (businessClient *businessMgr) getClusterInfo(cfg *rest.Config) (*v1alpha1.Cluster, error) {
	clientSet, err := clusterregistryclient.NewForConfig(cfg)
	if err != nil {
		return nil, juju.Trace(err)
	}
	return clientSet.ClusterregistryV1alpha1().Clusters(clusterRegistryNamespace).Get(businessClient.clusterName, v1.GetOptions{})
}

func getTokenFromSecret(cfg *rest.Config, controller *v1alpha1.ObjectReference) (string, error) {
	clientSet, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return "", juju.Trace(err)
	}
	secret, err := clientSet.CoreV1().Secrets(controller.Namespace).Get(controller.Name, v1.GetOptions{})
	if err != nil {
		return "", juju.Trace(err)
	}
	return string(secret.Data["token"]), nil
}
