package utils

func Delete(slice []string, deleted string) []string {
	index := -1
	for key, val := range slice {
		if val == deleted {
			index = key
			break
		}
	}
	if index > -1 {
		return append(slice[0:index], slice[index+1:]...)
	}
	return slice
}
