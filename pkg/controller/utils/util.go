package utils

import (
	"alauda.io/amlopr/pkg/controller/common"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"encoding/json"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// CloneMeta clean up an object meta and clone
func CloneMeta(obj metav1.ObjectMeta) (new metav1.ObjectMeta) {
	new = *obj.DeepCopy()
	new.ResourceVersion = ""
	new.UID = ""
	new.SelfLink = ""
	new.CreationTimestamp = metav1.Time{}
	return
}

func GetUnstructed(obj runtime.Object) (*unstructured.Unstructured, error) {
	bytes, e := json.Marshal(obj)
	if e != nil {
		return nil, e
	}
	unstructured := &unstructured.Unstructured{}
	e = json.Unmarshal(bytes, unstructured)
	if e != nil {
		//TODO zlc 处理missingkinderr
		//return nil, e
	}
	return unstructured, nil
}

func GetNameSpacedName(unstructured *unstructured.Unstructured) client.ObjectKey {
	return types.NamespacedName{
		Namespace: unstructured.GetNamespace(),
		Name:      unstructured.GetName(),
	}
}

func NewNamespacedName(name string, namespace string) client.ObjectKey {
	return types.NamespacedName{
		Name:      name,
		Namespace: namespace,
	}
}

func IsExist(client client.Client, object common.Object) (bool, *runtime.Object, error) {
	copyObject := object.DeepCopyObject()
	err := client.Get(context.TODO(), NewNamespacedName(object.GetName(), object.GetNamespace()), copyObject)
	if err != nil {
		if errors.IsNotFound(err) {
			return false, &copyObject, nil
		}
		return false, &copyObject, err
	}
	return true, &copyObject, nil
}

func GetMergedMapGroup(arguments []devops.PipelineTemplateArgumentGroup,
	parameters []devops.PipelineTemplateArgumentGroup) map[string]string {
	var result = make(map[string]string)
	getMapGroup(arguments, result)
	getMapGroup(parameters, result)
	return result
}

func getMapGroup(arguments []devops.PipelineTemplateArgumentGroup, result map[string]string) {
	for _, value := range arguments {
		for _, item := range value.Items {
			result[item.Name] = item.Value
		}
	}
}
