package utils

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"strings"
)

func GetMLBinding(Client client.Client, namespace string) (*v1alpha1.MLBinding, error) {
	bindingList := &v1alpha1.MLBindingList{}
	err := Client.List(context.TODO(), &client.ListOptions{Namespace: namespace}, bindingList)
	if err != nil {
		return nil, juju.Trace(err)
	}
	if len(bindingList.Items) < 1 {
		return nil, fmt.Errorf("not a aml project, config mlbinding first")
	}
	return &bindingList.Items[0], nil
}

// return nil when not exist
func GetClusterProjectBinding(clusterName string, namespace string) (*v1alpha1.MLBinding, error) {
	clientset, err := NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		return nil, juju.Trace(err)
	}

	bindingList, err := clientset.MlV1alpha1().MLBindings(namespace).List(v1.ListOptions{})
	if err != nil {
		return nil, juju.Trace(err)
	}
	if len(bindingList.Items) < 1 {
		return nil, nil
	}
	return &bindingList.Items[0], nil
}

func GetVolumes(mlBinding *v1alpha1.MLBinding) []corev1.Volume {
	if mlBinding == nil {
		return nil
	}
	pvcSlice := mlBinding.Spec.Pvc
	var volumes []corev1.Volume
	for _, pvc := range pvcSlice {
		volumes = append(volumes, pvc.Volume)
	}
	return volumes
}

func GetVolumeMount(mlBinding *v1alpha1.MLBinding) []corev1.VolumeMount {
	if mlBinding == nil {
		return nil
	}
	pvcSlice := mlBinding.Spec.Pvc
	var volumeMounts []corev1.VolumeMount
	for _, pvc := range pvcSlice {
		volumeMounts = append(volumeMounts, corev1.VolumeMount{
			Name:      pvc.Volume.Name,
			MountPath: pvc.MountPath + "/" + pvc.Volume.Name,
		})

	}
	return volumeMounts
}

func GetAMLFixedVolumeMount(mlBinding *v1alpha1.MLBinding, fixedPath string) []corev1.VolumeMount {
	if mlBinding == nil {
		return nil
	}
	pvcSlice := mlBinding.Spec.Pvc
	var volumeMounts []corev1.VolumeMount
	for _, pvc := range pvcSlice {
		volumeMounts = append(volumeMounts, corev1.VolumeMount{
			Name:      pvc.Volume.Name,
			MountPath: fixedPath + "/" + pvc.Volume.Name,
		})

	}
	return volumeMounts
}

func GetMountPath(path string, binding *v1alpha1.MLBinding) (string, error) {
	split := strings.Split(path, "/")
	if len(split) < 2 {
		return "", fmt.Errorf("invalid path: %s", path)
	}
	pvcSlice := binding.Spec.Pvc
	for _, pvc := range pvcSlice {
		if pvc.Volume.Name == split[1] {
			return pvc.MountPath + path, nil
		}
	}
	return "", fmt.Errorf("cannot find the mount path for url: %s", path)
}

func GetEnvs() []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name:  "JENKINS_URL",
			Value: "http://jenkins.default:80",
		},
	}

}
