package utils

import (
	"encoding/json"
	juju "github.com/juju/errors"
)

func UnmarshalJsonToMap(data string) (map[string]string, error) {
	var result = make(map[string]string)
	err := json.Unmarshal([]byte(data), &result)
	if err != nil {
		return nil, juju.Trace(err)
	}
	return result, nil
}
