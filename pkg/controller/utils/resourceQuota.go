package utils

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
)

// this file is not real resourceQuota
// it really for complete the request and limit in the pod spec
func FilteredLimitsResourceMapByKey(m map[v1.ResourceName]resource.Quantity) map[v1.ResourceName]resource.Quantity {
	newMap := make(map[v1.ResourceName]resource.Quantity)
	for k, v := range m {
		switch k {
		case v1.ResourceLimitsCPU:
			newMap[v1.ResourceCPU] = v
		case v1.ResourceLimitsMemory:
			newMap[v1.ResourceMemory] = v
		default:
			if k.String() == "limits.nvidia.com/gpu" {
				newMap[mlv1alpha1.NvidiaGPUResourceType] = v
			}
			if k.String() == "limits.amd.com/gpu" {
				newMap[mlv1alpha1.AMDGPUResourceType] = v
			}
			if k.String() == "limits.tencent.com/vcuda-core" {
				newMap[mlv1alpha1.TencentVCoreResourceType] = v
			}
			if k.String() == "limits.tencent.com/vcuda-memory" {
				newMap[mlv1alpha1.TenCentVMemoryResourceType] = v
			}
		}
	}
	return newMap
}
func FilteredRequestsResourceMapByKey(m map[v1.ResourceName]resource.Quantity) map[v1.ResourceName]resource.Quantity {

	newMap := make(map[v1.ResourceName]resource.Quantity)
	for k, v := range m {
		switch k {
		case v1.ResourceRequestsCPU:
			newMap[v1.ResourceCPU] = v
		case v1.ResourceRequestsMemory:
			newMap[v1.ResourceMemory] = v
		}
	}
	return newMap
}
