package utils

import (
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"reflect"
	"testing"
)

func TestInjectEnvForNvidia(t *testing.T) {
	podTemplateSpec := &v1.PodTemplateSpec{
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				{
					Resources: v1.ResourceRequirements{
						Limits: map[v1.ResourceName]resource.Quantity{
							"cpu":            resource.MustParse("600m"),
							"mem":            resource.MustParse("600Mi"),
							"nvidia.com/gpu": resource.MustParse("1"),
						},
					},
				},
			},
		},
	}

	podTemplateSpec = InjectEnvForNvidia(podTemplateSpec)

	vars := podTemplateSpec.Spec.Containers[0].Env
	for _, value := range vars {
		if reflect.DeepEqual(value, emptyEnvVar) {
			t.Error()
		}
	}

	podTemplateSpec = &v1.PodTemplateSpec{
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				{
					Resources: v1.ResourceRequirements{
						Limits: map[v1.ResourceName]resource.Quantity{
							"cpu":            resource.MustParse("600m"),
							"mem":            resource.MustParse("600Mi"),
							"nvidia.com/gpu": resource.MustParse("0"),
						},
					},
				},
			},
		},
	}

	podTemplateSpec = InjectEnvForNvidia(podTemplateSpec)
	vars = podTemplateSpec.Spec.Containers[0].Env
	for _, value := range vars {
		if reflect.DeepEqual(value, emptyEnvVar) {
			return
		}
	}
	t.Error()
}
