package ambassador

import (
	"testing"
)

func TestAmbassadorAnnotation_GetAnnotation(t *testing.T) {
	annotation := &AmbassadorAnnotation{
		Name:    "test",
		Prefix:  "/url",
		Rewrite: "/url/b",
		Service: "service",
		RetryPolicy: RetryPolicy{
			RetryOn:    "5xx",
			NumRetries: 10,
		},
	}
	strings, err := annotation.GetAnnotation()
	if err != nil {
		t.Error(err)
	}
	expected := `apiVersion: ambassador/v1
kind: Mapping
name: test
prefix: /url
rewrite: /url/b
service: service
retry_policy:
  retry_on: 5xx
  num_retries: 10
`
	actual := strings["getambassador.io/config"]
	if actual != expected {
		t.Errorf("expected: \n%s, actual:\n%s", expected, actual)
	}
}
