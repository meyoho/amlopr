package ambassador

import (
	"github.com/juju/errors"
	"gopkg.in/yaml.v2"
)

type AmbassadorAnnotation struct {
	APIVersion  string      `yaml:"apiVersion"`
	Kind        string      `yaml:"kind"`
	Name        string      `yaml:"name"`
	Prefix      string      `yaml:"prefix"`
	Rewrite     string      `yaml:"rewrite"`
	Service     string      `yaml:"service"`
	Timeout     int         `yaml:"timeout_ms,omitempty"`
	RetryPolicy RetryPolicy `yaml:"retry_policy,omitempty"`
}

type RetryPolicy struct {
	RetryOn    string `yaml:"retry_on,omitempty"`
	NumRetries int    `yaml:"num_retries,omitempty"`
}

func (a *AmbassadorAnnotation) GetAnnotation() (map[string]string, error) {
	if a.Kind == "" {
		a.Kind = "Mapping"
	}
	if a.APIVersion == "" {
		a.APIVersion = "ambassador/v1"
	}
	y, err := yaml.Marshal(a)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return map[string]string{"getambassador.io/config": string(y)}, nil
}
