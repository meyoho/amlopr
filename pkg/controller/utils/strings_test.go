package utils

import (
	"reflect"
	"testing"
)

func TestDelete(t *testing.T) {
	expect := []string{"a", "b", "c", "d"}
	actual := Delete(expect, "a")
	expect = []string{"b", "c", "d"}
	if !reflect.DeepEqual(actual, expect) {
		t.Errorf("expect: %#v, actual: %#v", expect, actual)
	}

	actual = Delete(expect, "c")
	expect = []string{"b", "d"}
	if !reflect.DeepEqual(actual, expect) {
		t.Errorf("expect: %#v, actual: %#v", expect, actual)
	}

	actual = Delete(expect, "e")
	expect = []string{"b", "d"}
	if !reflect.DeepEqual(actual, expect) {
		t.Errorf("expect: %#v, actual: %#v", expect, actual)
	}

	actual = Delete(expect, "d")
	expect = []string{"b"}
	if !reflect.DeepEqual(actual, expect) {
		t.Errorf("expect: %#v, actual: %#v", expect, actual)
	}

}
