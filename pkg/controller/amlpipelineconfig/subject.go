package amlpipelineconfig

import (
	"alauda.io/amlopr/pkg/apis/base"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
)

type AMLPipelineConfigSubject struct {
	SubjectInstance     *v1alpha1.AMLPipelineConfig
	subjectInstanceCopy *v1alpha1.AMLPipelineConfig
}

func (s *AMLPipelineConfigSubject) GetReadableInstance() *v1alpha1.AMLPipelineConfig {
	if s.subjectInstanceCopy == nil {
		s.subjectInstanceCopy = s.SubjectInstance.DeepCopy()
	}
	return s.subjectInstanceCopy
}

func (s *AMLPipelineConfigSubject) GetCurrentStage() v1alpha1.BaseStage {
	return s.SubjectInstance.Status.AmlBaseStatus.Phase
}

func (s *AMLPipelineConfigSubject) RemoveMLFinalizer() {
	finalizers := s.SubjectInstance.Finalizers
	s.SubjectInstance.Finalizers = utils.Delete(finalizers, base.PipelineConfigFinalizer)
	return
}

func (s *AMLPipelineConfigSubject) SetCurrentState(state v1alpha1.BaseState, phase v1alpha1.BaseStage, reason string, message string) {
	s.SubjectInstance.Status.AmlBaseStatus.State = state
	s.SubjectInstance.Status.AmlBaseStatus.Phase = phase
	s.SubjectInstance.Status.Reason = reason
	s.SubjectInstance.Status.Message = message
}

func (s *AMLPipelineConfigSubject) SetCurrentPhase(phase v1alpha1.BaseStage) {
	if phase == v1alpha1.AMLPipelineConfigPhaseEnum.Deleting {
		s.SubjectInstance.Labels = common.NewAmlLabel(s.SubjectInstance.Labels).
			Add("deleting", "true").Get()
	}
	s.SubjectInstance.Status.Phase = phase
}
