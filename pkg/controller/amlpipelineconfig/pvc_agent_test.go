package amlpipelineconfig

import (
	"alauda.io/amlopr/pkg/apis/base"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/config/template"
	"github.com/onsi/gomega"
	"sigs.k8s.io/yaml"
	"testing"
)

func TestGetAgentRaw(t *testing.T) {
	template.LoadTemplate(base.Global)
	projectBinding := &v1alpha1.MLBinding{}
	err := v1alpha1.ReadCrdFromFile("../../../config/samples/ml_v1alpha1_mlbinding.yaml", projectBinding)
	if err != nil {
		t.Error(err)
	}
	pod := getPodTemplate(projectBinding)
	s, err := yaml.Marshal(pod)
	if err != nil {
		t.Error(err)
	}

	expectedRequest := `metadata:
  creationTimestamp: null
  namespace: aml-storage
spec:
  containers:
  - command:
    - cat
    image: index.alauda.cn/alaudaorg/builder-tools:ubuntu
    name: tools
    resources:
      limits:
        cpu: 200m
        memory: 512Mi
      requests:
        cpu: 50m
        memory: 256Mi
    tty: true
    volumeMounts:
    - mountPath: /amlpvc/pvc-name
      name: pvc-name
    - mountPath: /amlpvc/pvc-name2
      name: pvc-name2
    - mountPath: /tmp
      name: volume-jenkins
    - mountPath: /var/run/docker.sock
      name: volume-docker
    workingDir: /home/jenkins/agent
  - args:
    - \$(JENKINS_SECRET)
    - \$(JENKINS_NAME)
    image: index.alauda.cn/alaudaorg/jnlp-slave:v2.4.3
    name: jnlp
    resources:
      limits:
        cpu: 200m
        memory: 512Mi
      requests:
        cpu: 50m
        memory: 256Mi
    volumeMounts:
    - mountPath: /amlpvc/pvc-name
      name: pvc-name
    - mountPath: /amlpvc/pvc-name2
      name: pvc-name2
    - mountPath: /tmp
      name: volume-jenkins
    - mountPath: /var/run/docker.sock
      name: volume-docker
    workingDir: /home/jenkins/agent
  volumes:
  - name: pvc-name
    persistentVolumeClaim:
      claimName: nginx-pvc
  - name: pvc-name2
    persistentVolumeClaim:
      claimName: pvc2
  - emptyDir: {}
    name: volume-jenkins
  - hostPath:
      path: /var/run/docker.sock
      type: ""
    name: volume-docker
status: {}
`

	g := gomega.NewGomegaWithT(t)
	g.Expect(expectedRequest).To(gomega.Equal(string(s)))

}
