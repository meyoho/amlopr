package amlpipelineconfig

//noinspection SpellCheckingInspection
import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"encoding/json"
	"fmt"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

type DevopsPipelineConfigComp struct {
	Client client.Client
	scheme *runtime.Scheme
	Parent *AMLPipelineConfigSubject

	amlTemplate        *v1alpha1.AMLPipelineTemplate
	mlBinding          *v1alpha1.MLBinding
	jenkinsClusterName string
	jenkinsNamespace   string
}

func NewPipelineConfigComp(reconcileAMLPipelineConfig *ReconcileAMLPipelineConfig, Parent *AMLPipelineConfigSubject) *DevopsPipelineConfigComp {
	pipelineConfigMgr := &DevopsPipelineConfigComp{
		Client: reconcileAMLPipelineConfig.Client,
		scheme: reconcileAMLPipelineConfig.scheme,
		Parent: Parent,
	}

	return pipelineConfigMgr
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) getAmlTemplate() (*v1alpha1.AMLPipelineTemplate, error) {
	if pipelineConfigMgr.amlTemplate == nil {
		amlPipelineTemplateName := pipelineConfigMgr.Parent.SubjectInstance.Spec.AMLPipelineTemplate.Name
		amlPipelineTemplateNamespace := pipelineConfigMgr.Parent.SubjectInstance.Spec.AMLPipelineTemplate.Namespace
		templateName := types.NamespacedName{Name: amlPipelineTemplateName, Namespace: amlPipelineTemplateNamespace}
		amlPipelineTemplate := &v1alpha1.AMLPipelineTemplate{}
		err := pipelineConfigMgr.Client.Get(context.TODO(), templateName, amlPipelineTemplate)
		if err != nil {
			return nil, juju.Trace(err)
		}

		pipelineConfigMgr.amlTemplate = amlPipelineTemplate
	}
	return pipelineConfigMgr.amlTemplate, nil
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) getMlBinding() (*v1alpha1.MLBinding, error) {
	if pipelineConfigMgr.mlBinding == nil {
		jenkinsClusterName, err := pipelineConfigMgr.getJenkinsClusterName()
		if err != nil {
			return nil, juju.Trace(err)
		}
		jenkinsNamespace, err := pipelineConfigMgr.getJenkinsNamespace()
		if err != nil {
			return nil, juju.Trace(err)
		}

		mlBinding, err := utils.GetClusterProjectBinding(jenkinsClusterName, jenkinsNamespace)
		if err != nil {
			return nil, juju.Trace(err)
		}
		if mlBinding == nil {
			mlBinding = &v1alpha1.MLBinding{}
			mlBinding.Namespace = jenkinsNamespace
		}

		pipelineConfigMgr.mlBinding = mlBinding
	}
	return pipelineConfigMgr.mlBinding, nil
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) getJenkinsClusterName() (string, error) {
	err := pipelineConfigMgr.initJenkins()
	if err != nil {
		return "", juju.Trace(err)
	}
	return pipelineConfigMgr.jenkinsClusterName, nil
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) getJenkinsNamespace() (string, error) {
	err := pipelineConfigMgr.initJenkins()
	if err != nil {
		return "", juju.Trace(err)
	}
	return pipelineConfigMgr.jenkinsNamespace, nil
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) initJenkins() error {
	if pipelineConfigMgr.jenkinsClusterName == "" || pipelineConfigMgr.jenkinsNamespace == "" {
		clusterName, namespace, err := pipelineConfigMgr.GetJenkinsBindingCluster(pipelineConfigMgr.Parent.GetReadableInstance().Spec.Arguments)

		if err != nil {
			return juju.Trace(err)
		}
		pipelineConfigMgr.jenkinsClusterName = clusterName
		pipelineConfigMgr.jenkinsNamespace = namespace
	}
	return nil
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) GetDevopsPipelineConfigName() string {
	return GetGeneratedDevopsPipelineConfigName(pipelineConfigMgr.Parent.SubjectInstance.Name)
}

func GetGeneratedDevopsPipelineConfigName(amlpipelineconfigName string) string {
	return "amlpipelineconfig-" + amlpipelineconfigName
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) delete() error {
	isExist, pipelineConfig, err := pipelineConfigMgr.isExist()
	if err != nil {
		return juju.Trace(err)
	}
	if isExist {
		return pipelineConfigMgr.Client.Delete(context.TODO(), pipelineConfig)
	}
	return nil
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) isExist() (bool, *devops.PipelineConfig, error) {
	pipelineConfig := &devops.PipelineConfig{}
	namespacedName := utils.NewNamespacedName(pipelineConfigMgr.GetDevopsPipelineConfigName(), pipelineConfigMgr.GetDevopsPipelineConfigNamespace())
	err := pipelineConfigMgr.Client.Get(context.TODO(), namespacedName, pipelineConfig)
	if err != nil {
		if errors.IsNotFound(err) {
			return false, pipelineConfig, nil
		}
		return false, pipelineConfig, err
	}

	return true, pipelineConfig, nil
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) GetDevopsPipelineConfigNamespace() string {
	return pipelineConfigMgr.Parent.SubjectInstance.Namespace
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) updateDevopsPipelineConfig() error {
	updated, err := pipelineConfigMgr.GetDevopsPipelineConfig()
	if err != nil {
		return juju.Trace(err)
	}

	orgin := &devops.PipelineConfig{}
	namespacedName := utils.NewNamespacedName(pipelineConfigMgr.GetDevopsPipelineConfigName(),
		pipelineConfigMgr.GetDevopsPipelineConfigNamespace())
	err = pipelineConfigMgr.Client.Get(context.TODO(), namespacedName, orgin)
	if err != nil {
		if errors.IsNotFound(err) {
			_, err := pipelineConfigMgr.createDevopsPipelineConfig()
			return juju.Trace(err)
		}
		return juju.Trace(err)
	}

	orgin.Spec = updated.Spec
	orgin.Status.Phase = devops.PipelineConfigPhaseCreating
	err = pipelineConfigMgr.Client.Update(context.TODO(), orgin)
	if err != nil {
		return juju.Trace(err)
	}

	return nil
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) createDevopsPipelineConfig() (*devops.PipelineConfig, error) {
	exist, pipelineConfig, err := pipelineConfigMgr.isExist()
	if err != nil || exist {
		return pipelineConfig, juju.Trace(err)
	}

	config, err := pipelineConfigMgr.GetDevopsPipelineConfig()
	if err != nil {
		return nil, juju.Trace(err)
	}

	err = controllerutil.SetControllerReference(pipelineConfigMgr.Parent.SubjectInstance, config,
		pipelineConfigMgr.scheme)
	if err != nil {
		return nil, juju.Trace(err)
	}

	err = pipelineConfigMgr.Client.Create(context.TODO(), config)
	if err != nil {
		return nil, juju.Trace(err)
	}

	return config, nil
}

//noinspection SpellCheckingInspection
func (pipelineConfigMgr *DevopsPipelineConfigComp) GetDevopsPipelineConfig() (*devops.PipelineConfig, error) {
	template, err := pipelineConfigMgr.getDevopsTemplateWithValue()
	if err != nil {
		return nil, err
	}

	jenkinsBinding, err := pipelineConfigMgr.getJenkinsBinding()
	if err != nil {
		return nil, juju.Trace(err)
	}

	devopsPipelineConfig := &devops.PipelineConfig{
		ObjectMeta: v1.ObjectMeta{
			Name:      pipelineConfigMgr.GetDevopsPipelineConfigName(),
			Namespace: pipelineConfigMgr.GetDevopsPipelineConfigNamespace(),
		},
		Spec: devops.PipelineConfigSpec{
			JenkinsBinding: *jenkinsBinding,
			Template:       *template,
			Strategy:       devops.PipelineStrategy{Jenkins: devops.PipelineStrategyJenkins{JenkinsfilePath: "Jenkinsfile"}},
			RunPolicy:      pipelineConfigMgr.Parent.SubjectInstance.Spec.RunPolicy,
		},
		Status: devops.PipelineConfigStatus{
			Phase: devops.PipelineConfigPhaseCreating, // initial phase should be creating
		},
	}

	devopsPipelineConfig.Annotations = common.NewAmlAnnotation().Create().AddMap(devopsPipelineConfig.Annotations).Get()
	devopsPipelineConfig.Labels = common.NewAmlLabel(devopsPipelineConfig.Labels).
		Add(AMLPipelineConfigLabelKey, AMLPipelineConfigLabelValue).
		Add(common.GetLabelKeyForCreatorOf(AMLPipelineConfigCreator), pipelineConfigMgr.Parent.SubjectInstance.Name).
		Get()
	return devopsPipelineConfig, err
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) getJenkinsBinding() (*devops.LocalObjectReference, error) {
	jenkinsClusterName, err := pipelineConfigMgr.getJenkinsClusterName()
	if err != nil {
		return nil, juju.Trace(err)
	}
	jenkinsBinding, err := getClusterJenkinsBinding(pipelineConfigMgr.Client, jenkinsClusterName,
		pipelineConfigMgr.Parent.GetReadableInstance().Namespace)
	if err != nil {
		return nil, juju.Trace(err)
	}
	return &devops.LocalObjectReference{Name: jenkinsBinding}, nil

}

func (pipelineConfigMgr *DevopsPipelineConfigComp) GetJenkinsBindingCluster(argumentGroups []devops.PipelineTemplateArgumentGroup) (string, string, error) {
	mapGroup := utils.GetMergedMapGroup(argumentGroups, nil)
	jenkinsNameSpace, ok := mapGroup["jenkinsNamespace"]
	if !ok || jenkinsNameSpace == "" {
		return getDefaultJenkinsCluster(
			pipelineConfigMgr.Client, pipelineConfigMgr.Parent.GetReadableInstance().Namespace)
	}
	jenkinsNameSpaceMap, err := utils.UnmarshalJsonToMap(jenkinsNameSpace)
	if err != nil {
		return "", "", juju.Trace(err)
	}
	cluster, hasCluster := jenkinsNameSpaceMap["cluster"]
	namespace, hasNamespace := jenkinsNameSpaceMap["namespace"]
	if hasCluster && hasNamespace {
		return cluster, namespace, nil
	}
	return "", "", fmt.Errorf("no jenkins cluster config")
}

func (pipelineConfigMgr *DevopsPipelineConfigComp) getDevopsTemplateWithValue() (*devops.PipelineTemplateWithValue, error) {
	amlTemplate, err := pipelineConfigMgr.getAmlTemplate()
	if err != nil {
		return nil, juju.Trace(err)
	}

	templateSource := devops.PipelineTemplateSource{
		PipelineTemplateRef: devops.PipelineTemplateRef{
			Kind:      amlTemplate.Spec.DevopsTemplate.Kind,
			Name:      amlTemplate.Spec.DevopsTemplate.Name,
			Namespace: amlTemplate.Spec.DevopsTemplate.Namespace,
		},
	}

	values := make(map[string]string)
	argumentGroups := pipelineConfigMgr.Parent.SubjectInstance.Spec.Arguments
	for _, argumentGroup := range argumentGroups {
		argumentValues := argumentGroup.Items
		for _, argumentValue := range argumentValues {
			values[argumentValue.Name] = argumentValue.Value
		}
	}

	binding, err := pipelineConfigMgr.getMlBinding()
	if err != nil {
		return nil, juju.Trace(err)
	}

	agentRaw, err := GetAgentRaw(binding)
	if err != nil {
		return nil, juju.Trace(err)
	}
	agent := devops.JenkinsAgent{
		Raw: agentRaw,
	}

	bytes, err := json.Marshal(pipelineagent{agent})
	if err != nil {
		return nil, juju.Trace(err)
	}

	values["_pipeline_"] = string(bytes)
	result := &devops.PipelineTemplateWithValue{
		PipelineTemplateSource: templateSource,
		Values:                 values,
	}
	return result, nil
}

type pipelineagent struct {
	Agent devops.JenkinsAgent `json:"agent"`
}

func isReady(config *devops.PipelineConfig) (bool, *common.AmlError) {
	switch config.Status.Phase {
	case devops.PipelineConfigPhaseReady:
		return true, nil
	case devops.PipelineConfigPhaseError:
		return false, common.NewAmlError(fmt.Sprintf("devops pipeline error: %s", config.Status.Reason),
			fmt.Sprintf("devops pipeline error: %s", config.Status.Message))
	default:
		return false, nil
	}
}
