package amlpipelineconfig

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"context"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	ErrorSting = "error generate jenkins config"
)

type FlowControlProvider struct {
}

func (fc *FlowControlProvider) Dispatch(reconcileAMLPipelineConfig *ReconcileAMLPipelineConfig, request reconcile.Request) error {
	parent := &v1alpha1.AMLPipelineConfig{}
	err := reconcileAMLPipelineConfig.Get(context.TODO(), request.NamespacedName, parent)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		} else {
			return err
		}
	}

	subject := &AMLPipelineConfigSubject{SubjectInstance: parent}
	if parent.DeletionTimestamp != nil {
		subject.SetCurrentPhase(v1alpha1.AMLPipelineConfigPhaseEnum.Deleting)
	}
	var phase = subject.GetCurrentStage()
	switch phase {
	case v1alpha1.AMLPipelineConfigPhaseEnum.Creating:
		err = fc.processCreatingPhase(reconcileAMLPipelineConfig, subject)
		break
	case v1alpha1.AMLPipelineConfigPhaseEnum.Updating:
		err = fc.processUpdatingPhase(reconcileAMLPipelineConfig, subject)
	case v1alpha1.AMLPipelineConfigPhaseEnum.Ready:
		err = fc.processReadyPhase(reconcileAMLPipelineConfig, subject)
	case v1alpha1.AMLPipelineConfigPhaseEnum.Deleting:
		err = fc.processDeletingPhase(reconcileAMLPipelineConfig, subject)
	}

	updateErr := reconcileAMLPipelineConfig.Client.Update(context.TODO(), subject.SubjectInstance)
	if err == nil && updateErr != nil {
		return updateErr
	}

	return err
}

func (fc *FlowControlProvider) processCreatingPhase(reconcileAMLPipelineConfig *ReconcileAMLPipelineConfig, subject *AMLPipelineConfigSubject) error {

	err := fc.convertToReady(reconcileAMLPipelineConfig, subject)
	if err != nil {
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Error, v1alpha1.AMLPipelineConfigPhaseEnum.Creating, ErrorSting, err.Error())
		return juju.Trace(err)
	}
	subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Creating, v1alpha1.AMLPipelineConfigPhaseEnum.Ready, "", "")
	return nil
}

func (fc *FlowControlProvider) processUpdatingPhase(reconcileAMLPipelineConfig *ReconcileAMLPipelineConfig, subject *AMLPipelineConfigSubject) error {
	err := fc.updateDevopsConfig(reconcileAMLPipelineConfig, subject)
	if err != nil {
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Error, v1alpha1.AMLPipelineConfigPhaseEnum.Updating, ErrorSting, err.Error())
	} else {
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Creating, v1alpha1.AMLPipelineConfigPhaseEnum.Ready, "", "")
	}
	return err
}

func (fc *FlowControlProvider) processDeletingPhase(reconcileAMLPipelineConfig *ReconcileAMLPipelineConfig, subject *AMLPipelineConfigSubject) error {
	devopsPipelineConfigComp := NewPipelineConfigComp(reconcileAMLPipelineConfig, subject)
	err := devopsPipelineConfigComp.delete()
	if err != nil {
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Error, v1alpha1.AMLPipelineConfigPhaseEnum.Deleting, "delete pipeline config error", err.Error())
	} else {
		subject.RemoveMLFinalizer()
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Deleting, v1alpha1.AMLPipelineConfigPhaseEnum.Deleting, "", "")
	}
	return err
}

func (fc *FlowControlProvider) processReadyPhase(reconcileAMLPipelineConfig *ReconcileAMLPipelineConfig, subject *AMLPipelineConfigSubject) error {
	devopsPipelineConfigComp := NewPipelineConfigComp(reconcileAMLPipelineConfig, subject)

	isExist, devopsPipelineConfig, err := devopsPipelineConfigComp.isExist()
	if err != nil {
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Error, v1alpha1.AMLPipelineConfigPhaseEnum.Ready, ErrorSting, err.Error())
		return juju.Trace(err)
	}

	if !isExist {
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Creating, v1alpha1.AMLPipelineConfigPhaseEnum.Creating, "", "")
		return nil
	}

	isReady, amlError := isReady(devopsPipelineConfig)
	if amlError != nil {
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Error, v1alpha1.AMLPipelineConfigPhaseEnum.Ready, amlError.Reason(), amlError.Message())
		return amlError
	}
	if isReady {
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Ready, v1alpha1.AMLPipelineConfigPhaseEnum.Ready, "", "")
	} else {
		subject.SetCurrentState(v1alpha1.AMLPipelineConfigStateEnum.Creating, v1alpha1.AMLPipelineConfigPhaseEnum.Ready, "", "")
	}
	return nil
}

func (fc *FlowControlProvider) updateDevopsConfig(reconcileAMLPipelineConfig *ReconcileAMLPipelineConfig, subject *AMLPipelineConfigSubject) error {
	devopsPipelineConfigComp := NewPipelineConfigComp(reconcileAMLPipelineConfig, subject)
	return devopsPipelineConfigComp.updateDevopsPipelineConfig()
}

func (fc *FlowControlProvider) convertToReady(reconcileAMLPipelineConfig *ReconcileAMLPipelineConfig, subject *AMLPipelineConfigSubject) error {
	devopsPipelineConfigComp := NewPipelineConfigComp(reconcileAMLPipelineConfig, subject)

	_, err := devopsPipelineConfigComp.createDevopsPipelineConfig()
	return juju.Trace(err)
}
