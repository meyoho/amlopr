package amlpipelineconfig

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/config/template"
	"alauda.io/amlopr/pkg/controller/utils"
	"github.com/juju/errors"
	"github.com/pborman/uuid"
	"k8s.io/api/core/v1"
	"sigs.k8s.io/yaml"
	"strings"
)

const pvcAgentRaw = `
{
kubernetes {
		  label 'aml_$NAMESPACE$_$UUID$_agent'
		  defaultContainer 'jnlp'
		  yaml """
__POD_TEMPLATE__
"""
		}
}
`

func GetAgentRaw(binding *v1alpha1.MLBinding) (string, error) {
	template := getPodTemplate(binding)
	bytes, err := yaml.Marshal(template)
	if err != nil {
		return "", errors.Trace(err)
	}
	result := strings.Replace(pvcAgentRaw, "__POD_TEMPLATE__", string(bytes), -1)
	result = strings.Replace(result, "$NAMESPACE$", binding.Namespace, -1)
	result = strings.Replace(result, "$UUID$", uuid.New()[0:6], -1)
	return result, nil
}

func getPodTemplate(binding *v1alpha1.MLBinding) *v1.Pod {
	re := template.GetJenkinsDefaultTemplate()
	re.Namespace = binding.Namespace
	containers := re.Spec.Containers
	for index := range containers {
		volumeMount := utils.GetAMLFixedVolumeMount(binding, "/amlpvc")
		containers[index].VolumeMounts = append(volumeMount, containers[index].VolumeMounts...)
	}
	re.Spec.Volumes = append(utils.GetVolumes(binding), re.Spec.Volumes...)
	//re.Spec.ServiceAccountName = namespace.ServiceAccountName

	return re
}
