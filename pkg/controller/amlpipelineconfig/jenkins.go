package amlpipelineconfig

import (
	"alauda.io/amlopr/pkg/apis/base"
	"alauda.io/amlopr/pkg/config"
	"alauda.io/amlopr/pkg/controller/utils"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	pkgclient "sigs.k8s.io/controller-runtime/pkg/client"
)

func getClusterJenkinBindings(client client.Client, clusterName string, namespace string) (*v1alpha1.JenkinsBindingList, error) {
	jenkinsBindingList := &v1alpha1.JenkinsBindingList{}
	listOptions := &pkgclient.ListOptions{}
	labelSelector := base.GetMLJenkinsBindingLabel(clusterName).GetSelectorString()
	listOptions.SetLabelSelector(labelSelector)
	listOptions.Namespace = namespace
	err := client.List(context.TODO(), listOptions, jenkinsBindingList)
	if err != nil {
		return nil, juju.Trace(err)
	}

	return jenkinsBindingList, nil
}

func getClusterJenkinsBinding(client client.Client, clusterName string, namespace string) (string, error) {
	jenkinsBindingList, err := getClusterJenkinBindings(client, clusterName, namespace)
	if err != nil {
		return "", juju.Trace(err)
	}

	if len(jenkinsBindingList.Items) == 0 {
		return "", fmt.Errorf("no jenkins config in cluster %s \n", clusterName)
	}
	return jenkinsBindingList.Items[0].Name, nil
}

func getDefaultJenkinsCluster(client client.Client, project string) (string, string, error) {
	configMap, err := config.GetProjectConfigMap(client, project)
	if err != nil {
		if errors.IsNotFound(err) {
			return "", "", fmt.Errorf("no default jenkins config")
		}
		return "", "", juju.Trace(err)
	}
	defaultCluster, ok := configMap.Data["defaultJenkinsCluster"]
	if !ok || defaultCluster == "" {
		return "", "", fmt.Errorf("no default jenkins config")
	}

	namespace, err := getRandomNamespace(defaultCluster, project)
	return defaultCluster, namespace, juju.Trace(err)
}

func getRandomNamespace(clusterName string, project string) (string, error) {
	clientset, err := utils.NewBusinessMgr(clusterName).K8sClient()

	if err != nil {
		return "", juju.Trace(err)
	}
	namespaceList, err := clientset.CoreV1().Namespaces().List(v1.ListOptions{
		LabelSelector: base.ProjectMLNamespaceLabel(project).GetSelectorString(),
	})
	if err != nil {
		return "", juju.Trace(err)
	}
	if len(namespaceList.Items) < 1 {
		fmt.Printf("no ml namespace in this cluster, use default")
		return "default", nil
	}
	return namespaceList.Items[0].Name, nil

}
