package base

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"context"
	gerrors "errors"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"time"
)

type FlowController interface {
	Dispatch(ReconcileProvider, SubjectBehavior) (reconcile.Result, error)
	DoAccept(SubjectBehavior)
	DoCheck(ReconcileProvider, SubjectBehavior)
	DoComponentSync(ReconcileProvider, SubjectBehavior)
	DoStatusSync(ReconcileProvider, SubjectBehavior)
	DoStatusUpdate(ReconcileProvider, SubjectBehavior) error
}

func FindAndInitInstance(client client.Client, request reconcile.Request, found runtime.Object) (bool, error) {
	err := client.Get(context.TODO(), request.NamespacedName, found)
	if err != nil {
		if errors.IsNotFound(err) {
			return false, nil
		} else {
			return false, err
		}
	}
	return true, nil
}

type DefaultFlowControlProvider struct {
	FnCheck func(ReconcileProvider, SubjectBehavior)
}

func (dfc *DefaultFlowControlProvider) Dispatch(provider ReconcileProvider, subject SubjectBehavior) (reconcile.Result, error) {
	if founded, err := FindAndInitInstance(provider.GetClient(), provider.GetRequest(), subject.GetInstance()); founded == false || err != nil {
		return reconcile.Result{}, nil
	}
	var state = subject.GetCurrentState()
	switch state {
	case mlv1alpha1.StateUnknown:
		dfc.DoAccept(subject)
	case mlv1alpha1.StateInitializing:
		dfc.DoCheck(provider, subject)
	case mlv1alpha1.StateInitializedError:
		dfc.prepare(subject)
	case mlv1alpha1.StateCreating,
		mlv1alpha1.StateSynced,
		mlv1alpha1.StatePartialSynced,
		mlv1alpha1.StateSyncFailed,
		mlv1alpha1.StateSuccess,
		mlv1alpha1.StatePending:
		dfc.DoComponentSync(provider, subject)
	case mlv1alpha1.StateFailed:
		dfc.prepare(subject)
	}
	if err := dfc.DoStatusUpdate(provider, subject); err != nil {
		return reconcile.Result{}, err
	}

	return reconcile.Result{}, nil
}
func (dfc *DefaultFlowControlProvider) prepare(subject SubjectBehavior) {
	subject.CopyResource()
}

func (dfc *DefaultFlowControlProvider) DoAccept(subject SubjectBehavior) {
	dfc.prepare(subject)
	subject.GetWritableStatus().State = mlv1alpha1.StateInitializing
	subject.GetWritableStatus().CreateAt = &metav1.Time{Time: time.Now()}

}

func (dfc *DefaultFlowControlProvider) DoCheck(provider ReconcileProvider, subject SubjectBehavior) {
	dfc.prepare(subject)
	dfc.FnCheck(provider, subject)
}
func (dfc *DefaultFlowControlProvider) DoComponentSync(provider ReconcileProvider, subject SubjectBehavior) {
	dfc.prepare(subject)
	var errSlice []error
	var syncErrNum = 0
	for _, c := range subject.GetComponents() {
		ok, err := c.Sync(provider)
		if !ok || err != nil {
			syncErrNum++
			errSlice = append(errSlice, err)
		}
	}
	if len(subject.GetComponents()) == syncErrNum {
		subject.GetWritableStatus().State = mlv1alpha1.StateSyncFailed
	} else if syncErrNum == 0 {
		subject.GetWritableStatus().State = mlv1alpha1.StateSynced
	} else {
		subject.GetWritableStatus().State = mlv1alpha1.StatePartialSynced
	}
	dfc.DoStatusSync(provider, subject)
}

func (dfc *DefaultFlowControlProvider) DoStatusSync(provider ReconcileProvider, subject SubjectBehavior) {
	dfc.prepare(subject)
	var readyNum, failedNum = 0, 0
	var amlErrSlice []*common.AmlError
	for _, c := range subject.GetComponents() {
		ok, amlErr := c.IsReady(provider)
		if ok {
			if amlErr != nil {
				subject.GetWritableStatus().State = mlv1alpha1.StateFailed
				subject.GetWritableStatus().Reason = amlErr.Reason()
				subject.GetWritableStatus().Message = amlErr.Message()
				subject.GetWritableStatus().FinishTime = &metav1.Time{Time: time.Now()}
				return
			}

			readyNum++
		} else {
			failedNum++
			if amlErr != nil {
				amlErrSlice = append(amlErrSlice, amlErr)
			}
		}
	}

	if readyNum == len(subject.GetComponents()) {
		subject.GetWritableStatus().State = mlv1alpha1.StateSuccess
		subject.GetWritableStatus().Reason = ""
		subject.GetWritableStatus().Message = ""
		if subject.GetWritableStatus().FinishTime != nil {
			subject.GetWritableStatus().FinishTime = &metav1.Time{Time: time.Now()}
		}
	} else {
		mergedErrs := common.MergeErrors(amlErrSlice)
		subject.GetWritableStatus().State = mlv1alpha1.StatePending
		subject.GetWritableStatus().Reason = mergedErrs.Reason()
		subject.GetWritableStatus().Message = mergedErrs.Message()
	}
}

func (dfc *DefaultFlowControlProvider) DoStatusUpdate(provider ReconcileProvider, subject SubjectBehavior) error {
	logf.SetLogger(logf.ZapLogger(false))
	log := logf.Log.WithName("")
	//set phase according to state
	/*
		StageCreating combined by state: Initializing , Creating , Synced, PartialSynced, SyncFailed
		StagePending  combined by state: Pending
		StageRunning  combined by state: Success
		StageFailed   combined by state: InitializedError
	*/

	switch subject.GetCurrentState() {
	case mlv1alpha1.StateInitializing,
		mlv1alpha1.StateCreating,
		mlv1alpha1.StateSynced,
		mlv1alpha1.StatePartialSynced,
		mlv1alpha1.StateSyncFailed:
		subject.GetWritableStatus().Phase = mlv1alpha1.StageCreating
	case mlv1alpha1.StatePending:
		subject.GetWritableStatus().Phase = mlv1alpha1.StagePending
	case mlv1alpha1.StateSuccess:
		subject.GetWritableStatus().Phase = mlv1alpha1.StageRunning
	case mlv1alpha1.StateInitializedError,
		mlv1alpha1.StateFailed:
		subject.GetWritableStatus().Phase = mlv1alpha1.StageFailed
	}

	instance := subject.GetWritableInstance()
	err := provider.GetClient().Update(context.TODO(), instance)
	if err != nil {
		log.Info("update subject::", "error:", err)
		return err
	}
	return nil
}

// rops interface

type ReconcileProvider interface {
	GetScheme() *runtime.Scheme
	GetClient() client.Client
	GetRequest() reconcile.Request
}

// Subject

type SubjectBehavior interface {
	GetInstance() runtime.Object
	GetWritableInstance() runtime.Object
	GetWritableStatus() *mlv1alpha1.AmlBaseStatus
	CopyResource()
	//
	GetCurrentState() mlv1alpha1.BaseState
	GetComponents() []ComponentBehavior
	AddComponents(...ComponentBehavior)
}

type DefaultSubject struct {
	components []ComponentBehavior
}

func (sub *DefaultSubject) GetComponents() []ComponentBehavior {
	return sub.components
}
func (sub *DefaultSubject) AddComponents(c ...ComponentBehavior) {
	sub.components = append(sub.components, c...)
}

//
//func (sub *DefaultSubject) CopyResource() {
//	fmt.Println(gerrors.New("not implements"))
//}
//func (sub *DefaultSubject) GetInstance() runtime.Object {
//	fmt.Println(gerrors.New("not implements"))
//	return nil
//}
//func (sub *DefaultSubject) GetWritableInstance() runtime.Object {
//	fmt.Println(gerrors.New("not implements"))
//	return nil
//}
//func (sub *DefaultSubject) GetWritableStatus() *mlv1alpha1.AmlBaseStatus {
//	fmt.Println(gerrors.New("not implements"))
//	return nil
//}
//func (sub *DefaultSubject) GetCurrentState() mlv1alpha1.BaseState {
//	fmt.Println(gerrors.New("not implements"))
//	return ""
//}

type ComponentBehavior interface {
	Sync(ReconcileProvider) (bool, error)

	GetEmptyResource() runtime.Object
	GetParsedResource() (runtime.Object, error)
	GetParentResoure() runtime.Object

	NeedUpdate(to runtime.Object) (bool, runtime.Object)

	//if ok but amlError not nil, represent the comp in an unrecoverable state
	//TODO simplifier way?
	IsReady(ReconcileProvider) (bool, *common.AmlError)
}

type DefaultComponentBehavior struct {
}

func (d *DefaultComponentBehavior) Sync(c ComponentBehavior, provider ReconcileProvider) (bool, error) {
	var err error
	targetResource, err := c.GetParsedResource()
	if err != nil {
		return false, err
	}

	vObj, ok := targetResource.(metav1.Object)
	if !ok {
		return false, gerrors.New(ErrResourceCast)
	}
	vRuntime, ok := targetResource.(runtime.Object)
	if !ok {
		return false, gerrors.New(ErrResourceCast)
	}
	ownerObj, ok := c.GetParentResoure().(metav1.Object)
	if !ok {
		return false, gerrors.New(ErrResourceCast)
	}
	if err = controllerutil.SetControllerReference(ownerObj, vObj, provider.GetScheme()); err != nil {
		return false, err
	}

	found := c.GetEmptyResource()
	err = provider.GetClient().Get(context.TODO(), types.NamespacedName{Name: vObj.GetName(), Namespace: provider.GetRequest().Namespace}, found)
	if err != nil && errors.IsNotFound(err) {
		err = provider.GetClient().Create(context.TODO(), vRuntime)
		if err != nil {
			return false, err
		}
	}
	needUpdate, updatedObject := c.NeedUpdate(targetResource)
	if needUpdate {
		err = provider.GetClient().Update(context.TODO(), updatedObject)
		if err != nil {
			return false, err
		}
	}
	return true, nil
}
