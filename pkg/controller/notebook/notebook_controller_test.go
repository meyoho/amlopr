/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package notebook

import (
	"alauda.io/amlopr/pkg/controller/notebook/test"
	"context"
	"fmt"
	"github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/api/resource"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"testing"
	"time"

	. "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	appv1 "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var c client.Client

const timeout = time.Second * 20

func Test_Reconcile_WhenAmlbinding_Missing(t *testing.T) {
	namespace := "withoutbinding"
	var expectedRequest = reconcile.Request{NamespacedName: types.NamespacedName{Name: "notebook-1", Namespace: namespace}}
	g := gomega.NewGomegaWithT(t)
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())

	recFn, _ := SetupTestReconcile(newReconciler(mgr))

	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())

	stopMgr, mgrStopped := StartTestManager(mgr, g)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	setUpEnvWithoutAmlbinding(g, mgr, namespace)
	instance, err := test.LoadYaml2Notebook("test/instance_nb.yaml")
	instance.Status.State = mlv1alpha1.StateInitializing //set to wanted init state
	instance.Namespace = namespace
	g.Expect(err).NotTo(gomega.HaveOccurred())
	err = mgr.GetClient().Create(context.TODO(), instance)
	defer mgr.GetClient().Delete(context.TODO(), instance)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	wanted := &mlv1alpha1.NoteBook{}
	g.Eventually(func() bool {
		if err := mgr.GetClient().Get(context.TODO(), expectedRequest.NamespacedName, wanted); err != nil {
			return false
		}
		if wanted.Status.State != mlv1alpha1.StateInitializedError {
			return false
		}
		return true
	}, timeout).Should(gomega.BeTrue())

	g.Expect(mlv1alpha1.StateInitializedError).To(gomega.BeEquivalentTo(wanted.Status.State))
	g.Expect(ErrProjectBindingNotReadyReason).To(gomega.BeEquivalentTo(wanted.Status.Reason))
}

func Test_Reconcile_WhenPVC_Missing(t *testing.T) {
	namespace := "withoutpvc"
	var expectedRequest = reconcile.Request{NamespacedName: types.NamespacedName{Name: "notebook-1", Namespace: namespace}}
	g := gomega.NewGomegaWithT(t)
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())

	recFn, _ := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())
	stopMgr, mgrStopped := StartTestManager(mgr, g)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	setUpEnvWithoutPVC(g, mgr, namespace)
	instance, err := test.LoadYaml2Notebook("test/instance_nb.yaml")
	instance.Namespace = namespace
	instance.Status.State = mlv1alpha1.StateInitializing //set to wanted init state
	g.Expect(err).NotTo(gomega.HaveOccurred())
	err = mgr.GetClient().Create(context.TODO(), instance)
	defer mgr.GetClient().Delete(context.TODO(), instance)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	wanted := &mlv1alpha1.NoteBook{}
	g.Eventually(func() bool {
		if err := mgr.GetClient().Get(context.TODO(), expectedRequest.NamespacedName, wanted); err != nil {
			return false
		}
		if wanted.Status.State != mlv1alpha1.StateInitializedError {
			return false
		}
		return true
	}, timeout).Should(gomega.BeTrue())
	fmt.Println(wanted.Status.Reason)
	g.Expect(mlv1alpha1.StateInitializedError).To(gomega.BeEquivalentTo(wanted.Status.State))
	g.Expect(wanted.Status.Reason).Should(gomega.ContainSubstring(ErrProjectStorageNotReadyReason))
}

// Test_Reconcile_When_Insufficient_PVC require pvcfoo and pvcfoo2 , just has pvcfoo on k8s
func Test_Reconcile_When_Insufficient_PVC(t *testing.T) {
	namespace := "insufficientpvc"
	var expectedRequest = reconcile.Request{NamespacedName: types.NamespacedName{Name: "notebook-1", Namespace: namespace}}
	g := gomega.NewGomegaWithT(t)
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())

	recFn, _ := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())
	stopMgr, mgrStopped := StartTestManager(mgr, g)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	setUpEnvInsufficient(g, mgr, namespace)
	instance, err := test.LoadYaml2Notebook("test/instance_nb.yaml")
	instance.Namespace = namespace
	instance.Status.State = mlv1alpha1.StateInitializing //set to wanted init state
	// add more pvc ,we required
	pvc2 := mlv1alpha1.NotebookPVC{
		ClaimName: "pvcfoo2",
		MountPath: "/home/jovyan/pvcfoo2/project",
		ReadOnly:  false,
		SubPath:   "project",
	}
	instance.Spec.Pvc = append(instance.Spec.Pvc, pvc2)

	g.Expect(err).NotTo(gomega.HaveOccurred())
	err = mgr.GetClient().Create(context.TODO(), instance)
	defer mgr.GetClient().Delete(context.TODO(), instance)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	wanted := &mlv1alpha1.NoteBook{}
	g.Eventually(func() bool {
		if err := mgr.GetClient().Get(context.TODO(), expectedRequest.NamespacedName, wanted); err != nil {
			return false
		}
		//fmt.Println("123=",wanted.Status.State)
		if wanted.Status.State != mlv1alpha1.StateInitializedError {
			return false
		}
		return true
	}, timeout).Should(gomega.BeTrue())
	fmt.Println(wanted.Status.Reason)
	g.Expect(mlv1alpha1.StateInitializedError).To(gomega.BeEquivalentTo(wanted.Status.State))
	g.Expect(wanted.Status.Reason).Should(gomega.ContainSubstring(ErrProjectStorageNotReadyReason))
}

func Test_Notebook_Reconcile(t *testing.T) {
	gomega.RegisterFailHandler(ginkgo.Fail)
	namespace := "test-ns"
	var expectedRequest = reconcile.Request{NamespacedName: types.NamespacedName{Name: "notebook-1", Namespace: namespace}}
	g := gomega.NewGomegaWithT(t)

	instance, err := test.LoadYaml2Notebook("test/instance_nb.yaml")
	instance.Namespace = namespace
	g.Expect(err).NotTo(gomega.HaveOccurred())

	// Setup the Manager and Controller.  Wrap the Controller Reconcile function so it writes each request to a
	// channel when it is finished.
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())

	c = mgr.GetClient()

	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())

	stopMgr, mgrStopped := StartTestManager(mgr, g)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	setUpCorrectEnv(g, mgr, namespace)
	// Create the NoteBook object and expect the Reconcile and Deployment to be created
	err = c.Create(context.TODO(), instance)
	defer c.Delete(context.TODO(), instance)
	g.Expect(err).NotTo(gomega.HaveOccurred())

	var lastState mlv1alpha1.BaseState
	for {
		wanted := &mlv1alpha1.NoteBook{}
		g.Eventually(func() bool {
			if err := c.Get(context.TODO(), expectedRequest.NamespacedName, wanted); err != nil {
				return false
			}

			if lastState != wanted.Status.State {
				fmt.Println("lastState=", lastState)
				fmt.Println("now.State=", wanted.Status.State)
				lastState = wanted.Status.State
				return true
			}

			return false
		}, timeout).Should(gomega.BeTrue())

		g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))

		switch wanted.Status.State {
		case mlv1alpha1.StateInitializing, mlv1alpha1.StateCreating:
			g.Expect(mlv1alpha1.StageCreating).To(gomega.BeEquivalentTo(wanted.Status.Phase))
		case mlv1alpha1.StatePending:
			g.Expect(mlv1alpha1.StagePending).To(gomega.BeEquivalentTo(wanted.Status.Phase))
		case mlv1alpha1.StateSuccess:
			g.Expect(mlv1alpha1.StageRunning).To(gomega.BeEquivalentTo(wanted.Status.Phase))
		}
		fmt.Println()
		fmt.Println("Phase=", wanted.Status.Phase)
		fmt.Println("State", wanted.Status.State)
		fmt.Println("next turn.")

		if wanted.Status.Phase == mlv1alpha1.StagePending && wanted.Status.State == mlv1alpha1.StatePending {
			break
		}
	}
	defer ginkgo.GinkgoRecover()
	// expect managed Resource have been created successfully
	statefulSet := &appv1.StatefulSet{}
	var nbsts *StatefulSetMgr
	gomega.Expect(func() error {
		nbsts = NewStatefulSetMgr(instance, &ReconcileNoteBook{c, mgr.GetScheme()})
		objkey := types.NamespacedName{Name: GetStatefulSetName(nbsts.Notebook), Namespace: expectedRequest.Namespace}

		return mgr.GetClient().Get(context.TODO(), objkey, statefulSet)
	}).To(gomega.Succeed())
	fmt.Println("statefulSet name : ", statefulSet.Name)
	svc := &corev1.Service{}
	gomega.Expect(func() error {
		nbsvc := NewServiceMgr(instance, nbsts, &ReconcileNoteBook{c, mgr.GetScheme()})
		objkey := types.NamespacedName{Name: GetServiceName(nbsvc.Notebook), Namespace: expectedRequest.Namespace}

		return mgr.GetClient().Get(context.TODO(), objkey, svc)
	}).To(gomega.Succeed())
	fmt.Println("service name : ", svc.Name)

}

func setUpCorrectEnv(g *gomega.GomegaWithT, mgr manager.Manager, namespace string) {
	amlbindingName := "bindingfoo"
	pvcName := "pvcfoo"
	mockCreatePvc(g, mgr, namespace, pvcName)
	mockCreateAmlbinding(g, mgr, namespace, amlbindingName, pvcName)
}

func setUpEnvInsufficient(g *gomega.GomegaWithT, mgr manager.Manager, namespace string) {
	amlbindingName := "bindingfoo"
	pvcName := "pvcfoo"
	mockCreatePvc(g, mgr, namespace, pvcName)
	mockCreateAmlbinding2pvc(g, mgr, namespace, amlbindingName, pvcName)
}

func setUpEnvWithoutAmlbinding(g *gomega.GomegaWithT, mgr manager.Manager, namespace string) {
	pvcName := "pvcfoo"
	mockCreatePvc(g, mgr, namespace, pvcName)
}
func setUpEnvWithoutPVC(g *gomega.GomegaWithT, mgr manager.Manager, namespace string) {
	amlbindingName := "bindingfoo"
	pvcName := "pvcfoo"
	mockCreateAmlbinding(g, mgr, namespace, amlbindingName, pvcName)
}

func mockCreatePvc(g *gomega.GomegaWithT, mgr manager.Manager, namespace, pvcName string) {
	pvc := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{Name: pvcName, Namespace: namespace},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Resources: corev1.ResourceRequirements{
				Requests: map[corev1.ResourceName]resource.Quantity{
					corev1.ResourceStorage: resource.MustParse("50Mi"),
				},
			},
		},
	}
	err := mgr.GetClient().Create(context.TODO(), pvc)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	pvccur := &corev1.PersistentVolumeClaim{}
	g.Eventually(func() bool {
		objKey := types.NamespacedName{
			Name: pvcName, Namespace: namespace,
		}
		if err := mgr.GetClient().Get(context.TODO(), objKey, pvccur); err != nil {
			return false
		}
		return true
	}, timeout).Should(gomega.BeTrue())
}

func mockCreateAmlbinding2pvc(g *gomega.GomegaWithT, mgr manager.Manager, namespace, amlbindingName, pvcName string) {
	amlbinding := &mlv1alpha1.MLBinding{
		ObjectMeta: metav1.ObjectMeta{Name: amlbindingName, Namespace: namespace},
		Spec: MLBindingSpec{
			Pvc: []PVC{
				{
					Volume: v1.Volume{
						Name: "pvc1",
						VolumeSource: v1.VolumeSource{
							PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
								ClaimName: pvcName,
							},
						},
					},
					MountPath: "/aml",
				},
				{
					Volume: v1.Volume{
						Name: "pvc2",
						VolumeSource: v1.VolumeSource{
							PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
								ClaimName: pvcName,
							},
						},
					},
					MountPath: "/aml",
				},
			},
		}}

	err := mgr.GetClient().Create(context.TODO(), amlbinding)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	cur := &mlv1alpha1.MLBinding{}
	g.Eventually(func() bool {
		objKey := types.NamespacedName{
			Name: amlbindingName, Namespace: namespace,
		}
		if err := mgr.GetClient().Get(context.TODO(), objKey, cur); err != nil {
			return false
		}
		return true
	}, timeout).Should(gomega.BeTrue())
}

func mockCreateAmlbinding(g *gomega.GomegaWithT, mgr manager.Manager, namespace, amlbindingName, pvcName string) {
	amlbinding := &mlv1alpha1.MLBinding{
		ObjectMeta: metav1.ObjectMeta{Name: amlbindingName, Namespace: namespace},
		Spec: MLBindingSpec{
			Pvc: []PVC{
				{
					Volume: v1.Volume{
						Name: "pvc1",
						VolumeSource: v1.VolumeSource{
							PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
								ClaimName: pvcName,
							},
						},
					},
					MountPath: "/aml",
				},
			},
		}}

	err := mgr.GetClient().Create(context.TODO(), amlbinding)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	cur := &mlv1alpha1.MLBinding{}
	g.Eventually(func() bool {
		objKey := types.NamespacedName{
			Name: amlbindingName, Namespace: namespace,
		}
		if err := mgr.GetClient().Get(context.TODO(), objKey, cur); err != nil {
			return false
		}
		return true
	}, timeout).Should(gomega.BeTrue())
}
