package notebook

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/utils"
	"context"
	"fmt"
	appv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"reflect"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"strings"
)

type StatefulSetMgr struct {
	Notebook       *mlv1alpha1.NoteBook
	ContainerName  string
	ContainerPort  int
	namespacedName types.NamespacedName
	reconcile      *ReconcileNoteBook
}

func NewStatefulSetMgr(ins *mlv1alpha1.NoteBook, reconcile *ReconcileNoteBook) *StatefulSetMgr {
	return &StatefulSetMgr{
		Notebook:      ins,
		ContainerPort: 8888,
		ContainerName: "notebook",
		namespacedName: types.NamespacedName{
			Namespace: ins.Namespace,
			Name:      GetStatefulSetName(ins),
		},
		reconcile: reconcile,
	}
}
func GetStatefulSetName(nb *mlv1alpha1.NoteBook) string {
	return fmt.Sprintf("%s-%s", GetNotebookUniqName(nb), strings.ToLower(common.KindStatefulSet))
}

//func (mgr *StatefulSetMgr) GetName() string {
//	return fmt.Sprintf("%s-%s", GetNotebookUniqName(mgr.Notebook), strings.ToLower(common.KindStatefulSet))
//}

func (mgr *StatefulSetMgr) NeedUpdate(fromSts, toSts *appv1.StatefulSet) bool {
	// load resourceNotebook before check if need update
	from := fromSts.Spec
	to := toSts.Spec
	var resEqual, imgEqual bool
	if len(from.Template.Spec.Containers) > 0 {
		resEqual = reflect.DeepEqual(from.Template.Spec.Containers[0].Resources, to.Template.Spec.Containers[0].Resources)
		imgEqual = reflect.DeepEqual(from.Template.Spec.Containers[0].Image, to.Template.Spec.Containers[0].Image)
		if !resEqual || !imgEqual {
			return true
		}
	}
	return false
}

func (mgr *StatefulSetMgr) ParseToStatefulSet() *appv1.StatefulSet {
	srvName := fmt.Sprintf("%s-%s", GetNotebookUniqName(mgr.Notebook), strings.ToLower(common.KindService))
	requestsMap := utils.FilteredRequestsResourceMapByKey(mgr.Notebook.Spec.Quota.Hard)
	limitsMap := utils.FilteredLimitsResourceMapByKey(mgr.Notebook.Spec.Quota.Hard)
	var volumeList []corev1.Volume
	var volumeMountList []corev1.VolumeMount
	var mapedVolume = make(map[string]corev1.Volume)
	for _, v := range mgr.Notebook.Spec.Pvc {
		if _, ok := mapedVolume[v.ClaimName]; !ok {
			mapedVolume[v.ClaimName] = corev1.Volume{
				Name: v.ClaimName,
				VolumeSource: corev1.VolumeSource{
					PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
						ClaimName: v.ClaimName,
						ReadOnly:  v.ReadOnly,
					},
				},
			}
		}
		volumeMount := corev1.VolumeMount{
			Name:      v.ClaimName,
			MountPath: v.MountPath,
			ReadOnly:  v.ReadOnly,
			SubPath:   v.SubPath,
		}
		volumeMountList = append(volumeMountList, volumeMount)
	}

	for _, v := range mapedVolume {
		volumeList = append(volumeList, v)
	}
	annotations := common.NewAmlAnnotation().Create().Get()
	annotations[mlv1alpha1.AnnotationsKeyNvidiaGpuVisibilityEnhancement()] = "on"
	labels := common.NewAmlLabel().
		Add(common.AmlLabelAppKey, AmlLabelAppValue).
		Add(common.GetLabelKeyForCreatorOf(NBCreator), mgr.Notebook.GetName()).
		Get()
	num := int32(1)
	rootID := int64(0) // always run as root
	args := NewDefaultArgs()
	sts := &appv1.StatefulSet{
		TypeMeta: metav1.TypeMeta{
			Kind:       common.KindStatefulSet,
			APIVersion: common.APIVersionStatefulSet,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:        GetStatefulSetName(mgr.Notebook),
			Namespace:   mgr.Notebook.GetNamespace(),
			Annotations: annotations,
			Labels:      labels,
		},
		Spec: appv1.StatefulSetSpec{
			PodManagementPolicy: appv1.ParallelPodManagement,
			Replicas:            &num,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			ServiceName: srvName,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: annotations,
					Labels:      labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name: "notebook",
							/*
								//pytorch:    www.myalauda.com:31413/alaudaorg/pytorch-notebook:v0.4.1
								//tensorflow: index.alauda.cn/alaudaorg/tensorflow-1.8.0-notebook-cpu:v0.5.7
							*/
							Args:            args.GetCommandArgs(mgr),
							Image:           mgr.Notebook.Spec.Framework.Image,
							ImagePullPolicy: corev1.PullAlways,
							Resources: corev1.ResourceRequirements{
								Limits:   limitsMap,
								Requests: requestsMap,
							},
							VolumeMounts: volumeMountList,
							Ports: []corev1.ContainerPort{
								{
									ContainerPort: int32(args.GetContainerPort().IntVal),
									Protocol:      corev1.ProtocolTCP,
								},
							},
							ReadinessProbe: &corev1.Probe{
								Handler: corev1.Handler{
									TCPSocket: &corev1.TCPSocketAction{
										Port: intstr.FromInt(mgr.ContainerPort),
									},
								},
								InitialDelaySeconds: 5,
								PeriodSeconds:       10,
							},
							LivenessProbe: &corev1.Probe{
								Handler: corev1.Handler{
									TCPSocket: &corev1.TCPSocketAction{
										Port: intstr.FromInt(mgr.ContainerPort),
									},
								},
								InitialDelaySeconds: 15,
								PeriodSeconds:       20,
							},
							SecurityContext: &corev1.SecurityContext{
								RunAsUser: &rootID,
							},
						},
					},
					Volumes: volumeList,
				},
			},
		},
	}
	if mgr.Notebook.Spec.Framework.ImagePullSecret != "" {
		sts.Spec.Template.Spec.ImagePullSecrets = []corev1.LocalObjectReference{{Name: mgr.Notebook.Spec.Framework.ImagePullSecret}}
	}
	return sts
}

func (mgr *StatefulSetMgr) IsStatefulSetReady(r *ReconcileNoteBook) (bool, mlv1alpha1.PodAspect, *common.AmlError) {
	var err error
	var isReady bool
	var podAspect = mlv1alpha1.PodAspect{}
	var amlError *common.AmlError
	found := &appv1.StatefulSet{}
	err = r.Client.Get(context.Background(), mgr.namespacedName, found)
	if err != nil {
		isReady = false
		return isReady, podAspect, nil
	}
	//check sts
	if found.Status.CurrentReplicas != 1 {
		log.Info(fmt.Sprintf("StatefulSet %v.%v [not ready]", mgr.namespacedName.Namespace, mgr.namespacedName.Name))
		return false, podAspect, common.NewAmlError(ErrStsNotReadyReason, ErrStsNotReadyMsg)
	}
	// check pod
	existsPod := &corev1.Pod{}
	req := types.NamespacedName{
		Namespace: mgr.Notebook.GetNamespace(),
		Name:      mgr.namespacedName.Name + "-0",
	}
	err = r.Client.Get(context.Background(), req, existsPod)
	if err != nil {
		log.Info(fmt.Sprintf("StatefulSet'pod %v.%v [not ready]", req.Namespace, req.Name))
		if errors.IsNotFound(err) {
			amlError = common.NewAmlError(ErrPodNotReadyReason, ErrPodNotReadyMsg)
		} else {
			amlError = common.NewAmlError(ErrPodNotReadyReason, err.Error())
		}
		return false, podAspect, amlError
	}
	// exists
	podReady, reason, msg := IsPodReady(&existsPod.Status)
	if podReady {
		isReady = true
		podAspect.Spec = existsPod.Spec
		podAspect.Status = existsPod.Status
	} else {
		var r, m string
		if existsPod.Status.Reason == "" {
			r = reason
		}
		if existsPod.Status.Message == "" {
			m = msg
		}
		log.Info(fmt.Sprintf("StatefulSet'pod %v.%v [not ready]", req.Namespace, req.Name))
		isReady = false
		amlError = common.NewAmlError(r, m)
	}
	return isReady, podAspect, amlError
}

func (mgr *StatefulSetMgr) Apply() (err error) {
	existsStatefulSet := &appv1.StatefulSet{}
	statefulSet := mgr.ParseToStatefulSet()
	err = mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsStatefulSet)
	if err != nil {
		if !errors.IsNotFound(err) {
			return
		}
		if err := controllerutil.SetControllerReference(mgr.Notebook, statefulSet, mgr.reconcile.scheme); err != nil {
			return err
		}
		if err = mgr.reconcile.Client.Create(context.Background(), statefulSet); err != nil {
			log.Error(err, fmt.Sprintf("when notebook statefulset %v.%v create", mgr.namespacedName.Namespace, mgr.namespacedName.Name))
			return err
		}
	}
	// update
	if mgr.NeedUpdate(existsStatefulSet, statefulSet) {
		if err := controllerutil.SetControllerReference(mgr.Notebook, statefulSet, mgr.reconcile.scheme); err != nil {
			return err
		}
		if err := mgr.reconcile.Client.Update(context.Background(), statefulSet); err != nil {
			log.Error(err, fmt.Sprintf("when notebook statefulset %v.%v updated", mgr.namespacedName.Namespace, mgr.namespacedName.Name))
			return err
		}
	}
	return
}

func (mgr *StatefulSetMgr) Delete() (err error) {
	existsStatefulSet := &appv1.StatefulSet{}
	err = mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsStatefulSet)
	if err != nil {
		if !errors.IsNotFound(err) {
			return err
		}
		// already not exists
		return nil
	}
	delFunc := func(*client.DeleteOptions) {}
	err = mgr.reconcile.Client.Delete(context.Background(), existsStatefulSet, delFunc)
	if err != nil {
		return err
	}
	return nil
}

func SetStoppedState(notebook *mlv1alpha1.NoteBook) {
	notebook.Status.State = mlv1alpha1.StateStopped
	notebook.Status.PodAspect = mlv1alpha1.PodAspect{}
	notebook.Status.ServiceAspect = mlv1alpha1.ServiceAspect{}
	notebook.Status.StartTime = nil
}
func SetStopFailedState(notebook *mlv1alpha1.NoteBook) {
	notebook.Status.State = mlv1alpha1.StateStopFailed
	notebook.Status.Message = "delete managed resources error"
	notebook.Status.Reason = "STOP_ERROR"
}

func SetApplyFailedState(notebook *mlv1alpha1.NoteBook) {
	notebook.Status.State = mlv1alpha1.StatePending
	notebook.Status.Message = "part of Notebook error"
	notebook.Status.Reason = "APPLY_ERROR"
}
