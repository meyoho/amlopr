package test

import (
	mlv1aplha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"encoding/json"
	"github.com/ghodss/yaml"
	"io/ioutil"
)

func LoadYaml2Notebook(filePath string) (*mlv1aplha1.NoteBook, error) {

	var err error
	var nb mlv1aplha1.NoteBook
	//var filePath = "instance_nb.yaml"
	data, err := ioutil.ReadFile(filePath)

	jsonBlob, err := yaml.YAMLToJSON(data)
	if err != nil {
		return &mlv1aplha1.NoteBook{}, err
	}
	err = json.Unmarshal(jsonBlob, &nb)
	if err != nil {
		return &mlv1aplha1.NoteBook{}, err
	}
	return &nb, nil
}
