package test

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"fmt"
	"testing"
)

func Test_convert_notebook(t *testing.T) {
	path := "instance_nb.yaml"
	nb, err := LoadYaml2Notebook(path)
	fmt.Println(nb, err)
}

func Test_generic(t *testing.T) {
	var ins interface{}

	//
	path := "instance_nb.yaml"
	nb, _ := LoadYaml2Notebook(path)

	ins = nb

	// check if ins = runtime.object
	//aa , ok :=ins.(runtime.Object)
	//if !ok {
	//	fmt.Println("cat runtime.object error")
	//}
	//fmt.Println(aa)
	ccc, ok := ins.(*v1alpha1.NoteBook)
	if !ok {
		fmt.Println("cat runtime.object error")
	}
	fmt.Println(ccc)
	// check if ins = status
	//bb , ok :=ins.(*v1alpha1.NoteBookStatus)
	//if !ok {
	//	fmt.Println("cat runtime.object error")
	//}
	//fmt.Println(bb)
}

func Test_generic_11(t *testing.T) {
	l := []string{"222", "333"}

	fmt.Println(l[0])
}
