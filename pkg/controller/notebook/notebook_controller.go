/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package notebook

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"context"
	"fmt"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/sets"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"time"
)

const (
	NBCreator    = "notebook"
	NBLabelKey   = "app"
	NBLabelValue = "notebook"
)

var ctlName = "notebook-controller"
var log = logf.Log.WithName(ctlName)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new NoteBook Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
// USER ACTION REQUIRED: update cmd/manager/main.go to call this ml.Add(mgr) to install this Controller
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileNoteBook{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New(ctlName, mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to NoteBook
	err = c.Watch(&source.Kind{Type: &mlv1alpha1.NoteBook{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}
	p := common.GetPredicateFuncs(NBLabelKey, NBLabelValue)
	//pod
	err = c.Watch(&source.Kind{Type: &corev1.Pod{}}, &handler.EnqueueRequestsFromMapFunc{
		ToRequests: common.GetMapFnForLabelKey(common.GetLabelKeyForCreatorOf(NBCreator)),
	}, p)
	if err != nil {
		return err
	}
	//sts
	err = c.Watch(&source.Kind{Type: &appsv1.StatefulSet{}}, &handler.EnqueueRequestsFromMapFunc{
		ToRequests: common.GetMapFnForLabelKey(common.GetLabelKeyForCreatorOf(NBCreator)),
	}, p)
	if err != nil {
		return err
	}
	//svc
	err = c.Watch(&source.Kind{Type: &corev1.Service{}}, &handler.EnqueueRequestsFromMapFunc{
		ToRequests: common.GetMapFnForLabelKey(common.GetLabelKeyForCreatorOf(NBCreator)),
	}, p)
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileNoteBook{}

// ReconcileNoteBook reconciles a NoteBook object
type ReconcileNoteBook struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a NoteBook object and makes changes based on the state read
// and what is in the NoteBook.Spec
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=ml.alauda.io,resources=notebooks,verbs=get;list;watch;create;update;patch;delete
func (r *ReconcileNoteBook) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	log.Info("start Reconcile")
	log.Info("request::", "Namespace:", request.Namespace, "Name:", request.Name)
	defer log.Info("Over Reconcile")
	return reconcile.Result{}, r.Dispatch(request)
}

func (r *ReconcileNoteBook) Dispatch(request reconcile.Request) error {
	var err error
	instance := &mlv1alpha1.NoteBook{}
	if err := common.ValidateRequest(r.Client, request.NamespacedName, instance); err != nil {
		return err
	}
	var needUpdate = true
	var instanceCopy = instance.DeepCopy()
	switch instance.Status.State {
	case mlv1alpha1.StateUnknown:
		r.acceptResource(instanceCopy)
	case mlv1alpha1.StateInitializing:
		r.doInitializing(instanceCopy)
	case mlv1alpha1.StateInitializedError:
		// pass
	case mlv1alpha1.StateCreating,
		mlv1alpha1.StateSynced,
		mlv1alpha1.StatePartialSynced,
		mlv1alpha1.StateSyncFailed,
		mlv1alpha1.StateSuccess,
		mlv1alpha1.StatePending:
		err = r.syncManagedResources(instanceCopy)
		if err != nil {
			log.Info("syncManagedResources", "err:", err)
		}
	case mlv1alpha1.StateStopping:
		err = r.deleteManagedResources(instanceCopy)
		if err != nil {
			log.Info("deleteManagedResources", "err:", err)
		}
	default:
		needUpdate = false
	}
	if needUpdate {
		//r.updateState(instanceCopy)
		updatePhaseStatus(instanceCopy)
		err := r.Client.Update(context.TODO(), instanceCopy)
		if err != nil {
			log.Info("update notebook::", "error:", err)
			return err
		}
	}
	return nil
}

func updatePhaseStatus(instanceCopy *mlv1alpha1.NoteBook) {
	//set phase according to state
	/*
		StageCreating combined by state: Initializing , Creating , Synced, PartialSynced, SyncFailed
		StagePending  combined by state: Pending
		StageRunning  combined by state: Success
		StageFailed   combined by state: InitializedError
	*/

	switch instanceCopy.Status.State {
	case mlv1alpha1.StateInitializing,
		mlv1alpha1.StateCreating,
		mlv1alpha1.StateSynced,
		mlv1alpha1.StatePartialSynced,
		mlv1alpha1.StateSyncFailed:
		instanceCopy.Status.Phase = mlv1alpha1.StageCreating
	case mlv1alpha1.StatePending:
		instanceCopy.Status.Phase = mlv1alpha1.StagePending
	case mlv1alpha1.StateSuccess,
		mlv1alpha1.StateStopFailed:
		instanceCopy.Status.Phase = mlv1alpha1.StageRunning
	case mlv1alpha1.StateInitializedError:
		instanceCopy.Status.Phase = mlv1alpha1.StageFailed
	case mlv1alpha1.StateStopped:
		instanceCopy.Status.Phase = mlv1alpha1.StageStopped
	}
}

func (r *ReconcileNoteBook) acceptResource(instanceCopy *mlv1alpha1.NoteBook) {
	instanceCopy.Status.State = mlv1alpha1.StateInitializing
	instanceCopy.Status.CreateAt = &metav1.Time{Time: time.Now()}
}

func (r *ReconcileNoteBook) doInitializing(instanceCopy *mlv1alpha1.NoteBook) {
	passed, errs := r.checkPass(instanceCopy)
	if passed {
		instanceCopy.Status.State = mlv1alpha1.StateCreating
		instanceCopy.Status.Reason = ""
		instanceCopy.Status.Message = ""
	} else {
		instanceCopy.Status.State = mlv1alpha1.StateInitializedError
		instanceCopy.Status.Reason = errs.Reason()
		instanceCopy.Status.Message = errs.Message()
	}
}

func (r *ReconcileNoteBook) deleteManagedResources(notebook *mlv1alpha1.NoteBook) error {
	stsMgr := NewStatefulSetMgr(notebook, r)
	stsErr := stsMgr.Delete()

	svcMgr := NewServiceMgr(notebook, stsMgr, r)
	svcErr := svcMgr.Delete()

	if stsErr != nil || svcErr != nil {
		SetStopFailedState(notebook)
		return fmt.Errorf("%v", notebook.Status.Message)
	}
	SetStoppedState(notebook)
	return nil
}

func (r *ReconcileNoteBook) syncManagedResources(notebookCopy *mlv1alpha1.NoteBook) error {
	stsMgr := NewStatefulSetMgr(notebookCopy, r)
	stsErr := stsMgr.Apply()

	svcMgr := NewServiceMgr(notebookCopy, stsMgr, r)
	svcErr := svcMgr.Apply()

	if stsErr != nil || svcErr != nil {
		SetApplyFailedState(notebookCopy)
		return fmt.Errorf("%s", notebookCopy.Status.Message)
	}
	r.SetAppliedState(notebookCopy)
	return nil
}

func (r *ReconcileNoteBook) SetAppliedState(notebookCopy *mlv1alpha1.NoteBook) {
	stsMgr := NewStatefulSetMgr(notebookCopy, r)
	svcMgr := NewServiceMgr(notebookCopy, stsMgr, r)
	//statefulSet
	podReady, podAspect, stsErrs := stsMgr.IsStatefulSetReady(r)
	notebookCopy.Status.PodAspect = podAspect
	if podReady {
		notebookCopy.Status.PodAspect.Ready = true
	} else {
		notebookCopy.Status.PodAspect.Ready = false
	}
	//service
	svcReady, serviceAspect, svcErrs := svcMgr.IsServiceReady(r)
	notebookCopy.Status.ServiceAspect = serviceAspect
	if svcReady {
		notebookCopy.Status.ServiceAspect.Ready = true
	} else {
		notebookCopy.Status.ServiceAspect.Ready = false
	}
	// notebook state
	if podReady && svcReady {
		SetSucceedState(notebookCopy)
	} else {
		var allErrs []*common.AmlError
		if stsErrs != nil {
			allErrs = append(allErrs, stsErrs)
		}
		if svcErrs != nil {
			allErrs = append(allErrs, svcErrs)
		}
		mergedErrs := common.MergeErrors(allErrs)
		SetPendingState(notebookCopy, mergedErrs.Reason(), mergedErrs.Message())
	}
}
func setState(notebook *mlv1alpha1.NoteBook, state mlv1alpha1.BaseState, reason, message string) {
	notebook.Status.State = state
	notebook.Status.Message = message
	notebook.Status.Reason = reason
}

func SetSucceedState(notebook *mlv1alpha1.NoteBook) {
	setState(notebook, mlv1alpha1.StateSuccess, "", "")
	if notebook.Status.StartTime == nil {
		notebook.Status.StartTime = &metav1.Time{Time: time.Now()}
	}
}
func SetPendingState(notebook *mlv1alpha1.NoteBook, reason, message string) {
	setState(notebook, mlv1alpha1.StatePending, reason, message)
}

func (r *ReconcileNoteBook) checkPass(instance *mlv1alpha1.NoteBook) (bool, *common.AmlError) {
	var err error
	// check MLBindingList
	mlBindingList := &mlv1alpha1.MLBindingList{}
	opts := &client.ListOptions{
		Namespace: instance.Namespace,
	}
	err = r.Client.List(context.TODO(), opts, mlBindingList)
	if err != nil || len(mlBindingList.Items) == 0 {
		if err != nil {
			log.Error(err, fmt.Sprintf("state: %s , mlbinding check pass :false ,err:%v",
				instance.Status.State, err))
		} else {
			log.Error(errors.NewNotFound(schema.GroupResource{Group: "ml.alauda.io", Resource: "MLBinding"}, ""),
				fmt.Sprintf("state: %s , mlbinding check pass :false , "+
					"len(mlBindingList.Items) = 0", instance.Status.State))
		}
		return false, common.NewAmlError(ErrProjectBindingNotReadyReason, ErrProjectBindingNotReadyMsg)
	}
	// check notebook required pvc(exists on k8s) should be satisfied
	pvcClaimList := &corev1.PersistentVolumeClaimList{}
	err = r.Client.List(context.TODO(), opts, pvcClaimList)
	if err != nil || len(pvcClaimList.Items) == 0 {
		if err != nil {
			log.Error(err, fmt.Sprintf("state: %s , pvcClaimList check pass :false", instance.Status.State))
		} else {
			log.Error(errors.NewNotFound(schema.GroupResource{Group: "v1", Resource: "PersistentVolumeClaim"}, ""),
				fmt.Sprintf("state: %s ,pvcClaimList check pass :false , len(pvcClaimList.Items) = 0", instance.Status.State))
		}
		return false, common.NewAmlError(ErrProjectStorageNotReadyReason, ErrProjectStorageNotReadyMsg)
	}
	var foundNum = 0
	pvcReqSet := &sets.String{}
	for _, requiredPvc := range instance.Spec.Pvc {
		pvcReqSet.Insert(requiredPvc.ClaimName)
	}
	pvcReqList := pvcReqSet.UnsortedList()
	for _, v := range pvcReqList {
		for _, pvc := range pvcClaimList.Items {
			if pvc.Name == v {
				foundNum++
			}
		}
	}
	if len(pvcReqList) != foundNum {
		log.Error(errors.NewBadRequest("insufficient pvcs"), fmt.Sprintf("state: %s ,len(pvcSlice) != foundNum", instance.Status.State))
		return false, common.NewAmlError(ErrProjectStorageNotReadyReason, ErrProjectStorageNotReadyMsg)
	}
	return true, nil
}

func GetNotebookUniqName(ins *mlv1alpha1.NoteBook) string {
	return fmt.Sprintf("%s-%s", TransUserName(ins.Spec.User), ins.Name)
}
