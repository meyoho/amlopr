package notebook

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/utils"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"testing"
)

func Test_json(t *testing.T) {

	AMLAnnotationsMap := map[string]string{
		mlv1alpha1.AnnotationProductAMLKey():     mlv1alpha1.AnnotationProductAMLValue,
		mlv1alpha1.AnnotationProductVersionKey(): mlv1alpha1.AnnotationProductVersionValue,
	}
	bytes, err := json.Marshal(AMLAnnotationsMap)
	if err != nil {
		fmt.Println(err)
	}
	newmap := make(map[string]string)
	err = json.Unmarshal(bytes, &newmap)
	newmap[mlv1alpha1.AnnotationProductVersionKey()] = "v0.8"
	if err != nil {
		fmt.Println(err)
	}

}

func Test_trans_user(t *testing.T) {
	user := "James  Bon"
	//assert
	assert.Equal(t, TransUserName(user), "james-bon", "replace space and lower string")

	user1 := "Ann Fox  Brown"
	assert.Equal(t, TransUserName(user1), "ann-fox-brown", "replace multi space and lower string")
}

func Test_FilteredLimitsResourceMapByKey(t *testing.T) {
	m := map[v1.ResourceName]resource.Quantity{
		"limits.cpu":            resource.MustParse("2"),
		"limits.memory":         resource.MustParse("1024Mi"),
		"limits.nvidia.com/gpu": resource.MustParse("1"),
		"limits.amd.com/gpu":    resource.MustParse("2"),
	}
	expectMap := map[v1.ResourceName]resource.Quantity{
		"cpu":            resource.MustParse("2"),
		"memory":         resource.MustParse("1024Mi"),
		"nvidia.com/gpu": resource.MustParse("1"),
		"amd.com/gpu":    resource.MustParse("2"),
	}

	assert.Equal(t, expectMap, utils.FilteredLimitsResourceMapByKey(m), "limit resource convert error")
}
