package notebook

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_getArgs(t *testing.T) {
	nb := &mlv1alpha1.NoteBook{}
	sts := NewStatefulSetMgr(nb, nil)
	s := NewDefaultArgs().GetCommandArgs(sts)
	expectSlice := []string{
		"jupyter",
		"notebook",
		"--debug",
		"--NotebookApp.base_url='/notebook///'",
		"--ip=\"0.0.0.0\"",
		"--port=8888",
		"--allow-root",
		"--NotebookApp.token=''"}
	assert.Equal(t, expectSlice, s)
}
