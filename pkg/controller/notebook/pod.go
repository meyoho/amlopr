package notebook

import (
	corev1 "k8s.io/api/core/v1"
)

func IsPodReady(podStatus *corev1.PodStatus) (bool, string, string) {
	var reason, msg string
	var isReady bool
	for _, cond := range podStatus.Conditions {
		if cond.Type == corev1.PodReady {
			if cond.Status == corev1.ConditionTrue {
				isReady = true
			} else {
				isReady, reason, msg = false, cond.Reason, cond.Message
			}
			break
		}
	}
	return isReady, reason, msg
}
