package notebook

import (
	"fmt"
	"k8s.io/apimachinery/pkg/util/intstr"
)

const (
	AmlLabelAppValue = "notebook"
)

type Args interface {
	GetCommand() []string
	GetContainerPort() intstr.IntOrString
}

type DefaultArgs struct {
	//prefixPath for ambassador route
	prefixPath string
}

func NewDefaultArgs() *DefaultArgs {
	return &DefaultArgs{
		prefixPath: "notebook",
	}

}

func (d DefaultArgs) GetCommandArgs(nbSts *StatefulSetMgr) []string {
	uniqName := fmt.Sprintf("%s/%s", nbSts.Notebook.Namespace, nbSts.Notebook.Name)
	return []string{
		"jupyter",
		"notebook",
		"--debug",
		"--NotebookApp.base_url='" + d.GetUniqRoutePrefix(uniqName) + "'",
		"--ip=\"0.0.0.0\"",
		//"--port=8888",
		fmt.Sprintf("--port=%d", nbSts.ContainerPort),
		"--allow-root",
		"--NotebookApp.token=''"}
}

func (d DefaultArgs) GetContainerPort() intstr.IntOrString {
	return intstr.FromInt(8888)
}

func (d DefaultArgs) GetUniqRoutePrefix(uniqName string) string {
	return fmt.Sprintf("/%s/%s/", d.prefixPath, uniqName)
}
