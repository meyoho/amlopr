package notebook

import (
	"strings"
)

func TransUserName(n string) string {
	if n == "" {
		return "anony"
	}
	return strings.Join(strings.Fields(strings.ToLower(n)), "-")
}
