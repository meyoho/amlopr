package notebook

import (
	mlv1alpha1 "alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/common"
	"alauda.io/amlopr/pkg/controller/modeldeploy"
	"context"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"strings"
)

type ServiceMgr struct {
	Notebook       *mlv1alpha1.NoteBook
	statefulSetMgr *StatefulSetMgr
	namespacedName types.NamespacedName
	reconcile      *ReconcileNoteBook
}

func NewServiceMgr(notebook *mlv1alpha1.NoteBook, statefulSetMgr *StatefulSetMgr, reconcile *ReconcileNoteBook) *ServiceMgr {
	return &ServiceMgr{
		Notebook:       notebook,
		statefulSetMgr: statefulSetMgr,
		namespacedName: types.NamespacedName{
			Namespace: notebook.Namespace,
			Name:      GetServiceName(notebook),
		},
		reconcile: reconcile,
	}
}

func GetServiceName(nb *mlv1alpha1.NoteBook) string {
	return fmt.Sprintf("%s-%s", GetNotebookUniqName(nb), strings.ToLower(common.KindService))
}

func (mgr *ServiceMgr) NeedUpdate(from, to *corev1.Service) bool {
	return false
}

func (mgr *ServiceMgr) ParseToService(statefulSetMgr *StatefulSetMgr) *corev1.Service {
	args := NewDefaultArgs()
	ns := mgr.Notebook.GetNamespace()
	namespacedName := fmt.Sprintf("%s/%s", ns, mgr.Notebook.GetName())
	routePrefix := args.GetUniqRoutePrefix(namespacedName)

	var svcRoute string
	svcName := fmt.Sprintf("%s-%s", GetNotebookUniqName(mgr.Notebook), strings.ToLower(common.KindService))
	svcPort := intstr.FromInt(statefulSetMgr.ContainerPort)
	type AmbassadorCfg = modeldeploy.AmbassadorCfg
	svcCfg := AmbassadorCfg{
		MappingName:      fmt.Sprintf("%s_%s_mapping", ns, mgr.Notebook.GetName()),
		Prefix:           routePrefix,
		ReWrite:          routePrefix,
		ServiceName:      svcName,
		ServicePort:      svcPort.String(),
		ServiceNamespace: ns,
		TimeoutMs:        300000,
	}
	svcRoute = fmt.Sprintf("%s", modeldeploy.GenAmbassadorConfigForService(svcCfg))
	annotations := common.NewAmlAnnotation().
		Create().
		Add("getambassador.io/config", svcRoute).
		Get()

	labels := common.NewAmlLabel().
		Add(common.AmlLabelAppKey, AmlLabelAppValue).
		Add(common.GetLabelKeyForCreatorOf(NBCreator), mgr.Notebook.GetName()).
		Get()

	return &corev1.Service{
		TypeMeta: metav1.TypeMeta{
			Kind:       common.KindService,
			APIVersion: common.APIVersionService,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:        GetServiceName(mgr.Notebook),
			Namespace:   mgr.namespacedName.Namespace,
			Annotations: annotations,
			Labels:      labels,
		},
		Spec: corev1.ServiceSpec{
			Selector: labels,
			Type:     corev1.ServiceTypeClusterIP,
			Ports: []corev1.ServicePort{
				{
					Name:       "8000-8888",
					Port:       int32(svcPort.IntValue()),
					Protocol:   corev1.ProtocolTCP,
					TargetPort: svcPort, //intstr.FromInt(statefulSetMgr.ContainerPort),
				},
			},
		},
	}
}

func (mgr ServiceMgr) IsServiceReady(r *ReconcileNoteBook) (bool, mlv1alpha1.ServiceAspect, *common.AmlError) {
	var serviceAspect = mlv1alpha1.ServiceAspect{}
	found := &corev1.Service{}
	err := r.Client.Get(context.TODO(), mgr.namespacedName, found)
	if err != nil {
		return false, serviceAspect, nil
	}
	log.Info("Service [IsStatefulSetReady] found just created instance")
	if !IsSvcReady(found.Spec) {
		return false, serviceAspect, common.NewAmlError(ErrSvcNotReadyReason, ErrSvcNotReadyMsg)
	}
	serviceAspect.Spec = found.Spec
	serviceAspect.Status = found.Status
	return true, serviceAspect, nil
}

func IsSvcReady(svcSpec corev1.ServiceSpec) bool {
	return svcSpec.Type == corev1.ServiceTypeClusterIP && svcSpec.ClusterIP != ""
}

func (mgr *ServiceMgr) Apply() (err error) {
	existsSvc := &corev1.Service{}
	err = mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsSvc)
	svc := mgr.ParseToService(mgr.statefulSetMgr)
	if err != nil {
		if !errors.IsNotFound(err) {
			return
		}
		if err := controllerutil.SetControllerReference(mgr.Notebook, svc, mgr.reconcile.scheme); err != nil {
			return err
		}
		if err = mgr.reconcile.Client.Create(context.Background(), svc); err != nil {
			log.Error(err, fmt.Sprintf("when notebook service %v.%v create", mgr.namespacedName.Namespace, mgr.namespacedName.Name))
			return err
		}
	}
	// update
	if mgr.NeedUpdate(existsSvc, svc) {
		if err := controllerutil.SetControllerReference(mgr.Notebook, svc, mgr.reconcile.scheme); err != nil {
			return err
		}
		if err := mgr.reconcile.Client.Update(context.Background(), svc); err != nil {
			log.Error(err, fmt.Sprintf("when notebook service %v.%v update", mgr.namespacedName.Namespace, mgr.namespacedName.Name))
		}
	}
	return
}

func (mgr *ServiceMgr) Delete() (err error) {
	existsSvc := &corev1.Service{}
	err = mgr.reconcile.Client.Get(context.Background(), mgr.namespacedName, existsSvc)
	if err != nil {
		if !errors.IsNotFound(err) {
			return err
		}
		// already not exists
		return nil
	}
	delFunc := func(*client.DeleteOptions) {}
	err = mgr.reconcile.Client.Delete(context.Background(), existsSvc, delFunc)
	if err != nil {
		return err
	}
	return nil
}
