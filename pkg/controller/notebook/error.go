package notebook

const (
	ErrMissingResourceReason = "NOT_FOUND_RESOURCE_SETTING_ERROR"
	ErMissingResourceMsg     = "Cpu and Memory must be set"

	ErrGpuVendorReason = "GPU_VENDOR_ERROR"
	ErrGpuVendorMsg    = "Currently support Gpu vendor is nvidia.com/gpu and amd.com/gpu"

	ErrProjectBindingNotReadyReason = "PROJECT_BINDING_NOT_READY"
	ErrProjectBindingNotReadyMsg    = "Project's binding not ready"

	ErrProjectStorageNotReadyReason = "PROJECT_PVC_NOT_READY"
	ErrProjectStorageNotReadyMsg    = "Project's pvc not founded or not valid"

	ErrPodNotReadyReason = "POD_NOT_RUNNING"
	ErrPodNotReadyMsg    = "Pod of Notebook not Running"

	ErrStsNotReadyReason = "STATEFULSET_NOT_RUNNING"
	ErrStsNotReadyMsg    = "StatefulSet of Notebook not Running"

	ErrSvcNotReadyReason = "SERVICE_NOT_RUNNING"
	ErrSvcNotReadyMsg    = "Service of Notebook not Running"

	ErrDevopsPipelineConfigNotReadyReason = "DEVOPS_PIPELINECONFIG_NOT_EXIST"
	ErrDevopsPipelineConfigNotReadyMsg    = "devops pipelineconfig not Running"

	ErrCrdNotReadyReason = "CRD_NOT_EXIST"
	ErrCrdNotReadyMsg    = "crd not Running"

	ErrDevopsPipelineNotReadyReason = "DEVOPS_PIPELINE_NOT_EXIST"
	ErrDevopsPipelineNotReadyMsg    = "devops pipeline not Running"
)
