package notebook

import (
	"alauda.io/amlopr/pkg/controller/modeldeploy"
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func Test_ambsvc(t *testing.T) {

	ns := "proj1"
	user := "usr"
	var svcRoute = `|
      ---
      apiVersion: ambassador/v0
      kind:  Mapping
      name:  $NS_$USER_mapping
      prefix: /ng/$USER/
      rewrite: /
      service: nginx-test.$NS`
	var nSvcRoute string
	nSvcRoute = strings.Replace(svcRoute, "$NS", ns, -1)
	nSvcRoute = strings.Replace(nSvcRoute, "$USER", user, -1)

	fmt.Println(nSvcRoute)
}

func Test_mp(t *testing.T) {
	projectName := "default"
	user := "fyuan"
	mountpath := "/aml"
	projectSubPath := fmt.Sprintf("%s/data", projectName)
	var genMountPoint = func(rootPath, subPath string) string {
		if !strings.HasSuffix(rootPath, "/") {
			rootPath = rootPath + "/"
		}
		return fmt.Sprintf("%s%s", rootPath, subPath)
	}

	a := genMountPoint(mountpath, projectSubPath)

	b := fmt.Sprintf("projects/%s/%s", projectName, user)
	fmt.Println(a)
	fmt.Println(b)
}

func Test_svc_mapping(t *testing.T) {
	ns := "testns"
	bookname := "book"
	routePrefix := "url"
	svcName := "testsvc"
	svcPort := "20000"
	svcRoute := `---
apiVersion: ambassador/v0
kind:  Mapping
name:  $UNIQ_mapping
prefix: $ROUTEPREFIX
rewrite: $ROUTEPREFIX
service: $SVC.$NS:$PORT
timeout_ms: 300000
use_websocket: true
`
	svcRoute = strings.Replace(svcRoute, "$UNIQ", fmt.Sprintf("%s_%s", ns, bookname), -1)
	svcRoute = strings.Replace(svcRoute, "$ROUTEPREFIX", routePrefix, -1)
	svcRoute = strings.Replace(svcRoute, "$SVC", svcName, -1)
	svcRoute = strings.Replace(svcRoute, "$NS", ns, -1)
	svcRoute = strings.Replace(svcRoute, "$PORT", svcPort, -1)

	///
	type AmbassadorCfg = modeldeploy.AmbassadorCfg
	lvl2Cfg := AmbassadorCfg{
		MappingName:      fmt.Sprintf("%s_%s_mapping", ns, bookname),
		Prefix:           routePrefix,
		ReWrite:          routePrefix,
		ServiceName:      svcName,
		ServicePort:      svcPort,
		ServiceNamespace: ns,
		TimeoutMs:        300000,
	}
	ambassadorConfig := fmt.Sprintf("%s", modeldeploy.GenAmbassadorConfigForService(lvl2Cfg))

	assert.Equal(t, ambassadorConfig, svcRoute)
}
