package pytorchtraining

import (
	utils2 "alauda.io/amlopr/pkg/controller/pytorchtraining/utils"
	"alauda.io/amlopr/pkg/controller/utils"
	"context"
	"fmt"
	juju "github.com/juju/errors"
	pytorch "github.com/kubeflow/pytorch-operator/pkg/apis/pytorch/v1beta2"
	commonv1beta2 "github.com/kubeflow/tf-operator/pkg/apis/common/v1beta2"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

type PyTorchJobComp struct {
	Parent *Subject
	Client client.Client
	scheme *runtime.Scheme
}

func NewPyTorchJobComp(parent *Subject, client client.Client, scheme *runtime.Scheme) *PyTorchJobComp {
	return &PyTorchJobComp{
		Parent: parent,
		Client: client,
		scheme: scheme,
	}
}
func (c *PyTorchJobComp) isExist() (bool, *pytorch.PyTorchJob, error) {
	pipelineConfig := &pytorch.PyTorchJob{}
	namespacedName := utils.NewNamespacedName(c.GetName(), c.GetNameSpace())
	err := c.Client.Get(context.TODO(), namespacedName, pipelineConfig)
	if err != nil {
		if errors.IsNotFound(err) {
			return false, pipelineConfig, nil
		}
		return false, pipelineConfig, err
	}

	return true, pipelineConfig, nil
}

func (c *PyTorchJobComp) createNotExist() (*pytorch.PyTorchJob, error) {
	exist, pyTorchJob, err := c.isExist()
	if err != nil || exist {
		return pyTorchJob, juju.Trace(err)
	}

	pyTorchJob, err = c.parseTo()
	if err != nil {
		return nil, juju.Trace(err)
	}

	err = controllerutil.SetControllerReference(c.Parent.SubjectInstance, pyTorchJob,
		c.scheme)
	if err != nil {
		return nil, juju.Trace(err)
	}

	err = c.Client.Create(context.TODO(), pyTorchJob)
	if err != nil {
		return nil, juju.Trace(err)
	}

	return pyTorchJob, nil
}

func (c *PyTorchJobComp) GetName() string {
	return fmt.Sprintf("%s", c.Parent.SubjectInstance.Name)
}

func (c *PyTorchJobComp) GetNameSpace() string {
	return c.Parent.SubjectInstance.Namespace
}

func (c *PyTorchJobComp) IsFinished(resource *pytorch.PyTorchJob) (bool, commonv1beta2.JobConditionType) {
	if resource.Status.Conditions != nil && len(resource.Status.Conditions) != 0 {
		jobCondition := resource.Status.Conditions[len(resource.Status.Conditions)-1]
		if jobCondition.Type == commonv1beta2.JobSucceeded ||
			jobCondition.Type == commonv1beta2.JobFailed {
			return true, jobCondition.Type
		}
	}
	return false, ""
}

func (c *PyTorchJobComp) IsSucceeded(jobConditionType commonv1beta2.JobConditionType) bool {
	return jobConditionType == commonv1beta2.JobSucceeded
}

func (c *PyTorchJobComp) parseTo() (*pytorch.PyTorchJob, error) {
	binding, err := utils.GetMLBinding(c.Client, c.Parent.SubjectInstance.Namespace)
	if err != nil {
		return nil, juju.Trace(err)
	}
	training := utils2.CompletePyTorchJobSpec(c.Parent.SubjectInstance, binding)
	return &pytorch.PyTorchJob{
		ObjectMeta: v1.ObjectMeta{
			Name:      c.GetName(),
			Namespace: c.GetNameSpace(),
			Labels:    map[string]string{"PyTorchTraining": c.Parent.SubjectInstance.Name},
		},
		Spec: *training.Spec.PyTorchJob.PyTorchJobSpec,
	}, nil
}
