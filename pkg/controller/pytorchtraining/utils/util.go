package utils

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/controller/utils"
	"github.com/kubeflow/pytorch-operator/pkg/apis/pytorch/v1beta2"
	common "github.com/kubeflow/tf-operator/pkg/apis/common/v1beta2"
	"k8s.io/api/core/v1"
	corev1 "k8s.io/api/core/v1"
)

func CompletePyTorchJobSpec(training *v1alpha1.PyTorchTraining, mlBinding *v1alpha1.MLBinding) *v1alpha1.PyTorchTraining {
	result := training.DeepCopy()
	if result.Spec.PyTorchJob.PyTorchJobSpec != nil {
		return result
	}

	specs := make(map[v1beta2.PyTorchReplicaType]*common.ReplicaSpec)
	originPyTorchJobSpec := training.Spec.PyTorchJob.Origin
	quota := originPyTorchJobSpec.PyTorchQuota
	for pyTorchReplicaType, resurceQuota := range quota {
		if resurceQuota.Replica != int32(0) {
			specs[pyTorchReplicaType] = getReplica(resurceQuota, originPyTorchJobSpec, mlBinding)
			//TODO this should be deleted after the tf-operator upgrade https://github.com/kubeflow/tf-operator/pull/1032
			specs[pyTorchReplicaType].Template.Annotations = map[string]string{"scheduling.k8s.io/group-name": training.Name}
		}
	}

	pyTorchJobSpec := v1beta2.PyTorchJobSpec{
		PyTorchReplicaSpecs: specs,
		BackoffLimit:        training.Spec.PyTorchJob.Origin.BackOffLimit,
	}
	result.Spec.PyTorchJob.PyTorchJobSpec = &pyTorchJobSpec
	return result
}

func getReplica(quota v1alpha1.ResourceQuota, originPyTorchJobSpec v1alpha1.OriginPyTorchJobSpec, mlBinding *v1alpha1.MLBinding) *common.ReplicaSpec {
	requestsMap := utils.FilteredRequestsResourceMapByKey(quota.Quota.Hard)
	limitsMap := utils.FilteredLimitsResourceMapByKey(quota.Quota.Hard)

	originImage := originPyTorchJobSpec.Image
	var image string
	if originImage.Tag != "" {
		image = originImage.RepositoryPath + ":" + originImage.Tag
	} else {
		image = originImage.RepositoryPath
	}
	podTemplateSpec := &v1.PodTemplateSpec{
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				{
					Name:            "pytorch",
					Image:           image,
					ImagePullPolicy: corev1.PullAlways,
					Resources: corev1.ResourceRequirements{
						Limits:   limitsMap,
						Requests: requestsMap,
					},
					Args:         originPyTorchJobSpec.Args,
					Env:          originPyTorchJobSpec.Env,
					VolumeMounts: utils.GetVolumeMount(mlBinding),
				},
			},
			Volumes: utils.GetVolumes(mlBinding),
			ImagePullSecrets: []corev1.LocalObjectReference{
				{Name: originImage.SecretName},
			},
		},
	}
	return &common.ReplicaSpec{
		Replicas: &quota.Replica,
		Template: *utils.InjectEnvForNvidia(podTemplateSpec),
	}
}
