package pytorchtraining

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/config"
	"alauda.io/amlopr/pkg/controller/amlpipeline"
	"alauda.io/amlopr/pkg/controller/common"
	trainingutil "alauda.io/amlopr/pkg/controller/tensorflowtraining/utils"
	"alauda.io/amlopr/pkg/controller/utils"
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"encoding/json"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type TrainingGen struct {
}

func (t TrainingGen) ErrorCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline, pipelineErr error) error {
	clientset, err := utils.NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		return juju.Trace(err)
	}
	created, err := clientset.MlV1alpha1().PyTorchTrainings(key.Namespace).Get(key.Name, v1.GetOptions{})

	if created.Status.Phase != v1alpha1.TrainingPhaseEnum.Finished {
		created.Status.Phase = v1alpha1.TrainingPhaseEnum.Finished
		created.Status.State = v1alpha1.TrainingStateEnum.Error
		created.Status.Reason = pipelineErr.Error()
		created.Status.Message = pipelineErr.Error()
		_, err = clientset.MlV1alpha1().PyTorchTrainings(key.Namespace).Update(created)
		return err
	}

	return nil
}

func (TrainingGen) DeleteCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline) error {
	clientset, err := utils.NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		if exist, existError := utils.NewBusinessMgr(clusterName).IsExistCluster(); existError != nil && exist == false {
			return nil
		}
		return juju.Trace(err)
	}
	err = clientset.MlV1alpha1().PyTorchTrainings(key.Namespace).Delete(key.Name, &v1.DeleteOptions{})
	if err != nil && errors.IsNotFound(err) {
		return nil
	}
	return juju.Trace(err)
}

func (TrainingGen) GenCrd(client client.Client,
	clusterName string,
	namespacedName types.NamespacedName,
	arguments []devops.PipelineTemplateArgumentGroup,
	pipeline *v1alpha1.AMLPipeline) (common.Object, error) {
	training := &v1alpha1.PyTorchTraining{}
	training.Namespace = namespacedName.Namespace
	training.Name = namespacedName.Name
	valueMap := utils.GetMergedMapGroup(arguments, pipeline.Spec.Parameters)

	image, err := trainingutil.GetImage(client, valueMap["outImageRepository"], clusterName, namespacedName.Namespace)
	if err != nil {
		return nil, juju.Trace(err)
	}
	training.Spec.PyTorchJob.Origin.Image = *image

	defineArgs, err := trainingutil.GetDefineArgs(clusterName, namespacedName.Namespace, valueMap["tensorboardPath"], valueMap["modelPath"])
	if err != nil {
		return nil, juju.Trace(err)
	}
	defineArgs, err = trainingutil.GetSelfDefineArgs(defineArgs, valueMap["entrypointArgs"])
	if err != nil {
		return nil, juju.Trace(err)
	}
	training.Spec.PyTorchJob.Origin.Args = defineArgs

	envVars, err := trainingutil.GetEnvVars(valueMap["envVar"])
	if err != nil {
		return nil, juju.Trace(err)
	}
	training.Spec.PyTorchJob.Origin.Env = envVars

	training.Spec.PyTorchJob.Origin.BackOffLimit = config.PytorchBackOffLimit()

	quota, err := GetTensorflowQuota(valueMap["resourceQuota"])
	if err != nil {
		return nil, juju.Trace(err)
	}
	training.Spec.PyTorchJob.Origin.PyTorchQuota = quota

	return training, nil
}

func (t TrainingGen) CancelCrd(clusterName string, key types.NamespacedName, amlPipeline *v1alpha1.AMLPipeline) error {
	return t.DeleteCrd(clusterName, key, amlPipeline)
}

func (TrainingGen) CreateCrdNotExist(crd common.Object, clusterName string) (common.Object, error) {
	training := crd.(*v1alpha1.PyTorchTraining)
	clientset, err := utils.NewBusinessMgr(clusterName).MLClient()
	if err != nil {
		return nil, juju.Trace(err)
	}
	created, err := clientset.MlV1alpha1().PyTorchTrainings(training.Namespace).Get(training.Name, v1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			return clientset.MlV1alpha1().PyTorchTrainings(training.Namespace).Create(training)
		}
		return created, juju.Trace(err)
	}
	return created, err
}

func GetTensorflowQuota(quota string) (v1alpha1.PyTorchQuota, error) {
	var result v1alpha1.PyTorchQuota
	err := json.Unmarshal([]byte(quota), &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func init() {
	annotation := v1alpha1.Annotation{Key: "ml.training", Value: "PyTorch"}
	err := amlpipeline.RegisterCrd(annotation, "PyTorchTraining", TrainingGen{})
	if err != nil {
		os.Exit(1)
	}
}
