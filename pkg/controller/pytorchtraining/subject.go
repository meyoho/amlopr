package pytorchtraining

import "alauda.io/amlopr/pkg/apis/ml/v1alpha1"

type Subject struct {
	SubjectInstance *v1alpha1.PyTorchTraining
}

func (s *Subject) GetCurrentStage() v1alpha1.BaseStage {
	return s.SubjectInstance.Status.AmlBaseStatus.Phase
}

func (s *Subject) SetCurrentState(state v1alpha1.BaseState, phase v1alpha1.BaseStage, reason string, message string) {
	s.SubjectInstance.Status.AmlBaseStatus.State = state
	s.SubjectInstance.Status.AmlBaseStatus.Phase = phase
	s.SubjectInstance.Status.Reason = reason
	s.SubjectInstance.Status.Message = reason
	return
}
