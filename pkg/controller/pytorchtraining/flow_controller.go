package pytorchtraining

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"context"
	juju "github.com/juju/errors"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type FlowControlProvider struct {
}

func (fc *FlowControlProvider) Dispatch(reconcilePyTorchTraining *ReconcilePyTorchTraining, request reconcile.Request) error {
	parent := &v1alpha1.PyTorchTraining{}
	err := reconcilePyTorchTraining.Get(context.TODO(), request.NamespacedName, parent)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		} else {
			return err
		}
	}

	subject := &Subject{parent}
	var phase = subject.GetCurrentStage()
	switch phase {
	case v1alpha1.TrainingPhaseEnum.Initializing,
		v1alpha1.TrainingPhaseEnum.Finished:
		break
	case v1alpha1.TrainingPhaseEnum.ImageBuilding:
		err = fc.processImageBuildingPhase(reconcilePyTorchTraining, subject)
		break
	case v1alpha1.TrainingPhaseEnum.Running:
		err = fc.processRunningPhase(reconcilePyTorchTraining, subject)
		break
	}

	updateErr := reconcilePyTorchTraining.Client.Update(context.TODO(), subject.SubjectInstance)
	if err == nil && updateErr != nil {
		return updateErr
	}

	return err
}

func (fc *FlowControlProvider) processRunningPhase(reconcilePyTorchTraining *ReconcilePyTorchTraining, subject *Subject) error {
	pyTorchJobComp := NewPyTorchJobComp(subject, reconcilePyTorchTraining.Client, reconcilePyTorchTraining.scheme)
	pyTorchJob, err := pyTorchJobComp.createNotExist()
	if err != nil {
		subject.SetCurrentState(v1alpha1.TrainingStateEnum.Error, v1alpha1.TrainingPhaseEnum.Running, "create tfjob failed", err.Error())
		return juju.Trace(err)
	}
	subject.SubjectInstance.Status.PyTorchJobStatus = &pyTorchJob.Status

	isFinished, conditionType := pyTorchJobComp.IsFinished(pyTorchJob)
	if isFinished {
		if pyTorchJobComp.IsSucceeded(conditionType) {
			subject.SetCurrentState(v1alpha1.TrainingStateEnum.Success, v1alpha1.TrainingPhaseEnum.Finished, "", "")
		} else {
			subject.SetCurrentState(v1alpha1.TrainingStateEnum.Failed, v1alpha1.TrainingPhaseEnum.Finished, "", "")
		}
		return nil
	}

	subject.SetCurrentState(v1alpha1.TrainingStateEnum.Running, v1alpha1.TrainingPhaseEnum.Running, "", "")
	return nil

}

func (fc *FlowControlProvider) processImageBuildingPhase(ReconcilePyTorchTraining *ReconcilePyTorchTraining, subject *Subject) error {
	subject.SetCurrentState(v1alpha1.TrainingStateEnum.ImageBuilding, v1alpha1.TrainingPhaseEnum.ImageBuilding, "", "")
	return nil
}
