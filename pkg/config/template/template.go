package template

import "alauda.io/amlopr/pkg/apis/base"

func LoadTemplate(mode base.Mode) {
	switch mode {
	case base.Business:
		LoadDataManager()
	case base.Global:
		LoadJenkins()
		LoadTensorboard()
	}
}
