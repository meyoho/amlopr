package template

import (
	"io/ioutil"
	"k8s.io/api/core/v1"
	"os"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/yaml"
)

var datamanagerPodTemplate = v1.PodTemplateSpec{}
var datamanagerPodTemplateString = `
metadata:
spec:
  containers:
  - image: index.alauda.cn/alaudaorg/amlfilebrowser:v1.7-b.1
    name: filebrowser
    resources:
      limits:
        cpu: 200m
        memory: 512Mi
      requests:
        cpu: 50m
        memory: 256Mi
`

func LoadDataManager() {
	bytes, err := ioutil.ReadFile("/etc/config/dataManageTemplate.yaml")
	logf.SetLogger(logf.ZapLogger(false))
	log := logf.Log.WithName("dataManager template")
	if err != nil {
		log.Error(err, "error while read dataManager initContainerTemplate, use default")
	} else {
		datamanagerPodTemplateString = string(bytes)
		log.Info("use dataManager template:", "dataManager Template", string(bytes))
		if len(datamanagerPodTemplateString) == 0 {
			log.Error(err, "nil config is not allowed ")
			os.Exit(1)
		}
	}

	err = yaml.Unmarshal([]byte(datamanagerPodTemplateString), &datamanagerPodTemplate)
	if err != nil {
		log.Error(err, "check the dataManager template in amlopr config ")
		os.Exit(1)
	}
}

func GetDataManagerTemplate() *v1.PodTemplateSpec {
	return datamanagerPodTemplate.DeepCopy()
}
