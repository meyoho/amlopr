package template

import (
	"io/ioutil"
	"k8s.io/api/core/v1"
	"os"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/yaml"
)

var jenkinsPodTemplate = v1.Pod{}

//sync charts when modify https://bitbucket.org/mathildetech/charts/src/master/global-aml/templates/amlopr/cm.yaml
var jenkinsPodTemplateString = `
metadata:
spec:
  containers:
  - command:
    - cat
    image: index.alauda.cn/alaudaorg/builder-tools:ubuntu
    name: tools
    resources:
      limits:
        cpu: 200m
        memory: 512Mi
      requests:
        cpu: 50m
        memory: 256Mi
    tty: true
    volumeMounts:
    - mountPath: /tmp
      name: volume-jenkins
    - mountPath: /var/run/docker.sock
      name: volume-docker
    workingDir: /home/jenkins/agent
  - name: jnlp
    args: ['\$(JENKINS_SECRET)', '\$(JENKINS_NAME)']
    image: index.alauda.cn/alaudaorg/jnlp-slave:v2.4.3
    resources:
      limits:
        cpu: 200m
        memory: 512Mi
      requests:
        cpu: 50m
        memory: 256Mi
    volumeMounts:
    - mountPath: /tmp
      name: volume-jenkins
    - mountPath: /var/run/docker.sock
      name: volume-docker
    workingDir: /home/jenkins/agent
  volumes:
  - emptyDir: {}
    name: volume-jenkins
  - hostPath:
      path: /var/run/docker.sock
      type: ""
    name: volume-docker
`

func LoadJenkins() {
	bytes, err := ioutil.ReadFile("/etc/config/jenkinsTemplate.yaml")
	logf.SetLogger(logf.ZapLogger(false))
	log := logf.Log.WithName("jenkins template")
	if err != nil {
		log.Error(err, "error while read initContainerTemplate, use default")
	} else {
		jenkinsPodTemplateString = string(bytes)
		log.Info("use Jenkins template:", "Jenkins Template", string(bytes))
		if len(jenkinsPodTemplateString) == 0 {
			log.Error(err, "nil config is not allowed ")
			os.Exit(1)
		}
	}

	err = yaml.Unmarshal([]byte(jenkinsPodTemplateString), &jenkinsPodTemplate)
	if err != nil {
		log.Error(err, "check the jenkins template in amlopr config ")
		os.Exit(1)
	}
}

func GetJenkinsDefaultTemplate() *v1.Pod {
	return jenkinsPodTemplate.DeepCopy()
}
