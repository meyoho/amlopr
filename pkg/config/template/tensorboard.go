package template

import (
	"io/ioutil"
	"k8s.io/api/core/v1"
	"os"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/yaml"
)

var tensorboardPodTemplate = v1.PodTemplateSpec{}
var tensorboardPodTemplateString = `
metadata:
spec:
  containers:
  - image: tensorflow/tensorflow:1.11.0
    name: tensorboard
    resources:
      limits:
        cpu: 200m
        memory: 512Mi
      requests:
        cpu: 50m
        memory: 256Mi
`

func LoadTensorboard() {
	bytes, err := ioutil.ReadFile("/etc/config/tensorboardTemplate.yaml")
	logf.SetLogger(logf.ZapLogger(false))
	log := logf.Log.WithName("tensorboard template")
	if err != nil {
		log.Error(err, "error while read tensorboard initContainerTemplate, use default")
	} else {
		tensorboardPodTemplateString = string(bytes)
		log.Info("use tensorboard template:", "tensorboard Template", string(bytes))
		if len(tensorboardPodTemplateString) == 0 {
			log.Error(err, "nil config is not allowed ")
			os.Exit(1)
		}
	}

	err = yaml.Unmarshal([]byte(tensorboardPodTemplateString), &tensorboardPodTemplate)
	if err != nil {
		log.Error(err, "check the tensorboard template in amlopr config ")
		os.Exit(1)
	}
}

func GetTensorboardDefaultTemplate() *v1.PodTemplateSpec {
	return tensorboardPodTemplate.DeepCopy()
}
