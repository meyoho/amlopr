package config

import (
	"context"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func GetConfigMap(client client.Client, configMapName string, configMapNamespace string) (*v1.ConfigMap, error) {
	configMap := &v1.ConfigMap{}
	namespaceName := types.NamespacedName{Name: configMapName, Namespace: configMapNamespace}
	err := client.Get(context.TODO(), namespaceName, configMap)
	if err != nil {
		return nil, err
	}
	return configMap, nil
}

func GetGlobalConfigMap(client client.Client) (*v1.ConfigMap, error) {
	configMapName := os.Getenv("CM_NAME")
	configMapNameSpace := os.Getenv("POD_NAMESPACE")
	return GetConfigMap(client, configMapName, configMapNameSpace)
}

func GetProjectConfigMap(client client.Client, namespace string) (*v1.ConfigMap, error) {
	return GetConfigMap(client, "ml-project-config", namespace)
}

func GetBaseUrl(configMap *v1.ConfigMap) string {
	return configMap.Data["UIBaseUrl"]
}
