package config

import "os"

const (
	NAMESPACE_KEY = "POD_NAMESPACE"
	DEV_MODE_KEY  = "DEV_MODE"
	DOMAIN_KEY    = "LABEL_BASE_DOMAIN"
)

func GetDomain() string {
	return GetEnvAsStringOrFallback(DOMAIN_KEY, "domain.io")
}

func AlaudaNameSpace() string {
	return os.Getenv(NAMESPACE_KEY)
}

func IsDevMode() bool {
	return os.Getenv(DEV_MODE_KEY) == "true"
}

func GetEnvAsStringOrFallback(key, defaultValue string) string {
	if v := os.Getenv(key); v != "" {
		return v
	}
	return defaultValue
}
