apiVersion: devops.alauda.io/v1alpha1
kind: ClusterPipelineTemplate
metadata:
  name: tensorflowserving
  annotations:
    displayName.zh-CN: "Tensorflow 模型发布"
    displayName.en: "Tensorflow Model Serving"
    description.zh-CN: ""
    description.en: "Tensorflow Model Serving"
    version: "0.1.0"
  labels:
    category: ModelServing
    latest: "true"
spec:
  agent:
    label: aml
  engine: graph
  arguments:
    - displayName:
        zh-CN: "image config"
        en: "镜像信息"
      items:
        - name: needImageBuild
          schema:
            type: boolean
          binding:
            - build-model-image.args.needImageBuild
          required: true
          display:
            type: alauda.io/imagebuildradio
            name:
              zh-CN: 构建镜像
              en: build image
            description:
              zh-CN: 构建镜像
              en: build image
        - name: framework
          required: true
          schema:
            type: string
          binding:
            - build-model-image.args.framework
          display:
            name:
              en: Framework
              zh-CN: 框架
            type: alauda.io/text
        - name: code
          schema:
            type: boolean
          binding:
            - build-model-image.args.code
          required: true
          display:
            type: alauda.io/coderadio
            name:
              zh-CN: 模型文件位置
              en: model path
            description:
              zh-CN: 模型文件位置
              en: model path
        - name: baseImageRepository
          schema:
            type: alauda.io/dockerimagerepositorymix
          binding:
            - build-model-image.args.baseImageRepository
          required: true
          display:
            type: alauda.io/dockerimagerepositorymix
            name:
              zh-CN: 基础镜像
              en: base image
            description:
              zh-CN: 目标镜像将基于该镜像构建
              en: base image
        - name: outImageRepository
          schema:
            type: alauda.io/dockerimagerepositorymix
          binding:
            - build-model-image.args.outImageRepository
          required: true
          display:
            type: alauda.io/dockerimagerepositorymix
            name:
              zh-CN: 目标镜像
              en: image
            description:
              zh-CN: 构建输出的镜像
              en: image
        - name: context
          schema:
            type: string
          binding:
            - build-model-image.args.context
          required: true
          default: ""
          display:
            type: string
            advanced: true
            name:
              zh-CN: 构建上下文
              en: build context
            description:
              zh-CN: docker构建的上下文
              en: docker build context
        - name: buildArguments
          schema:
            type: string
          binding:
            - build-model-image.args.buildArguments
          required: false
          default: ""
          display:
            type: string
            advanced: true
            description:
              en: Build Arguments
              zh-CN: 自定义更多的构建参数，对镜像构建进行更详细的配置
            name:
              en: Build Arguments
              zh-CN: 构建参数
        - name: retry
          schema:
            type: string
          binding:
            - build-model-image.args.retry
          required: false
          default: "3"
          display:
            advanced: true
            description:
              en: retries
              zh-CN: 生成镜像时的失败重试次数
            name:
              en: retry times
              zh-CN: 重试次数
            type: string
        - name: buildTimeout
          schema:
            type: string
          binding:
            - build-model-image.args.buildTimeout
          required: false
          display:
            type: string
            advanced: true
            description:
              en: timeout (s)
              zh-CN: 当前任务的执行时间若超过超时时间，则会中止发布任务，变为失败状态
            name:
              en: Timeout (s)
              zh-CN: 超时时间（秒）
        - name: codeRepository
          schema:
            type: alauda.io/coderepositorymix
          binding:
            - build-model-image.args.codeRepository
          required: true
          display:
            type: alauda.io/coderepositorymix
            name:
              zh-CN: 代码仓库
              en: codeRepository
            description:
              zh-CN: 模型文件可以来自github、bitbucket等仓库
              en: code Repository
        - name: Branch
          default: "*/master"
          required: false
          schema:
            type: string
          binding:
            - build-model-image.args.Branch
          display:
            type: string
            name:
              zh-CN: 代码分支
              en: code branch
            description:
              zh-CN: 代码在远程仓库中归属的分支
              en: code branch
        - name: relativeDirectory
          schema:
            type: string
          binding:
            - build-model-image.args.relativeDirectory
          required: true
          default: ""
          display:
            type: string
            name:
              zh-CN: 相对目录
              en: relative directory
            description:
              zh-CN: 一般是指模型名称命名的文件夹.以tensorflow为例,该目录下含有模型版本命名的文件夹.
              en: relative directory
        # when model in pvc
        - name: codePath
          schema:
            type: string
          binding:
            - build-model-image.args.codePath
          required: true
          default: ""
          display:
            type: alauda.io/modelfilepath
            name:
              zh-CN: 模型文件路径
              en: model path
            description:
              zh-CN: 一般是指模型名称命名的文件夹.以tensorflow为例,该目录下含有模型版本命名的文件夹.
              en: ""
  parameters:
    - description: modelName
      name: modelName
      type: string
      value: ""
    - description: modelVersion
      name: modelVersion
      type: string
      value: ""
    - description: outImageRepository
      name: outImageRepository
      type: string
      value: ""
    - description: Branch
      name: Branch
      type: string
      value: ""
    - description: relativeDirectory
      name: relativeDirectory
      type: string
      value: ""
    - description: modelPath
      name: codePath
      type: string
      value: ""
    - description: crdKindModelDeploy
      name: ModelDeployment
      type: string
      value: ""
    - description: namespace
      name: namespace
      type: string
      value: ""
    - name: cluster
      type: string
      value: "default"
      description: "cluster"
    - description: pipeline action
      name: action
      type: string
      value: ""
  stages:
    - name: Init
      tasks:
        - kind: ClusterPipelineTaskTemplate
          name: serving-prepare
    - name: BuildImage
      tasks:
        - kind: ClusterPipelineTaskTemplate
          name: build-model-image
    - name: DeployModel
      tasks:
        - kind: ClusterPipelineTaskTemplate
          name: serving-model
  withSCM: true