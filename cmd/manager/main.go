/*
Copyright 2018 fyuan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"alauda.io/amlopr/pkg/resource"
	"alauda.io/amlopr/pkg/resource/register/crd"
	"alauda.io/amlopr/pkg/resource/register/templates"
	"flag"
	"fmt"
	"github.com/spf13/pflag"
	"k8s.io/client-go/rest"
	"os"

	"alauda.io/amlopr/pkg/apis"
	"alauda.io/amlopr/pkg/apis/base"
	mlconfig "alauda.io/amlopr/pkg/config"
	"alauda.io/amlopr/pkg/config/template"
	"alauda.io/amlopr/pkg/controller"
	_ "alauda.io/amlopr/pkg/controller/modeldeploy"
	"alauda.io/amlopr/pkg/controller/utils"
	"alauda.io/amlopr/pkg/webhook"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/runtime/signals"
)

var templateFolder string
var templateNamespace string
var mode string
var multiClusterHost string
var clusterRegistryNamespace string

func main() {
	flag.StringVar(&templateFolder, "templatePath", "config/templates", "aml pipeline template folder.")
	flag.StringVar(&templateNamespace, "templateNamespace", "alauda-system", "the namespace to which the type of namespaced template will be created")
	flag.StringVar(&mode, "mode", "global", "the mode where the controller run ")
	flag.StringVar(&multiClusterHost, "multiClusterHost", "erebus", "the multiClusterHost ")
	flag.StringVar(&clusterRegistryNamespace, "clusterRegistryNamespace", "alauda-system", "the cluster info where namespace stores")
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()

	logf.SetLogger(logf.ZapLogger(false))
	log := logf.Log.WithName("main")
	log.Info("the mode is:", "mode", mode)

	utils.SetBusinessCfg(multiClusterHost, clusterRegistryNamespace)

	// Get a config to talk to the apiserver
	log.Info("setting up client for manager")
	cfg, err := config.GetConfig()
	if err != nil {
		log.Error(err, "unable to set up client config")
		os.Exit(1)
	}

	stop := signals.SetupSignalHandler()

	formatMode := base.Mode(mode)
	template.LoadTemplate(formatMode)
	// auto register crds
	autoRegister(cfg, stop, formatMode)
	// Create a new Cmd to provide shared dependencies and start components
	log.Info("setting up manager")
	var leaderElectionIDFormat = "aml%s-controller-lock"
	var leaderElectionID string
	if formatMode == base.Global {
		leaderElectionID = fmt.Sprintf(leaderElectionIDFormat, "-global")
	} else {
		leaderElectionID = fmt.Sprintf(leaderElectionIDFormat, "")
	}
	leaderElection := true
	if mlconfig.IsDevMode() {
		leaderElection = false
	}
	mgr, err := manager.New(cfg, manager.Options{
		LeaderElection:          leaderElection,
		LeaderElectionID:        leaderElectionID,
		LeaderElectionNamespace: mlconfig.GetEnvAsStringOrFallback("LEADER_ELECTION_NAMESPACE", "alauda-system"),
	})
	if err != nil {
		log.Error(err, "unable to set up overall controller manager")
		os.Exit(1)
	}

	webhookMgr, err := manager.New(cfg, manager.Options{})
	if err != nil {
		log.Error(err, "unable to set up webhook controller manager")
		os.Exit(1)
	}

	// Setup Scheme for all resources
	log.Info("setting up scheme")
	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		log.Error(err, "unable add APIs to scheme")
		os.Exit(1)
	}
	switch formatMode {
	case base.Global:
		log.Info("Setting up global controller")
		if err := controller.AddToGlobalManager(mgr); err != nil {
			log.Error(err, "unable to register global controllers to the manager")
			os.Exit(1)
		}

		log.Info("setting up global webhooks")
		if err := webhook.AddToGlobalManager(webhookMgr, formatMode); err != nil {
			log.Error(err, "unable to register global webhooks to the manager")
			os.Exit(1)
		}
	case base.Business:
		log.Info("Setting up controller")
		if err := controller.AddToManager(mgr); err != nil {
			log.Error(err, "unable to register controllers to the manager")
			os.Exit(1)
		}

		log.Info("setting up webhooks")
		if err := webhook.AddToManager(webhookMgr, formatMode); err != nil {
			log.Error(err, "unable to register webhooks to the manager")
			os.Exit(1)
		}
	default:
		log.Info("unknown mode,", "mode", mode)
	}

	go func() {
		if err := webhookMgr.Start(stop); err != nil {
			log.Error(err, "unable to run the webhook manager")
			os.Exit(1)
		}
	}()

	// Start the Cmd
	log.Info("Starting the Cmd.")
	//if err := mgr.Start(signals.SetupSignalHandler()); err != nil {
	if err := mgr.Start(stop); err != nil {
		log.Error(err, "unable to run the manager")
		os.Exit(1)
	}

}

func autoRegister(cfg *rest.Config, stop <-chan struct{}, mode base.Mode) {
	var crdPath = "config/crds"
	log := logf.Log.WithName("autoRegister")
	log.Info("init manager.")
	mgr, err := manager.New(cfg, manager.Options{})
	if err != nil {
		log.Error(err, "unable to set up overall controller manager")
		os.Exit(1)
	}

	log.Info("register extra resource.")
	if err := resource.Register(mgr); err != nil {
		log.Error(err, "unable to register extra resource to the manager")
		os.Exit(1)
	}

	go mgr.GetCache().Start(stop)
	mgr.GetCache().WaitForCacheSync(stop)

	log.Info("register crd.")
	err = crd.RegisterCrds(crdPath, mgr.GetClient())
	if err != nil {
		log.Error(err, "unable to registering crd")
		os.Exit(1)
	}
	log.Info("register aml templates.")

	if mode == base.Global {
		templatePreRegister := templates.NewAmlTemplateRegister(mgr, templateFolder, templateNamespace)
		err = templatePreRegister.RegisterTemplate()
		if err != nil {
			log.Error(err, "unable to aml templates.")
			os.Exit(1)
		}
	}

}
